package test.automation.panels.Google;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Link;
import test.automation.framework.Panel;

@Name("Footer")
@FindBy(id = "fbar")
public class Footer extends Panel {

    @Name("About")
    @FindBy(linkText = "About")
    public Link about;
}
