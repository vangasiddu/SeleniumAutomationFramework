package test.automation.panels.GiftRegistry;


import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Actions;
import test.automation.framework.Wait;

import java.util.List;


@Name("Header Bar")
@FindBy(css = "section.head-top.prim-bg-color>div.container,section.head-top")
public class HeaderPanel extends HtmlElement {

    @Name("User Profile")
    @FindBy(css = "button#dropdownMenuButton")
    public static TextBlock userProfile;

    @Name("User Info Link")
    @FindBy(css = "a.dropdown-item")
    public static List<TextBlock> userInfoLink;
    

    public static void selectProfileInfolink(String link) {
        Wait.untilElementPresent(userProfile);
        userProfile.click();
        Actions.waitUntil(() -> userInfoLink.stream().anyMatch(t -> t.getText().equals(link)));
        userInfoLink.stream().filter(t -> t.getText().equals(link)).findFirst().get().click();
    }

}
