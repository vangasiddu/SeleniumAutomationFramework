package test.automation.panels.GiftRegistry;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Actions;
import test.automation.framework.Wait;

import java.util.List;

@Name("Navigation Bar")
@FindBy(css = "section.nav-section")
public class NavigationPanel extends HtmlElement {

    @Name("Sub header links")
    @FindBy(css = "a.nav-text-color>span")
    public static List<TextBlock> subHeaderLinks;

    @Name("Home")
    @FindBy(xpath = "//span[contains(text(),'Home')]")
    public TextInput home;

    @Name("My Events")
    @FindBy(xpath = "//span[contains(text(),'My Events')]")
    public TextInput myEvents;

    @Name("About Us")
    @FindBy(xpath = "//span[contains(text(),'About Us')]")
    public TextInput aboutUs;

    @Name("Contact Us")
    @FindBy(xpath = "//span[contains(text(),'Contact Us')]")
    public TextInput contactUs;

        public static void selectSubHeaderLink(String link) {
        Actions.waitUntil(() -> subHeaderLinks.stream().anyMatch(t -> t.getText().equals(link)));
        subHeaderLinks.stream().filter(t -> t.getText().equals(link)).findFirst().get().click();
    }
}
