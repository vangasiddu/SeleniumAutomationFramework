package test.automation.panels.GiftRegistry;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;

@Name("Navigation Bar")
@FindBy(css = "section.nav-section")

public class StoreUserPanel extends HtmlElement {

    @Name("Role Management")
    @FindBy(xpath = "//li[@class='nav-item ng-star-inserted']/a[contains(text(),' Role Management')]")
    public Link roleManagement;

    @Name("User Management")
    @FindBy(xpath = "//li[@class='nav-item ng-star-inserted']/a[contains(text(),' User Management')]")
    public Link userManagement;

    @Name("Customer Management")
    @FindBy(xpath = "//li[@class='nav-item ng-star-inserted']/a[contains(text(),' Customer Management')]")
    public Link customerManagement;

    @Name("Event Management")
    @FindBy(xpath = "//li[@class='nav-item ng-star-inserted']/a[contains(text(),'Event Management')]")
    public Link eventManagement;

    @Name("Configuration Management")
    @FindBy(xpath = "//li[@class='nav-item dropdown ng-star-inserted']/a[contains(text(),' Configuration')]")
    public Link configuration;

}
