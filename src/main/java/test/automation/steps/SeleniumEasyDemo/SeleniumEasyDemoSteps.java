package test.automation.steps.SeleniumEasyDemo;

import cucumber.api.java.en.When;
import test.automation.pages.SeleniumEasyDemo.InputForm;
import test.automation.utils.UserUtils;

import static test.automation.framework.Page.*;

public class SeleniumEasyDemoSteps {

    @When("^I fill the selenium easy input form$")
    public void iFillTheSeleniumEasyInputForm() throws Throwable {

        ((InputForm) getCurrentPage()).fillInputForm(UserUtils.getUserMap());

        invokeMethod("fillInputForm", UserUtils.getUser());
    }
}
