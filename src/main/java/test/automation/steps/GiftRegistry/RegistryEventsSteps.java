package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import test.automation.framework.*;
import test.automation.models.User;
import test.automation.pages.GiftRegistry.*;
import test.automation.utils.EGRDB;
import test.automation.utils.UserUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static test.automation.framework.Actions.click;
import static test.automation.framework.Page.getElement;
import static test.automation.framework.Page.onPage;
import static test.automation.framework.Page.onPageVerify;
import static test.automation.framework.Runner.log;

public class RegistryEventsSteps {

    public static Integer eventId;


    private CreateEventPage createEventPage = new CreateEventPage();
    private UpdateEventPage updateEventPage = new UpdateEventPage();
    private StoreCreateEventPage storeCreateEventPage = new StoreCreateEventPage();
    private StoreEditEventPage storeEditEventPage = new StoreEditEventPage();
    private StoreEventConfirmationPage storeEventConfirmationPage = new StoreEventConfirmationPage();
    private EventConfirmationPage eventConfirmationPage = new EventConfirmationPage();
    private EventVerificationPage eventVerificationPage = new EventVerificationPage();
    private MyEventPage myEventPage = new MyEventPage();
    private ManageInvitationPage manageInvitationPage = new ManageInvitationPage();

    @And("^I verify all missing fields should be in red color while creating \"([^\"]*)\" event on create event page$")
    public void iVerifyErrorMessagesWhileCreatingEventWithMissingFieldsOnCreateEventPage(String event, List<String> elements) throws Throwable {
        onPage(CreateEventPage.class);
        if (event.equalsIgnoreCase("anniversary")) {
            ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(500,500)");
        }
        click("isCelebrantNo");
        click("saveAndContinue");

        switch (event) {
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
            case "anniversary":
                createEventPage.numberOfYears.sendKeys(Keys.CONTROL, "a");
                createEventPage.numberOfYears.sendKeys(Keys.DELETE);
                break;
        }

        elements.forEach(ele -> {
            Assert.assertTrue("Error - App: " + ele + " filed is not in red color", getElement(ele).getAttribute("aria-required").contains("true"));
        });
        log().info("Successfully verified error messages while registering with missing fields on update profile page");
    }

    @And("^I (enter|update) valid event details for \"([^\"]*)\" event on create event page$")
    public void iEnterValidEventDetailsForEventOnCreateEventPage(String condition, String event) throws Throwable {
        User userDetails = UserUtils.getNewUser();
        if (condition.equalsIgnoreCase("enter")) {
            switch (event) {
                case "babyShower":
                case "houseWarming":
                    createEventPage.enterBabyShowerEventDetails(userDetails);
                    break;
                case "wedding":
                    createEventPage.enterWeddingEventDetails(userDetails);
                    break;
                case "baptism":
                    createEventPage.enterBaptismEventDetails(userDetails);
                    break;
                case "birthdayAdult":
                case "birthdayKids":
                case "birthdayDebut":
                case "anniversary":
                    createEventPage.enterBirthdayAdultEventDetails(userDetails);
                    break;
            }
            createEventPage.saveAndContinue.click();
            if(createEventPage.errorMsz.exists()) {
                String eventType = createEventPage.eventTypeFiled.getText().split("\n")[0];
                String date = createEventPage.eventDate.getText();
                date = date.split("-")[2] + "-" + date.split("-")[0] + "-" + date.split("-")[1];
                EGRDB.cancelEventsOfUser(eventType, date, userDetails.getEmail());
                createEventPage.saveAndContinue.click();
            }
        } else if (condition.equalsIgnoreCase("update")) {
            switch (event) {
                case "babyShower":
                case "houseWarming":
                    updateEventPage.enterBabyShowerEventDetails(userDetails);
                    break;
                case "wedding":
                    updateEventPage.enterWeddingEventDetails(userDetails);
                    break;
                case "baptism":
                    updateEventPage.enterBaptismEventDetails(userDetails);
                    break;
                case "birthdayAdult":
                case "birthdayKids":
                case "birthdayDebut":
                case "anniversary":
                    updateEventPage.enterBirthdayAdultEventDetails(userDetails);
                    break;
            }
            updateEventPage.saveAndContinue.click();
            if(updateEventPage.errorMsz.exists()) {
                String eventType = updateEventPage.eventTypeFiled.getText().split("\n")[0];
                String date = updateEventPage.eventDate.getText();
                date = date.split("-")[2] + "-" + date.split("-")[0] + "-" + date.split("-")[1];
                EGRDB.cancelEventsOfUser(eventType, date, userDetails.getEmail());
                updateEventPage.saveAndContinue.click();
            }
        }
        log().info("Entered/updated valid details on create event page");
    }

    @And("^I enter previously created event details for \"([^\"]*)\" event on create event page$")
    public void iEnterPreviouslyCreatedDetailsForEventOnCreateEventPage(String event) throws Throwable {
        switch (event) {
            case "babyShower":
            case "houseWarming":
            case "baptism":
                createEventPage.enterBabyShowerEventDetailsWithSpecificDate(UserUtils.getUser());
                break;
            case "wedding":
                createEventPage.enterWeddingEventDetailsSpecificDate(UserUtils.getUser());
                break;
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
            case "anniversary":
                createEventPage.enterBirthdayAdultEventDetailsSpecificDate(UserUtils.getUser());
                break;
        }
        createEventPage.saveAndContinue.click();
    }

    @And("^I should see below messages on event confirmation page$")
    public void iShouldSeeBelowMessagesOnEventConfirmationPage(List<String> messages) throws Throwable {
        if (messages.size() == 1)
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.congratsMessage.getText(), messages.get(0));
        else {
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.congratsMessage.getText(), messages.get(0));
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.eventConfirmationMessage.getText(), messages.get(1).replace("'mail'", RegistryHomeSteps.email));
        }
        log().info("Verified event confirmation messages on event confimration page");
    }

    @Then("^I should be on GiftRegistry Event Invitation Page$")
    public void iShouldBeOnGiftRegistryEventInvitationPage() throws Throwable {
        onPage(ManageInvitationPage.class);
        eventId = Integer.parseInt(Browser.getDriver().getCurrentUrl().split("eventId=")[1]);
        log().info("Navigated to GiftRegistry Event Invitation Page");
    }

    @Then("^I should be on GiftRegistry Store Event Invitation Page$")
    public void iShouldBeOnGiftRegistryStoreEventInvitationPage() throws Throwable {
        onPage(StoreManageInvitationPage.class);
        eventId = Integer.parseInt(Browser.getDriver().getCurrentUrl().split("eventId=")[1]);
        log().info("Navigated to GiftRegistry Event Invitation Page");
    }

    @And("^I should see event code on event confirmation page$")
    public void iShouldSeeEventCodeOnEventConfirmationPage() throws Throwable {
        //       Assert.assertTrue("Error - App: Event Id is not displayed", eventConfirmationPage.eventCode.getText().contains(String.valueOf(eventId)));
        Wait.untilElementPresent(eventConfirmationPage.eventCode);
        Assert.assertTrue("Error - App: Event Id message is not displayed", eventConfirmationPage.eventCode.getText().contains("Event Code:"));
        log().info("Verified event code on event confirmation page");
    }

    @When("^I confirm the event confirmation online$")
    public void iConfirmTheEventConfirmationOnlineUsingHexcode() throws Throwable {
        String hexCode = EGRDB.getHexCodeForEvent(eventId);
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-verification?hc=" + hexCode;
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @And("^I should see the below success messages on event verification$")
    public void iShouldSeeTheBelowSuccessMessagesOnEventVerification(List<String> messages) throws Throwable {
        Assert.assertTrue("Error - App:Event verification success message is not displayed", EventVerificationPage.successMessage.getText().equalsIgnoreCase(messages.get(0)));
        Assert.assertTrue("Error - App:Event invitation message is not displayed", EventVerificationPage.eventInvitationMessage.getText().equalsIgnoreCase(messages.get(1).replace("'email'", RegistryHomeSteps.email)));
        log().info("Verified success message on event verification");
    }

    @And("^I should see the below success message on store event verification$")
    public void iShouldSeeTheBelowSuccessMessageOnStoreEventVerification(List<String> messages) throws Throwable {
        if (onPageVerify(StoreEventManagementPage.class)) {
            Assert.assertTrue("Error - App:Event verification success message is not displayed", StoreEventVerificationPage.successMessage.getText().equalsIgnoreCase(messages.get(0)));
        } else {
            Assert.assertTrue("Error - App:Event verification success message is not displayed", StoreEventVerificationPage.successMessage.getText().equalsIgnoreCase(messages.get(0)));
            Assert.assertTrue("Error - App:Event invitation message is not displayed", StoreEventVerificationPage.eventInvitationMessage.getText().equalsIgnoreCase(messages.get(1).replace("'email'", RegistryHomeSteps.email)));
        }
        log().info("Verified success message on event verification");
    }

    @And("^I should see below success message on event verification$")
    public void iShouldSeeBelowSuccessMessageOnEventVerification(List<String> messages) throws Throwable {
        Assert.assertTrue("Error - App:Event verification success message is not displayed", EventVerificationPage.successMessage.getText().equalsIgnoreCase(messages.get(0)));
        String emails = RegistryHomeSteps.email + " and " + UserUtils.getUser().getEmail();
        Assert.assertTrue("Error - App: Event invitation message is not displayed", EventVerificationPage.eventInvitationMessage.getText().equalsIgnoreCase(messages.get(1).replace("'email'", emails)));
        log().info("Verified success message on event verification");
    }

    @Then("^I should see the error \"([^\"]*)\" message for \"([^\"]*)\" on create event page$")
    public void iShouldSeeTheErrorMessageForOnCreateEventPage(String expectedMsg, String fieldName) throws Throwable {
        switch (fieldName) {
            case "zipCode":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.ZipInvalidErrorMsz.getText().toLowerCase()).contains(expectedMsg.toLowerCase()));
                break;
            case "giftDeliveryZip":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.ZipInvalidErrorMsz.getText().toLowerCase()).contains(expectedMsg.toLowerCase()));
                break;
            case "invalidGuestCount":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.invalidGuestsCountMsz.getText().toLowerCase()).equalsIgnoreCase(expectedMsg));
                break;
            case "celebrantPhoneNumber":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.mobileInvalidErrorMsz.getText().contains(expectedMsg)));
                break;
            case "celebrantName":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.celebrantInvalidErrorMsz.getText().contains(expectedMsg)));
                break;
            case "email":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.emailErrorMsz.getText().contains(expectedMsg)));
                break;
            case "coRegistrantCelebrant":
                Assert.assertTrue("Error - App:- Wrong error message is displaying for " + fieldName + " ", (createEventPage.coRegInvalidErrorMsz.getText().contains(expectedMsg)));
                break;
            case "brideGuestCardName":
            case "groomGuestCardName":
            case "firstPreferredName":
            case "secondPreferredName":
            case "preferredName":
                Assert.assertTrue("Error - App:- Wrong error message is displaying" + fieldName + " ", (createEventPage.pNameInvalidErrorMsz.getText().contains(expectedMsg)));
                break;
            case "numberOfYears":
                Assert.assertTrue("Error - App:- Wrong error message is displaying" + fieldName + " ", (createEventPage.yearsErrorMsz.getText().contains(expectedMsg)));

        }
        log().info("Error message is displayed successfully on the create event page");
    }

    @When("^I enter invalid details \"([^\"]*)\" with \"([^\"]*)\" value on create event page$")
    public void iEnterInvalidDetailsWithValueOnCreateEventPage(String name, String input) throws Throwable {
        User user = UserUtils.getNewUser();
        switch (name) {
            case "zipCode":
                user.setZipCode(input);
                createEventPage.eventZipcode.sendKeys(user.getZipCode());
                createEventPage.saveAndContinue.click();
                break;

            case "giftDeliveryZip":
                user.setZipCode(input);
                createEventPage.giftDeliveryZip.sendKeys(user.getZipCode());
                createEventPage.saveAndContinue.click();
                break;
            case "celebrantName":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(400,400)");
                createEventPage.isCelebrantNo.click();
                user.setFirstName(input);
                createEventPage.celebrantName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "celebrantPhoneNumber":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(400,400)");
                createEventPage.isCelebrantNo.click();
                user.setMobile(input);
                createEventPage.celebrantPhoneNumber.sendKeys(user.getMobile());
                createEventPage.saveAndContinue.click();
                break;
            case "email":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(400,400)");
                createEventPage.isCelebrantNo.click();
                user.setEmail(input);
                createEventPage.email.sendKeys(user.getEmail());
                createEventPage.saveAndContinue.click();
                break;
            case "coRegistrantCelebrant":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(400,400)");
                user.setFirstName(input);
                createEventPage.coRegistrantCelebrant.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "brideGuestCardName":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(700,700)");
                createEventPage.brideGuestCardNameFlag.click();
                user.setFirstName(input);
                createEventPage.brideGuestCardName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "groomGuestCardName":
                ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(700,700)");
                createEventPage.groomGuestCardNameFlag.click();
                user.setFirstName(input);
                createEventPage.groomGuestCardName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "firstPreferredName":
                user.setFirstName(input);
                createEventPage.firstPreferredName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "secondPreferredName":
                user.setFirstName(input);
                createEventPage.secondPreferredName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "preferredName":
                user.setFirstName(input);
                createEventPage.preferredName.sendKeys(user.getFirstName());
                createEventPage.saveAndContinue.click();
                break;
            case "invalidGuestCount":
                createEventPage.eventGuestsCount.sendKeys("sasads");
                createEventPage.saveAndContinue.click();
                break;
            case "numberOfYears":
                createEventPage.numberOfYears.clear();
                createEventPage.numberOfYears.sendKeys(input);
                createEventPage.saveAndContinue.click();
                break;
        }
        log().info("Successfully entered invalid details on event page");
    }

    @Then("^I should see all the fields resetting to default values for \"([^\"]*)\" event$")
    public void iShouldSeeAllTheFieldsResettingToDefaultValues(String event) throws Throwable {
        Actions.waitUntil(() -> createEventPage.eventTime.getAttribute("value").equals("12:00"));
        Assert.assertTrue("Error - App: Event Type is resetting after selecting reset button", createEventPage.eventTypeFiled.getAttribute("class").contains("mat-form-field-should-float"));
        Assert.assertTrue("Error - App: Event time is not resetting to 12:00 after selecting reset button", createEventPage.eventTime.getAttribute("value").equals("12:00"));
        Assert.assertTrue("Error - App: Event date is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.eventDate).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event venue is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.eventVenue).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event state is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.state).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event city is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.city).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event zipcode is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.eventZipcode).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery Address is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.giftDeliveryAddress).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery State is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.giftDeliveryState).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery City is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.giftDeliverycity).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Preferred registry branch is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.preferredRegistryBranch).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event city is not disabled after selecting reset button", createEventPage.city.getAttribute("class").contains("mat-select-disabled"));
        Assert.assertTrue("Error - App: Gift Delivery City is not disabled after selecting reset button", createEventPage.giftDeliverycity.getAttribute("class").contains("mat-select-disabled"));
        Assert.assertTrue("Error - App: Is Registrant Celebrant is not selected by default after selecting reset button", createEventPage.isCelebrantYes.getAttribute("class").contains("mat-radio-checked"));
        Assert.assertFalse("Error - App: Celebrant name is clearing after selecting reset button", createEventPage.labelElement(createEventPage.celebrantName).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertFalse("Error - App: Celebrant email is clearing after selecting reset button", createEventPage.labelElement(createEventPage.email).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertFalse("Error - App: Celebrant phone number is clearing after selecting reset button", createEventPage.labelElement(createEventPage.celebrantPhoneNumber).getAttribute("class").contains("mat-form-field-empty"));
        switch (event) {
            case "wedding":
                Assert.assertFalse("Error - App: Bride flag is selected by default after selecting reset button", createEventPage.brideGuestCardNameFlag.isSelected());
                Assert.assertFalse("Error - App: Groom flag is selected by default after selecting reset button", createEventPage.groomGuestCardNameFlag.isSelected());
                Assert.assertTrue("Error - App: Is Celebrant groom is not selected by default after selecting reset button", createEventPage.isCelebrantGroom.getAttribute("class").contains("mat-radio-checked"));
                Assert.assertTrue("Error - App: CoRegistrant Celebrant name is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.coRegistrantCelebrant).getAttribute("class").contains("mat-form-field-empty"));
                break;
            case "anniversary":
                Assert.assertTrue("Error - App: firstPreferred name is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.firstPreferredName).getAttribute("class").contains("mat-form-field-empty"));
                Assert.assertTrue("Error - App: firstPreferred name is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.secondPreferredName).getAttribute("class").contains("mat-form-field-empty"));
                Assert.assertTrue("Error - App: numberOfYears is not setting to 1 after selecting reset button", createEventPage.numberOfYears.getAttribute("value").contains("1"));
                break;
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
                Assert.assertTrue("Error - App: numberOfYears is not setting to 1 after selecting reset button", createEventPage.numberOfYears.getAttribute("value").contains("1"));
                Assert.assertTrue("Error - App: Preferred name is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.preferredName).getAttribute("class").contains("mat-form-field-empty"));
                break;
            default:
                Assert.assertTrue("Error - App: Preferred name is not clearing after selecting reset button", createEventPage.labelElement(createEventPage.preferredName).getAttribute("class").contains("mat-form-field-empty"));
                break;
        }
        log().info("Verified all the fields resetting to default values after selecting reset button");
    }

    @Then("^I should see all the fields resetting to default values for \"([^\"]*)\" event in store")
    public void iShouldSeeAllTheFieldsResettingToDefaultValuesInStore(String event) throws Throwable {
        Actions.waitUntil(() -> storeCreateEventPage.eventTime.getAttribute("value").equals("12:00"));
        Assert.assertTrue("Error - App: Event time is not resetting to 12:00 after selecting reset button", storeCreateEventPage.eventTime.getAttribute("value").equals("12:00"));
        Assert.assertTrue("Error - App: Event date is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.eventDate).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event venue is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.eventVenue).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event state is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.state).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event city is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.city).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event zipcode is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.eventZipcode).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery Address is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.giftDeliveryAddress).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery State is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.giftDeliveryState).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Gift Delivery City is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.giftDeliverycity).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Preferred registry branch is not clearing after selecting reset button", storeCreateEventPage.labelElement(createEventPage.preferredRegistryBranch).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertTrue("Error - App: Event city is not disabled after selecting reset button", storeCreateEventPage.city.getAttribute("class").contains("mat-select-disabled"));
        Assert.assertTrue("Error - App: Gift Delivery City is not disabled after selecting reset button", storeCreateEventPage.giftDeliverycity.getAttribute("class").contains("mat-select-disabled"));
        Assert.assertTrue("Error - App: Is Registrant Celebrant is not selected by default after selecting reset button", storeCreateEventPage.isCelebrantYes.getAttribute("class").contains("mat-radio-checked"));
        Assert.assertFalse("Error - App: Celebrant name is clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.celebrantName).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertFalse("Error - App: Celebrant email is clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.email).getAttribute("class").contains("mat-form-field-empty"));
        Assert.assertFalse("Error - App: Celebrant phone number is clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.celebrantPhoneNumber).getAttribute("class").contains("mat-form-field-empty"));
        switch (event) {
            case "Wedding":
                Assert.assertFalse("Error - App: Bride flag is selected by default after selecting reset button", storeCreateEventPage.brideGuestCardNameFlag.isSelected());
                Assert.assertFalse("Error - App: Groom flag is selected by default after selecting reset button", storeCreateEventPage.groomGuestCardNameFlag.isSelected());
                Assert.assertTrue("Error - App: Is Celebrant groom is not selected by default after selecting reset button", storeCreateEventPage.isCelebrantGroom.getAttribute("class").contains("mat-radio-checked"));
                Assert.assertTrue("Error - App: CoRegistrant Celebrant name is not clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.coRegistrantCelebrant).getAttribute("class").contains("mat-form-field-empty"));
                break;
            case "Anniversary":
                Assert.assertTrue("Error - App: firstPreferred name is not clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.firstPreferredName).getAttribute("class").contains("mat-form-field-empty"));
                Assert.assertTrue("Error - App: firstPreferred name is not clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.secondPreferredName).getAttribute("class").contains("mat-form-field-empty"));
                Assert.assertTrue("Error - App: numberOfYears is not setting to 1 after selecting reset button", storeCreateEventPage.numberOfYears.getAttribute("value").contains("1"));
                break;
            case "Birthday-Adult":
            case "Birthday-Kids":
            case "Birthday-Debut":
                Assert.assertTrue("Error - App: numberOfYears is not setting to 1 after selecting reset button", storeCreateEventPage.numberOfYears.getAttribute("value").contains("1"));
                Assert.assertTrue("Error - App: Preferred name is not clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.preferredName).getAttribute("class").contains("mat-form-field-empty"));
                break;
            default:
                Assert.assertTrue("Error - App: Preferred name is not clearing after selecting reset button", storeCreateEventPage.labelElement(storeCreateEventPage.preferredName).getAttribute("class").contains("mat-form-field-empty"));
                break;
        }
        log().info("Verified all the fields resetting to default values after selecting reset button");
    }

    @And("^I create the event \"([^\"]*)\" successfully$")
    public void iCreateTheEventSuccessfully(String event) throws Throwable {
        onPage("GiftRegistry CreateEventPage");
        iEnterValidEventDetailsForEventOnCreateEventPage("enter", event);
        iShouldBeOnGiftRegistryEventInvitationPage();
        click("saveAndContinue");
        onPage("GiftRegistry EventConfirmationPage");
        iShouldSeeEventCodeOnEventConfirmationPage();
        iConfirmTheEventConfirmationOnlineUsingHexcode();
        onPage("GiftRegistry EventVerificationPage");
        log().info("Created the event successfully");
    }

    @And("^I create \"([^\"]*)\" event with verification pending state")
    public void iCreateEventWithPeningConfirmation(String event) throws Throwable {
        onPage("GiftRegistry CreateEventPage");
        iEnterValidEventDetailsForEventOnCreateEventPage("enter", event);
        iShouldBeOnGiftRegistryEventInvitationPage();
        click("saveAndContinue");
        onPage("GiftRegistry EventConfirmationPage");
        iShouldSeeEventCodeOnEventConfirmationPage();
        log().info("Created the event successfully");
    }

    @And("^I create the event \"([^\"]*)\" in draft mode")
    public void iCreateTheEventInDraftMode(String event) throws Throwable {
        onPage("GiftRegistry CreateEventPage");
        iEnterValidEventDetailsForEventOnCreateEventPage("enter", event);
        iShouldBeOnGiftRegistryEventInvitationPage();
        iSelectOnLinkFromPage("My Events", "GiftRegistry ManageInvitationPage");
        iShouldSeeEventStatusAsOnMyEventsPage("Draft");
        log().info("Created the event in draft mode");
    }

    @And("^I create the event \"([^\"]*)\" successfully with previously created details$")
    public void iCreateTheEventSuccessfullyWithPreviouslyCreatedDetails(String event) throws Throwable {
        onPage("GiftRegistry CreateEventPage");
        iEnterPreviouslyCreatedDetailsForEventOnCreateEventPage(event);
        click("saveAndContinue");
        iShouldBeOnGiftRegistryEventInvitationPage();
        new RegistryHomeSteps().iClickOnSubmitButtonOnOnPage("saveAndContinue", null, "GiftRegistry ManageInvitationPage");
        onPage("GiftRegistry EventConfirmationPage");
        iShouldSeeEventCodeOnEventConfirmationPage();
        iConfirmTheEventConfirmationOnlineUsingHexcode();
        onPage("GiftRegistry EventVerificationPage");
        log().info("Created the event successfully");
    }

    @Then("^I should see \"([^\"]*)\" on my events page$")
    public void iShouldSeeOnMyEventsPage(String event) throws Throwable {
        onPage(MyEventPage.class);
        Actions.waitUntil(() -> !myEventPage.eventList.isEmpty(),5);
        WebElement element = myEventPage.eventList.stream().filter(ele -> ele.getText().contains(event)).findFirst().get();
        Assert.assertTrue(element.getText().equalsIgnoreCase(event));
        log().info("Created event is displayed on My Events page");
    }

    @When("^I select on \"([^\"]*)\" link from \"([^\"]*)\" page$")
    public void iSelectOnLinkFromPage(String link, String page) throws Throwable {
        if (page.equalsIgnoreCase("GiftRegistry EventVerificationPage")) {
            switch (link) {
                case "Home":
                    eventVerificationPage.navigationPanel.selectSubHeaderLink(link);
                    onPage(HomePage.class);
                    break;
                case "My Events":
                    eventVerificationPage.navigationPanel.selectSubHeaderLink(link);
                    onPage(MyEventPage.class);
                    break;
            }
        } else if (page.equalsIgnoreCase("GiftRegistry HomePage")) {
            switch (link) {
                case "Home":
                    new HomePage().navigationPanel.selectSubHeaderLink(link);
                    onPage(HomePage.class);
                    break;
                case "My Events":
                    new HomePage().navigationPanel.selectSubHeaderLink(link);
                    onPage(MyEventPage.class);
                    break;
            }
        } else {
            switch (link) {
                case "Home":
                    new HomePage().navigationPanel.selectSubHeaderLink(link);
                    onPage(HomePage.class);
                    Browser.getDriver().navigate().refresh();
                    break;
                case "My Events":
                    new HomePage().navigationPanel.selectSubHeaderLink(link);
                    onPage(MyEventPage.class);
                    break;
            }
        }
        log().info("Selected links from sub-header successfully");
    }

    @And("^I should see \"([^\"]*)\" message on \"([^\"]*)\" page$")
    public void iShouldSeeMessageOnPage(String expectedMsg, String page) throws Throwable {
        onPage(MyEventPage.class);
        myEventPage.noEventMessage.getText().equalsIgnoreCase(expectedMsg);
        log().info("Expected message is displayed successfully");
    }

    @And("^I should see event status as \"([^\"]*)\" on my events page$")
    public void iShouldSeeEventStatusAsOnMyEventsPage(String status) throws Throwable {
        onPage(MyEventPage.class);
        if (status.equalsIgnoreCase("Active")) {
            Actions.waitUntil(() -> myEventPage.activeEventStatus.exists());
            Assert.assertTrue(myEventPage.activeEventStatus.getText().equalsIgnoreCase(status));
        } else if (status.equalsIgnoreCase("Cancelled")) {
            Actions.waitUntil(() -> myEventPage.cancelEventStatus.exists());
            Assert.assertTrue(myEventPage.cancelEventStatus.getText().equalsIgnoreCase(status));
        } else if (status.equalsIgnoreCase("Draft")) {
            Actions.waitUntil(() -> myEventPage.draftEventStatus.exists());
            Assert.assertTrue(myEventPage.draftEventStatus.getText().equalsIgnoreCase(status));
        } else if (status.equalsIgnoreCase("In-active")) {
            Actions.waitUntil(() -> myEventPage.inactiveEventStatus.exists());
            Assert.assertTrue(myEventPage.inactiveEventStatus.getText().equalsIgnoreCase(status));
        } else {
            Actions.waitUntil(() -> myEventPage.pendingEventStatus.exists());
            Assert.assertTrue(myEventPage.pendingEventStatus.getText().equalsIgnoreCase(status));
        }
        log().info("Event status displayed properly on my events page");
    }


    @And("^I enter valid event details with in configured date for \"([^\"]*)\" event on create event page$")
    public void iEnterValidEventDetailsWithConfiguredDateForEventOnCreateEventPage(String event) throws Throwable {
        switch (event) {
            case "babyShower":
            case "houseWarming":
            case "baptism":
                createEventPage.enterBabyShowerEventDetailsWithInConfigured(UserUtils.getNewUser());
                break;
            case "wedding":
                createEventPage.enterWeddingEventDetailsWithInConfigured(UserUtils.getNewUser());
                break;
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
            case "anniversary":
                createEventPage.enterBirthdayAdultEventDetailsWithInConfigured(UserUtils.getNewUser());
                break;
        }
        log().info("Entered valid event details with in configured date for " + event + " event on create event page");
    }

    @Then("^I should see error message \"([^\"]*)\" on create event page$")
    public void iShouldSeeErrorMessageNotBeCreated(String message) throws Throwable {
        if (Browser.getDriver().getCurrentUrl().contains("create-invitation"))
            Assert.fail("Error - App:- Event is creating with in configured date");
        Assert.assertTrue("Error - App: Invalid message is displaying", createEventPage.errorMsz.getText().equalsIgnoreCase(message));
        log().info("Error message is displayed properly");
    }

    @When("^I click on \"([^\"]*)\" breadcrum on \"([^\"]*)\" page$")
    public void iClickOnBreadcrumOnPage(String element, String page) throws Throwable {
        onPage(page);
        click(element);
        log().info("Clicked on " + element + " successfully");
    }

    @And("^I should see event details persisting in Event Invitation page$")
    public void iShouldSeeEventDetailsPersistingInEventInvitationPage() throws Throwable {
        User user = UserUtils.getUser();
        String str = user.getEventDate();
        DateTimeFormatter fm = DateTimeFormatter.ofPattern("MMM dd yyyy");
        str = str.substring(4, str.length());
        LocalDate date = LocalDate.parse(str, fm);
        DateTimeFormatter ff = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        Thread.sleep(3000);
        Assert.assertTrue("Error - App: Event Date is not displaying correctly in invitation template page", updateEventPage.eventDate.getText().equalsIgnoreCase(ff.format(date)));
        Assert.assertTrue("Error - App: Event Time is not displaying correctly in invitation template page", updateEventPage.eventTime.getText().equalsIgnoreCase(user.getEventTime()));
        Assert.assertTrue("Error - App: Event Venue is not displaying correctly in invitation template page", updateEventPage.eventVenue.getText().equalsIgnoreCase(user.getEventVenue()));
        Assert.assertTrue("Error - App: Event City is not displaying correctly in invitation template page", updateEventPage.city.getText().equalsIgnoreCase(user.getEventCity()));
        Assert.assertTrue("Error - App: Event State is not displaying correctly in invitation template page", updateEventPage.state.getText().equalsIgnoreCase(user.getEventState()));
        Assert.assertTrue("Error - App: Celebrant phone number is not displaying correctly in invitation template page", updateEventPage.celebrantPhoneNumber.getText().equalsIgnoreCase(user.getMobile()));
        String name = EGRDB.fieldValue(RegistryHomeSteps.email, "first_name") + " " + EGRDB.fieldValue(RegistryHomeSteps.email, "last_name");
        Assert.assertTrue("Error - App: Celebrant name is not displaying correctly in invitation template page", updateEventPage.celebrantName.getText().equalsIgnoreCase(name));
        log().info("Verified all fields in invitation page");
    }

    @Then("^I should see event cancellation dialog$")
    public void iShouldSeeEventCancellationDialog() throws Throwable {
        Assert.assertTrue("Error - App: Event cancellation dialog is not displaying", myEventPage.cancellationDialogBox.isDisplayed());
        log().info("Event cancellation dialog verified");
    }

    @And("^I should see \"([^\"]*)\" on cancellation dialog$")
    public void iShouldSeeOnCancellationDialog(String message) throws Throwable {
        Assert.assertTrue("Error - App: Incorrect message is displaying on cancellation dialog box", myEventPage.textArea.getText().equalsIgnoreCase(message));
        log().info("Verified message on cancellation dialog box");
    }

    @And("^I enter reason for event cancellation$")
    public void iEnterReasonForEventCancellation() throws Throwable {
        myEventPage.cancellationReason.sendKeys("No reason");
        log().info("Entered reason for cancellation");
    }

    @Then("^I should see \"([^\"]*)\" message in snack bar$")
    public void iShouldSeeMessageInSnackBar(String message) throws Throwable {
        Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", myEventPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
        Wait.secondsUntilElementNotPresent(myEventPage.snackBar, 5);
        log().info("Verified success message in snack bar");
    }

    @Then("^I should be on GiftRegistry Event update Invitation Page$")
    public void iShouldBeOnGiftRegistryEventUpdateInvitationPage() throws Throwable {
        onPage(UpdateInvitationPage.class);
        eventId = Integer.parseInt(Browser.getDriver().getCurrentUrl().split("eventId=")[1]);
        log().info("Navigate to GiftRegistry event update invitation page");
    }

    @And("^I enter valid event details with is registrant celebrant no option for \"([^\"]*)\" event$")
    public void iEnterValidEventDetailsWithConfiguredDateForEventOnCreateEventPageWithNoOption(String event) throws Throwable {
        User userDetails = UserUtils.getNewUser();
        switch (event) {
            case "babyShower":
            case "houseWarming":
            case "baptism":
                createEventPage.enterBabyShowerEventDetailsWithNoOption(userDetails);
                break;
            case "wedding":
                createEventPage.enterWeddingEventDetailsWithNoOption(userDetails);
                break;
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
            case "anniversary":
                createEventPage.enterBirthdayAdultEventDetailsWithNoOption(userDetails);
                break;
        }
        createEventPage.saveAndContinue.click();
        if(createEventPage.errorMsz.exists()) {
            String eventType = createEventPage.eventTypeFiled.getText().split("\n")[0];
            String date = createEventPage.eventDate.getText();
            date = date.split("-")[2] + "-" + date.split("-")[0] + "-" + date.split("-")[1];
            EGRDB.cancelEventsOfUser(eventType, date, userDetails.getEmail());
            createEventPage.saveAndContinue.click();
        }
        log().info("Entered valid event details with registrant celebrant no option for " + event);
    }

    @And("^I should see celebrant information auto populate on create event page$")
    public void iShouldSeeCelebrantInformationAutoPopulateOnCreateEventPage() throws Throwable {
        User user = UserUtils.getUser();
        Assert.assertEquals("Error - App: Celebrant name is not persisted", createEventPage.celebrantName.getAttribute("value"), user.getFirstName() + " " + user.getLastName());
        Assert.assertEquals("Error - App: Celebrant email is not persisted", createEventPage.email.getAttribute("value"), user.getEmail());
        Assert.assertEquals("Error - App: Celebrant phone number is not persisted", createEventPage.celebrantPhoneNumber.getAttribute("value"), user.getMobile());
        log().info("Verified celebrant information auto populating on create event page");
    }

    @Then("^I should see \"([^\"]*)\" error message on create event page$")
    public void iShouldSeeErrorMessageOnCreateEventPage(String message) throws Throwable {
        Assert.assertTrue("Error - App:- Incorrect error message is displayed", createEventPage.errorMsz.getText().equalsIgnoreCase(message));
    }

    @When("^I change event status as In-Active in DB$")
    public void iChangeEventStatusAsInActiveInDB() throws Throwable {
        EGRDB.doInActiveForAnEvent(eventId);
    }

    @And("^I add the product to wishlist with Quantity \"([^\"]*)\" on \"([^\"]*)\"page$")
    public void iAddTheProductToWishlistWithQuantityOnPage(String qty, String page) throws Throwable {
        onPage(ItemDetailsPage.class);
        ItemDetailsPage.quantity.sendKeys(qty);
    }

    @Then("^I should see \"([^\"]*)\" message in snack bar on \"([^\"]*)\" page$")
    public void iShouldSeeMessageInSnackBarOnPage(String message, String page) throws Throwable {
        Thread.sleep(800);
        Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", ItemDetailsPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
        log().info("Verified success message in snack bar");
    }
}

