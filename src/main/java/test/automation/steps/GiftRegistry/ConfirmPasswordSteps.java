package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import test.automation.pages.GiftRegistry.ConfirmPasswordPage;
import test.automation.pages.GiftRegistry.GmailPage;

public class ConfirmPasswordSteps {

   private ConfirmPasswordPage confirmPasswordPage = new ConfirmPasswordPage();
   private GmailPage gmailPage = new GmailPage();

    @And("^I verify all UI checks on Confirm Password page$")
    public void iVerifyAllUIChecksOnConfirmPasswordPage() throws Throwable {
        Assert.assertTrue("New Password Textbox is not displayed on Confirm password page", confirmPasswordPage.NewPassword.isDisplayed());
        Assert.assertTrue("Confirm Password Textbox is not displayed on Confirm password page", confirmPasswordPage.ConfirmPassword.isDisplayed());
        Assert.assertTrue("Continue button is not displayed on Confirm password page", confirmPasswordPage.ContinueButton.isDisplayed());
        Assert.assertTrue("Sign in here link is not displayed on Confirm password page", confirmPasswordPage.SignInHere.isDisplayed());
    }

    @And("^I enter new password and confirm password$")
    public void iEnterNewPasswordAndConfirmPassword() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see \"([^\"]*)\" message$")
    public void iShouldSeeMessage(String expectedText) throws Throwable {
        Assert.assertTrue("Sign in here link is not displayed on Confirm password page", confirmPasswordPage.SuccessMessage.getText().equalsIgnoreCase(expectedText));
    }

    @And("^I Should see click here to login link$")
    public void iShouldSeeClickHereToLoginLink() throws Throwable {
        confirmPasswordPage.SignInHere.click();
    }

    @When("^I click on ResetLink came in my mail$")
    public void iClickOnResetLinkCameInMyMail() throws Throwable {
        gmailPage.confirmMail("fotgotpassword");
        gmailPage.ResetLink.click();
    }
}
