package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import test.automation.framework.Browser;
import test.automation.framework.Config;import test.automation.models.User;
import test.automation.pages.GiftRegistry.AdditionalDetailsPage;
import test.automation.pages.GiftRegistry.ProfilePage;
import test.automation.utils.UserUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static test.automation.framework.Page.getElement;
import static test.automation.framework.Page.onPage;
import static test.automation.framework.Runner.log;

public class ViewAndUpdateProfileSteps {
    AdditionalDetailsPage profilePage = new AdditionalDetailsPage();
    ProfilePage profilePage1 = new ProfilePage();

    @And("^I update profile details on \"([^\"]*)\" page$")
    public void iUpdateProfileDetailsOnPage(String page) throws Throwable {
        switch (page) {
            case "GiftRegistry AdditionalDetailsPage":
                onPage("GiftRegistry AdditionalDetailsPage");
                profilePage.updateAccount(UserUtils.getNewUser());
                break;
            case "GiftRegistry ProfilePage":
                onPage("GiftRegistry ProfilePage");
                profilePage1.updateAccount(UserUtils.getNewUser());
                break;
            default:
                break;
        }
        log().info("Successfully updated user profile details on profile page");
    }

    @Then("^I verify error messages while registering with missing fields on additional details page$")
    public void iVerifyErrorMessagesWhileRegisteringWithMissingFieldsOnAdditionalDetailsPage(List<String> elements) throws Throwable {
        profilePage.firstName.sendKeys(Keys.CONTROL, "a");
        profilePage.firstName.sendKeys(Keys.DELETE);
        profilePage.lastName.sendKeys(Keys.CONTROL, "a");
        profilePage.lastName.sendKeys(Keys.DELETE);
        profilePage.submit.click();

        elements.forEach(ele -> {
            Assert.assertTrue("Error - App: " + ele + " filed is not in red color", getElement(ele).getAttribute("aria-invalid").contains("true"));
        });
        log().info("Successfully verified all error messages with missing fields on additional profile page");
    }

    @Then("^I (should|should not) see the success \"([^\"]*)\" message on update profile page$")
    public void iShouldSeeTheSuccessMessageOnUpdateProfilePage(String condition, String expectedMessage) throws Throwable {
        Assert.assertEquals(profilePage.successmessage.getText(), expectedMessage);
        log().info("Successfully verified " + expectedMessage + " message on update profile page");
    }

    @And("^I select on \"([^\"]*)\" link from header$")
    public void iClickOnLinkFromHeader(String expectedLink) throws Throwable {
        profilePage.headerPanel.selectProfileInfolink(expectedLink);
        log().info("Successfully " + expectedLink + " link selectedfrom header");
    }


    @When("^I enter invalid details \"([^\"]*)\" with \"([^\"]*)\" value on additional details page$")
    public void iEnterInvalidDetailsWithValueOnAdditionalDetailsPage(String name, String input) throws Throwable {
        User user = UserUtils.getNewUser();
        switch (name) {
            case "telephone":
                user.setPhone(input);
                profilePage.telephone.sendKeys(user.getPhone());
                profilePage.submit.click();
                break;
            case "mobile":
                user.setMobile(input);
                profilePage.mobile.sendKeys(user.getMobile());
                profilePage.submit.click();
                break;
            case "zipCode":
                user.setZipCode(input);
                profilePage.zipCode.sendKeys(user.getZipCode());
                profilePage.submit.click();
                break;
        }
        log().info("Successfully entered invalid details on additional profile page");
    }

    @When("^I enter invalid details \"([^\"]*)\" with \"([^\"]*)\" value on update profile page$")
    public void iEnterInvalidDetailsWithValueOnUpdateProfilePage(String name, String input) throws Throwable {
        User user = UserUtils.getNewUser();
        switch (name) {
            case "telephone":
                profilePage1.telephone.clear();
                user.setPhone(input);
                profilePage1.telephone.sendKeys(user.getPhone());
                profilePage1.submit.click();
                break;
            case "mobile":
                profilePage1.mobile.clear();
                user.setMobile(input);
                profilePage1.mobile.sendKeys(user.getMobile());
                profilePage1.submit.click();
                break;
            case "zipCode":
                profilePage1.zipCode.clear();
                user.setZipCode(input);
                profilePage1.zipCode.sendKeys(user.getZipCode());
                profilePage1.submit.click();
                break;
        }
        log().info("Successfully entered invalid details on update profile page");
    }

    @Then("^I should see the error \"([^\"]*)\" message for \"([^\"]*)\" on (update|additional) (details|profile) page$")
    public void iShouldSeeTheErrorMessageForOnUpdateDetailsPage(String expectedMsg, String fieldName, String page, String page1) throws Throwable {
        if (page.equalsIgnoreCase("additional")) {
            switch (fieldName) {
                case "telephone":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for mobile", (profilePage.phoneInvalidErrorMsz.getText().contains(expectedMsg)));
                    break;
                case "mobile":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for mobile", (profilePage.mobileInvalidErrorMsz.getText().contains(expectedMsg)));
                    break;
                case "zipCode":
                    Assert.assertEquals("Error - App:- Wrong error message is displaying for zip code", profilePage.ZipInvalidErrorMsz.getText(), expectedMsg);
                    break;
            }
        } else if (page.equalsIgnoreCase("update")) {
            switch (fieldName) {
                case "telephone":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for mobile", (profilePage1.phoneInvalidErrorMsz.getText().contains(expectedMsg)));
                    break;
                case "mobile":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for mobile", (profilePage1.mobileInvalidErrorMsz.getText().contains(expectedMsg)));
                    break;
                case "zipCode":
                    Assert.assertEquals("Error - App:- Wrong error message is displaying for zip code", profilePage1.ZipInvalidErrorMsz.getText(), expectedMsg);
                    break;
            }
        }
        log().info("Successfully verified error message for filed " + fieldName + "on " + page + " profile page");
    }

    @Then("^I should see all newly updated details on \"([^\"]*)\" page$")
    public void iShouldSeeAllNewlyUpdatedDetailsOnPage(String arg0) throws Throwable {
        User user = UserUtils.getUser();
        Assert.assertEquals(profilePage1.AdditionalSpouse.getText(), user.getSpouse());
        Assert.assertEquals(profilePage1.AdditionalPhoneNumber.getText(), user.getPhone());
        Assert.assertEquals(profilePage1.AdditionalMobileNumber.getText(), user.getMobile());
        Assert.assertTrue(profilePage1.AdditionalAddressLine.getText().contains(user.getAddress()));
        Assert.assertEquals(profilePage1.AdditionalState.getText(), user.getState());
        Assert.assertEquals(profilePage1.AdditionalCity.getText(), user.getCity());
        Assert.assertEquals(profilePage1.AdditionalZipCode.getText(), user.getZipCode());
        log().info("Successfully verified all newly updated details on profile page");
    }


    @Then("^I verify error messages while registering with missing fields on update profile page$")
    public void iVerifyErrorMessagesWhileRegisteringWithMissingFieldsOnUpdateProfilePage(List<String> elements) throws Throwable {
        profilePage1.firstName.sendKeys(Keys.CONTROL, "a");
        profilePage1.firstName.sendKeys(Keys.DELETE);
        profilePage1.lastName.sendKeys(Keys.CONTROL, "a");
        profilePage1.lastName.sendKeys(Keys.DELETE);
        profilePage1.telephone.sendKeys(Keys.CONTROL, "a");
        profilePage1.telephone.sendKeys(Keys.DELETE);
        profilePage1.mobile.sendKeys(Keys.CONTROL, "a");
        profilePage1.mobile.sendKeys(Keys.DELETE);
        profilePage1.addressLine1.sendKeys(Keys.CONTROL, "a");
        profilePage1.addressLine1.sendKeys(Keys.DELETE);
        profilePage1.zipCode.sendKeys(Keys.CONTROL, "a");
        profilePage1.zipCode.sendKeys(Keys.DELETE);
        profilePage1.randomStateSelect();
        profilePage1.submit.click();
        elements.forEach(ele -> {
            Assert.assertTrue("Error - App: " + ele + " filed is not in red color", getElement(ele).getAttribute("aria-invalid").contains("true"));
        });
        log().info("Successfully verified error messages while registering with missing fields on update profile page");
    }

    @When("^I enter telephone details \"([^\"]*)\" with \"([^\"]*)\" value on (update profile|additional details) page$")
    public void iEnterTelephoneDetailsWithValueOnUpdateProfilePage(String fieldName, String input, String page) throws Throwable {
        User user = UserUtils.getNewUser();
        user.setPhone(input);
        switch(page){
            case "update profile":
                profilePage1.updateAccount(user);
                profilePage1.submit.click();
                break;
            case "additional details":
                onPage("GiftRegistry AdditionalDetailsPage");
                profilePage.updateAccount(user);
                profilePage.submit.click();
                break;
        }
        log().info("Successfully entered telephone details with input '" + input + "' value on update profile page");
    }

    @Then("^I should see all entered details on \"([^\"]*)\" page$")
    public void iShouldSeeAllEnteredDetailsOnPage(String page) throws Throwable {
        User user = UserUtils.getUser();
        Assert.assertTrue((profilePage1.userInfo.getText().contains(user.getTitle() + " " + user.getFirstName() + " " + user.getLastName())));
        Assert.assertEquals(profilePage1.AdditionalSpouse.getText(), user.getSpouse());
        Assert.assertEquals(profilePage1.AdditionalPhoneNumber.getText(), user.getPhone());
        Assert.assertEquals(profilePage1.AdditionalMobileNumber.getText(), user.getMobile());
        Assert.assertTrue(profilePage1.AdditionalAddressLine.getText().contains(user.getAddress()));
        Assert.assertEquals(profilePage1.AdditionalState.getText(), user.getState());
        Assert.assertEquals(profilePage1.AdditionalCity.getText(), user.getCity());
        Assert.assertEquals(profilePage1.AdditionalZipCode.getText(), user.getZipCode());
        log().info("Successfully verified all updated details on " + page + " page");
    }

    @When("^I refresh \"([^\"]*)\" page$")
    public void iRefreshPage(String page) throws Throwable {
        Browser.getDriver().navigate().refresh();
        log().info("Successfully refresh the " + page + " page");
    }

    @And("^I should not see duplicate states in select state drop down$")
    public void iShouldNotSeeDuplicateStatesInSelectStateDropDown() throws Throwable {
            Thread.sleep(1000);
            List<String> states = profilePage1.stateNames();
            List<String> afterDuplicationRemoved = states.stream().distinct().collect(Collectors.toList());
            Assert.assertEquals("Error - App: Duplicate states are displaying on select state drop down", states, afterDuplicationRemoved);
            log().info("Verified duplicate states occurrences in select state drop down");
        }
}
