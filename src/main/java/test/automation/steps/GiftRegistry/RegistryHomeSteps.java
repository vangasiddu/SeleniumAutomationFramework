package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import test.automation.framework.*;
import test.automation.models.User;
import test.automation.pages.GiftRegistry.*;
import test.automation.utils.EGRDB;
import test.automation.utils.UserUtils;

import java.util.List;
import java.util.Map;

import static test.automation.framework.Actions.click;
import static test.automation.framework.Runner.log;

public class RegistryHomeSteps extends Page {

    public static String email;
    public static String password;
    public static String celebrantEmail;
    public static String celebrantPassword;
    public static String getBrowserURL;
    public static String hexCodeFromDB;

    @Given("^I visit the website as new user$")
    public void iVisitTheWebsiteAsNewUser() throws Throwable {
        visit("Site HomePage");
        log().info("Navigated to " + Config.getUrl());
    }

    @And("^I click on \"([^\"]*)\" (button|link) on \"([^\"]*)\" page$")
    public void iClickOnSubmitButtonOnOnPage(String element, String type, String page) throws Throwable {
        if (element.equals("googleLoginbutton")) {
            onPage(page);
            click(element);
            switchWindow(1);
            Browser.maximize();
        } else if (element.equals("facebookLoginbutton")) {
            onPage(page);
            click(element);
            switchWindow(1);
            Browser.maximize();
        } else {
            onPage(page);
            click(element);
        }
        log().info("Clicked on " + element + " successfully");
    }

    @Then("^I verify error messages when enter with missing fields on EGR page$")
    public void iVerifyErrorMessagesWhenEnterWithMissingFieldsOnEGRPage(List<Map<String, String>> data) throws Throwable {
        for (Map<String, String> entry : data) {
            LoginPage.email.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
            LoginPage.email.sendKeys(Keys.DELETE);
            LoginPage.password.sendKeys(Keys.CONTROL, "a");
            LoginPage.password.sendKeys(Keys.DELETE);
            LoginPage.email.sendKeys(entry.get("Email"));
            LoginPage.password.sendKeys(entry.get("Password"));
            LoginPage.clickLoginButton();
            if (entry.get("Email").isEmpty() && !entry.get("Password").isEmpty()) {
                Assert.assertEquals("Error - Env: Invalid email error message is showing", LoginPage.emailErrorMsz.getText(), entry.get("Message"));
                Assert.assertFalse("Error - App: Password error msz is displaying after entering data in password filed", LoginPage.passwordErrorMsz.exists());
            } else if (!entry.get("Email").isEmpty() && entry.get("Password").isEmpty()) {
                Assert.assertFalse("Error - App: Email error msz is displaying after entering data in email filed", LoginPage.emailErrorMsz.exists());
                Assert.assertEquals("Error - Env: Invalid password error message is showing", LoginPage.passwordErrorMsz.getText(), entry.get("Message"));
            } else if (entry.get("Email").isEmpty() && entry.get("Password").isEmpty()) {
                Assert.assertEquals("Error - Env: Invalid email error message is showing", LoginPage.emailErrorMsz.getText(), entry.get("Message"));
                Assert.assertEquals("Error - Env: Invalid password error message is showing", LoginPage.emailErrorMsz.getText(), entry.get("Message"));
            }
            log().info("ERROR - ENV: Verified error messages when submitting with empty data in EGR sign in model");
        }
    }

    @Then("^I should see \"([^\"]*)\" error message$")
    public void iShouldBeErrorMessage(String message) throws Throwable {
        Assert.assertEquals(LoginPage.regNotSuccessfulErrorMsz.getText(), message);
        log().info("ERROR - ENV : Error message is displayed successfully");
    }

    @When("^I enter valid details on \"([^\"]*)\" page$")
    public void iEnterValidDetailsOnPage(String page) throws Throwable {
        switch (page) {
            case "GiftRegistry LoginPage":
            case "GiftRegistry RegistrationModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.register(UserUtils.getNewUser());
                break;
            case "GiftRegistry AdditionalDetailsPage":
                onPage("GiftRegistry AdditionalDetailsPage");
                ((AdditionalDetailsPage) getCurrentPage()).updateAccount(UserUtils.getUser());
                break;
            case "GiftRegistry StoreLoginPage":
                onPage("GiftRegistry StoreLoginPage");

            default:
                break;

        }
        log().info("Entered data on " + page + " page successfully");
    }

    @Then("^I should see verification email popup on \"([^\"]*)\" page$")
    public void iShouldSeeVerificationEmailPopupOnPage(String page) throws Throwable {
        switch (page) {
            case "Enterprise Gift Registry login":
                onPage("GiftRegistry LoginPage");
                TypifiedElement elementO = getElement("popup");
                Assert.assertTrue("popup" + " not displayed on " + getCurrentPageName(), elementO.exists() && elementO.isDisplayed());
                break;
            default:
                break;

        }
        log().info("email popup is displayed on " + page + "page");
    }

    @Then("^I confirm the registration online$")
    public void iConfirmTheRegistrationOnline() throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        String url = Config.getUrl() + "/registrant/account-verification?hc=" + hexCode;
        newTab();
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @Then("^I confirm the store registration online$")
    public void iConfirmTheStoreRegistrationOnline() throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        String url = Browser.getDriver().getCurrentUrl().replace("/registrant/reg-handler", "") + "/registrant/account-verification?hc=" + hexCode;
        newTab();
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @Then("^I confirm the store registration$")
    public void iConfirmTheStoreRegistration() throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        String url = getBrowserURL + "/registrant/account-verification?hc=" + hexCode;
        newTab();
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @When("^I enter already registered details on \"([^\"]*)\" page$")
    public void iEnterAlreadyRegisteredDetailsOnPage(String page) throws Throwable {
        switch (page) {
            case "GiftRegistry LoginPage":
                onPage("GiftRegistry LoginPage");
                LoginPage.register(UserUtils.getUser());
                break;
            case "GiftRegistry SignModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.signIn(UserUtils.getUser());
                break;
            case "GiftRegistry FacebookModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.facebookSignin(UserUtils.getUser());
                break;
            case "GiftRegistry GmailModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.gmailSignin(UserUtils.getUser());
                break;
            default:
                break;
        }
        log().info("ERROR - ENV: Error message is successfully displayed on " + page + "page");
    }

    @Then("^I verify error messages while registering with missing fields$")
    public void i_verify_error_messages_while_registering_with_missing_fields(Map<String, String> data) throws Throwable {
        onPage("GiftRegistry LoginPage");
        Assert.assertEquals("Error - App:- Wrong error message is displaying for first name", LoginPage.regfirstNameErrorMsz.getText(), data.get("first_name"));
        Assert.assertEquals("Error - App:- Wrong error message is displaying for last name", LoginPage.regfirstNameErrorMsz.getText(), data.get("last_name"));
        Assert.assertEquals("Error - App:- Wrong error message is displaying for Email", LoginPage.regfirstNameErrorMsz.getText(), data.get("Email"));
        Assert.assertEquals("Error - App:- Wrong error message is displaying for password", LoginPage.regfirstNameErrorMsz.getText(), data.get("Password"));
        Assert.assertEquals("Error - App:- Wrong error message is displaying for confirm password", LoginPage.regfirstNameErrorMsz.getText(), data.get("Confirm_Password"));
        log().info("ERROR - ENV: Verified error messages while registering with missing fields");
    }

    @Given("^I visit the website as registered user$")
    public void iVisitTheWebsiteAsRegisteredUser() throws Throwable {
        visit("Site HomePage");
//        ((HomePage)getCurrentPage()).RegistryButton.click();
        onPage(LoginPage.class);
        try {
            LoginPage.signIn(UserUtils.getUser());
            LoginPage.clickLoginButton();
        } catch (RuntimeException e) {
            System.out.println("Existing email not found, so creating new account");
            LoginPage.register(UserUtils.getUser());
        }
        onPage("GiftRegistry AdditionalDetailsPage");
        log().info("Successfully login with email: " + email + " and password: " + password);
    }

    @And("^I enter already registered details with case sensitive email on \"([^\"]*)\" page$")
    public void iEnterAlreadyRegisteredDetailsWithCaseSensitiveEmailOnPage(String page) throws Throwable {
        User user = UserUtils.getUser();
        user.setEmail(user.getEmail().toUpperCase());
        switch (page) {
            case "Enterprise Gift Registry login":
                onPage("GiftRegistry LoginPage");
                LoginPage.register(user);
                break;
            case "GiftRegistry SignModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.signIn(UserUtils.getValidUser("User with upper case email"));
                break;
            default:
                break;
        }
        log().info("ERROR - ENV: Error message is successfully displayed on " + page + "page");
    }

    @When("^I enter valid email and blank password on \"([^\"]*)\" page$")
    public void iEnterValidEmailAndBlankPasswordOnPage(String page) throws Throwable {
        switch (page) {
            case "Enterprise Gift Registry login":
                onPage("GiftRegistry LoginPage");
                LoginPage.register(UserUtils.getUser());
                break;
            case "GiftRegistry SignModel":
                onPage("GiftRegistry LoginPage");
                LoginPage.email.sendKeys(UserUtils.getUser().getEmail());
                LoginPage.password.sendKeys("");
                break;
            default:
                break;
        }
        log().info("Entered data successfully displayed on " + page + "page");
    }

    @Given("^I visit the website (as|with) \"([^\"]*)\" user$")
    public void iVisitTheWebsiteAsUser(String type, String user) throws Throwable {
        visit("Site HomePage");
//        ((HomePage)getCurrentPage()).RegistryButton.click();
        onPage(LoginPage.class);
        switch (user) {
            case "Registered":
                System.out.println("Registered user");
                LoginPage.signIn(UserUtils.getValidUser(user));
                LoginPage.clickLoginButton();
                break;
            case "First time user":
                LoginPage.register(UserUtils.getUser());
                LoginPage.clickRegisterButton();
                onPage("GiftRegistry RegistrationConfirmationPage");
                iConfirmTheRegistrationOnline();
                onPage("GiftRegistry AccountVerificationPage");
                ((AccountVerificationPage) getCurrentPage()).signInButton.click();
                onPage(LoginPage.class);
                ((LoginPage) getCurrentPage()).login(UserUtils.getUser());
                onPage("GiftRegistry AdditionalDetailsPage");
                break;
            case "New user":
                System.out.println("Existing email not found, so creating new account");
                iLoginAlreadyRegisteredDetailsOnPage("GiftRegistry SignModel");
                onPage("GiftRegistry AdditionalDetailsPage");
                break;
            case "three events cancellation":
                System.out.println("Registered user");
                LoginPage.signIn(UserUtils.getValidUser(user));
                LoginPage.clickLoginButton();
                break;
        }

        log().info("Navigated to " + Config.getUrl() + "as " + user + " user");
    }

    @And("^I should see the below message$")
    public void iShouldSeeTheBelowMessage(List<String> message) throws Throwable {
        String msz = message.get(0).replace("'email'", email);
        Assert.assertTrue("Error - App:- Registration confirmation message is not displayed correctly", RegistrationConfirmationPage.message.getText().equalsIgnoreCase(msz));
        log().info("Verified Registration confirmation message");
        getBrowserURL = Browser.getDriver().getCurrentUrl().replace("/registrant/reg-handler", "");
    }

    @And("^I get the \"([^\"]*)\" hexacode from database$")
    public void iGetTheHexaCodeFromDataBase(String type) throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        hexCodeFromDB = hexCode;
    }

    @And("^I verify new hexcode is generated for new confirmation link$")
    public void iVerifyTheHexCode() throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        Assert.assertTrue("ERROR:APP: Same hexacode found",!hexCode.equalsIgnoreCase(hexCodeFromDB));
    }

    @And("^I verify store registration confirmation with old hexacode$")
    public void iVerifyTheStoreRegistrationWithOldHexCode() throws Throwable {
        String hexCode = EGRDB.getHexCode(email);
        Assert.assertTrue("ERROR:APP: Same hexacode found",!hexCode.equalsIgnoreCase(hexCodeFromDB));
        String url = getBrowserURL + "/registrant/account-verification?hc=" + hexCodeFromDB;
        newTab();
        Browser.getDriver().get(url);
    }

    @And("^I should see the below success Account verification message$")
    public void iShouldSeeTheBelowSuccessAccountVerificationMessage(List<String> message) throws Throwable {
        Wait.untilElementPresent(AccountVerificationPage.message);
        Assert.assertEquals("Error - App:- Account verification message is not displayed correctly", AccountVerificationPage.message.getText(), message.get(0));
        log().info("Verified Account verification message");
    }

    @And("^I should see below error message on VerificationPendingPage$")
    public void iShouldSeeBelowErrorMessageOnVerificationPendingPage(List<String> message) throws Throwable {
        String msz = message.get(0).replace("'email'", email);
        Assert.assertTrue("Error - Ap:- Account verification pending message is not displayed correctly", VerificationPendingPage.message.getText().equalsIgnoreCase(msz));
        log().info("Verified Account verification pending message");
    }

    @And("^I login with already registered details on \"([^\"]*)\" page$")
    public void iLoginAlreadyRegisteredDetailsOnPage(String page) throws Throwable {
        new LoginPage().login(UserUtils.getUser());
        if (LoginPage.invalidLoginErrorMsz.exists() && LoginPage.invalidLoginErrorMsz.isDisplayed()) {
            LoginPage.register(UserUtils.getUser());
            LoginPage.clickRegisterButton();
            onPage("GiftRegistry RegistrationConfirmationPage");
            iConfirmTheRegistrationOnline();
            onPage("GiftRegistry AccountVerificationPage");
            new AccountVerificationPage().signInButton.click();
            onPage(LoginPage.class);
            new LoginPage().login(UserUtils.getUser());
        }
        log().info("Successfully login with email: " + email + " and password: " + password);
    }

    @Then("^I should see password in text format$")
    public void iShouldSeePasswordInTextFormat() throws Throwable {
        Assert.assertTrue("Error - App: Show password is not working", LoginPage.password.getAttribute("type").equals("text"));
        log().info("Verified show password button in sign in model");
    }

    @And("^I clear all the cookies and navigate to login page$")
    public void iClearAllTheCookiesAndRefreshThePage() throws Throwable {
        Browser.reStart();
        Browser.getDriver().get(Config.getUrl());
        onPage("GiftRegistry LoginPage");
        log().info("Cleared browser cookies and navigated to login page successfully");
    }

    @And("^I navigate to Gift Registry home$")
    public void iNavigateToGiftRegistryHome() throws Throwable {
        iLoginAlreadyRegisteredDetailsOnPage("");
        onPage(AdditionalDetailsPage.class);
        iEnterValidDetailsOnPage("GiftRegistry AdditionalDetailsPage");
        click("submit");
        onPage(HomePage.class);
        Browser.getDriver().navigate().refresh();
        Thread.sleep(1000);
        log().info("Navigated to " + AdditionalDetailsPage.class + " successfully");
    }

    @And("^I enter (registered|signin|Store User|Normal Store User|registration) details on \"([^\"]*)\" page$")
    public void iEnterRegisteredDetailsOnPage(String detail, String page) throws Throwable {
        if (detail.equalsIgnoreCase("registered")) {
            LoginPage.signIn(UserUtils.getValidUser("Registered"));
        } else if (detail.equalsIgnoreCase("Store User")) {
            StoreLoginPage.signIn(UserUtils.getValidUser("Store User"));
        } else if (detail.equalsIgnoreCase("Normal Store User")) {
            StoreLoginPage.signIn(UserUtils.getValidUser("Normal Store User"));
        } else if(detail.equalsIgnoreCase("registration")){
            iVisitTheWebsiteAsNewUser();
            onPage("GiftRegistry LoginPage");
            iLoginAlreadyRegisteredDetailsOnPage("");
            onPage(AdditionalDetailsPage.class);
            iEnterValidDetailsOnPage("GiftRegistry AdditionalDetailsPage");
            click("submit");
        }else {
            iVisitTheWebsiteAsNewUser();
            onPage("GiftRegistry LoginPage");
            iNavigateToGiftRegistryHome();
            new GuestInvitationSteps().iNavigateToGuestInvitationPage();
        }

        log().info("Entered all valid details on " + LoginPage.class + "page");
    }

    @And("^I click on enter key on keyboard and focus is on (login button|password field)$")
    public void iClickOnEnterKeyOnKeyboard(String condition) throws Throwable {
        if (condition.contains("login"))
            LoginPage.loginButton.sendKeys(Keys.ENTER);
        else if (condition.contains("password"))
            LoginPage.password.sendKeys(Keys.ENTER);
        log().info("Entered all valid details on " + LoginPage.class + "page");
    }

    @When("^I navigate to another company site in same session$")
    public void iNavigateToAnotherCompanySiteInSameSession() throws Throwable {
        String company_url = ((HomePage) getCurrentPage()).randomAnotherCompanyUrl();
        Browser.getDriver().navigate().to(company_url);
        log().info("Navigated to another company home page in same session");
    }

    @And("^I close the current window and open new window$")
    public void iCloseTheCurrentBrowserAndOpenNewBrowser() throws Throwable {
        newTab();
        switchWindow(0);
        closeCurrentTab();
        switchWindow(0);
        closeCurrentTab();
        switchWindow(0);
        log().info("Additional popup is displayed when opened new window in same session");
    }

    @And("^I visit the website in newly opened window$")
    public void iVisitTheWebsiteAsInNewlyOpenedWindow() throws Throwable {
        Browser.getDriver().get(Config.getUrl());
        log().info("Navigated to " + Config.getUrl());
    }

    @Then("^I should not see show password icon twice$")
    public void iShouldNotSeeShowPasswordIconTwice() throws Throwable {
        Assert.assertTrue("Error App: Show password icon is displaying twice in sign in page", LoginPage.showPasswordIcons.size() == 1);
        log().info("Show password icon is not displayed twice");
    }

    @When("^I enter invalid details \"([^\"]*)\" with \"([^\"]*)\" value on \"([^\"]*)\" page$")
    public void iEnterInvalidDetailsWithValueOnPage(String name, String input, String page) throws Throwable {
        User user = UserUtils.getNewUser();
        if (page.equalsIgnoreCase("GiftRegistry RegistrationModel")) {
            switch (name) {
                case "firstName":
                    LoginPage.regfirstName.clear();
                    user.setFirstName(input);
                    LoginPage.regfirstName.sendKeys(user.getFirstName());
                    LoginPage.register.click();
                    break;
                case "lastName":
                    LoginPage.reglastName.clear();
                    user.setLastName(input);
                    LoginPage.regfirstName.sendKeys(user.getLastName());
                    LoginPage.register.click();
                    break;
                case "email":
                    LoginPage.regEmail.clear();
                    user.setEmail(input);
                    LoginPage.regEmail.sendKeys(user.getEmail());
                    LoginPage.register.click();
                    break;
                case "password":
                    LoginPage.regPassword.clear();
                    user.setPassword(input);
                    LoginPage.regPassword.sendKeys(user.getPassword());
                    LoginPage.register.click();
                    break;
                case "confirmPassword":
                    LoginPage.regConfirmPassword.clear();
                    user.setPassword(input);
                    LoginPage.regPassword.sendKeys("sm123");
                    LoginPage.regConfirmPassword.sendKeys(user.getPassword());
                    LoginPage.register.click();
                    break;
            }
        } else if (page.equalsIgnoreCase("GiftRegistry SignModel")) {
            switch (name) {
                case "email":
                    user.setEmail(input);
                    LoginPage.email.sendKeys(user.getEmail());
                    LoginPage.loginButton.click();
                    break;
                case "password":
                    user.setPassword(input);
                    LoginPage.password.sendKeys(user.getPassword());
                    LoginPage.loginButton.click();
                    break;
            }
            log().info("Successfully entered invalid details on EGR login page");
        } else if (page.equalsIgnoreCase("GiftRegistry StoreLoginPage")) {
            switch (name) {
                case "userId":
                    StoreLoginPage.userId.sendKeys(input);
                    break;
                case "password":
                    StoreLoginPage.password.sendKeys(input);
                    break;
            }
            log().info("Successfully entered invalid details on Storeuser login page");
        }
    }


    @Then("^I should see the error \"([^\"]*)\" message for \"([^\"]*)\" on \"([^\"]*)\" of login page$")
    public void iShouldSeeTheErrorMessageForOnLoginPage(String expectedMsg, String fieldName, String section) throws Throwable {
        if (section.equalsIgnoreCase("GiftRegistry RegistrationModel")) {
            switch (fieldName) {
                case "firstName":
                    LoginPage.regfirstNameErrorMsz.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE);
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for first name", (LoginPage.regfirstNameErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
                case "lastName":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for last name", (LoginPage.reglastNameErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
                case "email":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for email", (LoginPage.regEmailErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
                case "password":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for password", (LoginPage.regPasswordErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
                case "confirmPassword":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for confirm password", (LoginPage.regConfirmPasswordErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
            }
        } else if (section.equalsIgnoreCase("GiftRegistry SignModel")) {
            switch (fieldName) {
                case "email":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for email", (LoginPage.emailErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
                case "password":
                    Assert.assertTrue("Error - App:- Wrong error message is displaying for password", (LoginPage.passwordErrorMsz.getText().equalsIgnoreCase(expectedMsg)));
                    break;
            }
        }
        log().info("Successfully verified error message for field " + fieldName + "on " + section + " login page");
    }

    @And("^I should see star for all required fields$")
    public void iShouldSeeStarForAllRequiredFields() throws Throwable {
        Assert.assertTrue("Error - App: Star (*) is not showing for login email filed", LoginPage.emailLabel.get(0).getText().contains("*"));
        Assert.assertTrue("Error - App: Star (*) is not showing for login password filed", LoginPage.emailLabel.get(1).getText().contains("*"));
        Assert.assertTrue("Error - App: Star (*) is not showing for registration email filed", LoginPage.passwordLabel.get(0).getText().contains("*"));
        Assert.assertTrue("Error - App: Star (*) is not showing for registration password filed", LoginPage.passwordLabel.get(1).getText().contains("*"));
        Assert.assertTrue("Error - App: Star (*) is not showing for registration first name filed", LoginPage.firstLabel.getText().contains("*"));
        Assert.assertTrue("Error - App: Star (*) is not showing for registration last name filed", LoginPage.lastNameLabel.getText().contains("*"));
        log().info("Verified Star (*) for all mandatory fields");
    }

    @And("^I navigate to registration confirmation tab$")
    public void iNavigateToRegistrationConfirmationTab() throws Throwable {
        switchWindow(0);
    }

    @And("^I select on \"([^\"]*)\" event on GiftRegistry Home page$")
    public void iSelectOnEventOnGiftRegistryHomePage(String event) throws Throwable {
        click(event);
        log().info("Clicked on " + event + " successfully");
    }

    @Then("^I should see (\\d+) response for event images$")
    public void iShouldSeeResponseForEventImages(int responseCode) throws Throwable {
        List<String> eventImagesLinks = HomePage.eventImagesLinks();
        for (int i = 0; i < eventImagesLinks.size(); i++) {
            Assert.assertEquals("Error - App: " + responseCode + " is not returning for " + eventImagesLinks.get(i), Rest.getResponseCode(eventImagesLinks.get(i)), responseCode);
        }
    }

    @And("^I sign in with celebrant login credentials$")
    public void iSignWithCelebrantLoginCredentails() throws Throwable {
        LoginPage.celebrantLogin(celebrantEmail, celebrantPassword);
        onPage(HomePage.class);
    }

    @And("^I sign in with guest login credentials$")
    public void iSignInWithGuestLoginCredentials() throws Throwable {
        LoginPage.guestLogin(email, password);
        onPage(HomePage.class);
    }

    @And("^I click on \"([^\"]*)\" link$")
    public void iClickOnLink(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see the privileges displayed$")
    public void iShouldSeeThePrivilegesDisplayed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see pagination displayed$")
    public void iShouldSeePaginationDisplayed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I select page \"([^\"]*)\" on the dropdown$")
    public void iSelectPageOnTheDropdown(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should be on the selected page$")
    public void iShouldBeOnTheSelectedPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I should see the \"([^\"]*)\" records displayed per page$")
    public void iShouldSeeTheRecordsDisplayedPerPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^I visit the website as store user$")
    public void iVisitTheWebsiteAsStoreUser() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I search the celebrant by \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iSearchTheCelebrantByOnPage(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see celebrant information on \"([^\"]*)\" page$")
    public void iShouldSeeCelebrantInformationOnPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I verify \"([^\"]*)\" status for the users$")
    public void iVerifyStatusForTheUsers(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see \"([^\"]*)\" link for the users$")
    public void iShouldSeeLinkForTheUsers(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I click on \"([^\"]*)\" button$")
    public void iClickOnButton(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see event status as \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iShouldSeeEventStatusAsOnPage(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see \"([^\"]*)\" popup containing \"([^\"]*)\" message$")
    public void iShouldSeePopupContainingMessage(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I enter the \"([^\"]*)\" message on the \"([^\"]*)\" popup$")
    public void iEnterTheMessageOnThePopup(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I update the details on \"([^\"]*)\" page$")
    public void iUpdateTheDetailsOnPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I select save on store template page$")
    public void iSelectSaveOnStoreTemplatePage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see \"([^\"]*)\" popup on \"([^\"]*)\" page$")
    public void iShouldSeePopupOnPage(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I should see the below message on GiftRegistry CreateWishList page$")
    public void iShouldSeeTheBelowMessageOnGiftRegistryCreateWishListPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see same products updated with quantity \"([^\"]*)\" in eidt wishlist page$")
    public void iShouldSeeSameProductsUpdatedWithQuantityInEidtWishlistPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I search with email in the search bar$")
    public void iSearchWithEmailInTheSearchBar() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I should see the status as \"([^\"]*)\"$")
    public void iShouldSeeTheStatusAs(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I click on ConfirmRegistration link$")
    public void iClickOnConfirmRegistrationLink() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see the search bar displayed$")
    public void iShouldSeeTheSearchBarDisplayed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I search with email on search bar$")
    public void iSearchWithEmailOnSearchBar() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see the user details displayed$")
    public void iShouldSeeTheUserDetailsDisplayed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I search with username on search bar$")
    public void iSearchWithUsernameOnSearchBar() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I should see status as \"([^\"]*)\" on customer management page$")
    public void iShouldSeeMessageOnPage(String expectedMsg) throws Throwable {
        Thread.sleep(300);
        Assert.assertEquals("ERROR: Wrong customer status found", expectedMsg, CustomerManagementPage.Status.getText());
        log().info("Expected message is displayed successfully");
    }
}
