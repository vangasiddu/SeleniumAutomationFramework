package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import org.junit.Assert;
import test.automation.framework.Page;
import test.automation.framework.Wait;
import test.automation.pages.GiftRegistry.ChangePasswordPage;
import cucumber.api.java.en.Then;
import test.automation.pages.GiftRegistry.LoginPage;
import test.automation.utils.UserUtils;

import java.util.List;

import static test.automation.framework.Runner.log;

public class ChangePasswordSteps extends Page {

    private ChangePasswordPage changePasswordPage = new ChangePasswordPage();

    @Then("^I should verify all UI check on change password page$")
    public void i_should_veify_all_UI_check_on_change_password_page() throws Throwable {
        Assert.assertTrue("Env - App: Old password text box is not displayed",changePasswordPage.password.isDisplayed());
        Assert.assertTrue("Env - App: New password text box is not displayed",changePasswordPage.newpassword.isDisplayed());
        Assert.assertTrue("Env - App: Confirm text box button is not displayed",changePasswordPage.confirmpassword.isDisplayed());
        Assert.assertTrue("Env - App: Submit button is not enabled",changePasswordPage.submitbutton.isEnabled());
        Assert.assertTrue("Env -App: Reset button is not enabled",changePasswordPage.resetbutton.isEnabled());
        Assert.assertTrue("Env - App: Change password header is not displayed",changePasswordPage.changePasswordHeader.isDisplayed());
        Assert.assertTrue("Env - App: Incorrect Change password header text is displayed",changePasswordPage.changePasswordHeader.getText().equalsIgnoreCase("Change Password"));
        log().info("Verified all UI checks on Change password page");
    }

    @Then("^I enter old password new password and confirm password$")
    public void i_enter_old_password_new_password_and_confirm_password() throws Throwable {
        onPage(ChangePasswordPage.class);
        ChangePasswordPage changePasswordPage = ((ChangePasswordPage)getCurrentPage());
        changePasswordPage.submit(UserUtils.getUser());
    }

    @Then("^I should see the success \"([^\"]*)\" message on GiftRegistry ChangePassword page$")
    public void iShouldSeeTheSuccessMessageOnGiftRegistryChangePasswordPage(String message) throws Throwable {
        Wait.untilElementPresent(changePasswordPage.submitbutton);
        Assert.assertEquals(changePasswordPage.successmessage.getText(),message);
        log().info("Successfully verified success message on GiftRegistry ChangePassword page");
    }

    @And("^I enter email id and changed password$")
    public void iEnterEmailIdAndChangedPassword() throws Throwable {
        onPage(LoginPage.class);
        LoginPage loginPage = ((LoginPage)getCurrentPage());
        loginPage.signInWithChangedPassword(UserUtils.getUser());
        log().info("Successfully entered email id and changed password on ChangePassword page");
    }

    @Then("^I should see the below fields should be in red color$")
    public void iShouldSeeTheBelowErrorMessagesOnChangePasswordPage(List<String> elements) throws Throwable {
        elements.forEach(ele -> {
            Assert.assertTrue("Error - App: " + ele + " filed is not in red color", getElement(ele).getAttribute("aria-invalid").contains("true"));
        });
        log().info("Successfully verified all error messages with missing fields on additional profile page");
    }

    @And("^I enter old password and new password confirm password without matching password restriction string$")
    public void iEnterOldPasswordAndNewPasswordConfirmPasswordWithoutMatchingPasswordRestrictionString() throws Throwable {
        changePasswordPage.password.sendKeys(UserUtils.getUser().getEmail());
        changePasswordPage.newpassword.sendKeys("asdadada@2");
        changePasswordPage.confirmpassword.sendKeys("asdadada@2");
        log().info("Successfully entered old password and new password confirm password without matching password restriction string");
    }

    @Then("^I should see below error message on ChangePasswordPage$")
    public void iShouldSeeBelowErrorMessageOnChangePasswordPage(List<String> errorMessages) throws Throwable {
        Assert.assertEquals("Env - App:Incorrect Password format error message is displayed", changePasswordPage.PasswordFormatErrMsz.getText(),errorMessages.get(0));
        log().info("Verified Password format error message in change password page");
    }

    @And("^I enter old password and unmatched new password and confirm password$")
    public void iEnterOldPasswordAndUnmatchedNewPasswordAndConfirmPassword() throws Throwable {
        changePasswordPage.password.sendKeys(UserUtils.getUser().getPassword());
        changePasswordPage.newpassword.sendKeys("Asdadada@20267");
        changePasswordPage.confirmpassword.sendKeys("Asdadada@202");
        log().info("Successfully entered old password and unmatched new password and confirm password");
    }

    @Then("^I (should|should not) see the error \"([^\"]*)\" message on ChangePasswordPage$")
    public void iShouldSeeTheErrorMessageOnChangePasswordPage(String Condition, String messge) throws Throwable {
        switch (Condition) {
            case "should":
                if (messge.contains("Passwords didn't match!"))
                    Assert.assertEquals("Env - App: Incorrect Passwords Mismatch error message is displayed",changePasswordPage.PasswordMisMatchedErrMsz.getText(), messge);
                else if (messge.contains("Current password entered"))
                    Assert.assertEquals("Env - App:Incorrect Old Password error message is displayed",changePasswordPage.OldPasswordMisMatchedErrMsz.getText(), messge);
                break;
            case "should not":
                if (messge.contains("Passwords didn't match!"))
                    Assert.assertFalse("Env - App: Passwords Mismatch error message is displayed after select Reset", changePasswordPage.PasswordMisMatchedErrMsz.exists());
                else if (messge.contains("Current password entered"))
                    Assert.assertFalse("Env - App: Old Password error message is displayed after select Reset",changePasswordPage.OldPasswordMisMatchedErrMsz.exists());
                break;
        }
        log().info("Verified password mismatch error messages on change password page");
    }

    @And("^I enter incorrect old password and matched new password and confirm password$")
    public void iEnterIncorrectOldPasswordAndMatchedNewPasswordAndConfirmPassword() throws Throwable {
        changePasswordPage.password.sendKeys("asdas");
        changePasswordPage.newpassword.sendKeys("Asdadada!20267");
        changePasswordPage.confirmpassword.sendKeys("Asdadada!20267");
        log().info("Successfully entered incorrect old password and matched new password and confirm password");
    }
}
