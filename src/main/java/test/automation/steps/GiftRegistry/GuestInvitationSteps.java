package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import test.automation.framework.*;
import test.automation.pages.GiftRegistry.*;
import test.automation.utils.EGRDB;
import test.automation.pages.GiftRegistry.GuestInvitationPage.*;

import java.util.List;

import static test.automation.framework.Actions.click;
import static test.automation.framework.Page.getCurrentPage;
import static test.automation.framework.Page.onPage;
import static test.automation.framework.Runner.log;
import static test.automation.pages.GiftRegistry.HomePage.viewMore;
import static test.automation.steps.GiftRegistry.RegistryWishlistSteps.qty;


public class GuestInvitationSteps extends Page {

    private String url;

    @Given("^I visit the website as guest user for \"([^\"]*)\" event$")
    public void iVisitTheWebsiteAsGuestUserForEvent(String event) throws Throwable {
        new RegistryHomeSteps().iVisitTheWebsiteAsNewUser();
        onPage("GiftRegistry LoginPage");
        new RegistryHomeSteps().iNavigateToGiftRegistryHome();
        HomePage.viewmore();
        HomePage.viewMore.click();
        click(event);
        new RegistryEventsSteps().iCreateTheEventSuccessfully(event);
        EventVerificationPage.addWishlistButton.click();
        onPage(CreateWishlistPage.class);
        new RegistryWishlistSteps().iAddProductToWishlistWithQuantityFromPage("4", "GiftRegistry CreateWishlistPage");
        new RegistryWishlistSteps().iClickOnElementOnGiftRegistryCreateWishlistPagePage("");
        onPage("GiftRegistry MyWishlistPage");
        MyWishlistPage.headerPanel.selectProfileInfolink("Sign Out");
        iNavigateToGuestInvitationPage();

    }

    @And("^I navigate to guest invitation page$")
    public void iNavigateToGuestInvitationPage() throws Throwable {
        String hexCode = EGRDB.getHexCodeForEvent(RegistryEventsSteps.eventId);
        url = (url == null || url.isEmpty()) ? Config.getUrl() + "/events/event-invitation?hexcode=" + hexCode : url;
        Browser.getDriver().close();
        Browser.reStart();
        Browser.getDriver().get(url);
        log().info("Navigated to guest invitation page by hitting the url: " + url);
    }

    @And("^I enter personal details on \"([^\"]*)\" page$")
    public void iLoginFromGuestInvitationPage(String page) throws Throwable {
        GuestInvitationPage.setSkipRegistration();
    }

    @Given("^I visit the active event invitation link for \"([^\"]*)\" using hexcode from DB$")
    public void iVisitTheActiveEventInvitationLinkUsingHexcodeFromDB(String event) throws Throwable {
        String hexCode = EGRDB.eventInvitationHexCode(event);
        if (hexCode == null)
            Assert.fail("No active events are available to fetch hexcode from DB");
        String url = Config.getUrl() + "/events/event-invitation?hexcode=" + hexCode;
        Browser.getDriver().get(url);
        log().info("Navigated to guest invitation page by hitting the url: " + url);
    }

    @When("^I navigate to website as guest user$")
    public void iNavigateToWebsiteAsGuestUser() throws Throwable {
        CreateWishlistPage.headerPanel.selectProfileInfolink("Sign Out");
        iNavigateToGuestInvitationPage();
    }

    @And("^I should see all item details on event Wishlist page that are added by celebrant$")
    public void iShouldSeeAllItemDetailsOnEventWishlistPageThatAreAddedByCelebrant() throws Throwable {
        Wait.secondsUntilElementNotPresent(EventWishlistPage.productSKU, 5);
        Assert.assertTrue(CreateWishlistPage.skuDetails.equalsIgnoreCase(EventWishlistPage.productSKU.getText()));
        Assert.assertTrue(EventWishlistPage.rqsQuantity.getText().equals(qty));
    }

    @Then("^I should see \"([^\"]*)\" error message on event wishlist page$")
    public void iShouldSeeErrorMessageOnEventWishlistPage(String expectedMsg) throws Throwable {
        onPage(EventWishlistPage.class);
        Wait.untilElementPresent(EventWishlistPage.snackBar);
        Assert.assertTrue("ERROR-ENV: Expected message is not displayed", EventWishlistPage.snackBar.getText().contains(expectedMsg));

    }

    @And("^I cancel the event by updating event status in DB$")
    public void iCancelTheEventByUpdatingEventStatusInDB() throws Throwable {
        EGRDB.cancelEvent(RegistryEventsSteps.eventId);
    }

    @And("^I should see My cart quantity as \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iShouldSeeMyCartQuantityAsOnPage(String quantity, String page) throws Throwable {
        onPage(EventWishlistPage.class);
        ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(0,0)");
        Wait.untilElementPresent(EventWishlistPage.myCartCount);
        Thread.sleep(2000);
        Assert.assertTrue(EventWishlistPage.myCartCount.getText().equalsIgnoreCase(quantity));
    }

    @And("^I should see the requested quantity is not editable by the guest user$")
    public void iShouldSeeTheRequestedQuantityIsNotEditableByTheGuestUser() throws Throwable {
        onPage(EventWishlistPage.class);
        Assert.assertNotEquals(EventWishlistPage.rqsQuantity.getTagName(), "input");
    }

    @And("^I should see the requested quantity is same as updated quantity$")
    public void iShouldSeeTheRequestedQuantityIsSameAsUpdatedQuantity() throws Throwable {
        onPage(EventWishlistPage.class);
        Assert.assertTrue("Error - App: Updated qantity is same as requested quantity", EventWishlistPage.rqsQuantity.getText().equals(qty));
    }

    @Then("^I should see same products added with quantity \"([^\"]*)\" in my cart page$")
    public void iShouldSeeSameProductsAddedWithQuantityInMyCartPage(String quantity) throws Throwable {
        onPage(MyCartPage.class);
//        String sample = CreateWishlistPage.skuDetails.replace("- ", "");
//        Assert.assertTrue(MyCartPage.skuIds().contains(sample));
        Assert.assertTrue(MyCartPage.skuIds().contains(CreateWishlistPage.skuDetails));
        Assert.assertTrue(MyCartPage.verifyQuantity().equals(quantity));
    }

    @And("^I delete all the items on MyCartPage page$")
    public void iDeleteAllTheItemsOnMyCartPagePage() throws Throwable {
        onPage(MyCartPage.class);
        Thread.sleep(20);
        MyCartPage.deleteProduct();
    }

    @And("^I should see the \"([^\"]*)\" message on GiftRegistry MyCartPage page$")
    public void iShouldSeeTheMessageOnGiftRegistryMyCartPagePage(String message) throws Throwable {
        onPage(MyCartPage.class);
        Assert.assertTrue("Error -App: Your cart is empty message shown", MyCartPage.cartEmpty.getText().equalsIgnoreCase(message));
    }

    @And("^I navigate to event wishlist page$")
    public void iNavigateToEventWishlistPage() throws Throwable {
        onPage(MyCartPage.class);
        Actions.waitUntil(() -> MyCartPage.eventWishlistLink.isDisplayed());
        MyCartPage.eventWishlistLink.click();
        onPage(EventWishlistPage.class);
    }

    @Then("^I should see the my cart count as \"([^\"]*)\"$")
    public void iShouldSeeTheMyCartCountAs(String quantity) throws Throwable {
        Assert.assertTrue("Error - App: Cart Quantity is updated to 0", EventWishlistPage.myCartCount.getText().equals(quantity));
    }

    @Then("^I should see the quantity updated on event wishlist page$")
    public void iShouldSeeTheQuantityUpdatedOnEventWishlistPage() throws Throwable {
        Assert.assertTrue("Error - App: Quantity updated on Event wishlist page", MyWishlistPage.verifyQuantity().equals(qty));
    }

    @And("^I navigate to previous guest invitation page with guest login details$")
    public void iNavigateToPreviousGuestInvitationPageWithGuestLoginDetails() throws Throwable {
        iNavigateToGuestInvitationPage();
        onPage(GuestInvitationPage.class);
        click("registerSignInBtn");
        onPage(LoginPage.class);
        LoginPage.guestLogin(RegistryHomeSteps.email, RegistryHomeSteps.password);
        onPage(GuestInvitationPage.class);
    }

    @Then("^I should see \"([^\"]*)\" message in snack bar on \"([^\"]*)\"page$")
    public void iShouldSeeMessageInSnackBarOnPage(String message, String page) throws Throwable {
        Thread.sleep(300);
        if (page.equalsIgnoreCase("GiftRegistry ItemDetailsPage")) {
            Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", ItemDetailsPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
            Wait.secondsUntilElementNotPresent(ItemDetailsPage.snackBar, 5);
        } else if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage")) {
            Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", CustomerManagementPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
            Wait.secondsUntilElementNotPresent(CustomerManagementPage.snackBar, 5);
        } else if (page.equalsIgnoreCase("GiftRegistry CreateWishlistPage")) {
            Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", CreateWishlistPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
            Wait.secondsUntilElementNotPresent(CreateWishlistPage.snackBar, 5);
        } else if (onPageVerify(StoreEventManagementPage.class)) {
            Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", StoreEventManagementPage.snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
            Wait.secondsUntilElementNotPresent(CreateWishlistPage.snackBar, 5);
        }
        log().info("Verified the message in snack bar");
    }

    @And("^I should see the below message on GiftRegistry MyCartPage page$")
    public void iShouldSeeTheBelowMessageOnGiftRegistryMyCartPagePage(List<String> message) throws Throwable {
        onPage(MyCartPage.class);
        Assert.assertTrue("Error - App: Invalid alert error message on my cart page", MyCartPage.cartIsEmpty.getText().equalsIgnoreCase(message.get(0)));
    }

    @And("^I visit the website opened in new window$")
    public void iVisitTheWebsiteInOpenedNewWindow() throws Throwable {
        Page.switchWindow(1);
        log().info("Gift Registry login page is displayed when opened new window in same session");
//        Page.switchWindow(2);
//        log().info("Gift Registry Account verification page is displayed when opened new window in same session");
    }

    @And("^I switch \"([^\"]*)\" from current to other tab$")
    public void iSwitchBetweenBrowserTabs(String moveType) throws Throwable {
//        if(moveType.equalsIgnoreCase("forward")) {
//            Browser.getDriver().getWindowHandles().size();
            Page.switchWindow(1);
//        }else{
//            Page.switchWindow(0);
//        }
        log().info("Navigated between browser tabs");
//        Page.switchWindow(2);
//        log().info("Gift Registry Account verification page is displayed when opened new window in same session");
    }

    @And("^I should see \"([^\"]*)\" and \"([^\"]*)\" icons on \"([^\"]*)\" page$")
    public void iShouldSeeAndIconsOnPage(String icon1, String icon2, String page) throws Throwable {
        onPage(page);
        Assert.assertTrue(icon1, new CustomerManagementPage().viewEvents.getAttribute("mattooltip").contains(icon1));
        Assert.assertTrue(icon2, new CustomerManagementPage().createEvent.getAttribute("mattooltip").contains(icon2));
        log().info(icon1 + " and " + icon2 + " is displayed successfully on " + page + "page");

    }
}





