package test.automation.steps.GiftRegistry;


import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import org.junit.Assert;
import test.automation.framework.Browser;
import test.automation.framework.Config;
import test.automation.pages.GiftRegistry.HomePage;
import test.automation.utils.EGRDB;

public class DBSteps {

    public  static Integer companyCode() {
        if (Config.getUrl().contains("all")) {
            return 1;
        } else if (Config.getUrl().contains("crateandbarrel")) {
            return 2;
        } else if (Config.getUrl().contains("babycompany")) {
            return 3;
        } else if (Config.getUrl().contains("toykingdom")) {
            return 4;
        } else if (Config.getUrl().contains("saci")) {
            return 5;
        } else if (Config.getUrl().contains("departmentstore")) {
            return 6;
        } else if (Config.getUrl().contains("ourhome")) {
            return 7;
        }
        return 0;
    }

    public static Integer companyCode(String event) {
        if (event.contains("babyShower")) {
            return 1;
        } else if (event.contains("baptism")) {
            return 2;
        } else if (event.contains("birthdayAdult")) {
            return 3;
        } else if (event.contains("birthdayKids")) {
            return 4;
        } else if (event.contains("wedding")) {
            return 5;
        } else if (event.contains("birthdayDebut")) {
            return 6;
        } else if (event.contains("anniversary")) {
            return 7;
        } else if (event.contains("houseWarming")) {
            return 8;
        }
        return 0;
    }

    @And("^I verify email id is persisting in ege_user table$")
    public void iVerifyEmailIdIsPersistingInEge_userTable() throws Throwable {
        Assert.assertTrue("Error - App: " + RegistryHomeSteps.email + " is not saved in egr_user table after register", EGRDB.emailIdsList().contains(RegistryHomeSteps.email));
    }

    @And("^I verify status code set to (\\d+)$")
    public void iVerifyStatusCodeSetTo(Integer statusCode) throws Throwable {
        Assert.assertEquals("Error - App: Invalid status code persisted", EGRDB.statusCode(RegistryHomeSteps.email), statusCode);
    }

    @And("^I verify company id is saving correctly$")
    public void iVerifyCompanyIdIsSavingCorrectly() throws Throwable {
        Assert.assertTrue("Error - App: Company code is incorrectly saving in egr_user", EGRDB.companyId(RegistryHomeSteps.email).equals(companyCode()));
    }

    @And("^I verify mandatory field values are persisting in egr_user table$")
    public void iVerifyMandatoryFieldValuesArePersistingInEgr_userTable() throws Throwable {
        Assert.assertTrue("Error - App: null value is stored in egr_user table created_by field after online registration is done",EGRDB.fieldValue(RegistryHomeSteps.email,"created_by") != null);
        Assert.assertTrue("Error - App: null value is stored in egr_user table modified_by field after online registration is done",EGRDB.fieldValue(RegistryHomeSteps.email,"modified_by") != null);
        Assert.assertTrue("Error - App: null value is stored in egr_user table verification_token field after online registration is done",EGRDB.fieldValue(RegistryHomeSteps.email,"verification_token") != null);
    }

    @And("^I verify event status code set to (\\d+)$")
    public void iVerifyEventStatusCodeSetTo(Integer statusCode) throws Throwable {
        Assert.assertEquals("Error - App: Invalid status code persisted", EGRDB.eventFieldValue(RegistryHomeSteps.email, "event_status"), statusCode.toString());
    }

    @And("^I should see event id in egr_event table$")
    public void iShouldSeeEventIdInEgr_eventTable() throws Throwable {
        String event_id = EGRDB.eventFieldValue(RegistryHomeSteps.email, "event_id");
        if (event_id == null)
            Assert.fail("Error - App: Event id is not persisting in egr_event table");
        else if (RegistryEventsSteps.eventId == null)
            Assert.fail("Error - App: Event id is not displaying in invitation template URL");
        else
            Assert.assertEquals("Error - App: Incorrect Event id is showing in egr_event table or invitation template url", event_id, RegistryEventsSteps.eventId.toString());
    }
}
