package test.automation.steps.GiftRegistry;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import test.automation.framework.*;
import test.automation.pages.GiftRegistry.*;

import static test.automation.framework.Actions.click;
import static test.automation.framework.Page.*;
import static test.automation.framework.Runner.log;
import static test.automation.pages.GiftRegistry.CreateWishlistPage.crateBarrelDropDown;
import static test.automation.pages.GiftRegistry.CreateWishlistPage.productsImage;

public class RegistryWishlistSteps {
    public static String qty;
    private static String skuID;

    @Then("^I should see same products (added|updated) with quantity \"([^\"]*)\" in my wishlist page$")
    public void iShouldSeeSameProductsAddedWithQuantityInMyWishlistPage(String action, String quantity) throws Throwable {
        onPage(MyWishlistPage.class);
        Assert.assertTrue(MyWishlistPage.skuIds().contains(CreateWishlistPage.skuDetails));
        Assert.assertTrue(MyWishlistPage.verifyQuantity().equals(quantity));
        log().info("Same products with same quantity is displayed on my wishlist page");
    }

    @When("^I add product to wishlist with quantity \"([^\"]*)\" from \"([^\"]*)\" page$")
    public void iAddProductToWishlistWithQuantityFromPage(String quantity, String page) throws Throwable {
        qty = quantity;
        switch (page) {
            case "GiftRegistry CreateWishlistPage":
                onPage(CreateWishlistPage.class);
                ((CreateWishlistPage) getCurrentPage()).selectRandomProduct(qty);
                break;
            case "GiftRegistry ItemDetailsPage":
                onPage(CreateWishlistPage.class);
                ((CreateWishlistPage) getCurrentPage()).selectProduct();
                onPage(ItemDetailsPage.class);
                ((ItemDetailsPage) getCurrentPage()).quantity.sendKeys(quantity);
                ((ItemDetailsPage) getCurrentPage()).addtoWishlistBtn.click();
                ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();", ((ItemDetailsPage) getCurrentPage()).backBtn);
                onPage(CreateWishlistPage.class);
                ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();", ((CreateWishlistPage) getCurrentPage()).myWishlistLink);
                break;
        }
        log().info("Product is added successfully");
    }

    @Then("^I should see my wishlist count as \"([^\"]*)\" on \"([^\"]*)\" Page$")
    public void iShouldSeeMyWishlistCountAsOnPage(String count, String page) throws Throwable {
        onPage(CreateWishlistPage.class);
        Assert.assertTrue(((CreateWishlistPage) getCurrentPage()).myWishlistCount.getText().equals(count));
        log().info("Wishlist count is displayed successfully");
    }

    @When("^I update the quantity of the product to \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iUpdateTheQuantityOfTheProductToOnPage(String quantity, String page) throws Throwable {
        qty = quantity;
        switch (page) {
            case "GiftRegistry MyWishlistPage":
                onPage(MyWishlistPage.class);
                ((MyWishlistPage) getCurrentPage()).updateQuantity(qty);
                break;
            case "GiftRegistry EventWishlistPage":
                onPage(EventWishlistPage.class);
                ((EventWishlistPage) getCurrentPage()).updateQuantity(qty);
                break;
            case "GiftRegistry MyCartPage":
                onPage(MyCartPage.class);
                ((MyCartPage) getCurrentPage()).updateQuantity(qty);
                }
        log().info("Quantity is updated successfully");
    }

    @When("^I delete the product on \"([^\"]*)\" page$")
    public void iDeleteTheProductOnPage(String page) throws Throwable {
        onPage(MyWishlistPage.class);
        Assert.assertTrue(MyWishlistPage.skuIds().contains(CreateWishlistPage.skuDetails));
        ((MyWishlistPage) getCurrentPage()).deleteProduct();
        log().info("Product is deleted successfully on the " + page);
    }

    @Then("^I should not see deleted product on \"([^\"]*)\" page$")
    public void iShouldNotSeeDeletedProductOnPage(String page) throws Throwable {
        Wait.secondsUntilElementNotPresent(MyWishlistPage.snackBar, 4);
        Assert.assertFalse(MyWishlistPage.skuIds().contains(CreateWishlistPage.skuDetails));
        log().info("Deleted product is not displayed");
    }

    @When("^I click on \"([^\"]*)\" option for the product on \"([^\"]*)\" page$")
    public void iClickOnForTheProductOnPage(String option, String page) throws Throwable {
        if (Config.getUrl().contains("/crateandbarrel")) {
            onPage(MyWishlistPage.class);
            ((MyWishlistPage) getCurrentPage()).mostLoved();
        }
        log().info("Options for crateandbarrel is is selected successfully");

    }

    @Then("^I should see \"([^\"]*)\" option selected for the same product on \"([^\"]*)\" page$")
    public void iShouldSeeOptionSelectedForTheSameProductOnPage(String arg0, String arg1) throws Throwable {
        onPage(MyWishlistPage.class);
        Wait.secondsUntilElementNotPresent(MyWishlistPage.snackBar, 4);
        new Steps().iShouldSeeElement("mostLovedSelected");
        log().info("option is selected successfully");
    }

    @When("^I click on \"([^\"]*)\" element on GiftRegistry CreateWishlistPage page$")
    public void iClickOnElementOnGiftRegistryCreateWishlistPagePage(String link) throws Throwable {
        ((JavascriptExecutor) Browser.getDriver()).executeScript("window.scrollTo(0,0)");
        ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();", (CreateWishlistPage.myWishlistLink));
        log().info("Clicked on mywishlist link successfully");
    }

    @And("^I select random category from select category menu$")
    public void iSelectRandomCategoryFromSelectCategoryMenu() throws Throwable {
        CreateWishlistPage.selectRandomCategory();
        log().info("Selected random category from category menu");
    }

    @And("^I should see \"([^\"]*)\" response for products image on \"([^\"]*)\" page$")
    public void iShouldSeeResponseForProductsImageOnPage(String responseCode, String page) throws Throwable {
        onPage(CreateWishlistPage.class);
        for (WebElement productImageLinks : productsImage) {
            String url = productImageLinks.getAttribute("href");
            Assert.assertEquals("Error - App: " + responseCode + "is not returning for " + url + " ", responseCode, Rest.getResponseCode(url));
        }
        log().info("images are displayed  on create wishlist page");
    }

    @And("^I should see add to cart button change to added to cart in disabled view$")
    public void iShouldSeeAddToCartButtonChangeToAddedToCartInDisabledView() throws Throwable {
        Assert.assertTrue("Erro - App: Button is not changed to added to cart", EventWishlistPage.addToCart.getAttribute("value").equalsIgnoreCase("Added to cart"));
        Assert.assertFalse("Error - App: Added to cart button is not disabled", EventWishlistPage.addToCart.isEnabled());

    }

    @And("^I should see my cart count updated to (\\d+)$")
    public void iShouldSeeMyCartCountUpdatedTo(Integer quantity) throws Throwable {
        Assert.assertTrue("Error - App: Count in my cart is not updated after adding product to my cart", EventWishlistPage.myCartCount.getText().equals(quantity.toString()));
    }

    @And("^I click addToCart button on EventWishlistPage page$")
    public void iClickAddToCartButtonOnEventWishlistPagePage() throws Throwable {
        Wait.secondsUntilElementNotPresent(EventWishlistPage.productSKU, 5);
        skuID = EventWishlistPage.productSKU.getText();
        click("addToCart");
        log().info("Clicked on addToCart button");
    }

    @And("^I should see selected product is added in cart page$")
    public void iShouldSeeSelectedProductIsAddedInCartPage() throws Throwable {
        Assert.assertTrue("Error - App: Added product from wish list is not showing in the cart", GuestCart.productSkuIds().contains(skuID));
    }

    @And("^I should see added product delected from the guest cart$")
    public void iShouldSeeAddedProductDelectedFromTheGuestCart() throws Throwable {
        Assert.assertFalse("Error - App: Product is not deleted from the guest cart when celebrant has deleted the same item from celebrant wish list",
                (GuestCart.productSkuIds().isEmpty() ? GuestCart.productSkuIds().isEmpty() : !GuestCart.productSkuIds().contains(skuID)));
    }

    @And("^I should see \"([^\"]*)\" in \"([^\"]*)\" element on \"([^\"]*)\"page$")
    public void iShouldSeeInElementOnPage(String text, String element, String page) throws Throwable {
        TypifiedElement elementO = getElement(element);
        String elementText = elementO.getAttribute("value");
        Assert.assertTrue(text + " is not displayed in " + element + " ( " + elementText + " )", elementText.equalsIgnoreCase(text));
    }

    @When("^I click on \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iClickOnOnPage(String arg0, String arg1) throws Throwable {
        EventWishlistPage.alertYes.click();
        //RoleManagementPage.submitBtn.click();
    }

}