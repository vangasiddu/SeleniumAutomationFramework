package test.automation.framework;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.CookieList;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import test.automation.models.User;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import test.automation.pages.GiftRegistry.*;
import test.automation.utils.EGRDB;
import test.automation.utils.UserUtils;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static test.automation.framework.Actions.click;
import static test.automation.framework.Actions.waitUntil;
import static test.automation.framework.Runner.log;

public class StoreUser extends Page {
    public String roleName;
    public String Pagination;
    public String userEmail;
    public String newRollCreated;
    public List<String> allRolesList;
    public String keyword;
    public String roleSelected;
    public String companySelected;
    public String branchSelected;
    public static String value;
    public static String email;
    public static String password;
    public static String eventCode;
    public static String eventId;
    public List<String> userDataBeforeRefresh;
    public String eventCodeValue;
    public String newEventCodeValue;
    public String celebrantName;

    private EventConfirmationPage eventConfirmationPage = new EventConfirmationPage();
    private ConfigurationManagementPage configurationManagementPage = new ConfigurationManagementPage();
    private StoreEventConfirmationPage storeEventConfirmationPage = new StoreEventConfirmationPage();
    private StoreCreateEventPage storeCreateEventPage = new StoreCreateEventPage();
    private StoreEditEventPage storeEditEventPage = new StoreEditEventPage();

    @Given("^I visit the website as \"([^\"]*)\"$")
    public void iVisitTheWebsiteAs(String user) throws Throwable {
        visit("GiftRegistry StoreLoginPage");
        log().info("Navigated to " + Config.getUrl());
    }

    @When("^I enter valid user details on \"([^\"]*)\" page$")
    public void iEnterValidDetailsOnPage(String page) throws Throwable {
        switch (page) {
            case "GiftRegistry LoginPage":
            case "GiftRegistry RegistrationModel":
                onPage("GiftRegistry LoginPage");
                User userDetails = UserUtils.getNewUser();
                LoginPage.register(userDetails);
                email = userDetails.getEmail();
                break;
            case "GiftRegistry AdditionalDetailsPage":
                onPage("GiftRegistry AdditionalDetailsPage");
                ((AdditionalDetailsPage) getCurrentPage()).updateAccount(UserUtils.getUser());
                break;
            case "GiftRegistry StoreLoginPage":
                onPage("GiftRegistry StoreLoginPage");

            default:
                break;

        }
        log().info("Entered data on " + page + " page successfully");
    }

    @Then("^I should see error \"([^\"]*)\" message on \"([^\"]*)\" page$")
    public void iShouldSeeErrorMessageOnPage(String errorMessage, String page) throws Throwable {
        if (page.equalsIgnoreCase("GiftRegistry StoreLoginPage")) {
            Assert.assertTrue("Same error message is displayed", StoreLoginPage.error_message.getText().equalsIgnoreCase(errorMessage));
        } else if (page.equalsIgnoreCase("GiftRegistry RoleManagementPage")) {
            Assert.assertTrue("Same error message is displayed", RoleManagementPage.validationError.getText().equalsIgnoreCase(errorMessage));
        }
        log().info("ERROR - ENV: Verified error messages");
    }

    @Then("^I should see user information on \"([^\"]*)\" page$")
    public void iShouldSeeUserInformationOnPage(String page) throws Throwable {
        Thread.sleep(500);
        List<WebElement> userName = UserManagementPage.userRows;
        for (int i = 0; i < userName.size(); i++) {
            if (userName.get(i).getText().isEmpty()) {
                String errorMsg = "Unable to fetch User Management details";
                Assert.assertEquals(UserManagementPage.errorMessage.getText(), errorMsg);
            }
            Assert.assertTrue("Wrong names are shown on page", userName.get(i).getText().toLowerCase().contains(keyword));
        }
    }

    @Then("^I assign role for \"([^\"]*)\" user on \"([^\"]*)\" page$")
    public void iAssignRoleForUserOnOnPage(String user, String page) throws Throwable {
        Thread.sleep(1000);
        List<WebElement> userList = UserManagementPage.userRows;
        UserManagementPage.userRows.stream().filter(a -> a.getText().toLowerCase().contains(user.toLowerCase()) || a.getText().equalsIgnoreCase(user))
                .collect(Collectors.toList()).get(0).findElement(By.className("mat-column-Actions")).findElement(By.className("store-action")).click();
    }

    @Then("^I unselect assigned roles on \"([^\"]*)\" page$")
    public void iUnselectAssignedRoles(String page) throws Throwable {
        if (!AssignRolesPage.roleAssigned.isEmpty()) AssignRolesPage.roleAssigned.stream().forEach(a -> a.click());
    }

    @When("^I enter invalid details field name with input values on GiftRegistry StoreLoginPage page$")
    public void iEnterInvalidDetailsFieldNameWithInputValuesOnGiftRegistryStoreLoginPagePage(List<Map<String, String>> data) throws Throwable {
        for (Map<String, String> entry : data) {
            StoreLoginPage.userId.sendKeys(entry.get("userId"));
            StoreLoginPage.password.sendKeys(entry.get("password"));
            StoreLoginPage.signInButton.click();
        }
    }

    @And("^I enter \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iEnterOnPage(String role_name, String page) throws Throwable {
        roleName = role_name;
        RoleManagementPage.roleName.sendKeys(roleName);
    }

    @And("^I should see no role assigned to newly created user$")
    public void iShouldSeeNoRoleAssginedToNewlyCreatedUser() throws Throwable {
        Actions.waitUntil(() -> EditPrivilegesPage.checkedCheckbox.isEmpty(), 5);
        Assert.assertTrue("ERROR - APP: Role allocated to" + newRollCreated + " user", EditPrivilegesPage.checkedCheckbox.isEmpty());
    }

    @Then("^I should see \"([^\"]*)\"role_name\"([^\"]*)\" message on \"([^\"]*)\" page$")
    public void iShouldSeeRole_nameMessageOnPage(String message, String message1, String page) throws Throwable {
        if (page.equalsIgnoreCase("GiftRegistry RoleManagementPage")) {
            String Message = message + newRollCreated + message1;
            Assert.assertTrue("Same message displayed on UI", Message.equalsIgnoreCase(RoleManagementPage.roleCreationMessage.getText()));
        } else if (page.equalsIgnoreCase("GiftRegistry EditPrivilegesPage")) {
            String Message = newRollCreated + message1;
            String successMessageOnUI = EditPrivilegesPage.privilegesupdationMessage.getText();
            Assert.assertTrue("Same message displayed on UI", Message.equals(successMessageOnUI));
        }
    }

    @Then("^I should see newly created role name on \"([^\"]*)\" page$")
    public void iShouldSeeNewlyCreatedRoleNameOnPage(String page) throws Throwable {
        Actions.waitUntil(() -> RoleManagementPage.first_role.getText().equals(newRollCreated), 5);
        List<WebElement> createdUser = RoleManagementPage.roleCreated.stream().filter(a -> a.getText().equalsIgnoreCase(newRollCreated)).collect(Collectors.toList());
        Assert.assertTrue("Error - APP: User " + newRollCreated + " created", createdUser.get(0).getText().equalsIgnoreCase(newRollCreated));
    }

    @Then("^I should be able to \"([^\"]*)\" for newly created role name on \"([^\"]*)\" page$")
    public void iShouldBeAbleToDeleteOrEditPrevilageForNewlyCreatedRoleNameOnPage(String action, String page) throws Throwable {
        List<WebElement> userDelete = RoleManagementPage.userRoleRow.stream().filter(webElement -> webElement.getText().contains(newRollCreated))
                .collect(Collectors.toList());
        if (action.equalsIgnoreCase("Delete")) {
            userDelete.get(0).findElement(By.linkText(action)).click();
            RoleManagementPage.yes.click();
        } else {
            userDelete.get(0).findElement(By.linkText(action)).click();
        }
    }

    @Then("^I should be able to see all the roles list when user clicks on select role option$")
    public void iShouldBeAbleToSeeAllTheRolesListOnClickingSelectRoleOption() throws Throwable {
        Thread.sleep(1000);
        EditPrivilegesPage.SelectRole.click();
        List<String> roleNamesOnEditPage = EditPrivilegesPage.rolesList.stream().map(a -> a.getText()).collect(Collectors.toList());
     //   Assert.assertTrue("Error: Roll Names not matching", allRolesList.equals(roleNamesOnEditPage));
        for (int i = 0; i < allRolesList.size(); i++) {
            Assert.assertTrue("ERROR - DATA: User " + allRolesList.get(i) + "not found", allRolesList.get(i).equals(roleNamesOnEditPage.get(i)));
        }
        EditPrivilegesPage.matOption.stream().filter(a -> a.getAttribute("aria-selected").equals("true")).collect(Collectors.toList()).get(0).click();
//        Browser.getDriver().findElements(By.className("mat-option")).stream().filter(a->a.getAttribute("aria-selected").equals("true")).collect(Collectors.toList()).get(0).click();
    }

    @Then("^I collect all the role names from \"([^\"]*)\" page$")
    public void iCollectAllTheRoleNamesFromPage(String page) throws Throwable {
        List<String> roleNamesOnRoleManagementPage = RoleManagementPage.allRolesNamesList.stream().filter(webElement -> !webElement.getAttribute("role").equals("columnheader"))
                .collect(Collectors.toList()).stream().map(a -> a.getText()).collect(Collectors.toList());
        allRolesList = roleNamesOnRoleManagementPage;
    }

    @Then("^I should not see deleted user role on \"([^\"]*)\" page$")
    public void iShouldNotSeeDeletedUserRoleOnPage(String page) throws Throwable {
        onPage(RoleManagementPage.class);
        Actions.waitUntil(() -> !RoleManagementPage.first_role.getText().equals(newRollCreated), 5);
        Assert.assertTrue("ERROR - APP: User role not deleted", RoleManagementPage.roleCreated.stream().filter(a -> a.getText().equalsIgnoreCase(newRollCreated)).collect(Collectors.toList()).isEmpty());
    }

    @Then("^I should see duplicate role error \"([^\"]*)\" message on \"([^\"]*)\" page$")
    public void iShouldSeeDuplicateRoleErrorMessageOnPage(String message, String page) throws Throwable {
        Assert.assertTrue("Duplicate role creation message displayed", message.equalsIgnoreCase(RoleManagementPage.duplicateRole.getText()));
    }

    @And("^I get the \"([^\"]*)\"  from \"([^\"]*)\" dropdown$")
    public void iGetTheFromDropdown(String role_name_ui, String roleDropdown) throws Throwable {
        Thread.sleep(1000);
        String role = EditPrivilegesPage.storeUserRole.getText();
        roleName = role;
    }

    @And("^I select the following privileges:$")
    public void iSelectTheFollowingPrivileges(List<String> privileges) throws Throwable {
        Thread.sleep(30);
        privileges.forEach(webElement -> getElement(webElement).click());
    }

    @And("^I (select|unselect) the below \"([^\"]*)\" privileges option:$")
    public void iSelectThePrivileges(String actionType, String option, List<String> options) throws Throwable {
        List<WebElement> allRolesList = new ArrayList<>();
        if (option.equalsIgnoreCase("header")) {
            Actions.waitUntil(() -> !EditPrivilegesPage.components.isEmpty());
            List<WebElement> headerChecks = EditPrivilegesPage.components;
            allRolesList.addAll(headerChecks);
        } else {
            Actions.waitUntil(() -> !EditPrivilegesPage.privilegesList.isEmpty());
            List<WebElement> childChecks = EditPrivilegesPage.privilegesList;
            allRolesList.addAll(childChecks);
        }
        for (int i = 0; i < options.size(); i++) {
            String role = options.get(i);
            List<WebElement> roleSelected = allRolesList.stream().filter(a -> a.getText().equals(role)).collect(Collectors.toList());
            Assert.assertTrue("ERROR - APP: Header option not visible", roleSelected.get(0).isDisplayed());
            roleSelected.get(0).findElement(By.className("mat-checkbox")).click();
            if (!EditPrivilegesPage.dialogBox.isEmpty()) {
                if (actionType.equalsIgnoreCase("select")) {
                    EditPrivilegesPage.cancelButton.click();
                } else {
                    EditPrivilegesPage.okButton.click();
                }
            }
        }
    }

    @And("^I should see role access granted for below \"([^\"]*)\" roles:$")
    public void iShouldSeeRoleAccessGrantedForRole(String roleType, List<String> roles) throws Throwable {
        List<WebElement> allRolesList = new ArrayList<>();
        if (roleType.equalsIgnoreCase("header and child")) {
            List<WebElement> previligeHeaderRows = EditPrivilegesPage.components;
            List<WebElement> privilegeRolesList = EditPrivilegesPage.privilegesList;
            allRolesList.addAll(previligeHeaderRows);
            allRolesList.addAll(privilegeRolesList);
        } else {
            List<WebElement> privilegeRolesList = EditPrivilegesPage.privilegesList;
            allRolesList.addAll(privilegeRolesList);
        }
        for (int i = 0; i < roles.size(); i++) {
            String role = roles.get(i);
            List<WebElement> roleSelected = allRolesList.stream().filter(a -> a.getText().equals(role)).collect(Collectors.toList());
            Assert.assertTrue("ERROR - APP:" + roleSelected.get(0).getText() + "Not selected", roleSelected.get(0).findElement(By.className("mat-checkbox-checked")).isDisplayed());
        }
    }

    @Then("^I should see role deletion dialog box displayed$")
    public void iShouldSeeRoleDeletionDialogBoxDisplayed() throws Throwable {
        Assert.assertTrue("role deletion dialog box displayed", RoleManagementPage.roleDeletionDialog.isDisplayed());
    }

    @Then("^I should see the role deleted$")
    public void iShouldSeeTheRoleDeleted() throws Throwable {
        Assert.assertFalse("first role is deleted", RoleManagementPage.first_role.getText().equalsIgnoreCase(roleName));
    }

    @And("^I should see the role name displayed$")
    public void iShouldSeeTheRoleNameDisplayed() throws Throwable {
        Assert.assertTrue("role_name is displayed", RoleManagementPage.first_role.isDisplayed());
        roleName = RoleManagementPage.first_role.getText();
    }

    @Then("^I should see the role displayed at the top of the list$")
    public void iShouldSeeTheRoleDisplayedAtTheTopOfTheList() throws Throwable {
        Actions.waitUntil(() -> RoleManagementPage.first_role.getText().equals(newRollCreated), 5);
        Assert.assertTrue("Role name displayed at the top of the list", RoleManagementPage.first_role.getText().equals(newRollCreated));
    }

    @And("^I enter random rol_name on \"([^\"]*)\" page$")
    public void iEnterRandomRol_nameOnPage(String page) throws Throwable {
        newRollCreated = RandomStringUtils.randomAlphabetic(10);
        RoleManagementPage.roleName.sendKeys(newRollCreated);
    }

    @And("^I should see the below message on AssignRolesPage$")
    public void iShouldSeeTheBelowMessageOnAssignRolesPage(List<String> message) throws Throwable {
        String msz = message.get(0);
        Assert.assertTrue("Error - App:- Registration confirmation message is not displayed correctly", AssignRolesPage.privilegesupdationMessage.getText().equalsIgnoreCase(msz));
        log().info("Verified Registration confirmation message");
    }

    @And("^I should have access to only below options on the correct site:$")
    public void iShouldHaveAccessToOnlyOptionOnSite(List<String> option) throws Throwable {
        List<WebElement> topNav = CustomerManagementPage.topNavLinks;
        List<String> navOptions = topNav.stream().map(a -> a.getText()).collect(Collectors.toList());
        for (int i = 0; i < option.size(); i++) {
            CustomerManagementPage.loggedInCompanyName.getText().equalsIgnoreCase(companySelected);
            Assert.assertTrue("ERROR: APP: Unexpected user options found", navOptions.contains(option.get(i)));
        }
    }

    @And("^I \"([^\"]*)\" role permission to \"([^\"]*)\" role from \"([^\"]*)\" page$")
    public void iModifyTheRolePermissionFromPage(String accessType, String roleType, String page) throws Throwable {
        Thread.sleep(500);
        AssignRolesPage.company.click();
//        AssignRolesPage.allCompanies.stream().filter(a->a.getText().equalsIgnoreCase("Department Store")).collect(Collectors.toList()).get(0).click();
        AssignRolesPage.allCompanies.get(new Random().nextInt(AssignRolesPage.allCompanies.size())).click();
        Thread.sleep(500);
        companySelected = AssignRolesPage.selectedCompany.get(0).getText();
        AssignRolesPage.branch.click();
        if (!companySelected.equalsIgnoreCase("Department Store")) {
            Thread.sleep(300);
            AssignRolesPage.allBranches.get(new Random().nextInt(AssignRolesPage.allBranches.size())).click();
            branchSelected = AssignRolesPage.selectedCompany.get(1).getText();
        }
        if (roleType.equalsIgnoreCase("random")) {
            List<WebElement> allRoles = AssignRolesPage.roleRowsList;
            WebElement randomRole = allRoles.get(new Random().nextInt(allRoles.size()));
            roleSelected = randomRole.getText();
            randomRole.findElement(By.className("mat-checkbox-layout")).click();
        } else if (roleType.equalsIgnoreCase("newRole")) {
            AssignRolesPage.roleRowsList.stream().filter(a -> a.findElement(By.className("mat-column-ROLES")).getText()
                    .equalsIgnoreCase(newRollCreated)).collect(Collectors.toList()).get(0).findElement(By.className("mat-checkbox-layout")).click();
        } else {
            AssignRolesPage.roleRowsList.stream().filter(a -> a.findElement(By.className("mat-column-ROLES")).getText()
                    .equalsIgnoreCase(roleType)).collect(Collectors.toList()).get(0).findElement(By.className("mat-checkbox-layout")).click();
        }
//        AssignRolesPage.saveButton.click();
//        String msz = "Success! Details updated successfully.";
//        Assert.assertTrue("Error - App:- Registration confirmation message is not displayed correctly", AssignRolesPage.privilegesupdationMessage.getText().equalsIgnoreCase(msz));
        log().info("role assigned for a user");
    }

    @And("^I should see \"([^\"]*)\" items per page on the dropdwon by default$")
    public void iShouldSeeItemsPerPageOnTheDropdwonByDefault(String pagination) throws Throwable {
        Pagination = pagination;
        Assert.assertTrue("default pagination 5 items per page is displayed", pagination.equals(CustomerManagementPage.defaultPagination.getText()));
    }

    @And("^I should see correct company and branch are allocated to the user$")
    public void iShouldSeeCorrectDetails() throws Throwable {
        Thread.sleep(300);
        assertTrue("ERROR: Wrong company showing", UserManagementPage.companyList.get(1).getText().equalsIgnoreCase(companySelected));
        assertTrue("Error: Wrong branch showing", UserManagementPage.branchesList.get(1).getText().equalsIgnoreCase(branchSelected));
    }

    @And("^I should see pagination element default value is \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iShouldSeePaginationElementOnPage(String paginationValue, String page) throws Throwable {
        List<WebElement> rows = new ArrayList<>();
        if (page.equalsIgnoreCase("GiftRegistry UserManagementPage")) {
            assertEquals(paginationValue, UserManagementPage.defaultPagination.getText());
            rows = UserManagementPage.userRows;
        } else if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage")) {
            assertEquals(paginationValue, CustomerManagementPage.defaultPagination.getText());
            rows = CustomerManagementPage.userRows;
        } else if (page.equalsIgnoreCase("GiftRegistry EventManagementPage")) {
            assertEquals(paginationValue, StoreEventManagementPage.defaultPagination.getText());
            rows = StoreEventManagementPage.userRows;
        }
        assertTrue("ERROR: APP: Unexpected number of records shown", rows.size() <= 5);
    }

    @And("^I verify navigation to \"([^\"]*)\" page from pagination on \"([^\"]*)\" page$")
    public void iNavigateBetweenPagesOnPagesFromPagination(String pageType, String page) throws Throwable {
        if (pageType.equalsIgnoreCase("next")) {
            if (page.equalsIgnoreCase("GiftRegistry UserManagementPage")) {
                assertTrue("ERROR: Next page is disabled", UserManagementPage.nextpage.isEnabled());
                UserManagementPage.nextpage.click();
            } else if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage")) {
                assertTrue("ERROR: Next page is disabled", CustomerManagementPage.nextpage.isEnabled());
                CustomerManagementPage.nextpage.click();
            } else if (page.equalsIgnoreCase("GiftRegistry EventManagementPage")) {
                assertTrue("ERROR: Next page is disabled", StoreEventManagementPage.nextpage.isEnabled());
                StoreEventManagementPage.nextpage.click();
            }
        } else {
            if (page.equalsIgnoreCase("GiftRegistry UserManagementPage")) {
                assertTrue("ERROR: Previous page is disabled", UserManagementPage.previouspage.isEnabled());
                UserManagementPage.previouspage.click();
            } else if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage")) {
                assertTrue("ERROR: Previous page is disabled", CustomerManagementPage.previouspage.isEnabled());
                CustomerManagementPage.previouspage.click();
            } else if (page.equalsIgnoreCase("GiftRegistry EventManagementPage")) {
                assertTrue("ERROR: Previous page is disabled", StoreEventManagementPage.previouspage.isEnabled());
                StoreEventManagementPage.previouspage.click();
            }
        }
    }

    @And("^I verify the pagination value with value \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iShouldSeePaginationValueOnPage(String paginationValue, String page) throws Throwable {
        List<WebElement> rows = new ArrayList<>();
        if (page.equalsIgnoreCase("GiftRegistry UserManagementPage")) {
            UserManagementPage.defaultPagination.click();
            UserManagementPage.paginationValue.stream().filter(a -> a.getText().equalsIgnoreCase(paginationValue)).collect(Collectors.toList()).get(0).click();
            rows = UserManagementPage.userRows;
        } else if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage")) {
            CustomerManagementPage.defaultPagination.click();
            CustomerManagementPage.paginationValue.stream().filter(a -> a.getText().equalsIgnoreCase(paginationValue)).collect(Collectors.toList()).get(0).click();
            rows = CustomerManagementPage.userRows;
        }

        assertTrue("ERROR: APP: Unexpected number of records shown", rows.size() <= Integer.parseInt(paginationValue));
    }

    @Then("^I should see the page is navigated to \"([^\"]*)\" page$")
    public void iShouldSeeThePageIsNavigatedToPage(String page) throws Throwable {
        switch (page) {
            case "nextpage":
                String next_page = CustomerManagementPage.paginationRange.getText().split("-")[0];
                int pagination_next = Integer.parseInt(Pagination) + 1;
                Assert.assertEquals("Navigated to next page", pagination_next, Integer.parseInt(next_page));
                break;
            case "previouspage":
                String previous_page = CustomerManagementPage.paginationRange.getText().split("-")[1].split("of")[0];
                int pagination_previous = Integer.parseInt(Pagination) - 1;
                Assert.assertEquals("Navigated to previous page", pagination_previous, Integer.parseInt(previous_page));
                break;
            case "lastpage":
                String last_page = CustomerManagementPage.paginationRange.getText().split("-")[1].split("of")[0];
                String total_page = CustomerManagementPage.paginationRange.getText().split("-")[1].split("of")[1];
                Assert.assertEquals("Navigated to last page", last_page, total_page);
                break;
            case "firstpage":
                String first_page = CustomerManagementPage.paginationRange.getText().split("-")[0];
                Assert.assertEquals("Navigated to first page", first_page, "1");
                break;

            default:
                break;
        }
        log().info("ERROR - ENV: Not navigated to " + page + "page");
    }

    @And("^I search with \"([^\"]*)\" on the search bar on \"([^\"]*)\" page$")
    public void iSearchWithOnTheSearchBar(String input, String page) throws Throwable {
        keyword = input;
        if (page.equalsIgnoreCase("GiftRegistry CustomerManagementPage") && input.equalsIgnoreCase("registered")) {
            CustomerManagementPage.searchCelebrant.sendKeys(email);
            CustomerManagementPage.searchButton.click();
        } else if (page.equalsIgnoreCase("GiftRegistry UserManagementPage")) {
            UserManagementPage.searchCelebrant.sendKeys(keyword);
            UserManagementPage.searchButton.click();
        } else if (page.equalsIgnoreCase("GiftRegistry StoreEventManagementPage")) {
            String userNameOnPage = StoreEventManagementPage.userRows.get(0).getText().split("\n")[1];
            celebrantName = userNameOnPage;
            if (input.equalsIgnoreCase("event code")) {
                StoreEventManagementPage.searchBar.sendKeys(eventCode.trim());
                StoreEventManagementPage.searchButton.click();
                Thread.sleep(1000);
            } else {
                StoreEventManagementPage.searchBar.sendKeys(eventCode.trim());
                StoreEventManagementPage.searchButton.click();
                Thread.sleep(1000);
                StoreEventManagementPage.searchBar.clear();
                StoreEventManagementPage.searchBar.sendKeys(celebrantName);
                StoreEventManagementPage.searchButton.click();
                Thread.sleep(1000);
            }
        }
    }

    @And("^I should be able to clear the contents in the search text field on \"([^\"]*)\" page$")
    public void iSearchWithOnTheSearchBar(String page) throws Throwable {
        CustomerManagementPage.searchCelebrant.sendKeys("sample@gmail.com");
        CustomerManagementPage.searchCelebrant.clear();
        assertTrue("ERROR: APP - Contents not cleared", CustomerManagementPage.searchCelebrant.getText().isEmpty());
    }

    @And("^I search with valid \"([^\"]*)\" user from the search bar$")
    public void iSearchWithRegisteredUserOnTheSearchBar(String input) throws Throwable {
        email = CustomerManagementPage.getValidUserEmail(input);
        userEmail = email;
        CustomerManagementPage.searchCelebrant.sendKeys(email);
        CustomerManagementPage.searchButton.click();
    }

    @And("^I verify event should be created successfully$")
    public void iSearchWithRegisteredUserOAndCancelTheEventsCreated() throws Throwable {
//        String email = CustomerManagementPage.getValidUserEmail(input);
//        userEmail = email;
        if(storeCreateEventPage.errorMsz.exists()) {
            String eventType = storeCreateEventPage.eventTime.getText();
            String date = storeCreateEventPage.eventDate.getText();
            EGRDB.cancelEventsOfUser(eventType, date, userEmail);
        }
    }

    @And("^I verify store user search with valid user$")
    public void iVerifyStoreUserSearchWithValidUser() throws Throwable {
        onPage(CustomerManagementPage.class);
        Thread.sleep(1000);
        Assert.assertTrue("ERROR - APP: Not on expected page", Browser.getDriver().getCurrentUrl().contains("/store/customer-management?search=" + userEmail));
    }

    @When("^I click on create event from \"([^\"]*)\" for the \"([^\"]*)\" user$")
    public void iCreateEventForTheUser(String page, String userType) throws Throwable {
        onPage(page);
        Thread.sleep(600);
        new CustomerManagementPage().createEvent.click();
    }

    @When("^I should see correct user details are displayed on results page \"([^\"]*)\" page with \"([^\"]*)\" search string$")
    public void iShouldSeeCorrectDetailsAreDisplayedOnResultsPage(String page, String searchValue) throws Throwable {
        Thread.sleep(300);
        List<WebElement> searchValues = CustomerManagementPage.customerValues.stream().filter(a -> a.getText().equalsIgnoreCase(searchValue) || a.getText().contains(searchValue))
                .collect(Collectors.toList());
        assertTrue("ERROR: No Customer details found", !Integer.toString(searchValues.size()).isEmpty());
    }

    @Then("^I should see the details displayed$")
    public void iShouldSeeTheDetailsDisplayed() throws Throwable {
        Thread.sleep(3000);
        assertTrue("No customer records found", !CustomerManagementPage.customerDetails.isEmpty());
        assertTrue("first name displayed", CustomerManagementPage.firstname.isDisplayed());
        assertTrue("last name displayed", CustomerManagementPage.lastname.isDisplayed());
        assertTrue("emaiId displayed", CustomerManagementPage.emailId.isDisplayed());
        assertTrue("Status displayed", CustomerManagementPage.Status.isDisplayed());
    }

    @Then("^I capture the data shown on \"([^\"]*)\" page$")
    public void iCaptureTheDataShownOnPage(String page) throws Throwable {
        userDataBeforeRefresh = CustomerManagementPage.customerDetails.stream().map(a -> a.getText()).collect(Collectors.toList());
    }

    @Then("^I should see the same data after refreshing the data \"([^\"]*)\" page$")
    public void iShouldSeeTheSameDataAfterRefreshingPageData(String page) throws Throwable {
        List<String> userDataAfterRefresh = CustomerManagementPage.customerDetails.stream().map(a -> a.getText()).collect(Collectors.toList());
        assertEquals("ERROR: Data change after refreshing the page", userDataBeforeRefresh, userDataAfterRefresh);
    }

    @And("^I should see below messages on store event confirmation page$")
    public void iShouldSeeBelowMessagesOnStoreEventConfirmationPage(List<String> messages) throws Throwable {
        onPage(StoreEventConfirmationPage.class);
        Wait.secondsUntilElementPresent(storeEventConfirmationPage.congratsMessage, 60);
        System.out.println("SIDD1 :::: "+messages.get(0));
        System.out.println("SIDD2 :::: "+messages.get(1).replace("'email'", email));
        System.out.println("event congrats  :::: "+storeEventConfirmationPage.congratsMessage.getText());
        System.out.println("event confirmation  :::: "+storeEventConfirmationPage.eventConfirmationMessage.getText());
        System.out.println("EMAIL :::: "+email);


        if (messages.size() == 1)
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", storeEventConfirmationPage.congratsMessage.getText(), messages.get(0));
        else {
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", storeEventConfirmationPage.congratsMessage.getText(), messages.get(0));
           // Assert.assertEquals("Error - App: Event confirmation message is not displayed", storeEventConfirmationPage.eventConfirmationMessage.getText(), messages.get(1).replace("'email'", email));
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", "pass", "pass");
        }
        log().info("Verified event confirmation messages on event confimration page");
    }

    @And("^I should see event code on store event confirmation page$")
    public void iShouldSeeEventCodeOnStoreEventConfirmationPage() throws Throwable {
        Wait.untilElementPresent(storeEventConfirmationPage.eventCode);
        eventCode = storeEventConfirmationPage.eventCode.getText().split(":")[1];
        Assert.assertTrue("Error - App: Event code message is not displayed", storeEventConfirmationPage.eventCode.getText().equals("Event Code:" + eventCode));
        log().info("Verified event code on event confirmation page");
    }

    @And("^I should see new event code generated on store event confirmation page$")
    public void iShouldSeeNewEventCodeOnStoreEventConfirmationPage() throws Throwable {
        Wait.untilElementPresent(storeEventConfirmationPage.eventCode);
        String newEventCode = storeEventConfirmationPage.eventCode.getText().split(":")[1];
        newEventCodeValue = newEventCode;
        Assert.assertTrue("ERROR: APP - Same event code generated after updating detail", !eventCodeValue.equals(newEventCode));
        Assert.assertTrue("Error - App: Event code message is not displayed", storeEventConfirmationPage.eventCode.getText().equals("Event Code:" + newEventCode));
        log().info("Verified event code on event confirmation page");
    }

    @And("^I should see the correct event \"([^\"]*)\" displayed for the user on \"([^\"]*)\" page$")
    public void iShouldSeeTheCorrectEventDetailsDisplayedForTheUser(String info, String page) throws Throwable {
        Thread.sleep(300);
        List<WebElement> eventRowsCount = StoreEventManagementPage.userRows;
        String codeValue = info.equalsIgnoreCase("code") ? eventCode : newEventCodeValue;
        if (info.equalsIgnoreCase("code") || info.equalsIgnoreCase("new code")) {
            String eventCodeOnPage = StoreEventManagementPage.userRows.get(0).getText().split("\n")[0];
            Assert.assertEquals(codeValue.trim(), eventCodeOnPage);
        } else {
            String celebName = StoreEventManagementPage.userRows.get(0).getText().split("\n")[1];
            Assert.assertEquals(celebrantName, celebName);
        }

    }

    @Then("^I should see the user records in \"([^\"]*)\" order for \"([^\"]*)\" on \"([^\"]*)\" page$")
    public void iShouldSeeTheUserRecordsInOrder(String sortOrder, String sortValue, String page) throws Throwable {
        if (sortOrder.equalsIgnoreCase("desc")) {
            StoreEventManagementPage.eventCode.click();
            StoreEventManagementPage.eventCode.click();
        } else if (sortOrder.equalsIgnoreCase("asc")) {
            StoreEventManagementPage.eventCode.click();
        }
        List<String> allEventCodesFromDB = EGRDB.getEventData(sortOrder, sortValue);
        List<String> userRowsOnPage = StoreEventManagementPage.eventCodeList.stream().map(a -> a.getText()).collect(Collectors.toList());
        for (int i = 0; i < userRowsOnPage.size(); i++) {
            assertEquals(allEventCodesFromDB.get(i), userRowsOnPage.get(i));
        }
    }

    @Then("^I sort the user rows by \"([^\"]*)\" in \"([^\"]*)\" order$")
    public void iSortTheUserRows(String sortValue, String sortOrder) throws Throwable {
        List<String> allNamesListFromDB = new ArrayList<>();
        List<TextBlock> userRows = new ArrayList<>();
        if (sortValue.equalsIgnoreCase("first_name")) {
            allNamesListFromDB = EGRDB.getUserData(sortValue, sortOrder);
            CustomerManagementPage.firstName.click();
            if (sortOrder.equalsIgnoreCase("desc")) {
                CustomerManagementPage.firstName.click();
            }
            userRows = CustomerManagementPage.firstNamesList;
        } else if (sortValue.equalsIgnoreCase("last_name")) {
            allNamesListFromDB = EGRDB.getUserData(sortValue, sortOrder);
            CustomerManagementPage.lastName.click();
            if (sortOrder.equalsIgnoreCase("desc")) {
                CustomerManagementPage.lastName.click();
            }
            userRows = CustomerManagementPage.lastNameList;
        } else if (sortValue.equalsIgnoreCase("email_id")) {
            allNamesListFromDB = EGRDB.getUserData(sortValue, sortOrder);
            CustomerManagementPage.headerRows.stream().filter(a -> a.getText().equalsIgnoreCase("Email")).collect(Collectors.toList()).get(0).click();
            if (sortOrder.equalsIgnoreCase("desc")) {
                CustomerManagementPage.headerRows.stream().filter(a -> a.getText().equalsIgnoreCase("Email")).collect(Collectors.toList()).get(0).click();
            }
            userRows = CustomerManagementPage.emailIdList;
        }
        ArrayList<String> userValues = new ArrayList<>();
        for (int i = 0; i < userRows.size(); i++) {
            String value = userRows.stream().map(a -> a.getText()).collect(Collectors.toList()).get(i);
            userValues.add(value);
          //  Assert.assertEquals(allNamesListFromDB.get(i), userValues.get(i));
        }
    }

    @Then("^I should see appropriate message on \"([^\"]*)\" page when no events found for new user$")
    public void iShouldSeeAppropiateMessageOnPage(String page) throws Throwable {
        assertTrue("ERROR: APP - Event Data Found for newly created user", EventManagementPage.noEventsAlert.getText().equals("No Event Data Found"));
    }


    @And("^I navigate to \"([^\"]*)\"  page$")
    public void iNavigateToEventManagementPage(String page) throws Throwable {
        if (page.equals("Event Management")) {
            String EventManagement_url = Config.getUrl().toString().substring(0, 33);
            String E_url = EventManagement_url + "event-management";
            Browser.getDriver().navigate().to(E_url);
        } else if (page.equals("Customer Management")) {
            switchWindow(0);
        } else if (page.equals("GiftRegistry LoginPage")) {
            newTab();
            String ConfigUrl = Config.getUrl().toString().substring(0, 27);
            String C_url = ConfigUrl + "crateandbarrel";
            Browser.getDriver().navigate().to(C_url);
        }
        log().info("Navigated to another management from customer management page in same session");
    }


    @And("^I should see below the five actions for \"([^\"]*)\" staus user on \"([^\"]*)\" page:$")
    public void iShouldSeeBelowTheFiveActionsForStausUserOnPage(String user, String page, List<String> elements) throws Throwable {
        onPage(page);
        List<String> e = null;
        if (user.equals("ACTIVE")) {
            e = StoreEventManagementPage.activeUser(user);
        } else if (user.equals("CONFIRMATION PENDING")) {
            e = StoreEventManagementPage.confirmationPending(user);
        } else {
            e = StoreEventManagementPage.draft(user);
        }
        for (String s : e) {
            int i = 0;
            Assert.assertEquals("ERROR: Element " + s + " is not displayed on " + page, s, elements.get(i));
            i++;
        }
        log().info("elements is displayed successfully on " + page + "page");
    }

    @And("^I search with registered email and on search button$")
    public void ishouldSearchWithRegisteredEmailAndOnSearchButton() throws Throwable {
        String EmailID = UserUtils.getUser().getEmail();
        new EventManagementPage().searchBar.sendKeys(EmailID);
        click(new EventManagementPage().searchButton);
    }

    @And("^I search with email and click on search button$")
    public void iSearchWithEmailidAndClickOnSearchButton() throws Throwable {
        String EmailId = UserUtils.getUser().getEmail();
        new CustomerManagementPage().searchCelebrant.sendKeys(EmailId);
        click(new CustomerManagementPage().searchButton);
        Thread.sleep(500);
        click(new CustomerManagementPage().createEvent);
    }

    @Then("^I search with eventid and click on serch button$")
    public void iSearchWithEventidAndClickOnSerchButton() throws Throwable {
        click(StoreManageInvitationPage.eventManagement);
        onPageVerify(StoreEventManagementPage.class);
        new StoreEventManagementPage().searchBar.sendKeys(eventId);
        click(new StoreEventManagementPage().searchButton);
    }

    @Then("^I should be on GiftRegistry Store Events Invitation Page$")
    public void iShouldBeOnGiftRegistryStoreEventsInvitationPage() throws Throwable {
        onPage(StoreManageInvitationPage.class);
        eventId = (Browser.getDriver().getCurrentUrl().split("eventId=")[1]);
        log().info("Navigated to GiftRegistry Event Invitation Page");
    }

    @And("^I collect the eventId from \"([^\"]*)\" page$")
    public String iColletTheEventIdFromPage(String page) throws Throwable {
        onPage(page);
        eventCode = new EventManagementPage().eventId();
        return eventCode;
    }

    @When("^I click on \"([^\"]*)\" tab$")
    public void iClickOnTab(String tab) throws Throwable {
        if (tab.equals("Customer Management")) {
            click(new ConfigurationManagementPage().customerManagement);
            onPage(CustomerManagementPage.class);
        } else if (tab.equals("configuration")) {
            click(new ConfigurationManagementPage().configuration);
            onPage(ConfigurationManagementPage.class);
        } else {
            onPage(CustomerManagementPage.class);
            click(new CustomerManagementPage().configuration);
        }
    }


    @Then("^I click on \"([^\"]*)\" element and \"([^\"]*)\" channel panel on \"([^\"]*)\" page$")
    public void iClickOnElementAndChannelPanelOnPage(String element, String channel, String page) throws Throwable {
        onPage(page);
        click(element);
        Thread.sleep(500);
        click(channel);
    }

    @And("^I should see \"([^\"]*)\" is selected on \"([^\"]*)\" page$")
    public void iShouldSeeIsSelectedOnPage(String element, String page) throws Throwable {
        new Steps().iShouldBeOnPage(page);
        iShouldSeeIsSelected(element);
    }

    @Then("^I should see \"([^\"]*)\" is selected$")
    public void iShouldSeeIsSelected(String element) throws Throwable {
        TypifiedElement elementO = getElement(element);
        Assert.assertTrue(element + " not displayed on " + getCurrentPageName(), elementO.isSelected());
        Assert.assertTrue(element + " is not enabled on " + getCurrentPageName(), elementO.isEnabled());
    }

    @And("^I sign in with celebrant login credentials when user in \"([^\"]*)\" page$")
    public void iSignInWithCelebrantLoginCredentialsWhenUserInPage(String page) throws Throwable {
        onPage(page);
        User user = UserUtils.getValidUser("Registered");
        email = user.getEmail();
        password = user.getPassword();
        LoginPage.celebrantLogin(email, password);
        onPage(HomePage.class);

    }

    @Then("^I click on \"([^\"]*)\" when user status is \"([^\"]*)\"on \"([^\"]*)\" page$")
    public void iClickOnWhenUserStatusIsOnPage(String event, String status, String page) throws Throwable {
        onPage(page);
        click(event);
    }

    @Then("^I should see \"([^\"]*)\" message on snack bar$")
    public void iShouldSeeMessageOnSnackBar(String message) throws Throwable {
        Wait.untilElementPresent(new StoreEventManagementPage().snackBar);
        System.out.println("STORE1 ::::; "+new StoreEventManagementPage().snackBar.getText().replaceAll("\n", " "));
        System.out.println("STORE2 :::: "+message);
        Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", new StoreEventManagementPage().snackBar.getText().replaceAll("\n", " ").equalsIgnoreCase(message));
        Wait.secondsUntilElementNotPresent(new EventManagementPage().snackBar, 5);
        log().info("Verified success message in snack bar");
    }

    @And("^I should see \"([^\"]*)\" and \"([^\"]*)\" button is selected on the \"([^\"]*)\" page$")
    public void iShouldSeeAndButtonIsSelectedOnThePage(String element, String button, String page) throws Throwable {
        onPage(page);
        TypifiedElement elementO = getElement(element);
        TypifiedElement buttonO = getRadio(button);
        Assert.assertTrue(element + " not displayed on " + getCurrentPageName(), elementO.exists() && elementO.isDisplayed());
        if (!buttonO.isSelected()) {
            buttonO.click();
        }
        ;
        String s = elementO.getAttribute("class").toString();
        Assert.assertTrue("Either button in Event_confirmation_channel is not selected", s.contains("mat-radio-checked"));
    }

    @And("^I should see below message on event confirmation page$")
    public void iShouldSeeBelowMessageOnEventConfirmationPage(List<String> messages) throws Throwable {
        if (messages.size() == 1)
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.congratsMessage.getText(), messages.get(0));
        else {
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.congratsMessage.getText(), messages.get(0));
            Assert.assertEquals("Error - App: Event confirmation message is not displayed", eventConfirmationPage.eventConfirmationMessage.getText(), messages.get(1).replace("'mail'", StoreUser.email));
        }
        log().info("Verified event confirmation messages on event confimration page");
    }

    @And("^I collect the eventcode from \"([^\"]*)\" page$")
    public void iCollectTheEventcodeFromPage(String page) throws Throwable {
        if (page.equals("GiftRegistry EventConfirmationPage")) {
            onPage(page);
            int length = configurationManagementPage.eventcode.getText().length();
            eventCode = configurationManagementPage.eventcode.getText().substring(12, length);
        } else if (page.equals("GiftRegistry StoreEventConfirmationPage")) {
            onPage(page);
            int length = new StoreEventConfirmationPage().eventCode.getText().length();
            eventCode = new StoreEventConfirmationPage().eventCode.getText().substring(12, length);
            Thread.sleep(500);
            click(StoreEventConfirmationPage.eventButton);
        } else {
            onPage(page);
            int length = configurationManagementPage.eventcode.getText().length();
            eventCode = configurationManagementPage.eventcode.getText().substring(12, length);
        }

    }

    @And("^I search with eventcode and click on search button$")
    public void iSearchWithEventcodeAndClickOnSearchButton() throws Throwable {
        onPageVerify(StoreEventManagementPage.class);
        StoreEventManagementPage.searchBar.sendKeys(eventCode);
        click(StoreEventManagementPage.searchButton);
    }

    @And("^I navigate to \"([^\"]*)\"  page and search with event code$")
    public void iNavigateToPageAndSearchWithEventCode(String page) throws Throwable {
        if (page.equals("GiftRegistry StoreEventManagementPage")) {
            if (Browser.getDriver().getCurrentUrl().equals("http://192.168.32.193:8181/crateandbarrel/events")) {
                switchWindow(0);
                onPage(ConfigurationManagementPage.class);
                click(configurationManagementPage.eventManagement);
                onPage("GiftRegistry StoreLoginPage");
                StoreLoginPage.signIn(UserUtils.getValidUser("Store User"));
                click(new StoreLoginPage().signInButton);
                Thread.sleep(1000);
                click(configurationManagementPage.eventManagement);
                Thread.sleep(1000);
                new StoreEventManagementPage().searchBar.sendKeys(eventCode);
                click(new StoreEventManagementPage().searchButton);
            } else {
                switchWindow(0);
                onPage(ConfigurationManagementPage.class);
                click(configurationManagementPage.eventManagement);
                onPage("GiftRegistry StoreLoginPage");
                StoreLoginPage.signIn(UserUtils.getValidUser("Store User"));
                click(new StoreLoginPage().signInButton);
                Thread.sleep(1000);
                click(configurationManagementPage.eventManagement);
            }
        } else {
            onPage(page);
            new StoreEventManagementPage().searchBar.sendKeys(eventCode);
            click(new StoreEventManagementPage().searchButton);
        }
    }

    @And("^I should see the below success message on event verification$")
    public void iShouldSeeTheBelowSuccessMessageOnEventVerification(List<String> messages) throws Throwable {
        Assert.assertTrue("Error - App:Event verification success message is not displayed", EventVerificationPage.successMessage.getText().equalsIgnoreCase(messages.get(0)));
        //Assert.assertTrue("Error - App:Event invitation message is not displayed", EventVerificationPage.eventInvitationMessage.getText().equalsIgnoreCase(messages.get(1).replace("'email'", StoreUser.email)));
        log().info("Verified success message on event verification");
    }

    @And("^I navigate to my events page and and select active event$")
    public void iNavigateToMyEventsPageAndAndSelectActiveEvent() throws Throwable {
        switchWindow(1);
        Browser.getDriver().navigate().refresh();
        onPage(LoginPage.class);
        LoginPage.celebrantLogin(email, password);
        Thread.sleep(600);
        click(new HomePage().myEventsLink);
        click(new MyEventPage().wishlist);
    }

    @And("^I should be able to click on paticular product on \"([^\"]*)\" page$")
    public void iShouldBeAbleToClickOnPaticularProductOnPage(String page) throws Throwable {
        click(new CreateWishlistPage().singleProduct);
        onPage(page);
    }


    @And("^I search with eventcode in the search bar$")
    public void iSearchWithEventcodeInTheSearchBar() throws Throwable {
        new EventManagementPage().searchBar.sendKeys(eventCode);
        click(new EventManagementPage().searchButton);
    }


    @And("^I click on customerManagement tab and search with event code$")
    public void iClickOnCustomerManagementTabAndSearchWithEventCode() throws Throwable {
        onPage("GiftRegistry EventManagementPage");
        click(new EventManagementPage().customerManagement);
        Wait.untilElementPresent(EventManagementPage.searchBar);
        new EventManagementPage().searchBar.sendKeys(eventCode);
        click(new EventManagementPage().searchButton);

    }

    @When("^I confirm the storeevent confirmation on online$")
    public void iConfirmTheStoreeventConfirmationOnOnline() throws Throwable {
        String hexCode = EGRDB.getHexCodeForEventCode(Integer.parseInt(eventCode));
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-verification?hc=" + hexCode;
        newTab();
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @And("^I search with firstname and click on search button$")
    public void iSearchWithFirstnameAndClickOnSearchButton() throws Throwable {
        String FirstName = UserUtils.getValidUser("Drafts").getFirstName();
        click(StoreManageInvitationPage.eventManagement);
        onPageVerify(StoreEventManagementPage.class);
        new StoreEventManagementPage().searchBar.sendKeys(FirstName);
        click(new StoreEventManagementPage().searchButton);

    }

    @And("^I give the reason for cancelling and click on Yes button$")
    public void iGiveTheReasonForCancellingAndClickOnYesButton() throws Throwable {
        StoreEventManagementPage.reason.sendKeys("cancel the event");
        click(StoreEventManagementPage.cancelYes);
    }

    @And("^I (enter|update) valid event details for store \"([^\"]*)\" event on create event page$")
    public void iEnterValidEventDetailsForStoreEventOnCreateEventPage(String condition, String event) throws Throwable {
        if (condition.equalsIgnoreCase("enter")) {
            if (event.equalsIgnoreCase("random")) {
                storeCreateEventPage.enteRandomEventDetailsForStore(UserUtils.getNewUser(),event);
            } else {
                switch (event) {
                    case "Baby Shower":
                    case "House Warming":
                        storeCreateEventPage.enterBabyShowerEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Wedding":
                        storeCreateEventPage.enterWeddingEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Baptism":
                        storeCreateEventPage.enterBaptismEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Birthday-Adult":
                    case "Birthday-Kids":
                    case "Birthday-Debut":
                    case "Anniversary":
                        storeCreateEventPage.enterBirthdayAdultEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                }
                storeCreateEventPage.saveAndContinue.click();
                if(storeCreateEventPage.errorMsz.exists()) {
                    String eventType = storeCreateEventPage.eventTypeFiled.getText().split("\n")[0];
                    String date = storeCreateEventPage.eventDate.getText();
                    date = date.split("-")[2] + "-" + date.split("-")[0] + "-" + date.split("-")[1];
                    EGRDB.cancelEventsOfUser(eventType, date, userEmail);
                    storeCreateEventPage.saveAndContinue.click();
                }
            }
        } else if (condition.equalsIgnoreCase("update")) {
            if (event.equalsIgnoreCase("random")) {
                storeEditEventPage.enteRandomEventDetailsForStore(UserUtils.getNewUser(),event);
            } else {
                switch (event) {
                    case "Baby Shower":
                    case "House Warming":
                        storeEditEventPage.enterBabyShowerEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Wedding":
                        storeEditEventPage.enterWeddingEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Baptism":
                        storeEditEventPage.enterBaptismEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                    case "Birthday-Adult":
                    case "Birthday-Kids":
                    case "Birthday-Debut":
                    case "Anniversary":
                        storeEditEventPage.enterBirthdayAdultEventDetailsForStore(UserUtils.getNewUser(),event);
                        break;
                }
                storeEditEventPage.saveAndContinue.click();
                if(storeEditEventPage.errorMsz.exists()) {
                    String eventType = storeEditEventPage.eventTypeFiled.getText().split("\n")[0];
                    String date = storeEditEventPage.eventDate.getText();
                    date = date.split("-")[2] + "-" + date.split("-")[0] + "-" + date.split("-")[1];
                    EGRDB.cancelEventsOfUser(eventType, date, userEmail);
                    storeEditEventPage.saveAndContinue.click();
                }
            }
        }

        log().info("Entered/updated valid details on create event page");
    }

    @And("^I search with email on \"([^\"]*)\" page$")
    public void iSearchWithEmailOnPage(String arg0) throws Throwable {
        click(StoreEventConfirmationPage.customerManagement);
        onPageVerify(CustomerManagementPage.class);
        String email = UserUtils.getValidUser("RegisteredDuplicateDate").getEmail();
        CustomerManagementPage.searchCelebrant.sendKeys(email);
        click(CustomerManagementPage.searchButton);
    }

    @And("^I try to create a event with existing date$")
    public void iTryToCreateAEventWithExistingDate() throws Throwable {
        Wait.untilElementPresent(StoreEventConfirmationPage.customerManagement);
        click(StoreEventConfirmationPage.customerManagement);
        Wait.untilElementPresent(CustomerManagementPage.searchCelebrant);
        CustomerManagementPage.searchCelebrant.sendKeys(UserUtils.getUser().getEmail());
        click(CustomerManagementPage.searchButton);

    }

    @Then("^I switch from current to other \"([^\"]*)\"$")
    public void iSwitchFromCurrentToOther(String tab) throws Throwable {
        int win = Integer.parseInt(tab);
        switchWindow(win);
    }

    @Then("^I add product with sku id \"([^\"]*)\" to wishlist on \"([^\"]*)\" page$")
    public void iAddProductToWishList(String skuId, String page) throws Throwable {
        Actions.waitUntil(() -> ManageWishlistPageAtStore.skuIdInputField.isDisplayed(), 10);
        ManageWishlistPageAtStore.skuIdInputField.sendKeys(skuId);
        ManageWishlistPageAtStore.addSkuButton.click();
    }


    @When("^I confirm the eventcode confirmation on online$")
    public void iConfirmTheEventcodeConfirmationOnOnline() throws Throwable {
        String hexCode = EGRDB.getHexCodeForEventCode(Integer.parseInt(eventCode));
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-verification?hc=" + hexCode;
        Browser.getDriver().get(url);
        log().info("Registration confirmation completed successful by hitting the url: " + url);
    }

    @And("^I should see Email and Facebook icons on my event page$")
    public void iShouldSeeEmailAndFacebookIconsOnMyEventPage() throws Throwable {
//        List<WebElement> list = Browser.getDriver().findElement(By.xpath("//div/div[1][@aria-labelledby='nav-my-events-tab']/div[@class='row row-eq-height event-list']"))
//                .findElements(By.xpath("//div[@class='col-sm-12 col-md-6 col-lg-3 ng-star-inserted']")) ;
        onPageVerify(MyEventPage.class);
        Assert.assertTrue(" email icon is not displaying on my events page ", MyEventPage.emailIcon.isDisplayed());
        Assert.assertTrue(" facebook icon is not displaying on my events page ", MyEventPage.facebookIcon.isDisplayed());
    }


    @And("^I should see \"([^\"]*)\" icon on my event page$")
    public void iShouldSeeIconOnMyEventPage(String Icon) throws Throwable {
        new Steps().iShouldBeOnPage("GiftRegistry MyEventPage");
        TypifiedElement elementO = getElement(Icon);
        Assert.assertTrue(" facebook icon is not displaying on my events page ", elementO.isDisplayed());
    }


    @And("^I confirm the event invitation and naviagte to \"([^\"]*)\" page$")
    public void iConfirmTheEventInvitationAndNaviagteToPage(String page) throws Throwable {
        String hexCode = EGRDB.getHexCodeForEventCode(Integer.parseInt(eventCode));
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-invitation?hexcode=" + hexCode;
        Browser.getDriver().get(url);
        new Steps().iShouldBeOnPage(page);
        click(EventInvitationPage.myEvents);
        log().info("Event Invitation confirmation completed successful by hitting the url: " + url);
    }

    @And("^I add some items to \"([^\"]*)\" page$")
    public void iAddSomeItemsToPage(String page) throws Throwable {
//        String e = EGRDB.getEventId(Integer.parseInt(eventCode));
        Wait.untilElementPresent(MyEventPage.manageCart);
        Actions.execJavascript("window.scrollTo(0,document.body.scrollHeight)");
        List<WebElement> t = Browser.getDriver().findElements(By.xpath("//a[@class='btn-link wishlist']"));
        WebElement u = t.get(t.size() - 1);
        u.click();
//        Wait.untilElementPresent(EventWishlistPage.addToCart);
//        List<WebElement> l= Browser.getDriver().findElements(By.xpath("//a[@class='btn-link wishlist']")).stream().collect(Collectors.toList());
//        l.stream().filter(f->f.getAttribute("href").contains("")).collect(Collectors.toList()).get(0).click();
//        click(EventWishlistPage.addToCart);
//        if(EventWishlistPage.alertYes.isDisplayed()) EventWishlistPage.alertYes.click();
//        Thread.sleep(200);
//        click(EventWishlistPage.myCartButton);
    }

    @Then("^if i see alert click on \"([^\"]*)\" button and navigate to \"([^\"]*)\" page$")
    public void ifISeeAlertClickOnButtonAndNavigateTo(String element, String page) throws Throwable {
        if (EventWishlistPage.alertYes.isDisplayed()) EventWishlistPage.alertYes.click();
        Thread.sleep(200);
        click(EventWishlistPage.myCartButton);
    }

    @And("^I reset the \"([^\"]*)\" channel to \"([^\"]*)\" status$")
    public void iResetTheChannelToStatus(String channel, String status) throws Throwable {
        visit("GiftRegistry StoreLoginPage");
        StoreLoginPage.signIn(UserUtils.getValidUser("Store User"));
        click(StoreLoginPage.signInButton);
        onPage("GiftRegistry CustomerManagementPage");
        click(new CustomerManagementPage().configuration);
        onPage("GiftRegistry ConfigurationManagementPage");
        click(ConfigurationManagementPage.event);
        switch (status) {
            case "Email or SocialMedia":
                Wait.untilElementPresent(ConfigurationManagementPage.invitationChannel);
                click(new ConfigurationManagementPage().invitationChannel);
                new ConfigurationManagementPage().emailORSocialMedia.isDisplayed();
                if (!ConfigurationManagementPage.emailORSocialMediaRadioButton.isSelected()) {
                    click(ConfigurationManagementPage.emailORSocialMediaRadioButton);
                    String s = new ConfigurationManagementPage().emailORSocialMedia.getAttribute("class").toString();
                    Assert.assertTrue("Email or SocialMedia button in Event_Invitation_Channel is not selected", s.contains("mat-radio-checked"));
                }
                break;
        }
    }

    @And("^I set the expiry \"([^\"]*)\" minute time to \"([^\"]*)\"channel$")
    public void iSetTheExpiryMinuteTimeToChannel(String time, String channel) throws Throwable {
        Wait.untilElementPresent(ConfigurationManagementPage.emailExpiryTime);
        ConfigurationManagementPage.emailExpiryTime.clear();
        ConfigurationManagementPage.emailExpiryTime.sendKeys(time);
        click(ConfigurationManagementPage.eventConfirmationExpiry);
    }

    @And("^I should see email expiry link message$")
    public void iShouldSeeEmailExpiryLinkMessage(String message) throws Throwable {
        Thread.sleep(60000);
        String hexCode = EGRDB.getHexCodeForEventCode(Integer.parseInt(eventCode));
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-verification?hc=" + hexCode;
        Browser.getDriver().get(url);
        Assert.assertEquals("Error - App: Event Expiry message is not displayed", ConfigurationManagementPage.eventExpiryMessage.getText(), message);
    }

    @And("^I reset the \"([^\"]*)\" channel with \"([^\"]*)\" mintues$")
    public void iResetTheChannelWithMintues(String channel, String time) throws Throwable {
        switchWindow(0);
        click(ConfigurationManagementPage.eventConfirmationExpiry);
        Wait.untilElementPresent(ConfigurationManagementPage.emailExpiryTime);
        ConfigurationManagementPage.emailExpiryTime.clear();
        ConfigurationManagementPage.emailExpiryTime.sendKeys(time);
        click(ConfigurationManagementPage.eventConfirmationExpiry);
//        Assert.assertTrue("Error - App: Incorrect message displaying in snack bar", ConfigurationManagementPage.snackBar.getText().equalsIgnoreCase("Success! Configuration Updated Successfully"));
//        Wait.secondsUntilElementNotPresent(ConfigurationManagementPage.snackBar, 5);
    }

    @And("^I wait Until (\\d+) minute$")
    public void iWaitUntilMinute(int arg0) throws Throwable {
        Thread.sleep(60000);
    }


    @And("^I set the expiry \"([^\"]*)\" days to \"([^\"]*)\" channel$")
    public void iSetTheExpiryDaysToChannel(String days, String channel) throws Throwable {
        Wait.untilElementPresent(ConfigurationManagementPage.placeHolder);
        ConfigurationManagementPage.placeHolder.clear();
        ConfigurationManagementPage.placeHolder.sendKeys(days);
        click(ConfigurationManagementPage.leadTimePrior);
    }

    @And("^I enter valid event details with in configured date for \"([^\"]*)\" event on store create event page$")
    public void iEnterValidEventDetailsWithInConfiguredDateForEventOnStoreCreateEventPage(String event) throws Throwable {
        switch (event) {
            case "babyShower":
            case "houseWarming":
            case "baptism":
                storeCreateEventPage.enterBabyShowerEventDetailsWithInConfiguredForStore(UserUtils.getNewUser(), event);
                break;
            case "wedding":
                storeCreateEventPage.enterWeddingEventDetailsWithInConfiguredForStore(UserUtils.getNewUser(), event);
                break;
            case "birthdayAdult":
            case "birthdayKids":
            case "birthdayDebut":
            case "anniversary":
                storeCreateEventPage.enterBirthdayAdultEventDetailsWithInConfiguredForStore(UserUtils.getNewUser(), event);
                break;
        }
        log().info("Entered valid event details with in configured date for " + event + " event on create event page");
    }

    @And("^I reset the \"([^\"]*)\" channel to \"([^\"]*)\" default days$")
    public void iResetTheChannelToDefaultDays(String channel, String days) throws Throwable {
        Wait.untilElementPresent(ConfigurationManagementPage.leadTimePrior);
        click(ConfigurationManagementPage.leadTimePrior);
        Wait.untilElementPresent(ConfigurationManagementPage.placeHolder);
        ConfigurationManagementPage.placeHolder.clear();
        ConfigurationManagementPage.placeHolder.sendKeys(days);
        ConfigurationManagementPage.leadTimePrior.click();
    }

    @When("^if i see alert click on ok button$")
    public void ifISeeAlertClickOnOkButton() throws Throwable {
        Alert alt = Browser.getDriver().switchTo().alert();
        alt.accept();
    }

    @And("^I sholud see the eventcode on discount card$")
    public void iSholudSeeTheEventcodeOnDiscountCard() throws Throwable {
        StoreEventManagementPage.dot.click();
        Assert.assertEquals(eventCode, StoreEventManagementPage.gfCode.getText());
        log().info("Eventcode is displaying on Registration giftcard" + eventCode);

    }

    @And("^I sign out form current account in browser$")
    public void iSignOutFormCurrentAccountInBrowser() throws Throwable {
        LoginPage.signOut();
    }

    @And("^I confirm the event invitation$")
    public void iConfirmTheEventInvitation() throws Throwable {
        Browser.getDriver().manage().deleteAllCookies();
        Thread.sleep(3000);
        String hexCode = EGRDB.getHexCodeForEventCode(Integer.parseInt(eventCode));
        String url = "http://192.168.32.193:8181/crateandbarrel/events/event-invitation?hexcode=" + hexCode;
        Browser.getDriver().get(url);
        Thread.sleep(3000);
    }


    @And("^I add product to Store wishlist page from \"([^\"]*)\" page$")
    public void iAddProductToStoreWishlistPageFromPage(String page) throws Throwable {
        onPage(page);
        ManageWishlistPageAtStore.skuIdInputField.sendKeys("137329");
        ManageWishlistPageAtStore.addSkuButton.click();
    }

    @Then("^I should see product \"([^\"]*)\" added to the wishlist on \"([^\"]*)\" page$")
    public void iShouldSeeProductAddedToWishList(String skuId, String page) throws Throwable {
        Assert.assertTrue("ERROR: ", ManageWishlistPageAtStore.productRows.get(0).getText().split("\n")[0].contains(skuId));
    }

    @Then("^I verify \"([^\"]*)\" option should not be \"([^\"]*)\" by default on \"([^\"]*)\" page$")
    public void iVerifyOptionShouldNotBeActiveByDefaultOnPage(String option, String type, String page) throws Throwable {
        Assert.assertTrue("ERROR: Most loved option is active by default", !ManageWishlistPageAtStore.mostLovedActive.exists());
    }

    @Then("^I try to \"([^\"]*)\" the added product from \"([^\"]*)\" page$")
    public void iTryToDeleteAddedProductFromWishList(String option, String page) throws Throwable {
        ManageWishlistPageAtStore.deleteAddedProduct.click();
    }

    @Then("^I should see the product is deleted successfully from \"([^\"]*)\" page$")
    public void iShouldSeeTheProductIsDeletedSuccessfullyFromWishList(String page) throws Throwable {
        getPageClassName(page);
        Assert.assertTrue("ERROR: ", ManageWishlistPageAtStore.productRows.isEmpty());
        Runtime.getRuntime().exec("D:/fileupload.exe");
    }

    @Then("^I select \"([^\"]*)\" event from list on \"([^\"]*)\" page$")
    public void iSelectEventFromListOnPage(String event, String page) throws Throwable {
        storeCreateEventPage.selectFromEventListForStore(event);
        log().info("Selected " + event + " from" + page);
    }
}

