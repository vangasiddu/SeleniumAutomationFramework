package test.automation.framework;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.URL;
import java.util.*;
import java.util.logging.Level;

import static test.automation.framework.Config.*;
import static test.automation.framework.Runner.log;

public final class Browser {

    private static WebDriver driver;

    private static void start() {

        try {
            if (getRemoteUrl() != null) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                if (getBrowser() != null) {
                    capabilities.setCapability(CapabilityType.BROWSER_NAME, getBrowser());
                }
                if (getBrowserVersion() != null) {
                    capabilities.setCapability(CapabilityType.BROWSER_VERSION, getBrowserVersion());
                    capabilities.setCapability(CapabilityType.VERSION, getBrowserVersion());
                }
                if (getPlatform() != null) {
                    capabilities.setCapability(CapabilityType.PLATFORM_NAME, getPlatform());
                    capabilities.setCapability("platform", getPlatform());
                }
                if (getPlatformVersion() != null) {
                    capabilities.setCapability("platformVersion", getPlatformVersion());
                }
                if (getDevice() != null) {
                    capabilities.setCapability("deviceName", getDevice());
                    if (getAppiumVersion() != null) {
                        capabilities.setCapability("appiumVersion", getAppiumVersion());
                    }
                    if (getPlatform() != null && getPlatform().toLowerCase().contains("ios")) {
                        driver = new IOSDriver(new URL(getRemoteUrl()), capabilities);
                    } else {
                        driver = new AndroidDriver(new URL(getRemoteUrl()), capabilities);
                    }
                    log().info(getDevice() + " started in cloud.");
                    return;
                }
                driver = new RemoteWebDriver(new URL(getRemoteUrl()), capabilities);
                log().info(getBrowser() + " started in cloud.");
                return;
            }

            if (getDevice() != null) {
                Map<String, String> emulationOptions = new HashMap<>();
                emulationOptions.put("deviceName", getDevice());
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", emulationOptions);
                chromeOptions.addArguments("--disable-extensions");
                chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                System.setProperty("webdriver.chrome.driver", getChromeDriver());
                driver = new ChromeDriver(chromeOptions);
                log().info(getDevice() + "Chrome Emulation started.");
                return;
            }

            switch (getBrowser().toLowerCase()) {
                case "firefox":
                    FirefoxOptions co = new FirefoxOptions();
                    co.setCapability("acceptInsecureCerts", true);
                    System.setProperty("webdriver.gecko.driver", getFirefoxDriver());
                    driver = new FirefoxDriver(co);
                    break;
                case "safari":
                    driver = new SafariDriver();
                    break;
                case "edge":
                    driver = new EdgeDriver();
                    break;
                case "chrome_headless":
                    ChromeOptions headlessOptions = new ChromeOptions();
                    headlessOptions.addArguments("--headless");
                    System.setProperty("webdriver.chrome.driver", getChromeDriver());
                    driver = new ChromeDriver(headlessOptions);
                    break;
                case "ie":
                    InternetExplorerOptions io = new InternetExplorerOptions();
                    io.setCapability("acceptInsecureCerts", true);
                    io.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                    io.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
                    io.setCapability("ignoreZoomSetting", true);
                    io.setCapability("ignoreProtectedModeSettings", true);
                    io.setCapability("initialBrowserUrl",Config.getUrl());
                    System.setProperty("webdriver.ie.driver", getIEDriver());
                    driver = new InternetExplorerDriver(io);
                    break;
                case "chrome":
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.addArguments("--test-type");
                    chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                    System.setProperty("webdriver.chrome.driver", getChromeDriver());
                    driver = new ChromeDriver(chromeOptions);
            }
            log().info(getBrowser() + " browser started.");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void maximize() {
        getDriver().manage().window().maximize();
    }

    public static boolean isStarted() {
        return driver != null;
    }

    public static void reStart() {
        if (isStarted()) {
            quit();
        }
        start();
        maximize();
        log().info(getBrowser() + " browser re-started.");
    }

    public static void quit() {
        if (isStarted()) {
         driver.quit();
         driver = null;
        }
        log().info(getBrowser() + " browser closed.");
    }

    public static WebDriver getDriver() {
        if (!isStarted()) {
            start();
            maximize();
        }
        return driver;
    }

    public static AndroidDriver getAndroidDriver() {
        if (getDriver() instanceof AndroidDriver) {
            return (AndroidDriver) getDriver();
        }
        return null;
    }

    public static IOSDriver getIOSDriver() {
        if (getDriver() instanceof IOSDriver) {
            return (IOSDriver) getDriver();
        }
        return null;
    }

    public static byte[] getScreenShot() {
        try {
            return ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (Exception e) {
            log().log(Level.WARNING, e.getMessage());
            return null;
        }
    }

    public static boolean isAlertPresent() {
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        try {
            Browser.getDriver().switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }
}
