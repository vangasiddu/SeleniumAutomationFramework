package test.automation.framework;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.*;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static test.automation.framework.Actions.*;
import static test.automation.framework.Runner.log;

public abstract class Page {

    private static Page currentPage;

    public Page() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(Browser.getDriver())), this);
        currentPage = this;
        log().info("Page Initialized: " + currentPage.getClass().getName());
    }

    public static final Page getCurrentPage() {
        return currentPage;
    }
    
    public static final String getCurrentPageName() {
        return currentPage.getClass().getSimpleName() + " page";
    }

    public static final String getPageClassName(String pageClassName) {
        return Page.class.getName().replace("framework.Page", "pages." + pageClassName.replace(" ", "."));
    }

    public static final void verifyPage(String pageClassName) {
        pageClassName = getPageClassName(pageClassName);
        try {
            verifyPage(Class.forName(pageClassName));
        } catch (ClassNotFoundException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(pageClassName + " not found!");
        }
    }

    public static final void verifyPage(Class pageClass) {
        if (!pageClass.getSuperclass().getSimpleName().equals("Page")) {
            throw new RuntimeException("Invalid page " + pageClass.getSimpleName());
        }
        String url = null;
        try {
            url = (String) pageClass.getDeclaredField("URL").get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
        }
        if (url != null) {
            String finalUrl = url.replace(Config.getUrl(),"");
            try {
                waitUntil(() -> Browser.getDriver().getCurrentUrl().contains(finalUrl));
            } catch (TimeoutException e) {
                log().log(Level.WARNING, e.getMessage());
                throw new RuntimeException("Not on page " + pageClass.getSimpleName() + " (" + url + ")");
            }
        }

        try {
            waitUntil(Actions::isPageLoaded);
        } catch (TimeoutException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(pageClass.getSimpleName() + "page (" + url + ") is taking too long to load." );
        }

        By verifyBy = null;
        try {
            verifyBy = (By) pageClass.getDeclaredField("VERIFY_BY").get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
        }
        if (verifyBy != null) {
            try {
                new WebDriverWait(Browser.getDriver(), 30).until(ExpectedConditions.visibilityOfElementLocated(verifyBy));
            } catch (TimeoutException e) {
                log().log(Level.WARNING, e.getMessage());
                throw new RuntimeException(pageClass.getSimpleName() + "page VERIFY_BY(" + verifyBy.toString() + ") is not displayed.");
            }
        }
        log().info("On Page: " + pageClass.getSimpleName());
    }

    public static final Page onPage(String pageClassName) {
        pageClassName = getPageClassName(pageClassName);
        try {
            return onPage(Class.forName(pageClassName));
        } catch (ClassNotFoundException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(pageClassName + " not found!");
        }
    }

    public static final Page onPage(Class pageClass) {
        verifyPage(pageClass);
        try {
            pageClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException("Instantiation failed for " + pageClass.getName());
        }
        return getCurrentPage();
    }

    public static final Page visit(String pageClassName) {
        pageClassName = getPageClassName(pageClassName);
        try {
            return visit(Class.forName(pageClassName));
        } catch (ClassNotFoundException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(pageClassName + " not found!");
        }
    }

    public static final boolean onPageVerify(Class pageClassName){
        verifyPage(pageClassName);
        try {
            pageClassName.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException("Instantiation failed for " + pageClassName.getName());
        }
        return getCurrentPage().equals(pageClassName);
    }

    public static final Page visit(Class pageClass) {
        try {
            Browser.getDriver().navigate().to((String) pageClass.getDeclaredField("URL").get(null));
        } catch (NoSuchFieldException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException("URL not found for " + pageClass.getSimpleName());
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(pageClass.getName() + " is not accessible.");
        }
        return onPage(pageClass);
    }

    public static final WebElement getWebElement(String element) {
        if (getCurrentPage() == null) {
            throw new RuntimeException("Not on valid page!");
        }
        String pageClass = getCurrentPageName();
        try {
            return ((WebElement) getCurrentPage().getClass().getDeclaredField(element).get(getCurrentPage()));
        } catch (NoSuchFieldException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not declared in " + pageClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not accessible from " + pageClass);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not an element in " + pageClass);
        }
    }

    public static final List<WebElement> getWebElements(String element) {
        if (getCurrentPage() == null) {
            throw new RuntimeException("Not on valid page!");
        }
        String pageClass = getCurrentPageName();
        try {
            return ((List<WebElement>) getCurrentPage().getClass().getDeclaredField(element).get(getCurrentPage()));
        } catch (NoSuchFieldException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not declared in " + pageClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not accessible from "+ pageClass);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not an element list in " + pageClass);
        }
    }

    public static final TypifiedElement getElement(String element) {
        try {
            return ((TypifiedElement) getWebElement(element));
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not an element in " + getCurrentPageName());
        }
    }

    public static final CheckBox getCheckBox(String checkBox) {
        try {
            return (CheckBox) getElement(checkBox);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(checkBox + " is not an check box in " + getCurrentPageName());
        }
    }

    public static final Form getForm(String form) {
        try {
            return (Form) getElement(form);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(form + " is not an form in " + getCurrentPageName());
        }
    }

    public static final Image getImage(String image) {
        try {
            return (Image) getElement(image);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(image + " is not an image in " + getCurrentPageName());
        }
    }

    public static final Radio getRadio(String radio) {
        try {
            return (Radio) getElement(radio);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(radio + " is not an radio in " + getCurrentPageName());
        }
    }

    public static final Select getSelect(String select) {
        try {
            return (Select) getElement(select);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(select + " is not an select in " + getCurrentPageName());
        }
    }

    public static final Table getTable(String table) {
        try {
            return (Table) getElement(table);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(table + " is not an table in " + getCurrentPageName());
        }
    }

    public static final TextInput getTextInput(String textInput) {
        try {
            return (TextInput) getElement(textInput);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(textInput + " is not an text input in " + getCurrentPageName());
        }
    }

    public static final FileInput getFileInput(String fileInput) {
        try {
            return (FileInput) getElement(fileInput);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(fileInput + " is not an file input in " + getCurrentPageName());
        }
    }

    public static final HtmlElement getPanel(String panel) {
        try {
            return ((HtmlElement) getWebElement(panel));
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(panel + " is not an panel in " + getCurrentPageName());
        }
    }

    public static final WebElement getWebElement(String element, String panel) {
        HtmlElement panelE = getPanel(panel);
        String panelClass = panelE.getClass().getSimpleName();
        try {
            return ((WebElement) panelE.getClass().getDeclaredField(element).get(panelE));
        } catch (NoSuchFieldException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not declared in " + panelClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not accessible from " + panelClass);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not an element in " + panelClass);
        }
    }

    public static final List<WebElement> getWebElements(String element, String panel) {
        HtmlElement panelE = getPanel(panel);
        String panelClass = panelE.getClass().getSimpleName();
        try {
            return ((List<WebElement>) panelE.getClass().getDeclaredField(element).get(panelE));
        } catch (NoSuchFieldException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not declared in " + panelClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not accessible from " + panelClass);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not a element list in " + panelClass);
        }
    }

    public static final TypifiedElement getElement(String element, String panel) {
        try {
            return ((TypifiedElement) getWebElement(element, panel));
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(element + " is not an element in " + panel);
        }
    }

    public static final CheckBox getCheckBox(String checkBox, String panel) {
        try {
            return (CheckBox) getElement(checkBox, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(checkBox + " is not an check box in " + panel);
        }
    }

    public static final Form getForm(String form, String panel) {
        try {
            return (Form) getElement(form, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(form + " is not an form in " + panel);
        }
    }

    public static final Image getImage(String image, String panel) {
        try {
            return (Image) getElement(image, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(image + " is not an image in " + panel);
        }
    }

    public static final Radio getRadio(String radio, String panel) {
        try {
            return (Radio) getElement(radio, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(radio + " is not an radio in " + panel);
        }
    }

    public static final Select getSelect(String select, String panel) {
        try {
            return (Select) getElement(select, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(select + " is not an select in " + panel);
        }
    }

    public static final Table getTable(String table, String panel) {
        try {
            return (Table) getElement(table, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(table + " is not an table in " + panel);
        }
    }

    public static final TextInput getTextInput(String textInput, String panel) {
        try {
            return (TextInput) getElement(textInput, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(textInput + " is not an text input in " + panel);
        }
    }

    public static final FileInput getFileInput(String fileInput, String panel) {
        try {
            return (FileInput) getElement(fileInput, panel);
        } catch (ClassCastException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(fileInput + " is not an file input in " + panel);
        }
    }

    public static final Object invokeMethod(String method, Object ... args) {
        if (getCurrentPage() == null) {
            throw new RuntimeException("Not on valid page!");
        }
        List<Class<?>> argsClass = Arrays.stream(args).map(Object::getClass).collect(Collectors.toList());
        String methodSignature = method + argsClass.stream().map(Class::getSimpleName).collect(Collectors.toList()).toString().replace("[", "(").replace("]", ")");
        String pageClass = getCurrentPageName();
        try {
            return getCurrentPage().getClass().getDeclaredMethod(method, argsClass.toArray(new Class<?>[args.length])).invoke(getCurrentPage(), args);
        } catch (NoSuchMethodException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(methodSignature + " is not declared in " + pageClass);
        } catch (IllegalArgumentException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException("Invalid arguments for " + methodSignature + " of " + pageClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(methodSignature + " is not accessible from " + pageClass);
        } catch (InvocationTargetException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(e.getCause());
        }
    }

    public static final Object invokePanelMethod(String method, String panel, Object ... args) {
        HtmlElement panelE = getPanel(panel);
        String panelClass = panelE.getClass().getSimpleName();
        List<Class<?>> argsClass = Arrays.stream(args).map(Object::getClass).collect(Collectors.toList());
        String methodSignature = method + argsClass.stream().map(Class::getSimpleName).collect(Collectors.toList()).toString().replace("[", "(").replace("]", ")");
        try {
            return panelE.getClass().getDeclaredMethod(method, argsClass.toArray(new Class<?>[args.length])).invoke(panelE, args);
        } catch (NoSuchMethodException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(methodSignature + " is not declared in " + panelClass);
        } catch (IllegalArgumentException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException("Invalid arguments for " + methodSignature + " of " + panelClass);
        } catch (IllegalAccessException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(methodSignature + " is not accessible from " + panelClass);
        } catch (InvocationTargetException e) {
            log().log(Level.WARNING, e.getMessage());
            throw new RuntimeException(e.getCause());
        }
    }

    public static WebDriver switchWindow(int index) {
        ArrayList<String> newTab = new ArrayList<>(Browser.getDriver().getWindowHandles());
        if (newTab.size() > index) {
            Browser.getDriver().switchTo().window(newTab.get(index));
        }
        return Browser.getDriver();
    }

    public static int windowSize() {
        return Browser.getDriver().getWindowHandles().size();
    }

    public static void newTab(){
        JavascriptExecutor jse = (JavascriptExecutor) Browser.getDriver();
        jse.executeScript("$(window.open())");
        switchWindow(windowSize()-1);
    }

    public static void closeCurrentTab(){
        Browser.getDriver().close();
    }
}
