package test.automation.framework;

import jdk.nashorn.internal.runtime.regexp.joni.Config;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Util {

    public static int getRandomIntBetween(int min, int max) {
        return new Random().nextInt(max + 1 - min) + min;
    }

    public static int getRandomIndex(int size) {
        return getRandomIntBetween(0, size -1);
    }

    public static String getRandom(int size, String input) {
        StringBuilder sb = new StringBuilder();
        while (sb.length() < size)
            sb.append(input.charAt(getRandomIndex(input.length())));
        return sb.toString();
    }

    public static String generateRandomFirstName() {
        String[] firstNames = {"A", "one", "SUSHMA", "SWEETY", "NAGARJUNA", "SATISH", "RENUKA", "SANNIHITA", "SARAT", "CHARLES",
                "JOSEPH", "THOMAS", "CHRISTOPHER"};
        return firstNames[new Random().nextInt(firstNames.length)];
    }

    public static String generateRandomLastName() {
        String[] lastNames = {"SANAPA", "BROWN", "JONES", "MILLER", "GARCIA", "RODRIGUEZ", "ANDERSON",
                "TAYLOR", "THOMAS", "MOORE"};
        return lastNames[new Random().nextInt(lastNames.length)];
    }

    public static String generateRandomEventVenue() {
        String[] firstNames = {"Metro Davao", "Carcar", "Cebu City", "Compostela", "Consolacion", "Cordova", "Danao", "Liloan", "Mandaue", "Minglanilla",
                "Naga", "San Fernando", "Talisay"};
        return firstNames[new Random().nextInt(firstNames.length)];
    }

    public static String getPassword() {return "Osius@20267";}

    public static String generateRandomEmail(int length) {
        if (length == 0) {
            length = 16;
        }

        String allowedChars = "abcdefghijklmnopqrstuvwxyz" + "1234567890";
        String email = RandomStringUtils.random(length, allowedChars);
        if (length > 5) {
            String miliseconds = String.format("%d", System.currentTimeMillis());
            email = email.substring(0, email.length() - 5);
            email = email + miliseconds.substring(miliseconds.length() - 5);
        }
        email = email.substring(0, email.length()) + "@void.osius.com";
        return email;
    }

    public static String getRandomString(int size) {
        return getRandom(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
    }

    public static String getRandomNumber(int size) {
        return getRandom(size, "0123456789");
    }

    public static String getRandomMobleNumber() {
        return getRandom(3, "0123456789") + "-" + getRandom(3, "01234567891") + "-" +getRandom(4, "01234567891");
    }
    public static String generatepreferredName() {
        String[] lastNames = {"SANAPA", "BROWN", "JONES", "MILLER", "GARCIA", "RODRIGUEZ", "ANDERSON",
                "TAYLOR", "THOMAS", "MOORE"};
        return lastNames[new Random().nextInt(lastNames.length)];
    }
}
