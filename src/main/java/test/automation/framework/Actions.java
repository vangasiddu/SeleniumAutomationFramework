package test.automation.framework;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.function.BooleanSupplier;
import java.util.logging.Level;

import static test.automation.framework.Page.*;
import static test.automation.framework.Runner.log;

public final class Actions {

    public static synchronized Object execJavascript(String script, Object... args) {
        try {
            JavascriptExecutor scriptExe = ((JavascriptExecutor) Browser.getDriver());
            return scriptExe.executeScript(script, args);
        } catch (Exception e) {
            log().log(Level.WARNING, e.getMessage());
            return "";
        }
    }


    public static boolean isPageLoaded() {
        String state = (String) execJavascript("return document.readyState;");
        return state.matches("complete|loaded|interactive");
    }

    public static boolean isJQueryActive() {
        Object jsResponse = execJavascript("return jQuery.active;");
        if (jsResponse instanceof Long) {
            return ((Long) jsResponse) == 0;
        } else if (jsResponse instanceof String) {
            String response = (String) jsResponse;
            return (response.startsWith("{\"hCode\"") || response.isEmpty());
        } else {
            return true;
        }
    }

    public static void waitUntil(BooleanSupplier condition, int seconds) {
        new WebDriverWait(Browser.getDriver(), seconds).until((WebDriver driver) -> condition.getAsBoolean());
    }

    public static void waitUntil(BooleanSupplier condition) {
        waitUntil(condition, 60);
    }

    public static void click(String element) {
        WebElement element1 = getElement(element);
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(),30);
        wait.until(ExpectedConditions.elementToBeClickable(element1));
        if (Config.getBrowser().equalsIgnoreCase("ie"))
            scrollToTheElement(element1);
        try {
            element1.click();
        } catch (Exception e) {
            ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",element1);
        }
        if (Config.getBrowser().equals("firefox") && Browser.isAlertPresent()) {
            Alert alt = Browser.getDriver().switchTo().alert();
            alt.accept();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void click(WebElement element) {
        if (Config.getBrowser().equalsIgnoreCase("ie"))
            scrollToTheElement(element);
        try {
            element.click();
        } catch (Exception e) {
            ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",element);
        }
        if (Config.getBrowser().equals("firefox") && Browser.isAlertPresent()) {
            Alert alt = Browser.getDriver().switchTo().alert();
            alt.accept();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void jsClick(String element) {
        execJavascript("arguments[0].click();", getElement(element).getWrappedElement());
    }

    public static boolean elementInView(WebElement el) {
        try {
            return el != null && (boolean) execJavascript("arguments[0].scrollIntoView(false);" +
                    "       var position = arguments[0].getBoundingClientRect();" +
                    "       var x = position.left + (position.width / 2);" +
                    "       var y = position.top + (position.height / 2);" +
                    "       var actual = document.elementFromPoint(x, y);" +
                    "       do { if(actual === arguments[0]) { return true; } } while(actual = actual.parentNode);" +
                    "       return false;", el);
        } catch (Exception ex){
            return false;
        }
    }

    public static void scrollToTheElement(WebElement element) {
        if (!elementInView(element))
            execJavascript("arguments[0].scrollIntoView(true);",element);
    }

    public static boolean exists(String element) {
        return getElement(element).exists();
    }

    public static boolean isDisplayed(String element) {
        TypifiedElement htmlElement = getElement(element);
        return htmlElement.exists() && htmlElement.isDisplayed();
    }

    public static boolean isEnabled(String element) {
        return getElement(element).isEnabled();
    }

    public static boolean isSelected(String element) {
        return getElement(element).isSelected();
    }

    public static void submit(String element) {
        getElement(element).submit();
    }

    public static void sendKeys(String element, String text) {
        getElement(element).sendKeys(text);
    }

    public static void clear(String element) {
        getElement(element).clear();
    }

    public static String getText(String element) {
        return getElement(element).getText();
    }

    public static String getTagName(String element) {
        return getElement(element).getTagName();
    }

    public static String getAttribute(String element, String attributeName) {
        return getElement(element).getAttribute(attributeName);
    }

    public static String getCssValue(String element, String propertyName) {
        return getElement(element).getCssValue(propertyName);
    }

    public static boolean isPanelDisplayed(String panel) {
        HtmlElement htmlElement = getPanel(panel);
        return htmlElement.exists() && htmlElement.isDisplayed();
    }

    public static void click(String element, String panel) {
        Page.getElement(element, panel).click();
    }

    public static void jsClick(String element, String panel) {
        execJavascript("arguments[0].click();", getElement(element, panel).getWrappedElement());
    }

    public static void jsClick(WebElement element) {
        execJavascript("arguments[0].click();", element);
    }

    public static boolean exists(String element, String panel) {
        return Page.getElement(element, panel).exists();
    }

    public static boolean isDisplayed(String element, String panel) {
        TypifiedElement htmlElement = Page.getElement(element, panel);
        return htmlElement.exists() && htmlElement.isDisplayed();
    }

    public static boolean isEnabled(String element, String panel) {
        return Page.getElement(element, panel).isEnabled();
    }

    public static boolean isSelected(String element, String panel) {
        return Page.getElement(element, panel).isSelected();
    }

    public static void submit(String element, String panel) {
        Page.getElement(element, panel).submit();
    }

    public static void sendKeys(String element, String text, String panel) {
        Page.getElement(element, panel).sendKeys(text);
    }

    public static void clear(String element, String panel) {
        Page.getElement(element, panel).clear();
    }

    public static String getText(String element, String panel) {
        return Page.getElement(element, panel).getText();
    }

    public static String getTagName(String element, String panel) {
        return Page.getElement(element, panel).getTagName();
    }

    public static String getAttribute(String element, String attributeName, String panel) {
        return Page.getElement(element, panel).getAttribute(attributeName);
    }

    public static String getCssValue(String element, String propertyName, String panel) {
        return Page.getElement(element, panel).getCssValue(propertyName);
    }

    public static void hoverForSelection(WebElement el) {
        org.openqa.selenium.interactions.Actions action = new org.openqa.selenium.interactions.Actions(Browser.getDriver());
        action.moveToElement(el).build().perform();
    }

    public static void waitUntil(ExpectedCondition<WebElement> webElementExpectedCondition) {
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 10);
        wait.until(webElementExpectedCondition);
    }
}
