package test.automation.framework;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class Rest {

    public static RequestSpecification request;
    public static Response response;
    public static ValidatableResponse validatableResponse;

    public static int getResponseCode(String url) {
        try {
            URL uri = new URL(url);
            HttpURLConnection http = (HttpURLConnection) uri.openConnection();
            return http.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 404;
    }
}
