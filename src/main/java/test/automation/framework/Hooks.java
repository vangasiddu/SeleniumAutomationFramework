package test.automation.framework;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import test.automation.utils.EGRDB;
import test.automation.utils.UserUtils;

import static test.automation.framework.Runner.log;

public final class Hooks {

    @Before
    public void beforeScenario(Scenario scenario) {
        log().info("Scenario Started: " + scenario.getName() );
    }

    @After
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed() && Browser.isStarted()) {
            scenario.embed(Browser.getScreenShot(), "image/png");
        }
       // Browser.quit();
        UserUtils.clearUser();
        DB.closeConnection();
        log().info("Scenario completed: " + scenario.getName());
        log().info("Scenario status: " + scenario.getStatus());
    }
}
