package test.automation.framework;

import com.rajatthareja.reportbuilder.Color;
import com.rajatthareja.reportbuilder.ReportBuilder;
import cucumber.api.cli.Main;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.*;

import static test.automation.framework.Config.*;
import static test.automation.framework.Config.getReportsDir;

public final class Runner {

    private static int runStatus = 0;
    private final static Logger logger = Logger.getLogger("Runner");

    public static void main(String ... args) {
        prepareReportDir();
        setLogFormat();
        runCucumber(getCucumberArgs(args));
        if (isRerun() && runStatus != 0 && Files.exists(Paths.get(getReportsDir() + "rerun.txt"))) {
            runCucumber(getCucumberRerunArgs());
        }
        buildReport();
        System.exit(runStatus);
    }

    public static Logger log() {
        return logger;
    }

    private static void setLogFormat() {
        try {
            logger.setUseParentHandlers(false);
            FileHandler fileHandler = new FileHandler(getReportsDir() + "log.html");
            logger.addHandler(fileHandler);
            LogHtmlFormatter logHtmlFormatter = new LogHtmlFormatter();
            fileHandler.setFormatter(logHtmlFormatter);
        } catch (Exception e) {
            log().log(Level.WARNING, e.getMessage());
        }
    }

    private static String[] getCucumberArgs(String ... args) {
        List<String> cucumberArgs = new ArrayList<>();
        cucumberArgs.add("--glue");
        cucumberArgs.add(getGlue());
        cucumberArgs.add("--plugin");
        cucumberArgs.add("json:" + getReportsDir() + "report.json");
        cucumberArgs.add("--plugin");
        cucumberArgs.add("rerun:" + getReportsDir() + "rerun.txt");
        cucumberArgs.add("--plugin");
        cucumberArgs.add("pretty");
        if (getTags() != null) {
            cucumberArgs.add("--tags");
            cucumberArgs.add(getTags());
        }
        if (getScenarios() != null) {
            cucumberArgs.add("--name");
            cucumberArgs.add(getScenarios());
        }
        if (isDryRun()) {
            cucumberArgs.add("--dry-run");
        }
        cucumberArgs.add("--strict");
        cucumberArgs.addAll(Arrays.asList(args));
        if (getFeatures() != null) {
            cucumberArgs.add(getFeatures());
        }
        log().info("Cucumber Options: " + cucumberArgs);
        return cucumberArgs.toArray(new String[cucumberArgs.size()]);
    }

    private static String[] getCucumberRerunArgs() {
        List<String> cucumberArgs = new ArrayList<>();
        cucumberArgs.add("--glue");
        cucumberArgs.add(getGlue());
        cucumberArgs.add("--plugin");
        cucumberArgs.add("json:" + getReportsDir() + "rerun.json");
        cucumberArgs.add("--plugin");
        cucumberArgs.add("pretty");
        cucumberArgs.add("--strict");
        cucumberArgs.add("@" + getReportsDir() + "rerun.txt");
        log().info("Cucumber Options: " + cucumberArgs);
        return cucumberArgs.toArray(new String[cucumberArgs.size()]);
    }

    private static void runCucumber(String ... cucumberArgs) {
        try {
            runStatus = Main.run(cucumberArgs, Thread.currentThread().getContextClassLoader());
        } catch (Throwable e) {
            e.printStackTrace();
            runStatus = 1;
        }
    }

    private static void buildReport() {
        ReportBuilder reportBuilder = new ReportBuilder();
        reportBuilder.setReportDirectory(getReportsDir());
        reportBuilder.setReportTitle("Test Automation Report");
        reportBuilder.setReportColor(Color.CYAN);
        reportBuilder.setAdditionalInfo("Date", LocalDateTime.now().toString());
        reportBuilder.setAdditionalInfo("URL", getUrl());
        reportBuilder.setAdditionalInfo("Browser", getBrowser());
        if (getBrowserVersion() != null) {
            reportBuilder.setAdditionalInfo("Browser Version", getBrowserVersion());
        }
        reportBuilder.setAdditionalInfo("Platform", getPlatform() != null ? getPlatform() : System.getProperty("os.name"));
        if (getPlatformVersion() != null) {
            reportBuilder.setAdditionalInfo("Platform Version", getPlatformVersion());
        }
        if (getDevice() != null) {
            reportBuilder.setAdditionalInfo("Device", getDevice());
        }
        reportBuilder.build(new File(getReportsDir() + "report.json"));
        if (isRerun() && runStatus != 0 && Files.exists(Paths.get(getReportsDir() + "rerun.json"))) {
            reportBuilder.setReportFileName("rerun");
            reportBuilder.setReportTitle("Rerun Report");
            reportBuilder.build(new File(getReportsDir() + "rerun.json"));
            reportBuilder.setReportFileName("final_report");
            reportBuilder.setReportTitle("Final Report");
            reportBuilder.build(new File(getReportsDir() + "report.json"), new File(getReportsDir() + "rerun.json"));
        }
    }

    private static void prepareReportDir() {
        new File(getReportsDir()).mkdirs();
        Arrays.asList("report.json", "report.html",
                "rerun.txt", "rerun.json", "rerun.html",
                "final_report.html").forEach(file -> new File(getReportsDir() + file).delete());
    }

    private static String getGlue() {
        return Runner.class.getName().replace(".framework.Runner", "");
    }
}
