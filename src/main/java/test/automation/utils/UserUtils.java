package test.automation.utils;

import test.automation.framework.Data;
import test.automation.framework.Util;
import test.automation.models.User;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static test.automation.framework.Data.getAsMap;
import static test.automation.framework.Data.getAsObject;

public class UserUtils {
    static User user;
    static final String TITLES[] = {"Mr.", "Ms.", "Mrs."};

    public static User getUser(){
        if (user == null)
            getNewUser();
        return user;
    }
    public static User getNewUser() {
        user = (User) Data.getAsObject("User");
        user.setTitle(TITLES[Util.getRandomIndex(TITLES.length)]);
        user.setFirstName(Util.generateRandomFirstName());
        user.setLastName(Util.generateRandomFirstName());
        user.setEmail(Util.generateRandomEmail(16));
        user.setPassword(Util.getPassword());
        user.setPhone(Util.getRandomNumber(7));
        //user.setMobile(Util.getRandomMobleNumber());
        user.setMobile("0744-812-8473");
        user.setEventVenue(Util.generateRandomEventVenue());
        user.setGiftDeliveryZip(Util.getRandomNumber(4));
        user.setZipCode(Util.getRandomNumber(4));
        user.setEventZipcode(Util.getRandomNumber(4));
        user.setGiftDeliveryAddress(Util.generateRandomEventVenue());
        user.setPreferredName(Util.generatepreferredName());

        return user;
    }

    public static User getValidUser(String type) {
        List<User> users = ((List<User>) ((Object) Data.getAsObjects("valid_users", User.class))).stream().filter(u -> u.getType().equals(type)).collect(Collectors.toList());
        user = users.get(Util.getRandomIndex(users.size()));;
        return user;
    }

    public static void clearUser() {
        if(user != null)
            user = null;
    }

    public static Map<String, Object> getUserMap() {
        return getAsMap("user_v1");
    }
}
