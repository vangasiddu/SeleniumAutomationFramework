package test.automation.utils;

import org.junit.Assert;
import test.automation.steps.GiftRegistry.DBSteps;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EGRDB {

    private static Connection connection;

    private static Connection connection() throws SQLException {
        if (connection == null)
            connection = getDBConnection();
        return connection;
    }

    private static Connection getDBConnection() throws SQLException {
        try {
            String dbURL = "jdbc:sqlserver://192.168.32.120;databaseName=egr_testing1";
            String user = "sa";
            String pass = "sa";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(dbURL, user, pass);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void cancelEventsOfUser(String eventType, String eventDate, String email) {
        try {
            List<String> dataList = new ArrayList<>();
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select ee.event_code,ee.event_status,eec.event_category_name,ee.event_category_id from egr_event ee " +
                    "join egr_event_category eec on ee.event_category_id = eec.event_category_id " +
                    "where event_date = '" + eventDate + "' and ee.email_address = '" + email + "' and event_category_name = '" + eventType + "'");
            while (resultSet.next()) {
                dataList.add(resultSet.getString("event_category_id"));
            }
            if (!resultSet.equals(null)) {
                for (int i = 0; i < dataList.size(); i++) {
                    stmt.executeUpdate("update egr_event set event_status='4' where event_category_id= '" + dataList.get(i) + "' and email_address = '" + email + "'");
                }
            }
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public static List<String> getUserData(String userValue, String sortOrder) {
        List<String> dataList = new ArrayList<>();
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select " + userValue + " from egr_user order by " + userValue + " " + sortOrder);
            while (resultSet.next()) {
                dataList.add(resultSet.getString(userValue));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dataList;
    }

    public static List<String> getEventData(String sortOrder, String sortValue) {
        List<String> dataList = new ArrayList<>();
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = null;
            if (sortOrder.equalsIgnoreCase("asc") || sortOrder.equalsIgnoreCase("desc"))
                resultSet = stmt.executeQuery("select ee. " + sortValue + " from egr_event ee \n" +
                        "join egr_company ec on ee.company_id = ec.company_id \n" +
                        "join egr_child_company ecc on ec.company_id = ecc.company_id \n" +
                        " where ec.company_id = '2' order by ee." + sortValue + " " + sortOrder);
            else {
                resultSet = stmt.executeQuery("select ee. " + sortValue + " from egr_event ee \n" +
                        "join egr_company ec on ee.company_id = ec.company_id \n" +
                        "join egr_child_company ecc on ec.company_id = ecc.company_id \n" +
                        " where ec.company_id = '2' order by ee.modified_on desc");
            }
            while (resultSet.next()) {
                dataList.add(resultSet.getString(sortValue));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dataList;
    }

    public static String getHexCode(String emailId) {
        String verificationToken = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_user where email_id = '" + emailId + "'");  //and status = " + status);
            while (resultSet.next()) {
                verificationToken = resultSet.getString("hex_code");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return verificationToken;
    }

    public static Integer statusCode(String emailId) {
        Integer statusCode = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_user where email_id = '" + emailId + "'");
            while (resultSet.next()) {
                statusCode = Integer.parseInt(resultSet.getString("status"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return statusCode;
    }

    public static List<String> emailIdsList() {
        List<String> emailIds = new ArrayList<>();
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet1 = stmt.executeQuery("select email_id from egr_user");
            while (resultSet1.next()) {
                emailIds.add(resultSet1.getString("email_id"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return emailIds;
    }

    public static Integer companyId(String email) {
        Integer companyId = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_user where email_id = '" + email + "'");
            while (resultSet.next()) {
                companyId = resultSet.getInt("company_id");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return companyId;
    }

    public static String fieldValue(String email, String fieldName) {
        String companyId = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_user where email_id = '" + email + "'");
            while (resultSet.next()) {
                companyId = resultSet.getString(fieldName);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return companyId;
    }

    public static String getHexCodeForEvent(int eventId) {
        String hexCode = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_event where event_id = " + eventId);
            while (resultSet.next()) {
                hexCode = resultSet.getString("hexcode");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hexCode;
    }

    public static String getHexCodeForEventCode(int eventCode) {
        String hexCode = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_event where event_code = " + eventCode);
            while (resultSet.next()) {
                hexCode = resultSet.getString("hexcode");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hexCode;
    }


    public static String getEventId(int eventCode) {
        String eventId = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_event where event_code = " + eventCode);
            while (resultSet.next()) {
                eventId = resultSet.getString("event_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventId;
    }

    public static String getActiveEventcode(String email) {
        String EventCode = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_event where event_status = 3 and email_address = '" + email + "'");
            while (resultSet.next()) {
                EventCode = resultSet.getString("even");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return EventCode;
    }

    public static String eventFieldValue(String email, String fieldName) {
        String filedValue = null;
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("Select * from egr_event where email_address = '" + email + "'");
            while (resultSet.next()) {
                filedValue = resultSet.getString(fieldName);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return filedValue;
    }

    public static void doInActiveForAnEvent(Integer eventId) {
        try {
            Statement stmt = connection().createStatement();
            stmt.executeUpdate("update egr_event set event_expired_date='2018-05-11 00:00:00.0000000' where event_id = " + eventId);
            stmt.executeUpdate("update egr_event set event_date='2018-06-13 00:00:00.0000000' where event_id = " + eventId);
            stmt.executeUpdate("update egr_event set event_status ='4' where event_id = " + eventId);
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public static String eventInvitationHexCode(String eventType) {
        List<String> hexCodes = new ArrayList<>();
        try {
            Statement stmt = connection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from egr_event where event_status = 3 and company_id = " + DBSteps.companyCode()
                    + "and event_category_id = " + DBSteps.companyCode(eventType));
            while (resultSet.next()) {
                hexCodes.add(resultSet.getString("hexcode"));
            }
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
        return hexCodes.get(hexCodes.size() == 1 ? 0 : new Random().nextInt(hexCodes.size() - 1));
    }

    public static void cancelEvent(Integer eventId) {
        try {
            Statement stmt = connection().createStatement();
            stmt.executeUpdate("update egr_event set event_status ='5' where event_id = " + eventId);
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public static void deleteid(String emailId) {
        try {
            Statement stmt = connection().createStatement();
            stmt.execute("Delete from egr_user where email_id = '" + emailId + "'");
        } catch (Exception ex) {
            Assert.fail(emailId + " is not exists in DB ");
        }
    }

    public static void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
