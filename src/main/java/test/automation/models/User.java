package test.automation.models;

public class User {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String website;
    private String spouse;
    private String suffix;
    private String mobile;
    private String title;
    private String type;
    private String newpassword;
    private String confirmpassword;
    private String eventVenue;
    private String giftDeliveryAddress;
    private String eventZipcode;
    private String giftDeliveryZip;
    private String coRegistrantCelebrant;
    private String eventDate;
    private String eventTime;
    private String eventState;
    private String eventCity;
    private String giftDeliveryState;
    private String giftDeliveryCity;
    public String giftDeliveryDate;
    public String eventType;
    private String preferredName;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String event) {
        this.eventType = event;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public String getGiftDeliveryAddress() {
        return giftDeliveryAddress;
    }

    public void setGiftDeliveryAddress(String giftDeliveryAddress) {
        this.giftDeliveryAddress = giftDeliveryAddress;
    }


    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventZipcode() {
        return eventZipcode;
    }

    public void setEventZipcode(String eventZipcode) {
        this.eventZipcode = eventZipcode;
    }

    public String getGiftDeliveryZip() {
        return giftDeliveryZip;
    }

    public void setGiftDeliveryZip(String giftDeliveryZip) {
        this.giftDeliveryZip = giftDeliveryZip;
    }

    public String getCoRegistrantCelebrant() {
        return coRegistrantCelebrant;
    }

    public void setCoRegistrantCelebrant(String coRegistrantCelebrant) {
        this.coRegistrantCelebrant = coRegistrantCelebrant;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public void setGiftsDeliveryDate(String giftDeliveryDate){
        this.giftDeliveryDate = giftDeliveryDate;
    }

    public String getEventState() {
        return eventState;
    }

    public void setEventState(String eventState) {
        this.eventState = eventState;
    }

    public String getEventCity() {
        return eventCity;
    }

    public void setEventCity(String eventCity) {
        this.eventCity = eventCity;
    }

    public String getGiftDeliveryState() {
        return giftDeliveryState;
    }

    public void setGiftDeliveryState(String giftDeliveryState) {
        this.giftDeliveryState = giftDeliveryState;
    }

    public String getGiftDeliveryCity() {
        return giftDeliveryCity;
    }

    public void setGiftDeliveryCity(String giftDeliveryCity) {
        this.giftDeliveryCity = giftDeliveryCity;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public String getPreferredName() {
        return preferredName;
    }
}
