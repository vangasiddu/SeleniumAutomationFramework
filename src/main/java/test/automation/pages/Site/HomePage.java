package test.automation.pages.Site;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class HomePage extends Page {

    public static final String URL = Config.getUrl() + "/";

    // Need to change below id once SME is integrated

    public static final By VERIFY_BY = By.id("email");

    @Name("take me registry button")
    @FindBy(id = "")
    public Button RegistryButton;

}

