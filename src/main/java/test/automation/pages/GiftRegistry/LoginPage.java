package test.automation.pages.GiftRegistry;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Browser;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.models.User;
import test.automation.panels.GiftRegistry.NavigationPanel;
import test.automation.steps.GiftRegistry.RegistryHomeSteps;

import java.util.List;

public class LoginPage extends Page {

    public static final String URL = Config.getUrl() + "/login";
    public static final By VERIFY_BY = By.id("email");

    @Name("Email")
    @FindBy(xpath = "//input[@id='email']")
    public static TextInput email;

    @Name("Password")
    @FindBy(xpath = "//input[@id='password']")
    public static TextInput password;

    @Name("Show Button")
    @FindBy(xpath = "//*[@id='password']/following::span[1]/child::*")
    public static Button showPassword;

    @Name("Show Button")
    @FindBy(xpath = "//*[@id='password']/following::span[1]/child::*")
    public static List<Button> showPasswordIcons;

    @Name("Login Button")
    @FindBy(xpath = "//div[contains(@class,'LoginForm-submit')]/button")
    public static Button loginButton;

    @Name("Reg First name")
    @FindBy(id = "reg-firstName")
    public static TextInput regfirstName;

    @Name("Reg Last name")
    @FindBy(id = "reg-lastName")
    public static TextInput reglastName;

    @Name("Reg email")
    @FindBy(id = "reg-email")
    public static TextInput regEmail;

    @Name("Reg password")
    @FindBy(id = "reg-password")
    public static TextInput regPassword;

    @Name("Reg Confirm password")
    @FindBy(id = "reg-confirmPassword")
    public static TextInput regConfirmPassword;

    @Name("Register Button")
    @FindBy(xpath = "//div[contains(@class,'RegistrationForm-submit')]/button")
    public static Button register;

    @Name("Google Button")
    @FindBy(xpath = "//button[contains(@class,'GoogleLoginButton-button')]")
    public static Button googleLoginbutton;

    @Name("Facebook Button")
    @FindBy(xpath = "//span[contains(@class,'FacebookLoginButton-signinWithFacebook')]")
    public static Button facebookLoginbutton;

    @Name("Error message")
    @FindBy(id = "error")
    public static HtmlElement errormessage;

    @Name("Success message")
    @FindBy(id = "")
    public static HtmlElement successmessage;

    @Name("Resend link")
    @FindBy(id = "")
    public static HtmlElement resendLink;

    @Name("popup")
    @FindBy(tagName = "h4")
    public HtmlElement popup;

    @Name("Forgot Password Button")
    @FindBy(linkText = "Forgot Password?")
    public Button ForgotPassword;

    @Name("Forgot password")
    @FindBy(xpath = "//a[contains(text(),'Forgot Password?')]")
    public Button forgotPwd;

    public void login(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getPassword();
        RegistryHomeSteps.celebrantEmail = user.getEmail();
        RegistryHomeSteps.celebrantPassword = user.getPassword();
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
        clickLoginButton();
    }


    @Name("Email error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[1]/p")
    public static HtmlElement emailErrorMsz;

    @Name("Password error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[2]/p")
    public static HtmlElement passwordErrorMsz;

    @Name("Reg First name error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[1]/p")
    public static HtmlElement regfirstNameErrorMsz;

    @Name("Reg Last name error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[2]/p")
    public static HtmlElement reglastNameErrorMsz;

    @Name("Reg email error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[3]/p")
    public static HtmlElement regEmailErrorMsz;

    @Name("Reg password error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[4]/p")
    public static HtmlElement regPasswordErrorMsz;

    @Name("Reg Confirm password error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[5]/p")
    public static HtmlElement regConfirmPasswordErrorMsz;

    @Name("Reg not successful error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[6]/p")
    public static HtmlElement regNotSuccessfulErrorMsz;

    @Name("Invalid login error message")
    @FindBy(xpath = "//*[@class='Login-loginOptions-3']/div/form/div[3]/p")
    public static HtmlElement invalidLoginErrorMsz;

    @Name("Email label")
    @FindBy(xpath = "//label[@for='email']")
    public static List<HtmlElement> emailLabel;

    @Name("Password label")
    @FindBy(xpath = "//label[@for='password']")
    public static List<HtmlElement> passwordLabel;

    @Name("First name label")
    @FindBy(xpath = "//label[@for='firstName']")
    public static HtmlElement firstLabel;

    @Name("Last name label")
    @FindBy(xpath = "//label[@for='lastName']")
    public static HtmlElement lastNameLabel;

    @Name("SignOut DropDown")
    @FindBy(xpath = "//button[@id='dropdownMenuButton']")
    public static WebElement signOutDropDown;

    @Name("Signout Link")
    @FindBy(xpath = "//a[contains(text(),'Sign Out')]")
    public static Link signOut;


    public static void register(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getPassword();
        regfirstName.sendKeys(user.getFirstName());
        reglastName.sendKeys(user.getLastName());
        regEmail.sendKeys(user.getEmail());
        regPassword.sendKeys(user.getPassword());
        regConfirmPassword.sendKeys(user.getPassword());
    }

    public static void signIn(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getPassword();
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
    }

    public static void celebrantLogin(String celebEmail, String celebPassword) {
        email.sendKeys(celebEmail);
        password.sendKeys(celebPassword);
        clickLoginButton();
    }

    public static void guestLogin(String email, String password) {
        celebrantLogin(email,password);
    }

    public static void signInWithChangedPassword(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getNewpassword();
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getNewpassword());
    }

    public static void clickRegisterButton() {
        register.click();
        if (Config.getBrowser().equals("firefox") && Browser.isAlertPresent()) {
            Alert alt = Browser.getDriver().switchTo().alert();
            alt.accept();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void clickLoginButton(){
        loginButton.click();
        if (Config.getBrowser().equals("firefox") && Browser.isAlertPresent()) {
            Alert alt = Browser.getDriver().switchTo().alert();
            alt.accept();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void facebookSignin(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getPassword();
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
        facebookLoginbutton.click();
    }

    public static void gmailSignin(User user) {
        RegistryHomeSteps.email = user.getEmail();
        RegistryHomeSteps.password = user.getPassword();
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
        googleLoginbutton.click();
    }

    public static void signOut(){
        signOutDropDown.click();
        signOut.click();
    }
}
