package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;
import java.util.stream.Collectors;

public class GuestCart extends Page {

    public static final String URL = Config.getUrl() + "/events/my-cart";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'MY CART')]");


    @Name("Product SKU details")
    @FindBy(xpath = "//span[contains(text(),'SKU ')]")
    public static List<TextBlock> productSKUIds;


    public static List<String> productSkuIds() {
        return productSKUIds.stream().map(TypifiedElement::getText).collect(Collectors.toList());
    }


}
