package test.automation.pages.GiftRegistry;

import jdk.nashorn.internal.runtime.options.Options;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Actions;
import test.automation.framework.Browser;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.models.User;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ProfilePage extends Page {
    public static final String URL = Config.getUrl() + "/registrant/profile";
    public static final By VERIFY_BY = By.className("profile-info-box");

    @Name("Edit Link")
    @FindBy(linkText = "Edit")
    public Link editLink;

    @Name("Title")
    @FindBy(className = "mat-select-value")
    public TextBlock title;

    @Name("Option")
    @FindBy(className = "mat-option-text")
    public List<TextBlock> Options;

    @Name("First name")
    @FindBy(name = "firstName")
    public TextInput firstName;

    @Name("Last name")
    @FindBy(name = "lastName")
    public TextInput lastName;

    @Name("Suffix")
    @FindBy(name = "suffix")
    public TextInput suffix;

    @Name("Spouse name")
    @FindBy(name = "spouseName")
    public TextInput spouse;

    @Name("Email")
    @FindBy(name = "email")
    public TextInput email;

    @Name("Telephone")
    @FindBy(name = "telephone")
    public TextInput telephone;

    @Name("Mobile")
    @FindBy(name = "mobile")
    public TextInput mobile;

    @Name("Address Line1")
    @FindBy(name = "addressLine1")
    public TextInput addressLine1;

    @Name("Address Line2")
    @FindBy(name = "addressLine1")
    public TextInput addressLine2;

    @Name("Address Line3")
    @FindBy(name = "addressLine3")
    public TextInput addressLine3;

    @Name("City")
    @FindBy(name = "city")
    public TextInput city;

    @Name("State")
    @FindBy(name = "state")
    public TextInput state;

    @Name("Zip code")
    @FindBy(name = "zipCode")
    public TextInput zipCode;

    @Name("Submit")
    @FindBy(xpath = "//span[contains(text(),'Update')]")
    public Button submit;

    @Name("Reset")
    @FindBy(xpath = "//span[contains(text(),'Reset')]")
    public Button reset;

    @Name("User Info")
    @FindBy(css = "div.profile-info>h3")
    public static HtmlElement userInfo;

    @Name("First name error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter First Name')]")
    public static HtmlElement firstNameErrorMsz;

    @Name("Last name error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Last Name')]")
    public static HtmlElement lastNameErrorMsz;

    @Name("Address error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Address')]")
    public static HtmlElement AddressErrorMsz;

    @Name("City error message")
    @FindBy(xpath = "//div[contains(text(),'Please select City')]")
    public static HtmlElement CityErrorMsz;

    @Name("City invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter a valid city')]")
    public static HtmlElement CityInvalidErrorMsz;

    @Name("Zipcode error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Zip Code')]")
    public static HtmlElement ZipErrorMsz;

    @Name("Zipcode invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Zip Code should be atleast 4 Characters') or contains(text(),'Please enter a valid Zip Code.')]")
    public static HtmlElement ZipInvalidErrorMsz;

    @Name("State error message")
    @FindBy(xpath = "//div[contains(text(),'Please select State')]")
    public static HtmlElement stateErrorMsz;

    @Name("Phone error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Telephone Number')]")
    public static HtmlElement phoneErrorMsz;

    @Name("Phone invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in one of these formats')]")
    public static HtmlElement phoneInvalidErrorMsz;

    @Name("Mobile error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Mobile Number')]")
    public static HtmlElement mobileErrorMsz;

    @Name("Mobile invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format:')]")
    public static HtmlElement mobileInvalidErrorMsz;

    @Name("Additional Information Spouse")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[1]/span[2]")
    public Button AdditionalSpouse;

    @Name("Additional Information Phone Number")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[2]/span[2]")
    public Button AdditionalPhoneNumber;

    @Name("Additional Information Mobile Number")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[3]/span[2]")
    public Button AdditionalMobileNumber;

    @Name("Additional Information Address Line")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[4]/span[2]")
    public Button AdditionalAddressLine;

    @Name("Additional Information State")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[5]/span[2]")
    public Button AdditionalState;

    @Name("Additional Information City")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[6]/span[2]")
    public Button AdditionalCity;

    @Name("Additional Information ZipCode")
    @FindBy(xpath = "//ul[@class='additional-info-list']/li[7]/span[2]")
    public Button AdditionalZipCode;

    @Name("Title error message")
    @FindBy(xpath = "//div[contains(text(),'Please select Title')]")
    public static HtmlElement titleErrormsz;

    public String randomStateSelect(){
        state.click();
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",random);
        return  random.getText();
    }

    public String randomCitySelect(){
        city.click();
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",random);
        return  random.getText();
    }

    public List<String> stateNames (){
      return Options.stream().map(ele -> ele.getText()).collect(Collectors.toList());
    }

    public void updateAccount(User user) throws InterruptedException {
        title.click();
        Actions.waitUntil(() -> Options.stream().anyMatch(t -> t.getText().equals(user.getTitle())));
        Options.stream().filter(t -> t.getText().equals(user.getTitle())).findFirst().get().click();
        telephone.clear();
        telephone.sendKeys(user.getPhone());
        mobile.clear();
        mobile.sendKeys(user.getMobile());
        addressLine1.sendKeys(user.getAddress());
        spouse.clear();
        spouse.sendKeys(user.getSpouse());
        user.setState(randomStateSelect());
        Thread.sleep(1000);
        user.setCity(randomCitySelect());
        zipCode.clear();
        zipCode.sendKeys(user.getZipCode());
    }

}


