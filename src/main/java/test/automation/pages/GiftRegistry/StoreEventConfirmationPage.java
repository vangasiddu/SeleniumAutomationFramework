package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class StoreEventConfirmationPage extends Page {
    public static final String URL = Config.getUrl() + "/events/event-confirmation";
    public static final By VERIFY_BY = By.tagName("app-event-confirmation");

    StoreEventManagementPage storeEventManagementPage = new StoreEventManagementPage();

    @Name("Resend Confirmation Link")
    @FindBy(css="a.btn.btn-primary")
    public Link resendConfirmationLink;

    @Name("Go to Wish list button")
    @FindBy(css="input.btn.btn-secondary")
    public TextBlock goToWishListButton;

    @Name("Event Confirmation Message")
    @FindBy(xpath="//div[@class='ng-star-inserted']//div/p[contains(text(),'Please confirm the event')]")
    public HtmlElement eventConfirmationMessage;

    @Name("Event Code")
    @FindBy(tagName="h4")
    public HtmlElement eventCode;

    @Name("Event Button")
    @FindBy(xpath = "//div[@class='text-center']/div[1]/div/p[2]/a")
    public static Link eventButton;

    @Name("Congrats message")
    @FindBy(css="div.text-center>p")
    public HtmlElement congratsMessage;

    @Name("Event Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][4]/a" )
    public static Link eventManagement;

    @Name("Confirm Event")
    @FindBy(xpath = "//img[@mattooltip='Confirm Event']")
    public static Link Confirmevent;

    @Name("Edit Event")
    @FindBy(xpath = "//img[@mattooltip='Edit Event']")
    public static Link editEvent;

    @Name("Customer Management")
    @FindBy(xpath = "//a[contains(text(),'Customer Management')]")
    public static TextBlock customerManagement;

    @Name("Search Field")
    @FindBy(xpath = "//div[@class='search-box']/mat-input-container/div/div/div/input")
    public static TextInput searchBar;

    @Name("Search Button")
    @FindBy(xpath = "//div[@class='search-btn']/button")
    public static Button searchButton;

}
