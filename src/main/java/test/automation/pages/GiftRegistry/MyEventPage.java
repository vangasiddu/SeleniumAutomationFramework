package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;

public class MyEventPage extends Page {

    public static final String URL = Config.getUrl() + "/events";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'View Event List')]");

    @Name("Home Page Banner")
    @FindBy(css = "div.home-banner-section")
    public static TextBlock homePageBanner;

    @Name("View Event List Header")
    @FindBy(xpath = "//h2[contains(text(), 'View Event List')]")
    public static TextBlock viewEventListHeader;

    @Name("Event list")
    @FindBy(xpath = "//div[@class='event-top']/span")
    public List<TextBlock> eventList;

    @Name("Create Event")
    @FindBy(xpath = "//a[contains(text(), 'Create your first Event')]")
    public Link createFirstEventLink;

    @Name("No Event message")
    @FindBy(tagName = "h3")
    public TextBlock noEventMessage;

    @Name("Active event status")
    @FindBy(xpath = "//span[@class='text-success' and text()='Active']")
    public TextBlock activeEventStatus;

    @Name("Pending event status")
    @FindBy(xpath = "//span[@class='text-success' and text()='Confirmation Pending']")
    public TextBlock pendingEventStatus;

    @Name("Cancel event status")
    @FindBy(xpath = "//span[@class='text-success' and text()='Cancelled']")
    public TextBlock cancelEventStatus;

    @Name("Draft event status")
    @FindBy(xpath = "//span[@class='text-success' and text()='Draft']")
    public TextBlock draftEventStatus;

    @Name("Inactive event status")
    @FindBy(xpath = "//span[@class='text-success' and text()='In-active']")
    public TextBlock inactiveEventStatus;

    @Name("Add invitation link")
    @FindBy(className = "fa-plus-square")
    public TextBlock addInvitation;

    @Name("Invited Events")
    @FindBy(id = "nav-invited-tab")
    public Link invitedEventsTab;

    @Name("My Events tab")
    @FindBy(id = "nav-my-events-tab")
    public Link myEvents;

    @Name("Wishlist")
    @FindBy(css = "div a.btn-link.wishlist")
    public TextBlock wishlist;

    @Name("View invitation")
    @FindBy(css = "div a.btn-link.send-invite")
    public TextBlock viewInvitation;

    @Name("Event menu dropdown")
    @FindBy(css = "a#dropdownMenuLink")
    public TextBlock dropdownMenuLink;

    @Name("Event menu dropdown list")
    @FindBy(css = "div.event-box-wrapper >div>div.dropdown-menu")
    public TextBlock dropdownMenuList;

    @Name("Event menu dropdown list")
    @FindBy(linkText = "Edit Event")
    public Link editEventLink;

    @Name("Event menu dropdown list")
    @FindBy(linkText = "Cancel Event")
    public Link cancelEventLink;

    @Name("Event menu dropdown list")
    @FindBy(linkText = "Resend Confirmation Link")
    public Link resendConfirmationLink;

    @Name("Event cancellation dialog box")
    @FindBy(className = "mat-dialog-container")
    public WebElement cancellationDialogBox;

    @Name("Event cancellation reason")
    @FindBy(id = "reason")
    public TextInput cancellationReason;

    @Name("Yes button")
    @FindBy(xpath = "//button[contains(text(),'Yes')]")
    public Button Yes;

    @Name("No button")
    @FindBy(xpath = "//button[contains(text(),'No')]")
    public Button No;

    @Name("Event cancellation text")
    @FindBy(css = "div.modal-body>h3")
    public HtmlElement textArea;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Resend Confirmation Link")
    @FindBy(xpath = "//a[contains(text(),'Resend Confirmation Link')]")
    public Link resendInvitationLink;

    @Name("Email Icon")
    @FindBy(xpath = "//a/i[@mattooltip='Resend Invitation']")
    public static Link emailIcon;

    @Name("Social Icon")
    @FindBy(xpath = "//div/a[4]/i")
    public static WebElement facebookIcon;

    @Name("Facebook Icon")
    @FindBy(xpath = "//div[@class='event-bottom']/a[3]")
    public static Link facebookIcon1;

    @Name("wishlist")
    @FindBy(xpath = "//a[@class='btn-link wishlist']")
    public static List<WebElement> wishList;

    @Name("Manage Cart")
    @FindBy(xpath = "//i[@mattooltip='Manage Cart']")
    public static Link manageCart;


    @Name("Home link")
    @FindBy(xpath = "//a[@class='nav-text-color']//span[text()='Home']")
    public Link homeLink;

}
