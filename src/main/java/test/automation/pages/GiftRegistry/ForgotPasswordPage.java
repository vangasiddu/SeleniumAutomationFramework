package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Page;

public class ForgotPasswordPage extends Page {

    public static final String URL = "";
    public static final By VERIFY_BY = By.id("");

    @Name("Email Id")
    @FindBy(id = "identifierId")
    public TextInput email;

    @Name("Send via email button")
    @FindBy(id = "")
    public Button SendViaEmail;

    @Name("Sign In here")
    @FindBy(id = "")
    public HtmlElement SignInHere;


}
