package test.automation.pages.GiftRegistry;


import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class ConfirmPasswordPage extends Page {

    public static final String URL = Config.getUrl() + "";
    public static final By VERIFY_BY = By.id("");

    @Name("New Password")
    @FindBy(id = "")
    public TextInput NewPassword;

    @Name("Confirm Password")
    @FindBy(id = "")
    public TextInput ConfirmPassword;

    @Name("Continue button")
    @FindBy(id = "")
    public Button ContinueButton;

    @Name("Sign In here")
    @FindBy(id = "")
    public HtmlElement SignInHere;

    @Name("Success Message")
    @FindBy(id = "")
    public HtmlElement SuccessMessage;


}
