package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.models.User;

public class ChangePasswordPage extends Page {

    public static final String URL = Config.getUrl() + "/registrant/changePwd";
    public static final By VERIFY_BY = By.cssSelector(".mat-primary, .mat-input-element");

    @Name("Password")
    @FindBy(name = "pwdOld")
    public TextInput password;

    @Name("New Password")
    @FindBy(name = "pwdNew")
    public TextInput newpassword;

    @Name("Confirm Password")
    @FindBy(name = "pwdConf")
    public TextInput confirmpassword;

    @Name("Submit Button")
    @FindBy(xpath = "//button[@type='submit']")
    public Button submitbutton;

    @Name("Reset Button")
    @FindBy(xpath = "//button[@type='reset']")
    public Button resetbutton;

    @Name("Success message")
    @FindBy(className = "alert-success")
    public HtmlElement successmessage;

    @Name("Ok button")
    @FindBy(className = "mat-primary")
    public Button okButton;

    @Name("Change Password Header")
    @FindBy(tagName = "h3")
    public Button changePasswordHeader;

    @Name("Old Password error message")
    @FindBy(xpath = "//div[contains(text(),'Old Password is required')]")
    public HtmlElement OldPswdErrorMsz;

    @Name("New Password error message")
    @FindBy(xpath = "//div[contains(text(),'New Password is required')]")
    public HtmlElement NewPswdErrorMsz;

    @Name("Confirm Password error message")
    @FindBy(xpath = "//div[contains(text(),'Confirm Password is required.')]")
    public HtmlElement ConfirmPswdErrorMsz;

    @Name("Password format error message")
    @FindBy(xpath = "//span[contains(text(),'Password should be min')]")
    public HtmlElement PasswordFormatErrMsz;

    @Name("Password format error message")
    @FindBy(xpath = "//span[contains(text(),'Passwords didn')]")
    public HtmlElement PasswordMisMatchedErrMsz;

    @Name("Password format error message")
    @FindBy(xpath = "//div[contains(text(),'Current password entered is incorrect')]")
    public HtmlElement OldPasswordMisMatchedErrMsz;

    public void submit(User user) {
        password.sendKeys(user.getPassword());
        newpassword.sendKeys(user.getNewpassword());
        confirmpassword.sendKeys(user.getNewpassword());
        submitbutton.click();
    }

    public void reset(User user) {
        password.sendKeys(user.getPassword());
        newpassword.sendKeys(user.getNewpassword());
        confirmpassword.sendKeys(user.getConfirmpassword());
        resetbutton.submit();
    }
}
