package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Browser;
import test.automation.framework.Config;
import test.automation.framework.Page;
import java.util.ArrayList;
import java.util.List;

public class EventManagementPage extends Page {

    public static final String URL = Config.getUrl() + "/store/event-management";
    public static final By VERIFY_BY = By.className("data-container");

    @Name("Cancel Event")
    @FindBy(xpath = "//div[contains(text(),'Cancel_Event')]")
    public static TextBlock CancelEvent_checkbox;

    @Name("Purchase Using Barcode Scanner")
    @FindBy(xpath = "//div[contains(text(),'Purchase_Using_Barcode_Scanner')]")
    public static TextBlock  PurchaseUsingBarcodeScanner_checkbox;

    @Name("Purchase Using Cart Barcode")
    @FindBy(xpath = "//div[contains(text(),'Purchase_Using_Cart_Barcode')]")
    public static TextBlock PurchaseUsingCartBarcode_checkbox;

    @Name("Update Wishlist")
    @FindBy(xpath = "//div[contains(text(),'Update_Wishlist')]")
    public static TextBlock UpdateWishlist_checkbox;

    @Name("View Wishlist")
    @FindBy(xpath = "//div[contains(text(),'View_Wishlist')]")
    public static TextBlock ViewWishlist_checkbox;

    @Name("Create Wishlist using PDT device")
    @FindBy(xpath = "//div[contains(text(),'Create_Wishlist_using_PDT_device')]")
    public static TextBlock CreateWishlistusingPDTdevice_checkbox;

    @Name("Create Wishlist Using Barcode Scanner")
    @FindBy(xpath = "//div[contains(text(),'Create_Wishlist_Using_Barcode_Scanner')]")
    public static TextBlock CreateWishlistUsingBarcodeScanner_checkbox;

    @Name("Re-Send Invitation")
    @FindBy(xpath = "//div[contains(text(),'Re-Send_Invitation')]")
    public static TextBlock ReSendInvitation_checkbox;

    @Name("Confirm Event")
    @FindBy(xpath = "//div[contains(text(),'Confirm_Event')]")
    public static TextBlock ConfirmEvent_checkbox;

    @Name("Update Event")
    @FindBy(xpath = "//div[contains(text(),'Update_Event')]")
    public static TextBlock UpdateEvent_checkbox;

    @Name("View_Events")
    @FindBy(xpath = "//div[contains(text(),'View_Events')]")
    public static TextBlock ViewEvents_checkbox;

    @Name("Create Event")
    @FindBy(xpath = "//div[contains(text(),'Create_Event')]")
    public static TextBlock CreateEvent_checkbox;

    @Name("Resend Confirm Event Link")
    @FindBy(xpath = "//div[contains(text(),'Resend_Confirm_Event_Link')]")
    public static TextBlock ResendConfirmEventLink_checkbox;

    @Name("Send Invitation Via Mail")
    @FindBy(xpath = "//div[contains(text(),'Send_Invitation_Via_Mail')]")
    public static TextBlock SendInvitationViaMail_checkbox;

    @Name("Send Invitation Via Social Media")
    @FindBy(xpath = "//div[contains(text(),'Send_Invitation_Via_Social_Media')]")
    public static TextBlock SendInvitationViaSocialMedia_checkbox;

    @Name("Print Event Card")
    @FindBy(xpath = "//div[contains(text(),'Print_Event_Card')]")
    public static TextBlock ManageWishlist_checkbox;

    @Name("Print Event Card")
    @FindBy(xpath = "//div[contains(text(),'Print_Event_Card')]")
    public static TextBlock PrintEventCard_checkbox;

    @Name("Edit Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[1]")
    public static TextBlock editEvent;

    @Name("Resend Invitation")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[2]")
    public static TextBlock resendInvitation;

    @Name("Confirm Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[3]")
    public static TextBlock confiromEvent;

    @Name("Manage Wishlist")
    @FindBy(xpath = "//div[@class=\"table-responsive user-table\"]/mat-table/mat-row[1]/mat-cell[6]/img[3]")
    public static TextBlock manageWishlist;

    @Name("Cancel Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[4]")
    public static TextBlock cancelEvent;

    @Name("Print Card")
    @FindBy(xpath = "//div[@class=\"table-responsive user-table\"]/mat-table/mat-row/mat-cell[6]/a/i")
    public static TextBlock printCard;

    @Name("Status")
    @FindBy(xpath = "//div[@class=\"table-responsive user-table\"]/mat-table/mat-row/mat-cell[5]")
    public static List<WebElement> status;

    @Name("Search Bar")
    @FindBy(xpath = "//div[@class='search-box']/mat-input-container/div/div/div/input")
    public static TextInput searchBar;

    @Name("Search Button")
    @FindBy(xpath = "//div[@class='search-btn']/button")
    public static Button searchButton;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public TextBlock snackBar;

    @Name("CustomerManagement")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][3]/a")
    public static Link customerManagement;

    @Name("No Event Alert")
    @FindBy(xpath = "//span[contains(text(),'No Event Data Found')]")
    public static TextBlock noEventsAlert;




    public static String eventId() {
        String Url = Browser.getDriver().getCurrentUrl().toString();
        String s = Url.substring(83, Url.length());
        return s;
    }


}
