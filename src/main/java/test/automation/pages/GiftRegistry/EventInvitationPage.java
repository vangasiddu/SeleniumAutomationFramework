package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class EventInvitationPage extends Page {

    public static final String URL = Config.getUrl() + "/events/event-invitation?";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'EVENT INVITATION')]");

    @Name("Header")
    @FindBy(xpath = "//h2[contains(text(),'EVENT INVITATION')]")
    public TextBlock header;

    @Name("Edit button")
    @FindBy(id = "btn-edit")
    public Button edit;

    @Name("Save and Continue button")
    @FindBy(linkText = "Save & Continue")
    public Button saveAndContinue;

    @Name("Event date")
    @FindBy(xpath =  "//p/eventdate")
    public TextBlock eventdate;

    @Name("Go To Wishlist")
    @FindBy(xpath = "//a[@class='btn btn-secondary']")
    public Button wishlistBtn;

    @Name("Event time")
    @FindBy(xpath = "//p/eventtime")
    public TextBlock eventtime;

    @Name("Event Venue")
    @FindBy(xpath = "//p/eventvenue")
    public TextBlock eventvenue;

    @Name("Celebrant phonenumber")
    @FindBy(xpath = "//span/phonenumber")
    public TextBlock celebrantphonenumber;

    @Name("Celebrant phonenumber")
    @FindBy(tagName = "celebrantname")
    public TextBlock celebrantname;

    @Name("Event state")
    @FindBy(xpath = "//p/eventstate")
    public TextBlock eventState;

    @Name("Event city")
    @FindBy(xpath = "//p/eventcity")
    public TextBlock eventCity;

    @Name("Template2")
    @FindBy(css = "ul.theme-list > li:nth-child(2) > a")
    public Link template2;

    @Name("Template1")
    @FindBy(xpath = "//ul[@class='theme-list']//a")
    public Link template1;

    @Name("My Events")
    @FindBy(xpath = "//a[@class='nav-text-color active']")
    public static Link myEvents;

   @Name("Register SignIn")
    @FindBy(xpath = "//*[@tabindex=\"-1\"]/div[2]/div[1]/button")
    public static WebElement registerSignIn;

    @Name("Skip Register")
    @FindBy(xpath = "//*[@tabindex=\"-1\"]/div[2]/div[2]/button")
    public static WebElement skipRegister;


}
