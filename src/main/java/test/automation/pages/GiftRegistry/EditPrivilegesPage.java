package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;


public class EditPrivilegesPage extends Page {
    public static final String URL = Config.getUrl() + "/store/role/edit";
    public static final By VERIFY_BY = By.className("data-container");

    @Name("update_privilege")
    @FindBy(xpath = "//div[contains(text(),'Update_Privilege')]")
    public static TextBlock Update_Privilege;

    @Name("delete_role")
    @FindBy(xpath = "//div[contains(text(),'Delete_Role')]")
    public static TextBlock Delete_Role;

    @Name("create_role")
    @FindBy(xpath = "//div[contains(text(),'Create_Role')]")
    public static TextBlock Create_Role;

    @Name("view_roles")
    @FindBy(xpath = "//div[contains(text(),'View_Roles')]")
    public static TextBlock View_Roles;

    @Name("update_role_name")
    @FindBy(xpath = "//div[contains(text(),'Update_Role_Name')]")
    public static TextBlock Update_Role_Name;

    @Name("Role Management")
    @FindBy(xpath = "//div[contains(text(),'Role Management')]")
    public static TextBlock Role_Management;

    @Name("User Management")
    @FindBy(xpath ="//div[contains(text(),'User Management')]")
    public static TextBlock User_Management;

    @Name("Customer Management")
    @FindBy(xpath ="//div[contains(text(),'Customer Management')]")
    public static TextBlock Customer_Management;

    @Name("Event Management")
    @FindBy(xpath ="//div[contains(text(),'Event Management')]")
    public static TextBlock Event_Management;

    @Name("Configuration Management")
    @FindBy(xpath ="//div[contains(text(),'Configuration Management')]")
    public static TextBlock Configuration_Management;

    @Name("Assign roles")
    @FindBy(xpath ="//div[contains(text(),'Assign_Roles')]")
    public static TextBlock Assign_Roles;

    @Name("View Users")
    @FindBy(xpath ="//div[contains(text(),'View_Users')]")
    public static TextBlock View_Users;

    @Name("Register Customer")
    @FindBy(xpath ="//div[contains(text(),'Register_Customer')]")
    public static TextBlock Register_Customer;

    @Name("View Customer")
    @FindBy(xpath ="//div[contains(text(),'View_Customers')]")
    public static TextBlock View_Customers;

    @Name("Resend Confirm Registration Link")
    @FindBy(xpath ="//div[contains(text(),'Resend_Confirm_Registration_Link')]")
    public static TextBlock Resend_Confirm_Registration_Link;

    @Name("Manage Configurations")
    @FindBy(xpath ="//div[contains(text(),'Manage_Configurations')]")
    public static TextBlock Manage_Configurations;

    @Name("view configurations")
    @FindBy(xpath ="//div[contains(text(),'View_Configurations')]")
    public static TextBlock View_Configurations;

    @Name("product attributes configurations")
    @FindBy(xpath ="//div[contains(text(),'Product_Attributes_Configuration-OMNI')]")
    public static TextBlock Product_Attributes_Configuration_OMNI;

    @Name("select role")
    @FindBy(name = "roleName")
    public static TextBlock SelectRole;

    @Name("Roles List")
    @FindBy(className = "mat-option-text")
    public static List<WebElement> rolesList;

    @Name("Selected role on Drop down")
    @FindBy(className = "mat-option")
    public static List<TextBlock> matOption;

    @Name("Role Row")
    @FindBy(className = "mat-row")
    public static List<WebElement> userRoleRow;

    @Name("Checked checkbox")
    @FindBy(className = "mat-checkbox-checked")
    public static List<WebElement> checkedCheckbox;

    @Name("Roles select checkbox")
    @FindBy(className = "mat-checkbox-layout")
    public static CheckBox roleSelectCheckBox;

    @Name("Select Role List")
    @FindBy(className = "ng-trigger")
    public static List<WebElement> allRoles;

    @Name("save")
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    public static TextBlock save;

    @Name("back")
    @FindBy(xpath = "//span[contains(text(),'Back')]")
    public static TextBlock back;

    @Name("components")
    @FindBy(className = "privilegesMap")
    public static List<WebElement> components;

    @Name("role_name")
    @FindBy(xpath = "//*[@id=\"mat-select-1\"]//span")
    public static TextBlock role_name;

    @Name("Store User Role")
    @FindBy(className = "mat-select-value")
    public static TextBlock storeUserRole;

    @Name("cancel_event")
    @FindBy(xpath = "//div[contains(text(),'Cancel_Event')]")
    public static TextBlock CancelEvent;

    @Name("PurchaseUsingBarcodeScanner")
    @FindBy(xpath = "//div[contains(text(),'Purchase_Using_Barcode_Scanner')]")
    public static TextBlock PurchaseUsingBarcodeScanner;

    @Name("PurchaseUsingCartBarcode")
    @FindBy(xpath = "//div[contains(text(),'Purchase_Using_Cart_Barcode')]")
    public static TextBlock PurchaseUsingCartBarcode;

    @Name("UpdateWishlist")
    @FindBy(xpath = "//div[contains(text(),'Update_Wishlist')]")
    public static TextBlock UpdateWishlist;

    @Name("ViewWishlist")
    @FindBy(xpath = "//div[contains(text(),'View_Wishlist')]")
    public static TextBlock ViewWishlist;

    @Name("Create_Wishlist_using_PDT_device")
    @FindBy(xpath = "//div[contains(text(),'Create_Wishlist_using_PDT_device')]")
    public static TextBlock CreateWishlistusingPDTdevice;

    @Name("CreateWishlistUsingBarcodeScanner")
    @FindBy(xpath = "//div[contains(text(),'Create_Wishlist_Using_Barcode_Scanner')]")
    public static TextBlock CreateWishlistUsingBarcodeScanner;

    @Name("Re-SendInvitation")
    @FindBy(xpath = "//div[contains(text(),'Re-Send_Invitation')]")
    public static TextBlock ReSendInvitation;

    @Name("Confirm_Event")
    @FindBy(xpath = "//div[contains(text(),'Confirm_Event')]")
    public static TextBlock ConfirmEvent;

    @Name("Update_Event")
    @FindBy(xpath = "//div[contains(text(),'Update_Event')]")
    public static TextBlock UpdateEvent;

    @Name("View_Events")
    @FindBy(xpath = "//div[contains(text(),'View_Events')]")
    public static TextBlock ViewEvents;

    @Name("Create_Event")
    @FindBy(xpath = "//div[contains(text(),'Create_Event')]")
    public static TextBlock CreateEvent;

    @Name("Resend_Confirm_Event_Link")
    @FindBy(xpath = "//div[contains(text(),'Resend_Confirm_Event_Link')]")
    public static TextBlock ResendConfirmEventLink;

    @Name("Send_Invitation_Via_Mail")
    @FindBy(xpath = "//div[contains(text(),'Send_Invitation_Via_Mail')]")
    public static TextBlock SendInvitationViaMail;

    @Name("Send_Invitation_Via_Social_Media")
    @FindBy(xpath = "//div[contains(text(),'Send_Invitation_Via_Social_Media')]")
    public static TextBlock SendInvitationViaSocialMedia;

    @Name("Manage_Wishlist")
    @FindBy(xpath = "//div[contains(text(),'Manage_Wishlist')]")
    public static TextBlock ManageWishlist;

    @Name("Print_Event_Card")
    @FindBy(xpath = "//div[contains(text(),'Print_Event_Card')]")
    public static TextBlock PrintEventCard;

    @Name("Edit Privileges Dialog Box")
    @FindBy(xpath = "//mat-dialog-container[@role='dialog']")
    public static List<WebElement> dialogBox;

    @Name("Ok Button")
    @FindBy(xpath = "//button[contains(text(),'OK')]")
    public static Button okButton;

    @Name("Cancel Button")
    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    public static Button cancelButton;

    @Name("Privilege Rows")
    @FindBy(className = "privilege")
    public static List<WebElement> privilegesList;

    @Name("privileges updation message")
    @FindBy(className = "alert-success")
    public static TextBlock privilegesupdationMessage;

    @Name("update_privilege_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-6-input\"]/..")
    public static CheckBox updatePrivilege;

    @Name("delete_privilege_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-7-input\"]/..")
    public static CheckBox deletePrivilege;

    @Name("create_privilege_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-8-input\"]/..")
    public static CheckBox CreateRole;

    @Name("view_roles_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-9-input\"]/..")
    public static CheckBox ViewRoles;

    @Name("update_role_name_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-10-input\"]/..")
    public static CheckBox UpdateRoleName;

    @Name("assign_roles_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-11-input\"]/..")
    public static CheckBox Assign_Roles_checkbox;

    @Name("view_users_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-12-input\"]/..")
    public static CheckBox View_Users_checkbox;

    @Name("register_customer_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-13-input\"]/..")
    public static CheckBox Register_Customer_checkbox;

    @Name("view_customer_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-14-input\"]/..")
    public static CheckBox View_Customers_checkbox;

    @Name("resend_confirm_registration_link_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-15-input\"]/..")
    public static CheckBox Resend_Confirm_Registration_Link_checkbox;

    @Name("Manage_Configurations_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-33-input\"]/..")
    public static CheckBox Manage_Configurations_checkbox;

    @Name("View_Configurations_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-34-input\"]/..")
    public static CheckBox View_Configurations_checkbox;

    @Name("Product_Attributes_Configuration_OMNI_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-35-input\"]/..")
    public static CheckBox Product_Attributes_Configuration_OMNI_checkbox;

    @Name("Role Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][1]/a")
    public static Link roleManagement;

    @Name("User Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][2]/a")
    public static Link userManagement;

    @Name("Customer Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][3]/a")
    public static Link customerManagement;

    @Name("Event Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][4]/a")
    public static Link eventManagement;

    @Name("Configuration")
    @FindBy(xpath = "//li[@class=\"nav-item dropdown ng-star-inserted\"]/a")
    public static Link  Configuration;

    @Name("CancelEvent_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-226-input\"]/..")
    public static CheckBox CancelEvent_checkbox;

    @Name("PurchaseUsingBarcodeScanner_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-227-input\"]/..")
    public static CheckBox PurchaseUsingBarcodeScanner_checkbox;

    @Name("PurchaseUsingCartBarcode_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-228-input\"]/..")
    public static CheckBox PurchaseUsingCartBarcode_checkbox;

    @Name("UpdateWishlist_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-229-input\"]/..")
    public static CheckBox UpdateWishlist_checkbox;

    @Name("ViewWishlist_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-230-input\"]/..")
    public static CheckBox ViewWishlist_checkbox;

    @Name("CreateWishlistusingPDTdevice_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-231-input\"]/..")
    public static CheckBox CreateWishlistusingPDTdevice_checkbox;

    @Name("CreateWishlistUsingBarcodeScanner_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-232-input\"]/..")
    public static CheckBox CreateWishlistUsingBarcodeScanner_checkbox;

    @Name("ReSendInvitation_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-233-input\"]/..")
    public static CheckBox ReSendInvitation_checkbox;

    @Name("ConfirmEvent_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-234-input\"]/..")
    public static CheckBox ConfirmEvent_checkbox;

    @Name("UpdateEvent_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-235-input\"]/..")
    public static CheckBox UpdateEvent_checkbox;

    @Name("ViewEvents_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-236-input\"]/..")
    public static CheckBox ViewEvents_checkbox;

    @Name("CreateEvent_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-237-input\"]/..")
    public static CheckBox CreateEvent_checkbox;

    @Name("ResendConfirmEventLink_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-238-input\"]/..")
    public static CheckBox ResendConfirmEventLink_checkbox;

    @Name("SendInvitationViaMail_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-239-input\"]/..")
    public static CheckBox SendInvitationViaMail_checkbox;

    @Name("SendInvitationViaSocialMedia_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-240-input\"]/..")
    public static CheckBox SendInvitationViaSocialMedia_checkbox;

    @Name("ManageWishlist_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-241-input\"]/..")
    public static CheckBox ManageWishlist_checkbox;

    @Name("PrintEventCard_checkbox")
    @FindBy(xpath = "//input[@id=\"mat-checkbox-242-input\"]/..")
    public static CheckBox PrintEventCard_checkbox;

}
