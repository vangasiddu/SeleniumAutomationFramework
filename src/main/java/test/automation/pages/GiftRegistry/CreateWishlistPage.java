package test.automation.pages.GiftRegistry;

import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Actions;
import test.automation.framework.Browser;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.panels.GiftRegistry.HeaderPanel;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class CreateWishlistPage extends Page {
    public static final String URL = Config.getUrl() + "/events/create-wish-list";
    public static final By VERIFY_BY = By.cssSelector("a.btn.btn-outline-secondary");

   public static String skuDetails = null;

   public static HeaderPanel headerPanel;

    @Name("My wishlist link")
    @FindBy(css="a.btn.btn-outline-secondary")
    public static Link myWishlistLink;

    @Name("My wishlist count")
    @FindBy(css="a.btn.btn-outline-secondary span")
    public TextBlock myWishlistCount;

    @Name("Shop category dropdown")
    @FindBy(id="dropdownMenuLink")
    public static Select shopCatagoryDropdown;

    @Name("Crate and Barrel drop down  ")
    @FindBy(xpath="//a[contains(text(),'Crate And Barrel')]")
    public static Select crateBarrelDropDown;

    @Name("Wishlist message")
    @FindBy(tagName = "h2")
    public TextBlock wishlistMessage;

    @Name("Product container")
    @FindBy(css="div.product-box.active")
    public static List<TextBlock> productContainer;

    @Name("Product")
    @FindBy(css="div.product-box.active")
    public static WebElement product;

    @Name("Add to wishlist button")
    @FindBy(css="input[value='Add to Wishlist']")
    public static Button addToWishListBtn;

    @Name("Quantity")
    @FindBy(css="input.form-control.ng-untouched.ng-pristine")
    public TextInput quantity;

    @Name("SKU id")
    @FindBy(xpath="//p[contains(text(),'SKU')]")
    public TextBlock skuId;

    @Name("Select Category")
    @FindBy(id="dropdownMenuLink")
    public static Link dropdownMenuLink;

    @Name("Sub Categories menu items")
    @FindBy(xpath="//*[@class='submenu']")
    public static List<WebElement> subMenus;

    @Name("Products image")
    @FindBy(xpath="//figure/a")
    public static List<TextBlock> productsImage;

    @Name("Product Image")
    @FindBy(xpath = "//figure/a")
    public static Link productImage;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Add To WishList")
    @FindBy(css = "input[value='Add to Wishlist']")
    public static Button addToWishList;

    @Name("product")
    @FindBy(xpath = "//div[@class='product-box active']/span")
    public static Link singleProduct;

    @Name("My Events")
    @FindBy(xpath = "//a[@class='nav-text-color active']")
    public static Link myEvents;

    public static void selectRandomCategory() throws InterruptedException {
        dropdownMenuLink.click();
        //crateBarrelDropDown.click();
        Assert.assertTrue("Error - App:- Sub category menus are not displaying",subMenus.size()>0);
        WebElement element = subMenus.size() == 1 ? subMenus.get(0) : subMenus.get(new Random().nextInt(subMenus.size() - 1));
        Actions.hoverForSelection(element);
        List<WebElement> subCategories = element.findElements(By.xpath("//ul[@class='submenu']//ul//li//ul//a"));
        Actions.jsClick(subCategories.get(new Random().nextInt(subCategories.size() - 1)));
    }

    public void selectRandomProduct(String quantity){
        Random random = new Random();
        WebElement option = productContainer.get(random.nextInt(productContainer.size()-1));
        skuDetails = option.findElement(By.tagName("p")).getText().replace("SKU - ", "");
        option.findElement(By.cssSelector("input.form-control.ng-untouched.ng-pristine")).clear();
        option.findElement(By.cssSelector("input.form-control.ng-untouched.ng-pristine")).sendKeys(quantity);
        option.findElement(By.cssSelector("input[value='Add to Wishlist']")).click();
    }

    public void selectProduct(){
        Random random = new Random();
        WebElement option = productContainer.get(random.nextInt(productContainer.size()-1));
        skuDetails = option.findElement(By.tagName("p")).getText().replace("SKU - ", "");
        option.findElement(By.tagName("a")).click();
    }

}
