package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;

public class UserManagementPage extends Page {

    public static final String URL = Config.getUrl() + "store/user";
    public static final By VERIFY_BY = By.className("main-container");

    @Name("User Management Header")
    @FindBy(xpath = "//div[contains(text(),'User Management')]")
    public TextBlock userManagementheader;

    @Name("User Name")
    @FindBy(className = "mat-column-lastName")
    public static TextBlock userName;

    @Name("search celebrant")
    @FindBy(name = "searchCelebrant")
    public static TextInput searchCelebrant;

    @Name("Company Header")
    @FindBy(className = "mat-column-companyName")
    public static TextBlock companyHeader;

    @Name("Company List")
    @FindBy(className = "mat-column-companyName")
    public static List<TextBlock> companyList;

    @Name("Branch Header")
    @FindBy(className = "mat-column-storeName")
    public static TextBlock branchHeader;

    @Name("Branch")
    @FindBy(className = "mat-column-storeName")
    public static List<TextBlock> branchesList;

    @Name("Actions Header")
    @FindBy(className = "mat-column-Actions")
    public static TextBlock actionsHeader;

    @Name("Assign Role Link")
    @FindBy(linkText="Assign Role")
    public static Link assignRole;

    @Name("Assign Role Links")
    @FindBy(className="store-action")
    public static List<Link> assignRoleLinks;

    @Name("Search Button")
    @FindBy(xpath = "//button[@type='submit']")
    public static Button searchButton;

    @Name("User Rows")
    @FindBy(className = "mat-row")
    public static List<WebElement> userRows;

    @Name("User Cells")
    @FindBy(className = "mat-cell")
    public static List<WebElement> userCells;

    @Name("Alert Message")
    @FindBy(className = "alert")
    public static TextBlock errorMessage;

    @Name("User Management")
    @FindBy(linkText = "User Management")
    public Link userManagement;

    @Name("nextpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-next mat-icon-button\"]")
    public static Button nextpage;

    @Name("previouspage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-previous mat-icon-button\"]")
    public static Button previouspage;

    @Name("lastpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-last mat-icon-button ng-star-inserted\"]")
    public static Button lastpage;

    @Name("firstpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-first mat-icon-button ng-star-inserted\"]")
    public static Button firstpage;

    @Name("next")
    @FindBy(xpath = "//div[@class=\"mat-select-value\"]")
    public static TextBlock defaultPagination;

    @Name("pagination range")
    @FindBy(className = "mat-paginator-range-label")
    public static TextBlock paginationRange;

    @Name("Pagination Value")
    @FindBy(className = "mat-option-text")
    public static List<WebElement> paginationValue;

}

