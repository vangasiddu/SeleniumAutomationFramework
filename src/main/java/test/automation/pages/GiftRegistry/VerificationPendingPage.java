package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class VerificationPendingPage extends Page {

    public static final String URL = Config.getUrl() + "/registrant/verification-pending";
    public static final By VERIFY_BY = By.tagName("h5");

    @Name("Registration Confirmation meassage")
    @FindBy(css= "div[class=ng-star-inserted]>div>p")
    public static HtmlElement message;

    @Name("Resend Verification Link")
    @FindBy(className= "btn-primary")
    public static Button resendVerificationLink;
}
