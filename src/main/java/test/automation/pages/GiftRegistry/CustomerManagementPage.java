package test.automation.pages.GiftRegistry;

import javafx.scene.control.Tab;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.utils.UserUtils;

import java.util.List;

public class CustomerManagementPage extends Page {
    public static final String URL = Config.getUrl() + "store/customer-management";
    public static final By VERIFY_BY = By.className("data-header");

    @Name("Top Navigation Links")
    @FindBy(className = "nav-link")
    public static List<WebElement> topNavLinks;

    @Name("User Management")
    @FindBy(linkText = "Users")
    public Link userManagement;

    @Name("Event Management")
    @FindBy(xpath ="//a[contains(text(),'Event Management')]")
    public static Link eventManagement;

    @Name("Customer Management Header")
    @FindBy(xpath = "//div[contains(text(),'Customer Management')]")
    public TextBlock customerManagementheader;

    @Name("First Name")
    @FindBy(xpath = "//button[contains(text(),'First Name')]")
    public static Button firstName;

    @Name("Last Name")
    @FindBy(xpath = "//button[contains(text(),'Last Name')]")
    public static Button lastName;

    @Name("Email")
    @FindBy(xpath = "//button[contains(text(),'Email')]")
    public static Button email;

    @Name("Pagination")
    @FindBy(className = "mat-paginator-container")
    public TextBlock pagination;

    @Name("Status")
    @FindBy(xpath = "//button[contains(text(),'Status')]")
    public Button status;

    @Name("Customer Status")
    @FindBy(className = "mat-cell")
    public static TextBlock custStatus;

    @Name("Create New Customer Link")
    @FindBy(xpath = "//i[@mattooltip=\"Create New Customer\"]")
    public TextBlock createNewCustomerIcon;

    @Name("refresh data icon")
    @FindBy(xpath = "//i[@mattooltip=\"Refresh Data\"]")
    public TextBlock refreshDataIcon;

    @Name("Store User Create Event")
    @FindBy(xpath = "//img[@mattooltip=\"Create Event\"]")
    public TextBlock createEvent;

    @Name("view event link")
    @FindBy(xpath = "//img[@mattooltip=\"View Events\"]")
    public TextBlock viewEvents;

    @Name("Resend Confirmation link")
    @FindBy(xpath = "//img[@class=\"store-action\"]")
    public TextBlock resendConfirmationLink;

    @Name("nextpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-next mat-icon-button\"]")
    public static Button nextpage;

    @Name("previouspage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-previous mat-icon-button\"]")
    public static Button previouspage;

    @Name("lastpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-last mat-icon-button ng-star-inserted\"]")
    public static Button lastpage;

    @Name("firstpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-first mat-icon-button ng-star-inserted\"]")
    public static Button firstpage;

    @Name("next")
    @FindBy(xpath = "//div[@class=\"mat-select-value\"]")
    public static TextBlock defaultPagination;

    @Name("pagination range")
    @FindBy(className = "mat-paginator-range-label")
    public static TextBlock paginationRange;

    @Name("actions")
    @FindBy(css = "mat-header-cell.cdk-column-ACTION")
    public static TextBlock action;

    @Name("Search Button")
    @FindBy(xpath = "//button[@type='submit']")
    public static Button searchButton;

    @Name("Role Management tab")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][1]/a")
    public static Link RoleManagement;

    @Name("Configurarion")
    @FindBy(xpath = "//li[@class=\"nav-item dropdown ng-star-inserted\"]")
    public static Link configuration;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("search celebrant")
    @FindBy(name = "searchCelebrant")
    public static TextInput searchCelebrant;

    @Name("Customer Details")
    @FindBy(className = "mat-row")
    public static List<WebElement> customerDetails;

    @Name("Customer Values")
    @FindBy(className = "mat-cell")
    public static List<WebElement> customerValues;

    @Name("firstname")
    @FindBy(xpath = "//div//mat-row[1]/mat-cell[1]")
    public static TextBlock firstname;

    @Name("lastname")
    @FindBy(xpath = "//div//mat-row[1]/mat-cell[2]")
    public static TextBlock lastname;

    @Name("emailId")
    @FindBy(xpath = "//div//mat-row[1]/mat-cell[3]")
    public static TextBlock emailId;

    @Name("Status")
    @FindBy(xpath = "//div//mat-row[1]/mat-cell[4]")
    public static TextBlock Status;

    @Name("firstNamesList")
    @FindBy(xpath = "//div//mat-row/mat-cell[1]")
    public static List<TextBlock> firstNamesList;

    @Name("lastNameList")
    @FindBy(xpath = "//div//mat-row/mat-cell[2]")
    public static List<TextBlock> lastNameList;

    @Name("emailIdList")
    @FindBy(xpath = "//div//mat-row/mat-cell[3]")
    public static List<TextBlock> emailIdList;

    @Name("Status List")
    @FindBy(xpath = "//div//mat-row/mat-cell[4]")
    public static List<TextBlock> statusList;

    @Name("User Logged in Company Name")
    @FindBy(className = "company-name")
    public static TextBlock loggedInCompanyName;

    @Name("User Rows")
    @FindBy(className = "mat-row")
    public static List<WebElement> userRows;

    @Name("User Cells")
    @FindBy(className = "mat-cell")
    public static List<WebElement> userCells;

    @Name("Pagination Value")
    @FindBy(className = "mat-option-text")
    public static List<WebElement> paginationValue;

    @Name("Rows Sorting")
    @FindBy(className = "mat-sort-header-arrow")
    public static WebElement sortingRows;

    @Name("Header Options")
    @FindBy(className = "mat-sort-header-button")
    public static List<WebElement> headerRows;

    public static String getValidUserEmail(String input){
        String email = UserUtils.getValidUser(input).getEmail();
        return email;
    }

}
