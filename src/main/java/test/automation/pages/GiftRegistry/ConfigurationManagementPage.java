package test.automation.pages.GiftRegistry;

import javafx.scene.control.Tab;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class ConfigurationManagementPage extends Page {

    public static final String URL = Config.getUrl() + "/store/configurations";
    public static final By VERIFY_BY = By.className("data-container");


    @Name("Event")
    @FindBy(xpath = "//div[2][@role='tab']")
    public static Link event;

    @Name("Event Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][4]/a")
    public static Link eventManagement;

    @Name("Event Button")
    @FindBy(xpath = "//div[@class='text-center']/div[1]/div/p[2]/a")
    public static Link eventButton;

    @Name("Event Confirmation Channel")
    @FindBy(xpath = "//*[@data-target=\"#24\"]/h5/button")
    public static Button eventConfirmationChannel;

    @Name("WishList")
    @FindBy(xpath = "//*[@data-target=\"#56\"]/h5/button")
    public static Button onlineWishlist;

    @Name("Either Radio Button")
    @FindBy(xpath = "//*[@value='EMAIL_OR_STORE']//div[@class='mat-radio-container']")
    public static Radio eitherRadioButton;

    @Name("Either")
    @FindBy(xpath = "//*[@value='EMAIL_OR_STORE']")
    public static Button either;

    @Name("Print Card Event")
    @FindBy(xpath = "//*[contains(text(),'PRINT_EVENT_CARD')]")
    public static Button printCardChannel;

    @Name("WishList Most Loved")
    @FindBy(xpath = "//*[contains(text(),'WISHLIST_MOST_LOVED_ICON')]")
    public  static Button wishListLovedChannel;

    @Name("Yes")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='YES']")
    public static Button yes;

    @Name("Yes Radio Button")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='YES']//*[@class='mat-radio-container']")
    public static Radio yesRadioButton;

    @Name("No")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='NO']")
    public static Button no;

    @Name("No Radio Button")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='NO']//*[@class='mat-radio-container']")
    public static Radio noRadioButton;

    @Name("Email or SocailMedia")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='EMAIL_OR_SOCIAL_MEDIA']")
    public static Button emailORSocialMedia;

    @Name("Email or SocailMedia Radio Button")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='EMAIL_OR_SOCIAL_MEDIA']//*[@class='mat-radio-container']")
    public static Button emailORSocialMediaRadioButton;

    @Name("SocialMedia")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='SOCIAL_MEDIA']")
    public static Button socialMedia;

    @Name("SocialMedia Radio Button")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='SOCIAL_MEDIA']//*[@class='mat-radio-container']")
    public static Button socialMediaRadioButton;

    @Name("Email")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='EMAIL']")
    public static Button email;

    @Name("Email Radio Button")
    @FindBy(xpath = "//*[@class='collapse show']//*[@value='EMAIL']//*[@class='mat-radio-container']")
    public static Button emailRadioButton;

    @Name("CustomerManagement")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][3]/a")
    public static Link customerManagement;

    @Name("Event Code")
    @FindBy(xpath = "//div[@class='text-center']/h4")
    public static Link eventcode;

    @Name("Configuration")
    @FindBy(xpath = "//li[@class=\"nav-item dropdown ng-star-inserted\"]/a")
    public static Link configuration;

    @Name("Search Button")
    @FindBy(xpath = "//div[@class='search-box']/mat-input-container/div/div/div/input")
    public static TextInput searchBar;

    @Name("Search Button")
    @FindBy(xpath = "//div[@class='search-btn']/button")
    public static Button searchButton;

    @Name("Registration Expiry")
    @FindBy(xpath = "//*[@id=\"heading1\"]/h5")
    public static Button registrationEmailExpiry;

    @Name("Registration")
    @FindBy(xpath = "//*[@id=\"mat-tab-label-0-0\"]/div")
    public static Link registration;

    @Name("WishList no button")
    @FindBy(xpath = "//mat-radio-button[@id='mat-radio-78']")
    public static Button wishlistNo;

    @Name("Event Invitation Channel")
    @FindBy(xpath = "//button[text()='EVENT_INVITATION_CHANNEL']")
    public static Button invitationChannel;

    @Name("Purchase")
    @FindBy(xpath = "//*[@role='tab'][3]/div")
    public static Link purchase;

    @Name("ByOnline Facility")
    @FindBy(xpath = "//*[@data-target='#68']")
    public static Button byOnlineFacility;

    @Name("ByOnlie_NO")
    @FindBy(xpath = "//mat-radio-button[@id='mat-radio-23']")
    public static Button byonlineNo;

    @Name("ByOnline_No Radio Button")
    @FindBy(xpath = "//mat-radio-button[@id='mat-radio-23']//input")
    public static Button byonlineNoRadioButton;

    @Name("Event Confirmation Expiry")
    @FindBy(xpath = "//button[contains(text(),'EVENT_CONFIRMATION_EMAIL_EXPIRY_TIME_IN_MINUTES')]")
    public static Button eventConfirmationExpiry;

    @Name("Event Expiry Time")
    @FindBy(xpath = "//input[@id='mat-input-10']")
    public static TextInput emailExpiryTime;

    @Name("Event Expiry Message")
    @FindBy(xpath = "//h2[contains(text(),'Your event verification link got expired')]")
    public static Link eventExpiryMessage;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Lead Time Create Event In Days")
    @FindBy(xpath = "//button[contains(text(),'LEAD_TIME_PRIOR_TO_CREATE_EVENT_IN_DAYS')]")
    public static Button leadTimePrior;

    @Name("Place Holder")
    @FindBy(xpath = "//input[@placeholder='In Days']")
    public static TextInput placeHolder;

    @Name("Guest Registration")
    @FindBy(xpath = "//button[contains(text(),'GUEST_REGISTRATION_REQUIRED_TO_VIEW_EVENT_DETAILS')]")
    public static Link guestRegistration;


}
