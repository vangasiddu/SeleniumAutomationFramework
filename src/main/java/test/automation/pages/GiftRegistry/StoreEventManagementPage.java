package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.w3c.dom.Text;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.ArrayList;
import java.util.List;

public class StoreEventManagementPage extends Page {
    public static final String URL = Config.getUrl() + "/store/event-management";
    public static final By VERIFY_BY = By.className("data-header");

    @Name("Confirm Event Registration")
    @FindBy(xpath = "//div[contains(text(),'Please confirm the event registration through the link we')]")
    public static TextBlock ConfirmationMessage;

    @Name("Role Management")
    @FindBy(xpath = "//a[contains(text(),'Role Management')]")
    public static TextBlock roleManagement;

    @Name("User Management")
    @FindBy(xpath = "//a[contains(text(),'User Management')]")
    public static TextBlock userManagement;

    @Name("Customer Management")
    @FindBy(xpath = "//a[contains(text(),'Customer Management')]")
    public static TextBlock customerManagement;

    @Name("Event Management")
    @FindBy(xpath = "//a[contains(text(),'Event Mangement')]")
    public static TextBlock eventManagement;

    @Name("Configuration Management")
    @FindBy(xpath = "//a[contains(text(),'Configuration')]")
    public static TextBlock configurationManagement;

    @Name("Page Name")
    @FindBy(className = "data-header")
    public static TextBlock pageName;

    @Name("Search Field")
    @FindBy(xpath = "//div[@class='search-box']/mat-input-container/div/div/div/input")
    public static TextInput searchBar;

    @Name("Search Button")
    @FindBy(xpath = "//div[@class='search-btn']/button")
    public static Button searchButton;

    @Name("Previous Page")
    @FindBy(xpath = "//a[@mattooltip='Previous page']")
    public static WebElement previousPage;

    @Name("Refresh Data")
    @FindBy(xpath = "//a[@mattooltip='Refresh Data']")
    public static WebElement refreshData;

    @Name("Event Code Column Header Text")
    @FindBy(xpath = "//button[contains(text(),'Event Code')]")
    public static TextBlock eventCode;

    @Name("Celebrant Name Column Header Text")
    @FindBy(xpath = "//button[contains(text(),'Celebrant Name')]")
    public static TextBlock celebrantName;

    @Name("Event Type Column Header Text")
    @FindBy(xpath = "//button[contains(text(),'Event Type')]")
    public static TextBlock eventType;

    @Name("Event Date Column Header Text")
    @FindBy(xpath = "//button[contains(text(),'Event Date')]")
    public static TextBlock eventDate;

    @Name("Status Header Text")
    @FindBy(xpath = "//button[contains(text(),'Status')]")
    public static TextBlock eventStatus;

    @Name("Actions Column Header Text")
    @FindBy(xpath = "//button[contains(text(),'Actions')]")
    public static TextBlock actions;

    @Name("Pagination Container")
    @FindBy(xpath = "//mat-paginator[@class='mat-paginator']")
    public static TextBlock paginationContainer;

    @Name("No Event Alert")
    @FindBy(xpath = "//span[contains(text(),'No Event Data Found')]")
    public static TextBlock noEventsFoundAlert;

    @Name("User Row")
    @FindBy(className = "mat-row")
    public static WebElement userRow;

    @Name("User Rows")
    @FindBy(className = "mat-row")
    public static List<WebElement> userRows;

    @Name("nextpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-next mat-icon-button\"]")
    public static Button nextpage;

    @Name("previouspage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-previous mat-icon-button\"]")
    public static Button previouspage;

    @Name("lastpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-last mat-icon-button ng-star-inserted\"]")
    public static Button lastpage;

    @Name("firstpage pagination")
    @FindBy(xpath = "//button[@class=\"mat-paginator-navigation-first mat-icon-button ng-star-inserted\"]")
    public static Button firstpage;

    @Name("next")
    @FindBy(xpath = "//div[@class=\"mat-select-value\"]")
    public static TextBlock defaultPagination;

    @Name("pagination range")
    @FindBy(className = "mat-paginator-range-label")
    public static TextBlock paginationRange;

    @Name("Pagination Value")
    @FindBy(className = "mat-option-text")
    public static List<WebElement> paginationValue;

    @Name("eventCodeList")
    @FindBy(xpath = "//div//mat-row/mat-cell[1]")
    public static List<TextBlock> eventCodeList;

    @Name("celebrantNamesList")
    @FindBy(xpath = "//div//mat-row/mat-cell[2]")
    public static List<TextBlock> celebNamesList;

    @Name("eventTypeList")
    @FindBy(xpath = "//div//mat-row/mat-cell[3]")
    public static List<TextBlock> emailList;

    @Name("Cancel Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[9]/img[3]")
    public static TextBlock cancelEvent;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Confirm Event")
    @FindBy(xpath = "//img[@mattooltip='Confirm Event']")
    public static Link Confirmevent;

    @Name("Edit Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[1]")
    public static TextBlock editEvent;

    @Name("Resend Invitation")
    @FindBy(xpath = "//img[@mattooltip='Send Invitation via mail']")
    public static TextBlock resendInvitation;

    @Name("Confirm Event")
    @FindBy(xpath = "//mat-row[@role=\"row\"]/mat-cell[6]/img[3]")
    public static TextBlock confiromEvent;

    @Name("Manage Wishlist")
    @FindBy(xpath = "//img[@mattooltip='Manage Wishlist']")
    public static TextBlock manageWishlist;

    @Name("Print Card")
    @FindBy(xpath = "//i[@mattooltip='Print Card']")
    public static TextBlock printCard;

    @Name("Status")
    @FindBy(xpath = "//div[@class=\"table-responsive user-table\"]/mat-table/mat-row/mat-cell[5]")
    public static List<WebElement> status;

    @Name("Cancel Yes")
    @FindBy(xpath = "//button[contains(text(),\"Yes\")]")
    public static Button cancelYes;

    @Name("Reason for cancel event")
    @FindBy(xpath = "//input[@id=\"reason\"]")
    public static TextInput reason;

    @Name("Send Invitation via mail")
    @FindBy(xpath = "//img[@mattooltip='Send Invitation via mail']")
    public static Image sendInvitationMail;

    @Name("PrintCard Registrant")
    @FindBy(xpath = "//mat-radio-button[@value='registrant']//div[@class='mat-radio-outer-circle']")
    public static Button printCardRegistrant;

    @Name("PrintCard Registrant")
    @FindBy(xpath = "//mat-radio-button[@value='guest']//div[@class='mat-radio-outer-circle']")
    public static Button printCardGuest;

    @Name("PrintCard Overlay")
    @FindBy(xpath = "//div//span[@class='toggle-dot']")
    public static Button dot;

    @Name("Gr Code")
    @FindBy(xpath = "//div[@class=\"gr-code\"]/h4")
    public static WebElement gfCode;

    public static List<String> activeUser(String user) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < status.size(); i++)
            if (status.get(i).getText().equals(user)) {
                list.add(editEvent.getAttribute("mattooltip"));
                list.add(resendInvitation.getAttribute("mattooltip"));
                list.add(manageWishlist.getAttribute("mattooltip"));
                list.add(cancelEvent.getAttribute("mattooltip"));
                list.add(printCard.getAttribute("mattooltip"));
                break;
            }
        return list;
    }

    public static List<String> confirmationPending(String user) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < status.size(); i++)
            if (status.get(i).getText().equals(user)) {
                list.add(editEvent.getAttribute("mattooltip"));
                list.add(resendInvitation.getAttribute("mattooltip"));
                list.add(confiromEvent.getAttribute("mattooltip"));
                list.add(cancelEvent.getAttribute("mattooltip"));
                break;
            }
        return list;
    }

    public static List<String> draft(String user) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < status.size(); i++)
            if (status.get(i).getText().equals(user)) {
                list.add(editEvent.getAttribute("mattooltip"));
                list.add(cancelEvent.getAttribute("mattooltip"));
                break;
            }
        return list;
    }

}
