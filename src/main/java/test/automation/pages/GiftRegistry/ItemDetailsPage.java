package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class ItemDetailsPage extends Page {
    public static final String URL = Config.getUrl() + "/events/cart-product-view";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'ITEM DETAILS')]");

    @Name("Header text")
    @FindBy(xpath="//h2[contains(text(),'ITEM DETAILS')]")
    public static TextBlock headerText;

    @Name("Add to Cart button")
    @FindBy(name="addtowishlist")
    public static Button addtoWishlistBtn;

    @Name("Back button")
    @FindBy(name="back")
    public static Button backBtn;

    @Name("Quantity")
    @FindBy(css="input.form-control.ng-pristine")
    public static TextInput quantity;

//    @Name("Product Name")
//    @FindBy(xpath="//h1[contains(text(),'French Kitchen Marble and Copper Trivet')]")
//    public TextBlock productName;

    @Name("Overview Block")
    @FindBy(xpath="//h2[contains(text(),'Overview')]")
    public static TextBlock overviewBlock;

    @Name("Details Block")
    @FindBy(xpath="//h2[contains(text(),'Details')]")
    public static TextBlock detailsBlock;

    @Name("Image Block")
    @FindBy(className ="products-slider")
    public static TextBlock imageBlock;

    @Name("Product Name")
    @FindBy(className = "shop-bar-left")
    public static TextBlock productName;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;
}
