package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import test.automation.framework.Actions;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.framework.Wait;
import test.automation.panels.GiftRegistry.HeaderPanel;
import test.automation.panels.GiftRegistry.NavigationPanel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static afu.org.checkerframework.checker.units.UnitsTools.s;

public class HomePage extends Page {

    public static final String URL = "/home";

    public HeaderPanel headerPanel;
    public NavigationPanel navigationPanel;

    public static final By VERIFY_BY = By.className("event-registry-section");

    @Name("take me registry button")
    @FindBy(id = "")
    public Button RegistryButton;

    @Name("Home Page Banner")
    @FindBy(className = "home-banner-section")
    public TextBlock homePageBanner;

    @Name("Header section")
    @FindBy(css = "section.head-top.header-bg-color")
    public TextBlock headerSection;

    @Name("Footer section")
    @FindBy(className = "footer-bg-color")
    public TextBlock footerSection;

    @Name("Bell icon")
    @FindBy(className = "header-text-color")
    public TextBlock bellIcon;

    @Name("User Profile")
    @FindBy(css = "button#dropdownMenuButton")
    public static TextBlock userProfile;

    @Name("Navigation Bar")
    @FindBy(css = "section.nav-section")
    public TextBlock navigationBar;

    @Name("Home link")
    @FindBy(xpath = "//a[@class='nav-text-color']//span[text()='Home']")
    public TextBlock homeLink;

    @Name("My Events link")
    @FindBy(xpath = "//a[@class='nav-text-color']//span[text()='My Events']")
    public TextBlock myEventsLink;

    @Name("Abount us link")
    @FindBy(xpath = "//a[@class='nav-text-color']//span[text()='About Us']")
    public TextBlock aboutUsLink;

    @Name("Contact Us link")
    @FindBy(xpath = "//a[@class='nav-text-color']//span[text()='Contact Us']")
    public TextBlock contactUsLink;

    @Name("Create your event ")
    @FindBy(css = "div.container>h2")
    public static TextBlock createYourEvent;

    @Name("Baby Shower")
    @FindBy(xpath = "//a[contains(text(),'Baby Shower')]")
    public TextBlock babyShower;

    @Name("Baptism")
    @FindBy(xpath = "//a[contains(text(),'Baptism')]")
    public TextBlock baptism;

    @Name("Birthday Adult")
    @FindBy(xpath = "//a[contains(text(),'Birthday-Adult')]")
    public TextBlock birthdayAdult;

    @Name("Birthday Kids")
    @FindBy(xpath = "//a[contains(text(),'Birthday-Kids')]")
    public TextBlock birthdayKids;

    @Name("Wedding")
    @FindBy(xpath = "//a[contains(text(),'Wedding')]")
    public TextBlock wedding;

    @Name("Birthday Debut")
    @FindBy(xpath = "//a[contains(text(),'Birthday-Debut')]")
    public TextBlock birthdayDebut;

    @Name("Anniversary")
    @FindBy(xpath = "//a[contains(text(),'Anniversary')]")
    public TextBlock anniversary;

    @Name("House Warming")
    @FindBy(xpath = "//a[contains(text(),'House Warming')]")
    public TextBlock houseWarming;

    @Name("View More")
    @FindBy(className = "primary-btn")
    public static Button viewMore;

    @Name("Event List")
    @FindBy(css = "div.event-box>a>figure")
    public static List<TextBlock> eventLists;

    @Name("Snack Bar")
    @FindBy(className = "mat-simple-snackbar")
    public static TextBlock snackBar;

    public String randomAnotherCompanyUrl() {
        List<String> list = new ArrayList<>(Arrays.asList("http://192.168.32.193:8181/crateandbarrel",
                "http://192.168.32.193:8181/babycompany",
                "http://192.168.32.193:8181/toykingdom",
                "http://192.168.32.193:8181/saci",
                "http://192.168.32.193:8181/departmentstore",
                "http://192.168.32.193:8181/ourhome"));
        list.remove(Config.getUrl());
        return list.get(new Random().nextInt(list.size()));
    }

    public static void selectEvent(String event){
        Wait.untilElementPresent(viewMore);
        viewMore.click();
        Actions.waitUntil(() -> eventLists.stream().anyMatch(t -> t.getText().equals(event)));
        eventLists.stream().filter(t -> t.getText().equals(event)).findFirst().get().click();
    }

    public static List<String> eventImagesLinks() {
        String host = Config.getUrl().split("8080/")[0] + "8080/";
        return eventLists.stream().map(ele -> host + ele.getAttribute("style").split("\\(\"")[1].split("\"\\)")[0]).collect(Collectors.toList());
    }

    public static void viewmore(){
        Actions.scrollToTheElement(viewMore);
    }
}
