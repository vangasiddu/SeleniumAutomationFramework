package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import sun.awt.image.ImageWatched;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;
import java.util.stream.Collectors;

public class MyCartPage extends Page {

    public static final String URL = Config.getUrl() + "/events/my-cart";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'My Cart')]");

    @Name("My cart header text")
    @FindBy(xpath="//h2[contains(text(),'My Cart')]")
    public TextBlock headerText;

    @Name("Item Details")
    @FindBy(xpath="//*[contains(text(),'Item Details')]")
    public TextBlock itemDetails;

    @Name("Product Image")
    @FindBy(xpath = "//*[@id=\"non-printable\"]//a/img")
    public TextBlock productImage;

    @Name("Event Wishlist link")
    @FindBy(xpath="//li[@class='breadcrumb-item']//a[text()='Event Wishlist']")
    public static Link eventWishlistLink;

    @Name("Item Price")
    @FindBy(xpath="//*[contains(text(),'Item Price')]")
    public TextBlock itemPrice;

    @Name("Quantity")
    @FindBy(xpath="//*[contains(text(),'Qty')]")
    public TextBlock quantity;

    @Name("Total")
    @FindBy(xpath="//*[contains(text(),'Total')]")
    public TextBlock total;

    @Name("Action")
    @FindBy(xpath="//*[contains(text(),'Action')]")
    public TextBlock action;

    @Name("Add Items link")
    @FindBy(css ="a.btn.btn-secondary")
    public Link addItemsLink;

    @Name("Estimate Order Total")
    @FindBy(css ="div.estimate-text")
    public TextBlock estimateOrderTotal;

    @Name("Buy Online")
    @FindBy(css = "//button[@value='BUY ONLINE']")
    public static Button buyOnline;

    @Name("Buy At Store Button")
    @FindBy(css = "button[value='BUY AT STORE']")
    public Button buyatStoreBtn;

    @Name("SKU id")
    @FindBy(id="sku")
    public static List<TextBlock> skuIds;

    @FindBy(xpath = "//*[@id=\"non-printable\"]//h3")
    public static TextBlock cartEmpty;

    @Name("Dialog Header Text")
    @FindBy(xpath = "//p[contains(text(),'We are saving')]")
    public static TextBlock dialogHeaderText;

    @Name("Cart Is Empty Text")
    @FindBy(xpath = "//h3[contains(text(),'Your cart is empty!')]")
    public static HtmlElement cartIsEmpty;

    @Name("Buying at Store proceed dialog box")
    @FindBy(xpath="//*[contains(text(),'Proceed')]")
    public Button buyingatStoreProceedbutton;

    @Name("Buying at Store cancellation dialog box")
    @FindBy(xpath="//*[contains(text(),'Cancel')]")
    public Button buyingatStoreCancelbutton;

    @Name("Add more items")
    @FindBy(css ="a.btn.btn-secondary")
    public Button addMoreItemsLink;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    public static List<String> skuIds(){
        return skuIds.stream().map(TypifiedElement::getText).collect(Collectors.toList());
    }

    public static String verifyQuantity() {
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                return option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).getAttribute("value");
            }
        }
        return null;
    }

    public void updateQuantity(String quantity){
        for (WebElement option : skuIds) {
            //if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).clear();
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).sendKeys(quantity);
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).sendKeys(Keys.TAB);
            //}
        }
    }

    public static void deleteProduct(){
        for (WebElement option : skuIds) {
            //if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/a[@class='btn-link btn-delete']")).click();
            //}
        }
    }

    public void itemPrice(){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td[1]")).click();
            }
        }
    }

    public void totalPrice(){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td[3]")).click();
            }
        }
    }

}