package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;

public class RoleManagementPage extends Page {
    public static final String URL = Config.getUrl() + "/store/role";
    public static final By VERIFY_BY = By.className("data-container");

   @Name("submit")
   @FindBy(xpath = "//span[contains(text(),'SUBMIT')]")
   public static Button submit;

   @Name("yes")
   @FindBy(xpath = "//button[contains(text(),'Yes')]")
   public static Button yes;

   @Name("no")
   @FindBy(xpath = "//button[contains(text(),'No')]")
   public static Button no;

   @Name("First_role")
   @FindBy(xpath = "//mat-row[1]/mat-cell[1]")
   public static TextBlock first_role;

   @Name("User Role Row")
   @FindBy(className = "mat-row")
   public static List<WebElement> userRoleRow;

   @Name("Role Headers")
   @FindBy(className = "header")
   public static List<WebElement> roleHeaders;

   @Name("Select All Checkboxes")
   @FindBy(className = "selectAll")
   public static List<CheckBox> selectAll;

   @Name("Select Checkbox")
   @FindBy(className = "mat-checkbox")
   public static CheckBox selectCheckbox;

   @Name("All Roll Names List")
   @FindBy(className = "mat-column-RoleName")
   public static List<WebElement> allRolesNamesList;

   @Name("Roles List")
   @FindBy(className = "mat-cell")
   public static List<TextBlock> roleCreated;

   @Name("role_delete_container")
   @FindBy(className = "mat-dialog-container")
   public static TextBlock roleDeletionDialog;

   @Name("Rolename_field")
   @FindBy(name = "roleName")
   public static TextInput roleName;

   @Name("Role names")
   @FindBy(xpath = "//button[contains(text(),'Role Name')]")
   public static TextBlock roleNames;

   @Name("Header")
   @FindBy(className = "data-header")
   public static TextBlock header;

   @Name("Edit Privilege")
   @FindBy(linkText = "Edit Privilege")
   public static Link editPrivilege;

   @Name("Delete")
   @FindBy(linkText = "Delete")
   public static Link delete;

   @Name("create new role")
   @FindBy(className = "mat-button-wrapper")
   public static Button createNewRole;

   @Name("role creation message")
   @FindBy(className = "alert-success")
   public static TextBlock roleCreationMessage;

   @Name("validation error")
   @FindBy(className = "validation-error")
   public static TextBlock validationError;

   @Name("duplicate role")
   @FindBy(className = "alert-danger")
   public static TextBlock duplicateRole;

   @Name("Actions")
   @FindBy(css = "mat-header-cell.cdk-column-Action")
   public static TextBlock actions;

   @Name("Submit Button")
   @FindBy(xpath = "//span[contains(text(),'SUBMIT')]")
   public static Button submitBtn;

   @Name("Cancel Button")
   @FindBy(xpath = "//span[contains(text(),'CANCEL')]")
   public static Button cancelBtn;

   @Name("Signout Button")
   @FindBy(xpath = "//li[contains(text(),'Sign Out')]")
   public static Button signoutBtn;

   @Name("Success Message")
   @FindBy(xpath = "//div[contains(text(),'Success! asd23232 role created successfully')]")
   public static TextBlock successMsg;

   @Name("Roles List Table")
   @FindBy(className = "user-table")
   public static WebElement roleListTable;

}
