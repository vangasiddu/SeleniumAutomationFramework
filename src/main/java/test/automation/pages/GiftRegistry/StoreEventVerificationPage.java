package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.panels.GiftRegistry.NavigationPanel;


public class StoreEventVerificationPage extends Page {
    public static final String URL = Config.getUrl() + "/events/event-verification";
    public static final By VERIFY_BY = By.linkText("Add Wishlist");

    public NavigationPanel navigationPanel;

    @Name("Add Wishlist")
    @FindBy(linkText="Add Wishlist")
    public static Button addWishlist;

    @Name("Success Message")
    @FindBy(xpath="//div//p[contains(text(),'Congratulations! You have successfully confirmed your event!')]")
    public static HtmlElement successMessage;

    @Name("Success Message")
    @FindBy(xpath="//div//p[contains(text(),'Your event invitation link has been sent')]")
    public static HtmlElement eventInvitationMessage;

}
