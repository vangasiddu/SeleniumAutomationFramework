package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.*;
import test.automation.models.User;
import test.automation.panels.GiftRegistry.HeaderPanel;

import java.util.List;
import java.util.Random;

import static test.automation.framework.Actions.click;

public class AdditionalDetailsPage extends Page {
    public static final String URL = Config.getUrl() + "registrant/additional-details";
    public static final By VERIFY_BY = By.name("firstName");

    public HeaderPanel headerPanel;

    @Name("Title")
    @FindBy(className = "mat-select-value")
    public TextBlock title;

    @Name("Option")
    @FindBy(className = "mat-option-text")
    public List<TextBlock> Options;

    @Name("First name")
    @FindBy(name = "firstName")
    public TextInput firstName;

    @Name("Last name")
    @FindBy(name = "lastName")
    public TextInput lastName;

    @Name("Suffix")
    @FindBy(name = "suffix")
    public TextInput suffix;

    @Name("Spouse name")
    @FindBy(name = "spouseName")
    public TextInput spouse;

    @Name("Email")
    @FindBy(name = "email")
    public TextInput email;

    @Name("Telephone")
    @FindBy(name = "telephone")
    public TextInput telephone;

    @Name("Mobile")
    @FindBy(name = "mobile")
    public TextInput mobile;

    @Name("Address Line1")
    @FindBy(name = "addressLine1")
    public TextInput addressLine1;

    @Name("City")
    @FindBy(name = "city")
    public TextBlock city;

    @Name("State")
    @FindBy(name = "state")
    public TextBlock state;

    @Name("Zip code")
    @FindBy(name = "zipCode")
    public TextInput zipCode;

    @Name("Submit")
    @FindBy(xpath = "//span[contains(text(),'Update')]")
    public Button submit;

    @Name("Reset")
    @FindBy(xpath = "//span[contains(text(),'Reset')]")
    public Button reset;

    @Name("Success message")
    @FindBy(id = "")
    public static HtmlElement successmessage;

    @Name("Title error message")
    @FindBy(xpath = "//div[contains(text(),'Please select Title')]")
    public static HtmlElement titleErrormsz;

    @Name("First name error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter First Name')]")
    public static HtmlElement firstNameErrorMsz;

    @Name("Last name error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Last Name')]")
    public static HtmlElement lastNameErrorMsz;

    @Name("Address error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Address')]")
    public static HtmlElement AddressErrorMsz;

    @Name("City error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter City')]")
    public static HtmlElement CityErrorMsz;

    @Name("City invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter a valid city')]")
    public static HtmlElement CityInvalidErrorMsz;

    @Name("Zipcode error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Zip Code')]")
    public static HtmlElement ZipErrorMsz;

    @Name("Zipcode invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Zip Code should be atleast 4 Characters') or contains(text(),'Please enter a valid Zip Code.')]")
    public static HtmlElement ZipInvalidErrorMsz;

    @Name("State error message")
    @FindBy(xpath = "//div[contains(text(),'Please select State')]")
    public static HtmlElement stateErrorMsz;

    @Name("Phone error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Telephone Number')]")
    public static HtmlElement phoneErrorMsz;

    @Name("Phone invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in one of these formats')]")
    public static HtmlElement phoneInvalidErrorMsz;

    @Name("Mobile error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter Mobile Number')]")
    public static HtmlElement mobileErrorMsz;

    @Name("Mobile invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format:')]")
    public static HtmlElement mobileInvalidErrorMsz;

    public void randomTitleSelect(){
        click(title);
        Options.get(new Random().nextInt(Options.size())).click();
    }

    public String randomStateSelect(){
       click(state);
       if (Options.size() == 0)
           click(state);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String state = random.getText();
        ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",random);
        return  state;
    }

    public String randomCitySelect(){
        click(city);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String city = random.getText();
        ((JavascriptExecutor) Browser.getDriver()).executeScript("arguments[0].click();",random);
        return city;
    }


    public void updateAccount(User user) throws InterruptedException {
        click(title);
        Actions.waitUntil(() -> Options.stream().anyMatch(t -> t.getText().equals(user.getTitle())));
        Options.stream().filter(t -> t.getText().equals(user.getTitle())).findFirst().get().click();
        telephone.sendKeys(user.getPhone());
        mobile.sendKeys(user.getMobile());
        addressLine1.sendKeys(user.getAddress());
        spouse.clear();
        spouse.sendKeys(user.getSpouse());
        user.setState(randomStateSelect());
        Thread.sleep(1000);
        user.setCity(randomCitySelect());
        zipCode.sendKeys(user.getZipCode());
    }

    public void updateData(User user) throws InterruptedException{
        telephone.sendKeys(user.getPhone());
        mobile.sendKeys(user.getMobile());
        addressLine1.sendKeys(user.getAddress());
        zipCode.sendKeys(user.getZipCode());
    }
}
