package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.*;
import test.automation.models.User;
import test.automation.panels.GiftRegistry.HeaderPanel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static test.automation.framework.Actions.click;

public class CreateEventPage extends Page {

    public HeaderPanel headerPanel;
    public String eventsDate;

    public static final String URL = "events/create-event?";

    public static final By VERIFY_BY = By.className("create-event-section");

    @Name("Create your event title")
    @FindBy(css = "div.event-info-box>h3")
    public static TextBlock createYourEvent;

    @Name("Event type")
    @FindBy(id = "eventType")
    public TextBlock eventType;

    @Name("Event type field")
    @FindBy(tagName = "mat-form-field")
    public TextBlock eventTypeFiled;

    @Name("Event date")
    @FindBy(id = "eventDate")
    public TextInput eventDate;

    @Name("Event time")
    @FindBy(name = "eventTime")
    public TextInput eventTime;

    @Name("Event Venue")
    @FindBy(name = "eventVenue")
    public TextInput eventVenue;

    @Name("Event Guests Count")
    @FindBy(name = "eventGuestsCount")
    public TextInput eventGuestsCount;

    @Name("Event state")
    @FindBy(name = "state")
    public TextBlock state;

    @Name("Event city")
    @FindBy(name = "city")
    public TextBlock city;

    @Name("Event Zipcode")
    @FindBy(id = "eventZipcode")
    public TextInput eventZipcode;

    @Name("Gift delivery Address")
    @FindBy(id = "deliveryAddress")
    public TextBlock giftDeliveryAddress;

    @Name("Gift delivery State")
    @FindBy(id = "deliveryState")
    public TextBlock giftDeliveryState;

    @Name("Gift delivery City")
    @FindBy(id = "deliveryCity")
    public TextBlock giftDeliverycity;

    @Name("Gift delivery Zipcode")
    @FindBy(id = "deliveryZipcode")
    public TextInput giftDeliveryZip;

    @Name("Gift Delivery Date")
    @FindBy(id = "giftDeliveryDate")
    public TextInput giftDeliveryDate;

    @Name("Is Registrant Celebrant? yes")
    @FindBy(xpath = "//mat-radio-button[contains(@id,'mat-radio')][1]")
    public Radio isCelebrantYes;

    @Name("Is Registrant Celebrant? no")
    @FindBy(xpath = "//mat-radio-button[contains(@id,'mat-radio')][2]")
    public Radio isCelebrantNo;

    @Name("Celebrant Name")
    @FindBy(id = "celebrantName")
    public TextInput celebrantName;

    @Name("Celebrant phoneNumber")
    @FindBy(id = "phoneNumber")
    public TextInput celebrantPhoneNumber;

    @Name("Celebrant email")
    @FindBy(id = "email")
    public TextInput email;

    @Name("Is Celebrant? Groom")
    @FindBy(xpath = "//mat-radio-button[@value ='groom']")
    public Radio isCelebrantGroom;

    @Name("Is Celebrant? Bride")
    @FindBy(xpath = "//mat-radio-button[@value ='bride']")
    public Radio isCelebrantBride;

    @Name("coRegistrantCelebrant")
    @FindBy(id = "coRegistrantCelebrant")
    public TextInput coRegistrantCelebrant;

    @Name("Names to appear on the guest card? Bride checkbox")
    @FindBy(id = "brideGuestCardNameFlag")
    public CheckBox brideGuestCardNameFlag;

    @Name("Names to appear on the guest card? Bride")
    @FindBy(id = "brideGuestCardName")
    public TextInput brideGuestCardName;

    @Name("Names to appear on the guest card?  Groom checkbox")
    @FindBy(id = "groomGuestCardNameFlag")
    public CheckBox groomGuestCardNameFlag;

    @Name("Names to appear on the guest card?  Groom")
    @FindBy(name = "groomGuestCardName")
    public TextInput groomGuestCardName;

    @Name("Loyalty Card")
    @FindBy(id = "loyaltyCard")
    public TextInput loyaltyCard;

    @Name("No Of Years")
    @FindBy(id = "numberOfYears")
    public TextInput numberOfYears;

    @Name("Opt For Email Promotions")
    @FindBy(name = "emailPromotionsOptIn")
    public CheckBox emailPromotionsOptIn;

    @Name("Opt For SMS Promotions")
    @FindBy(name = "smsPromotionsOptIn")
    public CheckBox smsPromotionsOptIn;

    @Name("Celebrant preferredName")
    @FindBy(id = "preferredName")
    public TextInput preferredName;

    @Name("Celebrant preferredRegistryBranch")
    @FindBy(id = "preferredRegistryBranch")
    public TextBlock preferredRegistryBranch;

    @Name("Save & Continue")
    @FindBy(xpath = "//button[@type='submit']")
    public Button saveAndContinue;

    @Name("Reset button")
    @FindBy(xpath = "//button[@type='reset']")
    public Button reset;

    @Name("Calendar")
    @FindBy(className = "mat-datepicker-toggle-default-icon")
    public TextBlock calendar;

    @Name("Option")
    @FindBy(className = "mat-option-text")
    public List<TextBlock> Options;

    @Name("First Preffered name")
    @FindBy(name="firstPreferredName")
    public TextInput firstPreferredName;

    @Name("Second Preffered name")
    @FindBy(name="secondPreferredName")
    public TextInput secondPreferredName;

    @Name("Home breadcrum")
    @FindBy(xpath="//li[@class='breadcrumb-item']/a[contains(text(),'Home')]")
    public TextBlock home;

    @Name("Zipcode invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Zip-code should be atleast 4 Characters') or contains(text(),'Please enter a valid Zip-code') or contains(text(),'Please Enter a valid Zip-code')]")
    public static HtmlElement ZipInvalidErrorMsz;

    @Name("Mobile invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format:')]")
    public static HtmlElement mobileInvalidErrorMsz;

    @Name("Invalid guest count error message")
    @FindBy(xpath = "//div[contains(text(),'Please Enter a valid Guests Count')]")
    public static HtmlElement invalidGuestsCountMsz;

    @Name("Celebrant name invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Celebrant Name should be alphanumeric')]")
    public static HtmlElement celebrantInvalidErrorMsz;

    @Name("Co registrant name invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Co-Registrant/Celebrant Name should be alphanumeric')]")
    public static HtmlElement coRegInvalidErrorMsz;

    @Name("Email error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format')]")
    public static HtmlElement emailErrorMsz;

    @Name("bride guest error message")
    @FindBy(xpath = "//div[contains(text(),//div[contains(text(),'Preferred Name should be alphanumeric')])]")
    public static HtmlElement pNameInvalidErrorMsz;

    @Name("Number Of Years error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter valid Number of Years.')]")
    public static HtmlElement yearsErrorMsz;

    @Name("Error message")
    @FindBy(xpath = "//div[contains(text(),'days prior to the') or contains(text(),'exceeded Event cancellations') or contains(text(),'Unbale to create event')]")
    public HtmlElement errorMsz;

    public WebElement labelElement(WebElement element) {
        String locator = element.getAttribute("id");
        return Browser.getDriver().findElement(By.xpath("//label[@for='" + locator + "']"));
    }

    private String selectRandomDate() throws InterruptedException {
        Actions.execJavascript("window.scrollTo(0,0)");
        Thread.sleep(1000);
        calendar.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"));
        WebElement randomDate = dates.get(new Random().nextInt(dates.size()-1));
        String date = randomDate.getAttribute("aria-label");
        randomDate.click();
        Actions.waitUntil(()-> !eventDate.getAttribute("value").isEmpty());
        eventsDate = date;
        return date;
    }

    private String setGiftDeliveryDate() throws InterruptedException {
        Thread.sleep(1000);
        giftDeliveryDate.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell")).stream().collect(Collectors.toList());
        List<WebElement> disabledDates = Browser.getDriver().findElements(By.className("mat-calendar-body-disabled")).stream().collect(Collectors.toList());
        dates.removeAll(disabledDates);
        List<WebElement> rangeDatesToSelect = dates;
        WebElement randomDate = dates.get(new Random().nextInt(rangeDatesToSelect.size()));
        String date = randomDate.getAttribute("aria-label");
        randomDate.click();
        Actions.waitUntil(() -> !eventDate.getAttribute("value").isEmpty());
        return date;
    }

    private void selectEventDate(String date) {
        Actions.execJavascript("window.scrollTo(0,0)");
        Actions.waitUntil(() -> Options.isEmpty());
        calendar.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"));
        dates.stream().filter(element -> element.getAttribute("aria-label").contains(date)).findFirst().get().click();
        Actions.waitUntil(()-> !eventDate.getAttribute("value").isEmpty());
    }

    private void selectRandomDateWithInCongigured() throws InterruptedException {
        Actions.execJavascript("window.scrollTo(0,0)");
        Thread.sleep(1000);
        calendar.click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"))
                .stream().filter(webElement -> !webElement.getAttribute("class").contains("mat-calendar-body-disabled")).collect(Collectors.toList());
        if (dates.size() > 10)
            dates = dates.subList(0, 9);
        WebElement randomDate = dates.size() == 1 ? dates.get(0) : dates.get(new Random().nextInt(dates.size() - 1));
        randomDate.click();
    }

    private String selectRandomTime() throws InterruptedException {
        Wait.untilElementPresent(eventTime);
        eventTime.click();
        List<WebElement> time = Browser.getDriver().findElements(By.cssSelector("button[id^=timepicker-item-id]")).
                stream().filter(webElement -> !webElement.getAttribute("class").contains("active")).collect(Collectors.toList());
        time.get(new Random().nextInt(time.size()-1)).click();
        Browser.getDriver().findElement(By.cssSelector("button.atp-ref-dialog-close")).click();
        Thread.sleep(1000);
        // Actions.waitUntil(()-> !eventTime.getAttribute("value").equals("12:00"));
        return eventTime.getAttribute("value");
    }

    private String randomStateSelect() throws InterruptedException {
        Thread.sleep(1000);
        Actions.execJavascript("window.scrollTo(200,200)");
        Actions.execJavascript("arguments[0].click();",state);
        if (Options.isEmpty())
            Actions.execJavascript("arguments[0].click();",state);
        Wait.untilElementPresent(Options.get(0));
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String state = random.getText();
        Actions.execJavascript("arguments[0].click();",random);
        return  state;
    }

    private String randomCitySelect(){
        city.click();
        if (Options.isEmpty())
            city.click();
        Wait.untilElementPresent(Options.get(0));
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String city = random.getText();
        Actions.execJavascript("arguments[0].click();",random);
        return  city;
    }

    private String selectRandomGiftDeliveryState() throws InterruptedException {
        Thread.sleep(1000);
        giftDeliveryState.click();
        if (Options.isEmpty())
            click(giftDeliveryState);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String state = random.getText();
        Actions.execJavascript("arguments[0].click();",random);
        return  state;
    }

    private String selectRandomGiftDeliveryCity(){
        giftDeliverycity.click();
        if (Options.isEmpty())
            click(giftDeliverycity);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String city = random.getText();
        Actions.execJavascript("arguments[0].click();",random);
        return  city;
    }

    private String selectRandomPreferredRegistryBranch() throws InterruptedException {
        Actions.execJavascript("arguments[0].click();",preferredRegistryBranch);
        Thread.sleep(1000);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String branch = random.getText();
        Actions.execJavascript("arguments[0].click();",random);
        return  branch;
    }

    public void enterBabyShowerEventDetails(User user){
        try {
            user.setEventDate(selectRandomDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            selectRandomPreferredRegistryBranch();
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBaptismEventDetails(User user){
        try {
            user.setEventDate(selectRandomDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetails(User user){
        try {
            user.setEventDate(selectRandomDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            numberOfYears.clear();
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetails(User user){
        try {
            user.setEventDate(selectRandomDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBabyShowerEventDetailsWithInConfigured(User user){
        try {
            selectRandomDateWithInCongigured();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelect();
            Thread.sleep(1000);
            randomCitySelect();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryState();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCity();
            giftDeliveryZip.sendKeys(user.getZipCode());
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetailsWithInConfigured(User user){
        try {
            selectRandomDateWithInCongigured();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelect();
            Thread.sleep(1000);
            randomCitySelect();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryState();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCity();
            giftDeliveryZip.sendKeys(user.getZipCode());
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetailsWithInConfigured(User user){
        try {
            Thread.sleep(1000);
            selectRandomDateWithInCongigured();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelect();
            Thread.sleep(1000);
            randomCitySelect();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryState();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCity();
            giftDeliveryZip.sendKeys(user.getZipCode());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBabyShowerEventDetailsWithNoOption(User user) {
        enterBabyShowerEventDetails(user);
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterBirthdayAdultEventDetailsWithNoOption(User user){
        enterBirthdayAdultEventDetails(user);
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterWeddingEventDetailsWithNoOption(User user){
        enterWeddingEventDetails(user);
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterBabyShowerEventDetailsWithSpecificDate(User user) {
        try {
            selectEventDate(user.getEventDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetailsSpecificDate(User user){
        try {
            selectEventDate(user.getEventDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            numberOfYears.clear();
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetailsSpecificDate(User user){
        try {
            selectEventDate(user.getEventDate());
            user.setEventTime(selectRandomTime());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelect());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelect());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryState());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCity());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDate());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranch();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
