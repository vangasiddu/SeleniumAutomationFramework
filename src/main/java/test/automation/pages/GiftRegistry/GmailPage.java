package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.*;
import test.automation.models.User;

import java.util.List;

import static test.automation.framework.Actions.waitUntil;


public class GmailPage extends Page {

    public static final String URL = "https://accounts.google.com/signin";
    public static final By VERIFY_BY = By.id("identifierId");

    @Name("Email")
    @FindBy(id = "identifierId")
    public TextInput email;

    @Name("Password")
    @FindBy(name = "password")
    public TextInput password;

    @Name("Next button")
    @FindBy(xpath = "//span[contains(text(), 'Next')]")
    public Button nextButton;

    @Name("Gmail Icon")
    @FindBy(className = "WaidBe")
    public Button gmailIcon;

    @Name("Mail Search")
    @FindBy(name = "q")
    public Button mailSearch;

    @Name("Search button")
    @FindBy(id = "gbqfb")
    public Button searchButton;

    @Name("Reset Link")
    @FindBy(id = "gbqfb")
    public HtmlElement ResetLink;

    @Name("Confirmation Link")
    @FindBy(xpath = "//span/b[contains(text(),'Registration Confirmation')]")
    public List<HtmlElement> confirmatiomail;


    @Name("Confirmation Link")
    @FindBy(linkText = "click here to confirm registration")
    public Link ConfirmationLink;

    @Name("Error Text")
    @FindBy(xpath= "//*[@id=\"view_container\"]/div/div/div[2]/div/div[1]/div/form/content/div[1]/div/div[2]/div[2]")
    public TextBlock Message;


    public void gmailLogin(User user) {
        email.sendKeys(user.getEmail());
        Actions.click("nextButton");
        Wait.untilElementPresent(password);
        password.sendKeys(user.getPassword());
        Wait.secondsUntilElementPresent(nextButton,10);
        Actions.click("nextButton");
        if (Wait.untilElementPresent(gmailIcon))
            gmailIcon.click();
    }

    public void confirmMail(String subject) throws InterruptedException {
        Wait.untilElementPresent(mailSearch);
        mailSearch.sendKeys(subject);
        searchButton.click();
        waitUntil(() -> confirmatiomail.get(confirmatiomail.size()-1).isDisplayed());
        confirmatiomail.get(confirmatiomail.size()-1).click();
        Wait.untilElementPresent(ConfirmationLink);
        ConfirmationLink.click();
    }


}
