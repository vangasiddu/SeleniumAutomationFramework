package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;

public class EventWishlistPage extends Page {

    public static final String URL = Config.getUrl() + "/events/event-wishlist";
    public static final By VERIFY_BY = By.cssSelector("a.btn.btn-outline-secondary");

    @Name("My Cart")
    @FindBy(css = "a.btn.btn-outline-secondary")
    public TextBlock myCart;

    @Name("Event wishlist header")
    @FindBy(tagName = "h2")
    public TextBlock eventWishlistHeader;

    @Name("Product SKU details")
    @FindBy(xpath = "//p[contains(text(),'SKU - ')]")
    public static TextBlock productSKU;

    @Name("Requested quantity")
    @FindBy(xpath = "//div[@class='availablity clearfix']//child::span")
    public static TextBlock rqsQuantity;

    @Name("My cart link")
    @FindBy(css="a.btn.btn-outline-secondary")
    public Link myCartLink;

    @Name("My cart count")
    @FindBy(css="a.btn.btn-outline-secondary span")
    public static TextBlock myCartCount;

    @Name("Add to cart button")
    @FindBy(css="input[value='Add to Cart']")
    public Button addToCartBtn;

    @Name("SKU id")
    @FindBy(xpath="//p[contains(text(),'SKU')]")
    public static List<TextBlock> skuIds;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Add to cart")
    @FindBy(xpath = "//input[@value='Add to Cart']")
    public static Button addToCart;

    @Name("Product Image")
    @FindBy(xpath = "//figure/a")
    public static Link productImage;

    @Name("Alert Dialog Box")
    @FindBy(className ="modal-title")
    public TextBlock alertDialog;

    @Name("Alert Yes button")
    @FindBy(xpath="//*[contains(text(),'Yes')]")
    public static Button alertYes;

    @Name("Alert No button")
    @FindBy(xpath="//*[contains(text(),'No')]")
    public Button alertNo;

    @Name("My Cart button")
    @FindBy(xpath = "//*[contains(text(),'My Cart')]")
    public static Button myCartButton;

    public void updateQuantity(String quantity){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::div//child::div//child::input")).clear();
                option.findElement(By.xpath("//ancestor::div//child::div//child::input")).sendKeys(quantity);
            }
        }
    }

}
