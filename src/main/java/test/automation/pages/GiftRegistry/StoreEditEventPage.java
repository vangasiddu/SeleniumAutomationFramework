package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.*;
import test.automation.models.User;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static test.automation.framework.Actions.click;


public class StoreEditEventPage extends Page {

    public String eventsDate;

    public static final String URL = Config.getUrl() + "store/event-management/events/update-event?";

    public static final By VERIFY_BY = By.className("create-event-section");

    @Name("Event type")
    @FindBy(id = "eventType")
    public static TextBlock eventType;

    @Name("Event type field")
    @FindBy(tagName = "mat-form-field")
    public TextBlock eventTypeFiled;

    @Name("Event date")
    @FindBy(id = "eventDate")
    public TextInput eventDate;

    @Name("Event time")
    @FindBy(name = "eventTime")
    public TextInput eventTime;

    @Name("Event Venue")
    @FindBy(name = "eventVenue")
    public TextInput eventVenue;

    @Name("Event Guests Count")
    @FindBy(name = "eventGuestsCount")
    public TextInput eventGuestsCount;

    @Name("Event state")
    @FindBy(name = "state")
    public TextBlock state;

    @Name("Event city")
    @FindBy(name = "city")
    public TextBlock city;

    @Name("Event Zipcode")
    @FindBy(id = "eventZipcode")
    public TextInput eventZipcode;

    @Name("Gift delivery Address")
    @FindBy(id = "deliveryAddress")
    public TextBlock giftDeliveryAddress;

    @Name("Gift delivery State")
    @FindBy(id = "deliveryState")
    public TextBlock giftDeliveryState;

    @Name("Gift delivery City")
    @FindBy(id = "deliveryCity")
    public TextBlock giftDeliverycity;

    @Name("Gift delivery Zipcode")
    @FindBy(id = "deliveryZipcode")
    public TextInput giftDeliveryZip;

    @Name("Gift Delivery Date")
    @FindBy(id = "giftDeliveryDate")
    public TextInput giftDeliveryDate;

    @Name("Is Registrant Celebrant? yes")
    @FindBy(xpath = "//mat-radio-button[contains(@id,'mat-radio')][1]")
    public Radio isCelebrantYes;

    @Name("Is Registrant Celebrant? no")
    @FindBy(xpath = "//mat-radio-button[contains(@id,'mat-radio')][2]")
    public Radio isCelebrantNo;

    @Name("Celebrant Name")
    @FindBy(id = "celebrantName")
    public TextInput celebrantName;

    @Name("Celebrant phoneNumber")
    @FindBy(id = "phoneNumber")
    public TextInput celebrantPhoneNumber;

    @Name("Celebrant email")
    @FindBy(id = "email")
    public TextInput email;

    @Name("Is Celebrant? Groom")
    @FindBy(xpath = "//mat-radio-button[@value ='groom']")
    public Radio isCelebrantGroom;

    @Name("Is Celebrant? Bride")
    @FindBy(xpath = "//mat-radio-button[@value ='bride']")
    public Radio isCelebrantBride;

    @Name("coRegistrantCelebrant")
    @FindBy(id = "coRegistrantCelebrant")
    public TextInput coRegistrantCelebrant;

    @Name("Names to appear on the guest card? Bride checkbox")
    @FindBy(id = "brideGuestCardNameFlag")
    public CheckBox brideGuestCardNameFlag;

    @Name("Names to appear on the guest card? Bride")
    @FindBy(id = "brideGuestCardName")
    public TextInput brideGuestCardName;

    @Name("Names to appear on the guest card?  Groom checkbox")
    @FindBy(id = "groomGuestCardNameFlag")
    public CheckBox groomGuestCardNameFlag;

    @Name("Names to appear on the guest card?  Groom")
    @FindBy(name = "groomGuestCardName")
    public TextInput groomGuestCardName;

    @Name("Loyalty Card")
    @FindBy(id = "loyaltyCard")
    public TextInput loyaltyCard;

    @Name("No Of Years")
    @FindBy(id = "numberOfYears")
    public TextInput numberOfYears;

    @Name("Opt For Email Promotions")
    @FindBy(name = "emailPromotionsOptIn")
    public CheckBox emailPromotionsOptIn;

    @Name("Opt For SMS Promotions")
    @FindBy(name = "smsPromotionsOptIn")
    public CheckBox smsPromotionsOptIn;

    @Name("Celebrant preferredName")
    @FindBy(id = "preferredName")
    public TextInput preferredName;

    @Name("Celebrant preferredRegistryBranch")
    @FindBy(id = "preferredRegistryBranch")
    public TextBlock preferredRegistryBranch;

    @Name("Save & Continue")
    @FindBy(xpath = "//button[@type='submit']")
    public Button saveAndContinue;

    @Name("Reset button")
    @FindBy(xpath = "//button[@type='reset']")
    public Button reset;

    @Name("Calendar")
    @FindBy(className = "mat-datepicker-toggle-default-icon")
    public TextBlock calendar;

    @Name("Option")
    @FindBy(className = "mat-option-text")
    public static List<TextBlock> Options;

    @Name("First Preffered name")
    @FindBy(name = "firstPreferredName")
    public TextInput firstPreferredName;

    @Name("Second Preffered name")
    @FindBy(name = "secondPreferredName")
    public TextInput secondPreferredName;

    @Name("Home breadcrum")
    @FindBy(xpath = "//li[@class='breadcrumb-item']/a[contains(text(),'Home')]")
    public TextBlock home;

    @Name("Zipcode invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Zip-code should be atleast 4 Characters') or contains(text(),'Please enter a valid Zip-code') or contains(text(),'Please Enter a valid Zip-code')]")
    public static HtmlElement ZipInvalidErrorMsz;

    @Name("Mobile invalid error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format:')]")
    public static HtmlElement mobileInvalidErrorMsz;

    @Name("Invalid guest count error message")
    @FindBy(xpath = "//div[contains(text(),'Please Enter a valid Guests Count')]")
    public static HtmlElement invalidGuestsCountMsz;

    @Name("Celebrant name invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Celebrant Name should be alphanumeric')]")
    public static HtmlElement celebrantInvalidErrorMsz;

    @Name("Co registrant name invalid error message")
    @FindBy(xpath = "//div[contains(text(),'Co-Registrant/Celebrant Name should be alphanumeric')]")
    public static HtmlElement coRegInvalidErrorMsz;

    @Name("Email error message")
    @FindBy(xpath = "//div[contains(text(),'This entry must be in this format')]")
    public static HtmlElement emailErrorMsz;

    @Name("bride guest error message")
    @FindBy(xpath = "//div[contains(text(),//div[contains(text(),'Preferred Name should be alphanumeric')])]")
    public static HtmlElement pNameInvalidErrorMsz;

    @Name("Number Of Years error message")
    @FindBy(xpath = "//div[contains(text(),'Please enter valid Number of Years.')]")
    public static HtmlElement yearsErrorMsz;

    @Name("Error message")
    @FindBy(xpath = "//div[contains(text(),'days prior to the') or contains(text(),'exceeded Event cancellations') or contains(text(),'same day with same Event Category')]")
    public HtmlElement errorMsz;

    public WebElement labelElement(WebElement element) {
        String locator = element.getAttribute("id");
        return Browser.getDriver().findElement(By.xpath("//label[@for='" + locator + "']"));
    }

    private String selectRandomDateForStore() throws InterruptedException {
        Actions.execJavascript("window.scrollTo(0,0)");
        Thread.sleep(1000);
        calendar.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"));
        WebElement randomDate = dates.get(new Random().nextInt(dates.size() - 1));
        String date = randomDate.getAttribute("aria-label");
        randomDate.click();
        Actions.waitUntil(() -> !eventDate.getAttribute("value").isEmpty());
        eventsDate = date;
        return date;
    }

    private String setGiftDeliveryDateForStore() throws InterruptedException {
        Thread.sleep(1000);
        giftDeliveryDate.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell")).stream().collect(Collectors.toList());
        List<WebElement> disabledDates = Browser.getDriver().findElements(By.className("mat-calendar-body-disabled")).stream().collect(Collectors.toList());
        dates.removeAll(disabledDates);
        List<WebElement> rangeDatesToSelect = dates;
        WebElement randomDate = dates.get(new Random().nextInt(rangeDatesToSelect.size()));
        String date = randomDate.getAttribute("aria-label");
        randomDate.click();
        Actions.waitUntil(() -> !eventDate.getAttribute("value").isEmpty());
        return date;
    }

    private void selectEventDateForStore(String date) {
        Actions.execJavascript("window.scrollTo(0,0)");
        Actions.waitUntil(() -> Options.isEmpty());
        calendar.click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        Browser.getDriver().findElement(By.className("mat-calendar-next-button")).click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"));
        dates.stream().filter(element -> element.getAttribute("aria-label").contains(date)).findFirst().get().click();
        Actions.waitUntil(() -> !eventDate.getAttribute("value").isEmpty());
    }

    private void selectRandomDateWithInCongiguredForStore() throws InterruptedException {
        Actions.execJavascript("window.scrollTo(0,0)");
        Thread.sleep(1000);
        calendar.click();
        List<WebElement> dates = Browser.getDriver().findElements(By.className("mat-calendar-body-cell"))
                .stream().filter(webElement -> !webElement.getAttribute("class").contains("mat-calendar-body-disabled")).collect(Collectors.toList());
        if (dates.size() > 10)
            dates = dates.subList(0, 9);
        WebElement randomDate = dates.size() == 1 ? dates.get(0) : dates.get(new Random().nextInt(dates.size() - 1));
        randomDate.click();
    }

    private String selectRandomTimeForStore() throws InterruptedException {
        Wait.untilElementPresent(eventTime);
        eventTime.click();
        List<WebElement> time = Browser.getDriver().findElements(By.cssSelector("button[id^=timepicker-item-id]")).
                stream().filter(webElement -> !webElement.getAttribute("class").contains("active")).collect(Collectors.toList());
        time.get(new Random().nextInt(time.size() - 1)).click();
        Browser.getDriver().findElement(By.cssSelector("button.atp-ref-dialog-close")).click();
        Thread.sleep(1000);
        // Actions.waitUntil(()-> !eventTime.getAttribute("value").equals("12:00"));
        return eventTime.getAttribute("value");
    }

    private String randomStateSelectForStore() throws InterruptedException {
        Thread.sleep(1000);
        Actions.execJavascript("window.scrollTo(200,200)");
        Actions.execJavascript("arguments[0].click();", state);
        if (Options.isEmpty())
            Actions.execJavascript("arguments[0].click();", state);
        Wait.untilElementPresent(Options.get(0));
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String state = random.getText();
        Actions.execJavascript("arguments[0].click();", random);
        return state;
    }

    private String randomEventTypeSelectForStore(String selectOption) throws InterruptedException {
        Thread.sleep(1000);
        Actions.execJavascript("window.scrollTo(200,200)");
        Actions.execJavascript("arguments[0].click();", eventType);
        if (Options.isEmpty())
            Actions.execJavascript("arguments[0].click();", eventType);
        Wait.untilElementPresent(Options.get(0));
        WebElement selected = null;
        if (!selectOption.equalsIgnoreCase("random")) {
            selected = Options.stream().filter(a -> a.getText().equalsIgnoreCase(selectOption)).collect(Collectors.toList()).get(0);
        }
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        WebElement selectedOption = selectOption.equalsIgnoreCase("random") ? random : selected;
        String eventType = selectedOption.getText();
        Actions.execJavascript("arguments[0].click();", selectedOption);
        return eventType;
    }

    private String randomCitySelectForStore() {
        city.click();
        if (Options.isEmpty())
            city.click();
        Wait.untilElementPresent(Options.get(0));
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String city = random.getText();
        Actions.execJavascript("arguments[0].click();", random);
        return city;
    }

    private String selectRandomGiftDeliveryStateForStore() throws InterruptedException {
        Thread.sleep(1000);
        giftDeliveryState.click();
        if (Options.isEmpty())
            click(giftDeliveryState);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String state = random.getText();
        Actions.execJavascript("arguments[0].click();", random);
        return state;
    }

    private String selectRandomGiftDeliveryCityForStore() {
        giftDeliverycity.click();
        if (Options.isEmpty())
            click(giftDeliverycity);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String city = random.getText();
        Actions.execJavascript("arguments[0].click();", random);
        return city;
    }

    private String selectRandomPreferredRegistryBranchForStore() throws InterruptedException {
        Actions.execJavascript("arguments[0].click();", preferredRegistryBranch);
        Thread.sleep(1000);
        WebElement random = Options.get(new Random().nextInt(Options.size()));
        String branch = random.getText();
        Actions.execJavascript("arguments[0].click();", random);
        return branch;
    }

    public void enteRandomEventDetailsForStore(User user, String event) {
        try {
            String eve = randomEventTypeSelectForStore(event);
            user.setEventType(eve);
            user.setEventDate(selectRandomDateForStore());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            if (eve.equalsIgnoreCase("Wedding")) coRegistrantCelebrant.sendKeys(user.getFirstName());
            selectRandomPreferredRegistryBranchForStore();
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBabyShowerEventDetailsForStore(User user, String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            user.setEventDate(selectRandomDateForStore());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            selectRandomPreferredRegistryBranchForStore();
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBaptismEventDetailsForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            user.setEventDate(selectRandomDateForStore());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetailsForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            user.setEventDate(selectRandomDateForStore());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            numberOfYears.clear();
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetailsForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            user.setEventDate(selectRandomDateForStore());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBabyShowerEventDetailsWithInConfiguredForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            selectRandomDateWithInCongiguredForStore();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelectForStore();
            Thread.sleep(1000);
            randomCitySelectForStore();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryStateForStore();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCityForStore();
            giftDeliveryZip.sendKeys(user.getZipCode());
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetailsWithInConfiguredForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            selectRandomDateWithInCongiguredForStore();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelectForStore();
            Thread.sleep(1000);
            randomCitySelectForStore();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryStateForStore();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCityForStore();
            giftDeliveryZip.sendKeys(user.getZipCode());
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetailsWithInConfiguredForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            Thread.sleep(1000);
            selectRandomDateWithInCongiguredForStore();
            eventVenue.sendKeys(user.getEventVenue());
            randomStateSelectForStore();
            Thread.sleep(1000);
            randomCitySelectForStore();
            eventZipcode.sendKeys(user.getZipCode());
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            selectRandomGiftDeliveryStateForStore();
            Thread.sleep(1000);
            selectRandomGiftDeliveryCityForStore();
            giftDeliveryZip.sendKeys(user.getZipCode());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBabyShowerEventDetailsWithNoOptionForStore(User user) {
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterBirthdayAdultEventDetailsWithNoOptionForStore(User user) {
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterWeddingEventDetailsWithNoOptionForStore(User user,String event) {
        enterWeddingEventDetailsForStore(user,event);
        isCelebrantNo.click();
        celebrantName.sendKeys(user.getFirstName());
        celebrantName.sendKeys(" " + user.getLastName());
        celebrantPhoneNumber.sendKeys(user.getMobile());
        email.sendKeys(user.getEmail());
    }

    public void enterBabyShowerEventDetailsWithSpecificDateForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            selectEventDateForStore(user.getEventDate());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterBirthdayAdultEventDetailsSpecificDateForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            selectEventDateForStore(user.getEventDate());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            numberOfYears.clear();
            numberOfYears.sendKeys(Util.getRandomNumber(2));
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterWeddingEventDetailsSpecificDateForStore(User user,String event) {
        try {
            user.setEventType(randomEventTypeSelectForStore(event));
            selectEventDateForStore(user.getEventDate());
            user.setEventTime(selectRandomTimeForStore());
            eventVenue.sendKeys(user.getEventVenue());
            user.setEventState(randomStateSelectForStore());
            Thread.sleep(1000);
            user.setEventCity(randomCitySelectForStore());
            eventZipcode.sendKeys(user.getZipCode());
            eventGuestsCount.sendKeys(Util.getRandomNumber(3));
            giftDeliveryAddress.sendKeys(user.getGiftDeliveryAddress());
            user.setGiftDeliveryState(selectRandomGiftDeliveryStateForStore());
            Thread.sleep(1000);
            user.setGiftDeliveryCity(selectRandomGiftDeliveryCityForStore());
            giftDeliveryZip.sendKeys(user.getZipCode());
            user.setGiftsDeliveryDate(setGiftDeliveryDateForStore());
            celebrantPhoneNumber.clear();
            celebrantPhoneNumber.sendKeys(user.getMobile());
            coRegistrantCelebrant.sendKeys(user.getFirstName());
            emailPromotionsOptIn.select();
            smsPromotionsOptIn.select();
            selectRandomPreferredRegistryBranchForStore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}