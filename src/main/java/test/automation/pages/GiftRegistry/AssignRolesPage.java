package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import test.automation.framework.Config;
import test.automation.framework.Page;

import javax.xml.soap.Text;
import java.util.List;

public class AssignRolesPage extends Page {
    public static final String URL = Config.getUrl() + "store/user/edit";
    public static final By VERIFY_BY = By.className("main-container");

    @Name("Assign Roles Header")
    @FindBy(xpath = "//div[contains(text(),'Assign Roles')]")
    public static TextBlock assignRoles;

    @Name("Company Name")
    @FindBy(name = "companyName")
    public static Select company;

    @Name("Branch Name")
    @FindBy(name = "branchName")
    public static Select branch;

    @Name("All Companies List")
    @FindBy(className = "mat-option")
    public static List<WebElement> allCompanies;

    @Name("All Branches List")
    @FindBy(className = "mat-option-text")
    public static List<WebElement> allBranches;

    @Name("Save Button")
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    public static TextBlock saveButton;

    @Name("privileges updation success message")
    @FindBy(className = "alert-success")
    public static TextBlock privilegesupdationMessage;

    @Name("Back Button")
    @FindBy(xpath = "//span[contains(text(),'Back')]")
    public static TextBlock backButton;

    @Name("All Roles Rows")
    @FindBy(className = "mat-row")
    public static List<WebElement> roleRowsList;

    @Name("All Cells")
    @FindBy(className = "mat-cell")
    public static List<WebElement> roleCellList;

    @Name("Role Assigned CheckBoxes")
    @FindBy(className = "mat-checkbox-checked")
    public static List<WebElement> roleAssigned;

    @Name("Signout Button")
    @FindBy(xpath = "//li[contains(text(),'Sign Out')]")
    public static Button signoutBtn;

    @Name("Selected Company")
    @FindBy(className = "mat-select-value-text")
    public static List<TextBlock> selectedCompany;

    @Name("Role Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][1]/a")
    public static Link roleManagement;

    @Name("User Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][2]/a")
    public static Link userManagement;

    @Name("Customer Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][3]/a")
    public static Link customerManagement;

    @Name("Event Management")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][4]/a")
    public static Link eventManagement;

    @Name("Configuration")
    @FindBy(xpath = "//li[@class=\"nav-item dropdown ng-star-inserted\"]/a")
    public static Link  Configuration;

}
