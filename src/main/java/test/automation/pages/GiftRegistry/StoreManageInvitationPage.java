package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class StoreManageInvitationPage extends Page {

    public static final String URL = Config.getUrl() + "/event-management/events/manage-invitation?eventId";
    public static final By VERIFY_BY = By.linkText("Save & Continue");

    @Name("Edit button")
    @FindBy(id = "btn-edit")
    public Button edit;

    @Name("Save and Continue button")
    @FindBy(linkText = "Save & Continue")
    public Button saveAndContinue;

    @Name("Event date")
    @FindBy(xpath =  "//p/eventdate")
    public TextBlock eventdate;

    @Name("Event time")
    @FindBy(xpath = "//p/eventtime")
    public TextBlock eventtime;

    @Name("Event Venue")
    @FindBy(xpath = "//p/eventvenue")
    public TextBlock eventvenue;

    @Name("Celebrant phonenumber")
    @FindBy(xpath = "//span/phonenumber")
    public TextBlock celebrantphonenumber;

    @Name("Celebrant phonenumber")
    @FindBy(tagName = "celebrantname")
    public TextBlock celebrantname;

    @Name("Event state")
    @FindBy(xpath = "//p/eventstate")
    public TextBlock eventState;

    @Name("Event city")
    @FindBy(xpath = "//p/eventcity")
    public TextBlock eventCity;

    @Name("Template2")
    @FindBy(css = "ul.theme-list > li:nth-child(2) > a")
    public Link template2;

    @Name("Template1")
    @FindBy(xpath = "//ul[@class='theme-list']//a")
    public Link template1;

    @Name("Role Management tab")
    @FindBy(xpath = "//li[@class=\"nav-item ng-star-inserted\"][4]/a")
    public static Link eventManagement;
}
