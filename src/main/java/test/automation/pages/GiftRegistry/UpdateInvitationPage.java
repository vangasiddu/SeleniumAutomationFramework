package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class UpdateInvitationPage extends Page {

    public static final String URL = Config.getUrl() + "events/manage-invitation?eventId";
    public static final By VERIFY_BY = By.linkText("Save & Continue");

    @Name("Edit button")
    @FindBy(id = "btn-edit")
    public Button edit;

    @Name("Save and Continue button")
    @FindBy(linkText = "Save & Continue")
    public Button saveAndContinue;

}
