package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.panels.GiftRegistry.HeaderPanel;

import java.util.List;
import java.util.stream.Collectors;

public class MyWishlistPage extends Page {
    public static final String URL = Config.getUrl() + "/events/my-wish-list";
    public static final By VERIFY_BY = By.xpath("//h2[contains(text(),'My Wishlist')]");

    public static HeaderPanel headerPanel;

    @Name("My wishlist header text")
    @FindBy(xpath="//h2[contains(text(),'My Wishlist')]")
    public TextBlock headerText;

    @Name("No items text")
    @FindBy(xpath="//div[contains(text(),'There are no Items to display in your WishList.')]")
    public TextBlock noItemsInWishlist;

    @Name("Add more items")
    @FindBy(css="input.btn.btn-secondary.btn-sm-block")
    public Button addMoreItemsLink;

    @Name("SKU id")
    @FindBy(id="sku")
    public static List<TextBlock> skuIds;

    @Name("SKU id")
    @FindBy(id="sku")
    public static TextBlock skuId;

    @Name("Quantity")
    @FindBy(css="input.form-control.input-qty")
    public TextBlock quantity;

    @Name("Snack Bar")
    @FindBy(className = "mat-snack-bar-container")
    public static TextBlock snackBar;

    @Name("Most loved selected")
    @FindBy(css = "a.active.btn-most-loved.ng-star-inserted")
    public static TextBlock mostLovedSelected;

    @Name("Most loved")
    @FindBy(css = "a.btn-most-loved.ng-star-inserted")
    public static TextBlock mostLoved;

    @Name("Delete link")
    @FindBy(css = "a.btn-link.btn-delete")
    public static TextBlock delete;

    @Name("Product image")
    @FindBy(css = "img.product-img.img-fluid.ng-star-inserted")
    public static TextBlock productImage;

    @Name("Shopping details")
    @FindBy(xpath = "//th[contains(text(),'Item, Avaliability and shopping details')]")
    public static TextBlock shoppingDetails;

    @Name("Add More Items link")
    @FindBy(css ="input.btn.btn-secondary")
    public Link addMoreItems;

    @Name("Alert Dialog Box")
    @FindBy(className ="modal-title")
    public TextBlock alertDialog;

    @Name("Alert Yes button")
    @FindBy(xpath="//*[contains(text(),'Yes')]")
    public static Button alertYes;

    @Name("Alert No button")
    @FindBy(xpath="//*[contains(text(),'No')]")
    public Button alertNo;

    public static List<String> skuIds(){
        return skuIds.stream().map(TypifiedElement::getText).collect(Collectors.toList());
    }

    public static String verifyQuantity() {
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                return option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).getAttribute("value");
            }
        }
        return null;
    }

    public void updateQuantity(String quantity){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).clear();
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/input")).sendKeys(quantity);
            }
        }
    }

    public void deleteProduct(){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/a[@class='btn-link btn-delete']")).click();
            }
        }
    }

    public void mostLoved(){
        for (WebElement option : skuIds) {
            if (option.getText().equalsIgnoreCase(CreateWishlistPage.skuDetails)) {
                option.findElement(By.xpath("//ancestor::td//following-sibling::td/a[@class='btn-most-loved ng-star-inserted']")).click();
            }
        }
    }
}
