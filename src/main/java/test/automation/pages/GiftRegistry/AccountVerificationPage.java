package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class AccountVerificationPage extends Page {

    public static final String URL = Config.getUrl() + "/registrant/account-verification";
    public static final By VERIFY_BY = By.tagName("h2");

    @Name("Registration Confirmation meassage")
    @FindBy(tagName= "h2")
    public static HtmlElement message;

    @Name("SignIn button")
    @FindBy(css = "button.btn.btn-primary")
    public Button signInButton;
}
