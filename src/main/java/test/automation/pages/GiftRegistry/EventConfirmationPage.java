package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class EventConfirmationPage extends Page {
    public static final String URL = Config.getUrl() + "/events/event-confirmation";
    public static final By VERIFY_BY = By.tagName("app-event-confirmation");


    @Name("Resend the Confirmation Link")
    @FindBy(css="a.btn.btn-primary")
    public Link resendTheConfirmationLink;

    @Name("Go to Wish list button")
    @FindBy(name="additems")
    public Button goToWishListButton;

   @Name("Event Confirmation Message")
    @FindBy(xpath="//div[@class='ng-star-inserted']//div/p[contains(text(),'Please confirm the event')]")
    public HtmlElement eventConfirmationMessage;

   @Name("Event Code")
    @FindBy(tagName="h4")
    public TextBlock eventCode;

    @Name("Congrats message")
    @FindBy(css="div.text-center>p")
    public HtmlElement congratsMessage;

}
