package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.panels.GiftRegistry.HeaderPanel;
import test.automation.panels.GiftRegistry.NavigationPanel;

public class GuestInvitationPage extends Page {
    public static final String URL = Config.getUrl() + "/events/event-invitation?";
    public static By VERIFY_BY = By.xpath("//a[@class='btn btn-secondary']");

    public HeaderPanel headerPanel;
    public NavigationPanel navigationPanel;

    @Name("Go to wishlist")
    @FindBy(xpath = "//a[@class='btn btn-secondary']")
    public Button wishlistBtn;

    @Name("Email")
    @FindBy(id = "email")
    public static TextBlock email;

    @Name("Mobile")
    @FindBy(id = "phoneNumber")
    public static TextBlock guestPhoneNumber;

    @Name("Continue Button")
    @FindBy(xpath = "//button[@class='btn btn-primary' and text() = 'Continue']")
    public static Button continueButton;

    @Name("Register or signin button")
    @FindBy(xpath = "//button[@class='btn btn-primary' and text() = 'REGISTER / SIGN IN']")
    public static Button registerSignInBtn;

    @Name("Skip registration button")
    @FindBy(xpath = "//button[@class='btn btn-primary' and text() = 'SKIP REGISTRATION']")
    public static Button skipRegistration;

    public static void setSkipRegistration() throws InterruptedException {
        email.sendKeys("xyz12345@gmail.com");
        Thread.sleep(1000);
        guestPhoneNumber.sendKeys("999-999-9999");
        continueButton.click();
    }
}
