package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;

public class RegistrationConfirmationPage extends Page {

    public static final String URL = Config.getUrl() + "/registrant/reg-handler";
    public static final By VERIFY_BY = By.cssSelector("div[class=ng-star-inserted]>div>p");

    @Name("Registration Confirmation link")
    @FindBy(className= "btn-primary")
    public Button RegistrationConfirmation;

    @Name("Registration Confirmation meassage")
    @FindBy(css= "div[class=ng-star-inserted]>div>p")
    public static HtmlElement message;

}
