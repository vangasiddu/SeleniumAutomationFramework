package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.framework.StoreUser;
import test.automation.models.User;
import test.automation.utils.UserUtils;

/**
 * Created by statavarthy on 6/20/2018.
 */
public class StoreLoginPage extends Page {
    public static final String URL = Config.getUrl();
    public static final By VERIFY_BY = By.className("store-login-section");

    @Name("Login header")
    @FindBy(className = "login-header")
    public static TextBlock loginHeader;

    @Name("userId")
    @FindBy(name = "userId")
    public static TextInput userId;

    @Name("password")
    @FindBy(name = "password")
    public static TextInput password;

    @Name("sigin")
    @FindBy(name = "signin")
    public static Button signInButton;

    @Name("logo")
    @FindBy(id = "logo")
    public static Link logo;

    @Name("error message")
    @FindBy(className = "alert-danger")
    public static TextBlock error_message;

   public static void signIn(User user){
       userId.sendKeys(user.getEmail());
       password.sendKeys(user.getPassword());
   }

}
