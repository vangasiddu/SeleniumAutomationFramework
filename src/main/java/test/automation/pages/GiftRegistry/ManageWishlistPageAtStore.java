package test.automation.pages.GiftRegistry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;

import java.util.List;

public class ManageWishlistPageAtStore extends Page {

    public static final String URL = Config.getUrl() + "/event-management/wishlist?";
    public static final By VERIFY_BY = By.className("data-header");

    @Name("Page Name")
    @FindBy(className = "data-header")
    public static TextBlock pageName;

    @Name("Page Rows")
    @FindBy(className = "row")
    public static List<TextBlock> pageRows;

    @Name("Product Rows")
    @FindBy(className = "mat-row")
    public static List<TextBlock> productRows;

    @Name("Page Back Button")
    @FindBy(xpath = "//a[@mattooltip=\"Back to Event Management\"]")
    public static Link pageBackButton;

    @Name("Sku Input Field")
    @FindBy(xpath = "//input[@placeholder='Enter SKU code to Add to Wishlist']")
    public static TextInput skuIdInputField;

    @Name("Add Sku Input Button")
    @FindBy(xpath = "//button[@class='store-btn btn btn-primary']")
    public static Button addSkuButton;

    @Name("Refresh Page")
    @FindBy(xpath = "//i[@mattooltip=\"Refresh Data\"]")
    public static Link refreshPage;

    @Name("Upload File")
    @FindBy(xpath = "//label[@class='uploadLabel store-btn']")
    public static Link uploadFile;

    @Name("Most loved")
    @FindBy(css = "a.btn-most-loved.ng-star-inserted")
    public static TextBlock mostLoved;

    @Name("SKU Code")
    @FindBy(xpath = "//button[contains(text(),'SKU Code')]")
    public static Button skuCode;

    @Name("Product Name")
    @FindBy(xpath = "//button[contains(text(),'Product Name')]")
    public static Button productName;

    @Name("Quantity")
    @FindBy(xpath = "//mat-header-cell[contains(text(),'Qty')]")
    public static TextBlock quantity;

    @Name("Action")
    @FindBy(xpath = "//mat-header-cell[contains(text(),'Action')]")
    public static TextBlock action;

    @Name("Most Loved Active")
    @FindBy(xpath = "//a[contains(@class,'Active btn-most-loved')]")
    public static Link mostLovedActive;

    @Name("Most Loved Not Active")
    @FindBy(xpath = "//a[contains(@class,'btn-most-loved')]")
    public static Link mostLovedNotActive;

    @Name("Quantity Input")
    @FindBy(xpath = "//input[@class='form-control input-qty']")
    public static TextInput qtyInput;

    @Name("Delete Product")
    @FindBy(xpath = "//a[@mattooltip='Delete']")
    public static Link deleteAddedProduct;

}
