package test.automation.pages.SeleniumEasyDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.*;
import test.automation.framework.Config;
import test.automation.framework.Page;
import test.automation.models.User;
import test.automation.panels.SeleniumEasyDemo.NavigationBar;

import java.util.Map;

public class InputForm extends Page {

    public static final String URL = Config.getUrl() + "/test/input-form-demo.html";
    public static final By VERIFY_BY = By.id("contact_form");

    public NavigationBar navigationBar;

    @Name("Input Form")
    @FindBy(id = "contact_form")
    public Form input;

    @Name("First Name")
    @FindBy(name = "first_name")
    public TextInput firstName;

    @Name("Last Name")
    @FindBy(name = "last_name")
    public TextInput lastName;

    @Name("Email")
    @FindBy(name = "email")
    public TextInput email;

    @Name("Phone Number")
    @FindBy(name = "phone")
    public TextInput phone;

    @Name("Address")
    @FindBy(name = "address")
    public TextInput address;

    @Name("City")
    @FindBy(name = "city")
    public TextInput city;

    @Name("State")
    @FindBy(name = "state")
    public Select state;

    @Name("Zip Code")
    @FindBy(name = "zip")
    public TextInput zipCode;

    @Name("Website")
    @FindBy(name = "website")
    public TextInput website;

    @Name("Have Hosting")
    @FindBy(name = "hosting")
    public Radio haveHosting;

    @Name("Project Description")
    @FindBy(name = "comment")
    public TextInput projectDescription;

    @Name("Send Button")
    @FindBy(xpath = "//button[contains(text(), 'Send')]")
    public Button sendButton;

    public void fillInputForm(Map<String, Object> data) {
        String state = (String) data.remove("state");
        input.fill(data);
        this.state.selectByVisibleText(state);
    }

    public void fillInputForm(User user) {
        firstName.sendKeys(user.getFirstName());
        lastName.sendKeys(user.getLastName());
        email.sendKeys(user.getEmail());
        phone.sendKeys(user.getPhone());
        address.sendKeys(user.getAddress());
        city.sendKeys(user.getCity());
        state.selectByVisibleText(user.getState());
        zipCode.sendKeys(user.getZipCode());
        website.sendKeys(user.getWebsite());
    }
}
