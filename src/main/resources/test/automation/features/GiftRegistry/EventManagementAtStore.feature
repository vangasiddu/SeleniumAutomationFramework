@V4_Store
Feature: Event Management - At store

  Scenario: Verify that all the fields on event management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "eventManagement" button on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry EventManagementPage" page
    And I should see following elements on "GiftRegistry EventManagementPage" page:
      | roleManagement          |
      | userManagement          |
      | customerManagement      |
      | eventManagement         |
      | configurationManagement |
      | pageName                |
      | searchBar               |
      | searchButton            |
      | eventCode               |
      | celebrantName           |
      | eventType               |
      | eventDate               |
      | eventStatus             |
      | actions                 |
      | paginationContainer     |

  Scenario: Verify create event page form fields
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I verify all missing fields should be in red color while creating "babyShower" event on create event page
      | eventType               |
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | eventGuestsCount        |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | giftDeliveryDate        |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredRegistryBranch |

  Scenario Outline: Verify Anniversary event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Anniversary" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please Enter a valid Zip-code                   |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | firstPreferredName   | @          | Preferred Name should be alphanumeric           |
      | firstPreferredName   | 1          | Preferred Name should be alphanumeric           |
      | firstPreferredName   | as!@       | Preferred Name should be alphanumeric           |
      | secondPreferredName  | @          | Preferred Name should be alphanumeric           |
      | secondPreferredName  | 1          | Preferred Name should be alphanumeric           |
      | secondPreferredName  | as!@       | Preferred Name should be alphanumeric           |
      | numberOfYears        | @          | Please enter valid Number of Years.             |
      | numberOfYears        | 321        | Please enter valid Number of Years.             |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify House Warming event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "House Warming" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please Enter a valid Zip-code                   |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Baby Shower event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Baby Shower" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Baptism event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Baptism" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Birthday Adult event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Birthday-Adult" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | numberOfYears        | @          | Please enter valid Number of Years.             |
      | numberOfYears        | 321        | Please enter valid Number of Years.             |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Birthday Kids event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Birthday-Kids" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | numberOfYears        | @          | Please enter valid Number of Years.             |
      | numberOfYears        | 321        | Please enter valid Number of Years.             |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Birthday Debut event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Birthday-Debut" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format:              |
      | email                | ederf!.com | This entry must be in this format:              |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | numberOfYears        | @          | Please enter valid Number of Years.             |
      | numberOfYears        | 321        | Please enter valid Number of Years.             |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count               |

  Scenario Outline: Verify Wedding event page form field error messages when invalid input is provided to them
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I select "Wedding" event from list on "GiftRegistry StoreCreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name            | input      | error message                                       |
      | zipCode               | !          | Zip-code should be atleast 4 Characters             |
      | zipCode               | !@34       | Please Enter a valid Zip-code                       |
      | zipCode               | 1          | Zip-code should be atleast 4 Characters             |
      | giftDeliveryZip       | !          | Zip-code should be atleast 4 Characters             |
      | giftDeliveryZip       | !@34       | Please Enter a valid Zip-code                       |
      | giftDeliveryZip       | 1          | Zip-code should be atleast 4 Characters             |
      | celebrantName         | 1          | Celebrant Name should be alphanumeric               |
      | celebrantName         | @          | Celebrant Name should be alphanumeric               |
      | celebrantName         | as!@       | Celebrant Name should be alphanumeric               |
      | celebrantPhoneNumber  | 1          | This entry must be in this format: XXX-XXX-XXXX     |
      | celebrantPhoneNumber  | @          | This entry must be in this format: XXX-XXX-XXXX     |
      | email                 | @          | This entry must be in this format:                  |
      | email                 | ederf!.com | This entry must be in this format:                  |
      | invalidGuestCount     | sdsdsd     | Please Enter a valid Guests Count                   |
      | coRegistrantCelebrant | @          | Co-Registrant/Celebrant Name should be alphanumeric |
      | coRegistrantCelebrant | 1          | Co-Registrant/Celebrant Name should be alphanumeric |
      | coRegistrantCelebrant | as!@       | Co-Registrant/Celebrant Name should be alphanumeric |
      | brideGuestCardName    | @          | Preferred Name should be alphanumeric               |
      | brideGuestCardName    | 1          | Preferred Name should be alphanumeric               |
      | brideGuestCardName    | as!@       | Preferred Name should be alphanumeric               |
      | groomGuestCardName    | @          | Preferred Name should be alphanumeric               |
      | groomGuestCardName    | 1          | Preferred Name should be alphanumeric               |
      | groomGuestCardName    | as!@       | Preferred Name should be alphanumeric               |

  Scenario Outline: Verify reset option functionality on create event form
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    And I enter valid event details for store "<Event_Name>" event on create event page
    When I click on "reset" button on "GiftRegistry StoreCreateEventPage" page
    Then I should see all the fields resetting to default values for "<Event_Name>" event in store
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

#  Scenario: verify user created event for one site and same event should not be displayed for other sites on the same browser and same session
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    When I search with valid "Registered" user from the search bar
#    And I verify store user search with valid user
#    Then I should see the details displayed
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    And I enter valid event details for store "babyShower" event on create event page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreCreateEventPage" page
#    Then I should be on GiftRegistry Store Event Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see below messages on store event confirmation page
#      | Congrats! Your Event has been created!                                    |
#      | Please confirm the event registration through the link we sent to - email |
#    And I should see event code on store event confirmation page
#    When I click on "eventManagement" button on "GiftRegistry StoreEventConfirmationPage" page
#    And I search with "event code" on the search bar on "GiftRegistry StoreEventManagementPage" page
#    Then I should see the correct event "code" displayed for the user on "GiftRegistry StoreEventManagementPage" page

  Scenario Outline: Verify event search using event code on event management page for all types of events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    When I click on "eventManagement" button on "GiftRegistry StoreEventConfirmationPage" page
    And I search with "event code" on the search bar on "GiftRegistry StoreEventManagementPage" page
    Then I should see the correct event "code" displayed for the user on "GiftRegistry StoreEventManagementPage" page
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

  Scenario Outline: Verify event search using celebrant name on event management page for all types of events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    When I click on "eventManagement" button on "GiftRegistry StoreEventConfirmationPage" page
    And I search with "celebrant" on the search bar on "GiftRegistry StoreEventManagementPage" page
    Then I should see the correct event "celebrant name" displayed for the user on "GiftRegistry StoreEventManagementPage" page
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

  Scenario: Verify the pagination drop down on event management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry CustomerManagementPage" page
    And I verify the pagination value with value "10" on "GiftRegistry CustomerManagementPage" page
    And I verify the pagination value with value "15" on "GiftRegistry CustomerManagementPage" page

  Scenario: Verify the page navigation using pagination elements on event management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry CustomerManagementPage" page
    And I verify navigation to "next" page from pagination on "GiftRegistry CustomerManagementPage" page
    And I verify navigation to "previous" page from pagination on "GiftRegistry CustomerManagementPage" page

  Scenario: Verify the sorting order display of event records based event code on event management page in default order
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "eventManagement" button on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry StoreEventManagementPage" page
    And I should see the user records in "default" order for "event_code" on "GiftRegistry StoreEventManagementPage" page

  Scenario: Verify the sorting order display of event records based event code on event management page in ascending
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "eventManagement" button on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry StoreEventManagementPage" page
    And I should see the user records in "asc" order for "event_code" on "GiftRegistry StoreEventManagementPage" page

  Scenario: Verify the sorting order display of event records based event code on event management page in descending
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "eventManagement" button on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry StoreEventManagementPage" page
    And I should see the user records in "desc" order for "event_code" on "GiftRegistry StoreEventManagementPage" page

  Scenario Outline: Verify by updating the event details which already created
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    When I click on "eventManagement" button on "GiftRegistry StoreEventConfirmationPage" page
    And I search with "event code" on the search bar on "GiftRegistry StoreEventManagementPage" page
    Then I should see the correct event "code" displayed for the user on "GiftRegistry StoreEventManagementPage" page
    And I click on "editEvent" link on "GiftRegistry StoreEventManagementPage" page
    When I update valid event details for store "<Event_Name>" event on create event page
    And I click on "saveAndContinue" button on "GiftRegistry StoreEditEventPage" page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    And I should see "Success! Event updated" message in snack bar on "GiftRegistry StoreEventManagementPage"page
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |





