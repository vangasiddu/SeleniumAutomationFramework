@V3
Feature: Cart Management

  Scenario Outline: Verify all the fields present on the mycart page when no item added as a guest anonymous user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    And I should see the below message on GiftRegistry MyCartPage page
      |Your cart is empty! |
    Then I should see following elements on "GiftRegistry MyCartPage" page:
      | headerText           |
      | itemDetails          |
      | itemPrice            |
      | quantity             |
      | total                |
      | action               |
      | addItemsLink         |

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify all the fields present on the mycart page when items added as a guest anonymous user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see "myCart" element on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should see following elements on "GiftRegistry MyCartPage" page:
      | headerText           |
      | itemDetails          |
      | itemPrice            |
      | quantity             |
      | total                |
      | action               |
      | addMoreItemsLink     |
      | estimateOrderTotal   |
      | buyOnline            |
      | buyatStoreBtn        |

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  Scenario Outline: Verify all the fields present on the mycart page when no item added as a guest registered  user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    And I should see the below message on GiftRegistry MyCartPage page
      |Your cart is empty! |
    Then I should see following elements on "GiftRegistry MyCartPage" page:
      | headerText           |
      | itemDetails          |
      | itemPrice            |
      | quantity             |
      | total                |
      | action               |
      | addItemsLink         |

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify all the fields present on the mycart page when items added as a guest registered user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    When I click on "alertYes" on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see "myCart" element on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should see following elements on "GiftRegistry MyCartPage" page:
      | headerText           |
      | itemDetails          |
      | itemPrice            |
      | quantity             |
      | total                |
      | action               |
      | addMoreItemsLink     |
      | estimateOrderTotal   |
      | buyOnline            |
      | buyatStoreBtn        |

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking on add more items link  in My cart page as a guest anonymous user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    When I click on "addMoreItemsLink" button on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking on add more items link  in My cart page as a guest registered user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    When I click on "alertYes" on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    When I click on "addMoreItemsLink" button on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  Scenario Outline: Verify by clicking on buy at store button in My cart page
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see "myCart" element
    #And I should see all item details on event Wishlist page that are added by celebrant
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    When I click on "buyatStoreBtn" button on "GiftRegistry MyCartPage" page
    And I should see the below message on GiftRegistry MyCartPage page
      |We are saving your cart for buying at store. But your online cart will be deleted! |
    Then I should see following elements on "GiftRegistry MyCartPage" page:
      |     buyingatStoreProceedbutton    |
      |     buyingatStoreCancelbutton     |

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  Scenario Outline: Verify by clicking on proceed button on dialog box in My cart page
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see "myCart" element
    #And I should see all item details on event Wishlist page that are added by celebrant
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    When I click on "buyatStoreBtn" button on "GiftRegistry MyCartPage" page
    And I click on "buyingatStoreProceedbutton" button on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry MyCartPage" page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  Scenario Outline: Verify by clicking on cancel button on dialog box in My cart page
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see "myCart" element
    #And I should see all item details on event Wishlist page that are added by celebrant
    When I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    When I click on "buyatStoreBtn" button on "GiftRegistry MyCartPage" page
    And I click on "buyingatStoreCancelbutton" button on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry MyCartPage" page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by deleting the items added by the celebrant  for guest registered user on my cart page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    And I delete all the items on MyCartPage page
    And I should see the "Your cart is empty!" message on GiftRegistry MyCartPage page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by deleting the items added by the celebrant for guest anonymous user on my cart page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    And I delete all the items on MyCartPage page
    And I should see the "Your cart is empty!" message on GiftRegistry MyCartPage page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by updating qty for the items added by the celebrant  for guest registered user on My Cart page 
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    Then I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    When I click on "alertYes" on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "3" on "GiftRegistry MyCartPage" page
    Then I should see "Success! Item has been updated" message in snack bar
    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify the error message by adding order qty more than requested qty for guest registered user on My Cart page 
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    Then I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    When I click on "alertYes" on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "6" on "GiftRegistry MyCartPage" page
    Then I should see "Quantity should not be more than requested (4) quantity" message in snack bar on "GiftRegistry MyCartPage" page
    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify the error message by adding order qty more than requested qty for guest anonymous user on My Cart page 
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    When I update the quantity of the product to "4" on "GiftRegistry EventWishlistPage" page
    Then I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "6" on "GiftRegistry MyCartPage" page
    Then I should see "Fail Quantity should not be more than requested (4) quantity" message in snack bar on "GiftRegistry MyCartPage" page
    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by updating qty for the items added by the celebrant for guest anonymous user on My Cart page 
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "3" on "GiftRegistry MyCartPage" page
    Then I should see "Success! Item has been updated" message in snack bar

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify product quantity in event wishlist page once after deleting product from my cart page for guest registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    And I enter registration details on "GiftRegistry SignModel" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry MyCartPage" page
    And I delete all the items on MyCartPage page
    And I navigate to event wishlist page
    Then I should see the my cart count as "0"

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify product quantity in event wishlist page once after updating product quantity from my cart page for guest registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    And I enter registration details on "GiftRegistry SignModel" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    And I update the quantity of the product to "4" on "GiftRegistry MyCartPage" page
    And I navigate to event wishlist page
    Then I should see the quantity updated on event wishlist page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

