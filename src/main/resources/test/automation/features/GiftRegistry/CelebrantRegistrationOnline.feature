@V1
Feature: Celebrant registration - online

  Scenario: Verify celebrant is successfully registered online
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    When I confirm the registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |

  @use_regression
  Scenario: Verify the message when user again try to confirm his verified account through registration confirmation link
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    When I confirm the registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |
    When I confirm the registration online
    And I should see the below success Account verification message
      | Your account has already been verified. |

  @use_regression
  Scenario: Verify that all the fields are present on Enterprise gift registry login page
    Given I visit the website as new user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry LoginPage" page
    Then I should see following elements on "GiftRegistry LoginPage" page:
      | email               |
      | password            |
      | showPassword        |
      | loginButton         |
      | regfirstName        |
      | reglastName         |
      | regEmail            |
      | regPassword         |
      | regConfirmPassword  |
      | register            |
      | googleLoginbutton   |
      | facebookLoginbutton |
      | ForgotPassword      |

  @use_regression
  Scenario: Verify error messages for all required fields when no input on EGR login page.
    Given I visit the website as new user
#    When I click on "Take Me to Gift Registry" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry LoginPage" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I verify error messages while registering with missing fields
      | first_name       | Required |
      | last_name        | Required |
      | Email            | Required |
      | Password         | Required |
      | Confirm_Password | Required |

  @use_regression
  Scenario Outline: Verify error messages for all required fields when invalid input on EGR login page.
    Given I visit the website as new user
    # When I click on "Take Me to Gift Registry" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter invalid details "<field name>" with "<input>" value on "GiftRegistry RegistrationModel" page
    Then I should see the error "<error message>" message for "<field name>" on "GiftRegistry RegistrationModel" of login page
    Examples:
      | field name      | input | error message                                  |
#      | firstName       | !@#$12241414142422@19292 | Sorry, name may only contain letters and hyphens and cannot exceed 20 characters. |
#      | lastName        | !@#$12241414142422@19292 | Sorry, name may only contain letters and hyphens and cannot exceed 20 characters. |
      | email           | !@#$  | Invalid email                                  |
      | password        | !@#$  | Password must consist of at least 8 characters |
      | confirmPassword | !@#$s | Password and Confirm Password must match       |

  @use_regression
  Scenario: Verify error message if celebrant is already registered
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    When I clear all the cookies and navigate to login page
    And I enter already registered details on "GiftRegistry LoginPage" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should see "Registration not successful. Email already exist." error message

  @use_regression @ppr
  Scenario: Verify error message when celebrant login without confirming his/her account
    Given I visit the website as new user
  #  When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I clear all the cookies and navigate to login page
    And I enter already registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry VerificationPendingPage" page
    And I should see below error message on VerificationPendingPage
      | We've already sent an email to 'email'. Please click the email link to verify your account. |
    And I should see "resendVerificationLink" link on "GiftRegistry VerificationPendingPage" page

  @use_regression
  Scenario: verify user loggedIn for one site and login page should display for other sites on the same browser and same session
    Given I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry HomePage" page
    When I navigate to another company site in same session
    Then I should be on "GiftRegistry LoginPage" page

  Scenario: Verify * for all required fields on login page
    Given I visit the website as new user
#   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I should see star for all required fields

  @Defect-1483 @wip
  Scenario: Verify additional details page should be displayed when user refresh the registration confirmation page
    Given I visit the website as new user
#   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    When I confirm the registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I navigate to registration confirmation tab
    When I refresh "RegistrationConfirmationPage" page
    Then I should be on "GiftRegistry AdditionalDetailsPage" page