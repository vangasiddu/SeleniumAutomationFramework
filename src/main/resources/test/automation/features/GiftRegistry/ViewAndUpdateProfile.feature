@V1
Feature: Additional details and profile page functionalities

  @use_regression
  Scenario: Verify celebrant is able to view the additional details Page when login first time
    Given I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
      And I should see following elements on "GiftRegistry AdditionalDetailsPage" page:
      | title        |
      | firstName    |
      | lastName     |
      | suffix       |
      | spouse       |
      | email        |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
      | reset        |
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
      And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry HomePage" page

  Scenario: Verify that all the fields are present on additional details page when login first time
    Given I visit the website as "First time user" user
    When I should be on "GiftRegistry AdditionalDetailsPage" page
    Then I should see following elements on "GiftRegistry AdditionalDetailsPage" page:
      | title        |
      | firstName    |
      | lastName     |
      | email        |
      | suffix       |
      | spouse       |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
      | reset        |


  Scenario: Verify error messages for all required fields when no input on additional details page.
    Given I visit the website as "First time user" user
      And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I should see following elements on "GiftRegistry AdditionalDetailsPage" page:
      | title        |
      | firstName    |
      | lastName     |
      | email        |
      | suffix       |
      | spouse       |
      | telephone    |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
      | reset        |
    Then I verify error messages while registering with missing fields on additional details page
      | firstName    |
      | lastName     |
      | telephone    |
      | mobile       |
      | addressLine1 |
      | state        |
      | zipCode      |


  Scenario Outline: Verify error messages for all required fields when invalid input on additional details page.
    Given I visit the website as "First time user" user
      And I should be on "GiftRegistry AdditionalDetailsPage" page
      And I should see following elements on "GiftRegistry AdditionalDetailsPage" page:
      | title        |
      | firstName    |
      | lastName     |
      | suffix       |
      | spouse       |
      | email        |
      | telephone    |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
      | reset        |
    When I enter invalid details "<field name>" with "<input>" value on additional details page
    Then I should see the error "<error message>" message for "<field name>" on additional details page
    Examples:
      | field name | input | error message                                   |
      | telephone  | 1     | This entry must be in one of these formats      |
      | telephone  | @     | This entry must be in one of these formats      |
      | mobile     | 1     | This entry must be in this format: XXX-XXX-XXXX |
      | mobile     | @     | This entry must be in this format: XXX-XXX-XXXX |
      | zipCode    | !     | Zip Code should be atleast 4 Characters         |
      | zipCode    | !@34  | Please enter a valid Zip Code.                  |
      | zipCode    | 1     | Zip Code should be atleast 4 Characters         |

  Scenario Outline: Verify user should be able to enter telephone on different formats on additional details page.
    Given I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter telephone details "<field name>" with "<input>" value on additional details page
    Then I should be on "GiftRegistry HomePage" page
    Examples:
      | field name | input          |
      | telephone  | 1223456        |
      | telephone  | 12-2345623     |
      | telephone  | 412-2345624    |

  @use_regression
  Scenario: Verify that all the fields are present on profile page for existing user
    Given I visit the website as "First time user" user
      And I should be on "GiftRegistry AdditionalDetailsPage" page
      And I should see following elements on "GiftRegistry AdditionalDetailsPage" page:
      | title        |
      | firstName    |
      | lastName     |
      | suffix       |
      | spouse       |
      | email        |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
      | reset        |
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
      And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see all entered details on "GiftRegistry ProfilePage" page

  @use_regression
  Scenario: Verify existing user can able to update his profile
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see following elements on "GiftRegistry ProfilePage" page:
      | title        |
      | firstName    |
      | lastName     |
      | email        |
      | suffix        |
      | spouse        |
      | telephone    |
      | mobile       |
      | addressLine1 |
      | city         |
      | state        |
      | zipCode      |
      | submit       |
    When I update profile details on "GiftRegistry ProfilePage" page
      And I click on "submit" button on "GiftRegistry ProfilePage" page
    Then I should see all newly updated details on "GiftRegistry ProfilePage" page

  Scenario: Verify celebrant is able to navigate to registry profile page without changing data in update profile page
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "submit" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
    And I should see "editLink" link on "GiftRegistry ProfilePage" page

  Scenario: Verify celebrant is able to navigate to registry profile page after refreshing update profile page
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
    When I refresh "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see "editLink" link on "GiftRegistry ProfilePage" page

@ppr
  Scenario: Verify error messages for all required fields when no input on update profile page.
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see following elements on "GiftRegistry ProfilePage" page:
        | title        |
        | firstName    |
        | lastName     |
        | email        |
        | suffix       |
        | spouse       |
        | telephone    |
        | mobile       |
        | addressLine1 |
        | city         |
        | state        |
        | zipCode      |
        | submit       |
        | reset        |
      And I verify error messages while registering with missing fields on update profile page
        | firstName    |
        | lastName     |
        | telephone    |
        | mobile       |
        | addressLine1 |
        | zipCode      |

  Scenario Outline: Verify error messages for all required fields when invalid input on update profile page.
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see following elements on "GiftRegistry ProfilePage" page:
        | title        |
        | firstName    |
        | lastName     |
        | email        |
        | suffix       |
        | spouse       |
        | telephone    |
        | mobile       |
        | addressLine1 |
        | city         |
        | state        |
        | zipCode      |
        | submit       |
        | reset        |
    When I enter invalid details "<field name>" with "<input>" value on update profile page
    Then I should see the error "<error message>" message for "<field name>" on update profile page
    Examples:
      | field name | input | error message                                   |
      | telephone  | 1     | This entry must be in one of these formats      |
      | telephone  | @     | This entry must be in one of these formats      |
      | mobile     | 1     | This entry must be in this format: XXX-XXX-XXXX |
      | mobile     | @     | This entry must be in this format: XXX-XXX-XXXX |
      | zipCode    | !     | Zip Code should be atleast 4 Characters         |
      | zipCode    | !@34  | Please enter a valid Zip Code.                  |
      | zipCode    | 1     | Zip Code should be atleast 4 Characters         |

  Scenario Outline: Verify user should be able to enter telephone on different formats on update profile page.
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see following elements on "GiftRegistry ProfilePage" page:
        | title        |
        | firstName    |
        | lastName     |
        | email        |
        | suffix        |
        | spouse        |
        | telephone    |
        | mobile       |
        | addressLine1 |
        | city         |
        | state        |
        | zipCode      |
        | submit       |
    When I enter telephone details "<field name>" with "<input>" value on update profile page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should see "editLink" link on "GiftRegistry ProfilePage" page
    Examples:
      | field name | input |
      | telephone  | 1223456        |
      | telephone  | 12-2345623     |
      | telephone  | 412-2345624    |

  @use_regression
  Scenario: Verify duplicate states should not be displayed in select state dropdown on update profile page.
    Given I visit the website as "Registered" user
      And I should be on "GiftRegistry HomePage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    When I click on "editLink" button on "GiftRegistry ProfilePage" page
    Then I should be on "GiftRegistry ProfilePage" page
      And I should not see duplicate states in select state drop down

  @use_regression
  Scenario: Verify User should able to see additional details popup after closing the current browser and open again.
    Given I visit the website as "First time user" user
    When I should be on "GiftRegistry AdditionalDetailsPage" page
      And I close the current window and open new window
      And I visit the website in newly opened window
    Then I should be on "GiftRegistry AdditionalDetailsPage" page