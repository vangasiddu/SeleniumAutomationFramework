@V1
Feature: Change password


  Scenario: Verify celebrant able to change password through change password button in profile settings
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    When I select on "Change Password" link from header
     Then I should be on "GiftRegistry ChangePasswordPage" page
    And I should verify all UI check on change password page
    And I enter old password new password and confirm password
    Then I should see the success "Your Password has been successfully changed!" message on GiftRegistry ChangePassword page
    And I click on "okButton" button on "GiftRegistry ChangePasswordPage" page
    Then I should be on "GiftRegistry LoginPage" page

  @use_regression
  Scenario: Verify celebrant is able to login with changed password
    Given I visit the website as new user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    And I enter old password new password and confirm password
    Then I should see the success "Your Password has been successfully changed!" message on GiftRegistry ChangePassword page
    And I click on "okButton" button on "GiftRegistry ChangePasswordPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter email id and changed password
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry HomePage" page

  @use_regression
  Scenario: Verify error message when "New password" and "Confirm Password" are not matched on change password page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    And I enter old password and unmatched new password and confirm password
    And I click on "submitbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should see the error "Passwords didn't match!" message on ChangePasswordPage
    And I click on "resetbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should not see the error "Passwords didn't match!" message on ChangePasswordPage
    And I should not see the error "Current password entered is incorrect" message on ChangePasswordPage

  @use_regression
  Scenario: Verify error message when old password is not matched on change password page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    And I enter incorrect old password and matched new password and confirm password
    And I click on "submitbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should see the error "Current password entered is incorrect" message on ChangePasswordPage
    And I click on "resetbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should not see the error "Current password entered is incorrect" message on ChangePasswordPage

  @use_regression
  Scenario: Verify password restrictions error message for "New password" and "Confirm Password" fields on change password page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    And I enter old password and new password confirm password without matching password restriction string
    And I click on "submitbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should see below error message on ChangePasswordPage
      | Password should be min. 12 characters and should contain at least one upper case, one lower case, one special character (only .,?-_()':;!) and a number |

  @use_regression
  Scenario: Verify error messages when user tries to submit without any input on change password page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    And I click on "submitbutton" button on "GiftRegistry ChangePasswordPage" page
    Then I should see the below fields should be in red color
      | password        |
      | newpassword     |
      | confirmpassword |