@V4_Store
Feature: Customer Management at Store

Scenario: Verify User should be navigated to Customer Management page after logging into the application
Given I visit the website as "StoreUser"
And I should be on "GiftRegistry StoreLoginPage" page
When I enter Store User details on "GiftRegistry StoreLoginPage" page
And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
Then I should be on "GiftRegistry CustomerManagementPage" page
#
#  Scenario: Verify by creating new role in Role Management page
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
#    Then I should be on "GiftRegistry RoleManagementPage" page
#    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
#    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
#    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
#    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
#    And I should see newly created role name on "GiftRegistry RoleManagementPage" page

#  Scenario: Verify role permissions modification to a user from assign roles page
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
#    Then I should be on "GiftRegistry UserManagementPage" page
#    When I search with "test2" on the search bar on "GiftRegistry UserManagementPage" page
#    When I assign role for "test2" user on "GiftRegistry UserManagementPage" page
#    Then I should be on "GiftRegistry AssignRolesPage" page
#    And I "modify" role permission to "random" role from "GiftRegistry AssignRolesPage" page
#    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
#    And I should see the below message on AssignRolesPage
#      | Success! Details updated successfully. |
#
#
#  Scenario: Verify the sorting order display of user records based on first name in descending order
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    And I sort the user rows by "first_name" in "desc" order
#
# ############## NEED TO CHANGE DATE START##############
#  Scenario Outline: Verify store representative should be able to create event at store for registered users
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    When I search with valid "Registered" user from the search bar
#    And I verify store user search with valid user
#    Then I should see the details displayed
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    Then I should be on "GiftRegistry StoreCreateEventPage" page
#    When I enter valid event details for store "<Event_Name>" event on create event page
#    Then I should be on GiftRegistry Store Event Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see below messages on store event confirmation page
#      | Congrats! Your Event has been created!                                    |
#      | Please confirm the event registration through the link we sent to - email |
#    And I should see event code on store event confirmation page
#    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
#    And I search with eventcode and click on search button
#    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
#    And I should see "Success! Event has been Confirmed" message on snack bar
#    Examples:
#      | Event_Name     |
#      | House Warming  |
#      | Wedding        |
#      | Birthday-Adult |
#      | Birthday-Kids  |
#      | Birthday-Debut |
#
#
#   ############## NEED TO CHANGE DATE END##############
#
######################### SPECIAL CASE START #############
#  Scenario Outline: Verify event search using celebrant name on event management page for all types of events
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    When I search with valid "Registered" user from the search bar
#    And I verify store user search with valid user
#    Then I should see the details displayed
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    Then I should be on "GiftRegistry StoreCreateEventPage" page
#    When I enter valid event details for store "<Event_Name>" event on create event page
#    Then I should be on GiftRegistry Store Event Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see below messages on store event confirmation page
#      | Congrats! Your Event has been created!                                    |
#      | Please confirm the event registration through the link we sent to - email |
#    And I should see event code on store event confirmation page
#    When I click on "eventManagement" button on "GiftRegistry StoreEventConfirmationPage" page
#    And I search with "celebrant" on the search bar on "GiftRegistry StoreEventManagementPage" page
#    Then I should see the correct event "celebrant name" displayed for the user on "GiftRegistry StoreEventManagementPage" page
#    Examples:
#      | Event_Name     |
#      | House Warming  |
##      | Birthday-Adult |
##      | Birthday-Kids  |
##      | Birthday-Debut |
##      | Wedding        |
#  ########################## SPECIAL CASE END ##############################
#
#  Scenario: Verify when the user select either button in Event_Confirmation_Channel should able to create the in store and confirm on store
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    Then I click on "configuration" tab
#    And I click on "event" element and "eventConfirmationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
#    And I should see "either" and "eitherRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
#    And I click on "Customer Management" tab
#    When I search with valid "Registered" user from the search bar
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    Then I should be on "GiftRegistry StoreCreateEventPage" page
#    When I enter valid event details for store "Wedding" event on create event page
#    Then I should be on GiftRegistry Store Event Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see below messages on store event confirmation page
#      | Congrats! Your Event has been created!                                      |
#      | Please confirm the event registration through the link we sent to - 'email' |
#    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
#    And I search with eventcode and click on search button
#    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
#    And I should see "Success! Event has been Confirmed" message on snack bar
#
#  ##################################################################
#
#  Scenario: Verify admin should be able to see all the available roles in select role drop down on edit privileges page
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
#    Then I should be on "GiftRegistry RoleManagementPage" page
#    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
#    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
#    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
#    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
#    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
#    And I collect all the role names from "GiftRegistry RoleManagementPage" page
#    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
#    Then I should be on "GiftRegistry EditPrivilegesPage" page
#    And I should be able to see all the roles list when user clicks on select role option
#
#
#  Scenario: Verify error messages for all required fields when invalid input on store login page.
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter invalid details field name with input values on GiftRegistry StoreLoginPage page
#      | userId    | password      |
#      | !@#$      | !@#$          |
#    Then I should see error "We are unable to process your request at the moment. Please try after some time!" message on "GiftRegistry StoreLoginPage" page
#
#
#  Scenario: Verify by deleting role in Role Management page
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
#    Then I should be on "GiftRegistry RoleManagementPage" page
#    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
#    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
#    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
#    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
#    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
#    And I should be able to "Delete" for newly created role name on "GiftRegistry RoleManagementPage" page
#    Then I should not see deleted user role on "GiftRegistry RoleManagementPage" page
#
#
#  Scenario: Verify the user able to see printCard for active users when print card channel in no status
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    Then I click on "configuration" tab
#    And I click on "event" element and "printCardChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
#    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
#    And I click on "Customer Management" tab
#    When I search with valid "Registered" user from the search bar
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    Then I should be on "GiftRegistry StoreCreateEventPage" page
#    When I enter valid event details for store "Wedding" event on create event page
#    Then I should be on GiftRegistry Store Event Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see below messages on store event confirmation page
#      | Congrats! Your Event has been created!                                      |
#      | Please confirm the event registration through the link we sent to - 'email' |
#    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
#    And I search with eventcode and click on search button
#    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
#    And I should not see "printCard" element on "GiftRegistry StoreEventManagementPage" page


#
#  Scenario Outline: Verify the user able to cancel the event in store for all events
#    Given I visit the website as "StoreUser"
#    And I should be on "GiftRegistry StoreLoginPage" page
#    When I enter Store User details on "GiftRegistry StoreLoginPage" page
#    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
#    Then I should be on "GiftRegistry CustomerManagementPage" page
#    When I search with valid "Registered" user from the search bar
#    And I verify store user search with valid user
#    Then I should see the details displayed
#    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
#    Then I should be on "GiftRegistry StoreCreateEventPage" page
#    When I enter valid event details for store "<Event_Name>" event on create event page
#    Then I should be on GiftRegistry Store Events Invitation Page
#    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
#    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
#    And I should see event code on store event confirmation page
#    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
#    And I search with eventcode and click on search button
#    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
#    And I should see "Success! Event has been Confirmed" message on snack bar
#    And I click on "cancelEvent" link on "GiftRegistry StoreEventManagementPage" page
#    And I give the reason for cancelling and click on Yes button
#    And I should see "Success! Event has been cancelled" message on snack bar
#    Examples:
#      | Event_Name     |
#  #    | House Warming  |
#      | Wedding        |
##      | Birthday-Adult |
##      | Birthday-Kids  |
##      | Birthday-Debut |