@V4_Store
Feature: User Management at Store

  Scenario: Verify navigation to user management page when user clicks on user management link on page header
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page

  Scenario: Verify the UI elements on User management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    And I should see following elements on "GiftRegistry UserManagementPage" page:
      | userManagementheader |
      | userName             |
      | companyHeader        |
      | branchHeader         |
      | actionsHeader        |
      | searchButton         |

  Scenario Outline: Verify store user is should be able to search celebrant with first name, last name and email
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "<keyword>" on the search bar on "GiftRegistry UserManagementPage" page
    Then I should see user information on "GiftRegistry UserMangementPage" page
    Examples:
      | keyword |
      | sushma  |
      | karri   |
#      | gunti.pm@gmail.com     |

  Scenario Outline: Verify no user details found message on user management page when no records found while searching for an user
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "<keyword>" on the search bar on "GiftRegistry UserManagementPage" page
    Then I should see user information on "GiftRegistry UserMangementPage" page
    Examples:
      | keyword            |
      | gunti              |
      | purushotham        |
      | gunti.pm@gmail.com |

  Scenario: Verify navigation to assign roles page from user management page by clicking on assign role link
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I click on "assignRole" link on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page

  Scenario: Verify all the fields present in Assign roles page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I click on "assignRole" link on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I should see following elements on "GiftRegistry AssignRolesPage" page:
      | assignRoles |
      | company     |
      | branch      |
      | saveButton  |
      | backButton  |

  Scenario: Verify role permissions modification to a user from assign roles page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "sushma" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "sushma" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I "modify" role permission to "random" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    And I should see the below message on AssignRolesPage
      | Success! Details updated successfully. |

  Scenario: Verify by clicking back button while assigning a role to an user from Assign roles page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I click on "assignRole" link on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    When I click on "backButton" button on "GiftRegistry AssignRolesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page

  Scenario: Verify the pagination functionality on user management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry UserManagementPage" page
    And I verify the pagination value with value "10" on "GiftRegistry UserManagementPage" page
    And I verify the pagination value with value "20" on "GiftRegistry UserManagementPage" page

  Scenario: Verify the page navigation using pagination options on user management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "userManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry UserManagementPage" page
    And I verify navigation to "next" page from pagination on "GiftRegistry UserManagementPag" page
    And I verify navigation to "previous" page from pagination on "GiftRegistry UserManagementPag" page

  Scenario: Verify user is able to create role, assign permissions related "CUSTOMER MANAGEMENT ONLY" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | Customer Management |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should have access to only below options on the correct site:
      | Customer Management |

  Scenario: Verify user is able to create role, assign permissions related "ROLE MANAGEMENT ONLY" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | Role Management |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I should have access to only below options on the correct site:
      | Role Management |

  Scenario: Verify user is able to create role, assign permissions related "USER MANAGEMENT ONLY" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | User Management |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    And I should have access to only below options on the correct site:
      | User Management |
    And I click on "userManagement" link on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    And I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    Then I should see correct company and branch are allocated to the user

  Scenario: Verify user is able to create role, assign permissions related "EVENT MANAGEMENT ONLY" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | Event Management |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry EventManagementPage" page
    And I should have access to only below options on the correct site:
      | Event Management |

  Scenario: Verify user is able to create role, assign permissions related "CONFIGURATION MANAGEMENT ONLY" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | Configuration Management |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry ConfigurationManagementPage" page
    And I should have access to only below options on the correct site:
      | Configuration |

  Scenario: Verify user is able to create role, assign "ALL THE AVAILABLE PERMISSIONS" to the newly created role and assign the new role to a user successfully
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I select the below "header" privileges option:
      | Customer Management      |
      | Configuration Management |
      | User Management          |
      | Event Management         |
      | Role Management          |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    When I click on "userManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry UserManagementPage" page
    When I search with "parameshwar" on the search bar on "GiftRegistry UserManagementPage" page
    When I assign role for "parameshwar" user on "GiftRegistry UserManagementPage" page
    Then I should be on "GiftRegistry AssignRolesPage" page
    And I unselect assigned roles on "GiftRegistry AssignRolesPage" page
    And I "select" role permission to "newRole" role from "GiftRegistry AssignRolesPage" page
    And I click on "saveButton" button on "GiftRegistry AssignRolesPage" page
    Then I click on "signoutBtn" button on "GiftRegistry AssignRolesPage" page
    And I should be on "GiftRegistry StoreLoginPage" page
    And I enter Normal Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should have access to only below options on the correct site:
      | Customer Management |
      | User Management     |
      | Event Management    |
      | Role Management     |
      | Configuration       |
