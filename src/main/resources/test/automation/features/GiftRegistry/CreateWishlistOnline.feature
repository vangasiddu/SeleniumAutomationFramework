@V2
Feature: Wishlist creation - online

  Scenario Outline: Verify all the fields present on the create wishlist page when no item added in wishlist
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    And I should see my wishlist count as "0" on "GiftRegistry CreateWishlistPage" Page
    And I should see following elements on "GiftRegistry CreateWishlistPage" page:
      | wishlistMessage      |
      | myWishlistLink       |
      | myWishlistCount      |
      | shopCatagoryDropdown |
      | addToWishListBtn     |
      | quantity             |
    And I should see "200" response for products image on "GiftRegistry CreateWishlistPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify user should be able to add products in wishlist for all events from create wishlist page
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    And I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should see same products added with quantity "2" in my wishlist page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify user should be able to add products in wishlist for all events from item details page
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should see same products added with quantity "2" in my wishlist page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify user should be able to update products quantity on my wishlist page
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    Then I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    And I should see same products added with quantity "2" in my wishlist page
    When I update the quantity of the product to "4" on "GiftRegistry MyWishlistPage" page
    Then I should see same products updated with quantity "4" in my wishlist page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify user should be able to delete products on my wishlist page
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    And I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should see same products added with quantity "2" in my wishlist page
    When I delete the product on "GiftRegistry MyWishlistPage" page
    Then I should not see deleted product on "GiftRegistry MyWishlistPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify user should be able to add product in most loved category on my wishlist page
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should see same products added with quantity "2" in my wishlist page
    When I click on "mostLoved" option for the product on "GiftRegistry MyWishlistPage" page
    Then I should see "mostLoved" option selected for the same product on "GiftRegistry MyWishlistPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
       | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify celebrant is able to navigate to create wishlist page from Event confirmation page by clicking on 'Add Wishlist' button
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    And I should see "addWishlistButton" button on "GiftRegistry EventVerificationPage" page
    And I click on "addWishlistButton" element on "GiftRegistry EventVerificationPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify all fields are present on my wishlist page when no wishlist created
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    And I should see my wishlist count as "0" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should be on "GiftRegistry MyWishlistPage" page
    And I should see following elements on "GiftRegistry MyWishlistPage" page:
      | headerText         |
      | addMoreItemsLink |
      | noItemsInWishlist  |
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify all fields are present on my wishlist page when wishlist created
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    And I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should be on "GiftRegistry MyWishlistPage" page
    And I should see following elements on "GiftRegistry MyWishlistPage" page:
      | headerText         |
      | addMoreItemsLink   |
      | quantity           |
      | mostLoved          |
      | delete             |
      | skuId              |
      | productImage       |

    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify user should be able to add products in wishlist for all events from create wishlist page using shop by category
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    And I select random category from select category menu
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    Then I should see "Success! Item Added to Wishlist" message in snack bar on "GiftRegistry CreateWishlistPage"page
    And I should see my wishlist count as "1" on "GiftRegistry CreateWishlistPage" Page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should be on "GiftRegistry MyWishlistPage" page
    And I should see following elements on "GiftRegistry MyWishlistPage" page:
      | headerText         |
      | addMoreItems       |
      | addMoreItemsLink   |
      | quantity           |
      | mostLoved          |
      | delete             |
      | skuId              |
      | productImage       |
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify all the fields present in the item details page
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    And I should see event status as "Active" on my events page
    Then I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I click on "productImage" element on "GiftRegistry CreateWishlistPage" page
    Then I should be on "GiftRegistry ItemDetailsPage" page
    And I should see following elements on "GiftRegistry ItemDetailsPage" page:
      | headerText       |
      | quantity         |
      | addtoWishlistBtn |
      | overviewBlock    |
      | detailsBlock     |
      |  imageBlock      |
      |  backBtn         |
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |


