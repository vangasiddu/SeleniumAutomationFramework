Feature: SME DB verification features

  Scenario: Verify email id, company id and status code in egr_user table after Celebrant registration, before confirmation
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry LoginPage" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I verify email id is persisting in ege_user table
    And I verify status code set to 2
    And I verify company id is saving correctly

  Scenario: Verify status code is changed to 1 in egr_user table after Celebrant registration confirmation is done
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry LoginPage" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    When I confirm the registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I verify status code set to 1


  Scenario: Verify egr_user table mandatory fields after Celebrant registration
    Given I visit the website as new user
 #   When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry LoginPage" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I verify mandatory field values are persisting in egr_user table


  Scenario: Verify event status for event in pending, active and cancellation status
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry EventInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I verify event status code set to 2
    And I should see event code on event confirmation page
    And I should see "resendConfirmationLink" link on "GiftRegistry EventConfirmationPage" page
    When I confirm the event confirmation online
    Then I should be on "GiftRegistry EventVerificationPage" page
    And I verify event status code set to 3
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    And I click on "dropdownMenuButton" button on "GiftRegistry MyEventPage" page
    And I click on "cancelEventLink" link on "GiftRegistry MyEventPage" page
    And I should see "Yes" element
    And I should see "No" element
    And I enter reason for event cancellation
    When I click on "Yes" button on "GiftRegistry MyEventPage" page
    Then I should see "Success! Event has been cancelled" message in snack bar
    And I verify event status code set to 5

  Scenario: Verify event_id in egr_event after event creation done
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry EventInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see event id in egr_event table
