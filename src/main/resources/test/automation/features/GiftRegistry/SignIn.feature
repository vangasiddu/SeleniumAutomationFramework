@V1
Feature: Sign In

  @use_regression
  Scenario: Verify celebrant is login successfully when already registered.
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I login with already registered details on "GiftRegistry SignModel" page
    Then I should be on "GiftRegistry AdditionalDetailsPage" page

    @defect-1485
    Scenario: Verify celebrant is able to login successfully using enter key and focus is on Login button.
    Given I visit the website as new user
#   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on enter key on keyboard and focus is on login button
    Then I should be on "GiftRegistry HomePage" page


   Scenario: Verify celebrant is able to login successfully using enter key and focus is on password field.
    Given I visit the website as new user
#   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on enter key on keyboard and focus is on password field
    Then I should be on "GiftRegistry HomePage" page

  @use_regression
  Scenario: Verify error messages for all required fields when no input on EGR login page.
    Given I visit the website as new user
#      And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    Then I verify error messages when enter with missing fields on EGR page
      | Email          | Password  | Message  |
      | test@gmail.com |           | Required |
      |                | test@1234 | Required |
      |                |           | Required |

  @use_regression
  Scenario Outline: Verify error messages for all required fields when invalid input on signIn modal section on EGR login page.
    Given I visit the website as new user
    # When I click on "Take Me to Gift Registry" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I enter invalid details "<field name>" with "<input>" value on "GiftRegistry SignModel" page
    Then I should see the error "<error message>" message for "<field name>" on "GiftRegistry SignModel" of login page
    Examples:
      | field name | input | error message                                   |
      | test@gmail.com   | test@#$@  | Invalid email and password. |
      | test##@gmail.com | test@1234 | Invalid email               |

  @use_regression
    Scenario: Verify celebrant is able to login successfully if provided emailId is Case Sensitive
    Given I visit the website as new user
  #      And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter already registered details with case sensitive email on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry HomePage" page

  @use_regression
  Scenario: Verify celebrant login password should display when user selected password view option while signIn.
    Given I visit the website as new user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter already registered details on "GiftRegistry SignModel" page
    And I click on "showPassword" button on "GiftRegistry LoginPage" page
    Then I should see password in text format

  Scenario: Verify show password icon should not display twice in sign in page.
    Given I visit the website as new user
    Then I should be on "GiftRegistry LoginPage" page
    When I enter already registered details on "GiftRegistry SignModel" page
    Then I should not see show password icon twice

    #These test case will fill with existing user
  Scenario: Verify celebrant is login successfully if registered using Gmail
    Given I visit the website as new user
    Then I should be on "GiftRegistry LoginPage" page
    And I click on "googleLoginbutton" button on "GiftRegistry LoginPage" page
    And I enter email and password and click on next button
#    Then I should be on "GiftRegistry AdditionalDetailsPage" page
#    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
#    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry HomePage" page

  @wip
  Scenario: Verify celebrant is login successfully if already registered using Facebook (Social Media)
    Given I visit the website as new user
    Then I should be on "GiftRegistry LoginPage" page
    And I click on "facebookLoginbutton" button on "GiftRegistry LoginPage" page
    When I enter already registered details on "GiftRegistry FacebookModel" page
    Then I should be on "GiftRegistry HomePage" page









