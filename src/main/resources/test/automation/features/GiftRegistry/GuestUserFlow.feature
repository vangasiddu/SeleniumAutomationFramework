@V3
Feature: Guest user flow

  Scenario Outline: Validate anonymous guest user able to view event details(Invitation) page received via Celebrant's invitation email for all the events
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify the guest registration(including registration confirmation) on guest login page
    Given I visit the active event invitation link for "<event>" using hexcode from DB
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    When I confirm the registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |
    And I click on "signInButton" button on "GiftRegistry AccountVerificationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I login with already registered details on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify the guest sign in through guest login page
    Given I visit the active event invitation link for "<event>" using hexcode from DB
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Validate whether Registered guest user able to  view event details(Invitation) page received via Celebrant's invitation email for all the events
    Given I visit the active event invitation link for "<event>" using hexcode from DB
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I select on "My Events" link from "GiftRegistry GuestInvitationPage" page
    When I click on "invitedEventsTab" button on "GiftRegistry MyEventPage" page
    Then I should see "<event1>" on my events page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario: Verify My Events tab(should view Events List/Invited Events)for Registered guest user.
    Given I visit the active event invitation link for "babyShower" using hexcode from DB
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I select on "My Events" link from "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry MyEventPage" page:
      | MyEventsTab      |
      | invitedEventsTab |

  Scenario: Verify anonymous guest should not view the My events page
    Given I visit the active event invitation link for "babyShower" using hexcode from DB
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I select on "My Events" link from "GiftRegistry GuestInvitationPage" page
    And I should be on "GiftRegistry LoginPage" page

  Scenario Outline: Verify anonymous user should able to navigate to event wishlist page by clicking "Go to Wishlist" button on guest invitation template
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify registered guest user should able to navigate to event wishlist page by clicking "Go to Wishlist" button on guest invitation template
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Validate whether "anonymous" guest user able to view the latest invitation page which was resent by celebrant
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    When I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    Then I should be on "GiftRegistry EventVerificationPage" page
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    And I should see event status as "Active" on my events page
    When I select on "Sign Out" link from header
    And I navigate to guest invitation page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Validate whether "registered" guest user able to view the latest invitation page which was resent by celebrant
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    When I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    Then I should be on "GiftRegistry EventVerificationPage" page
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    And I should see event status as "Confirmation Pending" on my events page
    And I click on "dropdownMenuLink" link on "GiftRegistry MyEventPage" page
    And I should see "resendInvitationLink" element
    And I click on "resendInvitationLink" link on "GiftRegistry MyEventPage" page
    When I select on "Sign Out" link from header
    And I navigate to guest invitation page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify all item details on Wishlist page that are added by celebrant to My wishlist for anonymous user
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    When I click on "wishlist" element on "GiftRegistry MyEventPage" page
    Then I should be on "GiftRegistry CreateWishlistPage" page
    And I add product to wishlist with quantity "10" from "GiftRegistry CreateWishlistPage" page
    When I navigate to website as guest user
    And I should be on "GiftRegistry GuestInvitationPage" page
    And I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see all item details on event Wishlist page that are added by celebrant
    And I should see "myCart" button on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify all item details on Wishlist page that are added by celebrant to My wishlist for Registered guest user
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    When I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I click on "addMoreItemsLink" link on "GiftRegistry MyWishlistPage" page
    Then I should be on "GiftRegistry CreateWishlistPage" page
    And I add product to wishlist with quantity "10" from "GiftRegistry CreateWishlistPage" page
    When I navigate to website as guest user
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see all item details on event Wishlist page that are added by celebrant
    And I should see "myCart" button on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify by entering more qty than the celebrant requested for anonymous guest user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "6" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    Then I should see "Quantity should not be more than requested quantity" error message on event wishlist page
    And I should see My cart quantity as "0" on "GiftRegistry EventWishlistPage" page

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify by entering more qty than the celebrant requested for registered guest user
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "6" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    Then I should see "Quantity should not be more than requested quantity" error message on event wishlist page
    And I should see My cart quantity as "0" on "GiftRegistry EventWishlistPage" page

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify the cancelled event in the Invited Events of My Events tab for registered guest user.
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I cancel the event by updating event status in DB
    And I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    When I click on "invitedEventsTab" button on "GiftRegistry MyEventPage" page
    Then I should see event status as "Cancelled" on my events page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify anonymous guest user is able to add wishlist product to my cart from event wishlist page for all the event
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "1" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see My cart quantity as "1" on "GiftRegistry EventWishlistPage" page
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    And I should be on "GiftRegistry MyCartPage" page
    Then I should see same products added with quantity "1" in my cart page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify registered user is able to add wishlist product to my cart from event wishlist page for all the event
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter signin details on "GiftRegistry SignModel" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "1" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see My cart quantity as "1" on "GiftRegistry EventWishlistPage" page
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    And I should be on "GiftRegistry MyCartPage" page
    Then I should see same products added with quantity "1" in my cart page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify celebrant requested item qty is not editable on event wishlist page for guest registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see all item details on event Wishlist page that are added by celebrant
    And I should see the requested quantity is not editable by the guest user

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify the updated qty is displayed in the celebrant requested item qty field in event wishlist page for the registered guest user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see the requested quantity is same as updated quantity

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify the updated qty is displayed in the celebrant requested item qty field in wishlist for the anonymous guest user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    And I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should see the requested quantity is same as updated quantity

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify all fields are present on event wishlist page when product added by the celebrant on registered guest wishlist page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see following elements on "GiftRegistry EventWishlistPage" page:
      | eventWishlistHeader |
      | myCart              |
      | productSKU          |
      | rqsQuantity         |
      | myCartLink          |
      | myCartCount         |
      | addToCartBtn        |

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline:Verify all fields are present on event wishlist page when no wishlist created by the celebrant on registered guest wishlist page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see following elements on "GiftRegistry EventWishlistPage" page:
      | eventWishlistHeader |
      | myCart              |
      | myCartLink          |
      | myCartCount         |
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline:Verify all fields are present on event wishlist page when wishlist created by the celebrant on anyonymous guest wishlist page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry EventWishlistPage" page:
      | eventWishlistHeader |
      | myCart              |
      | productSKU          |
      | rqsQuantity         |
      | myCartLink          |
      | myCartCount         |
      | addToCartBtn        |

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline:Verify all fields are present on event wishlist page when no wishlist created by the celebrant on anyonymous guest wishlist page
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should see following elements on "GiftRegistry EventWishlistPage" page:
      | eventWishlistHeader |
      | myCart              |
      | myCartLink          |
      | myCartCount         |
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: : Verify celebrant requested item qty is not editable on wishlist page for guest anonymous user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I should see all item details on event Wishlist page that are added by celebrant
    And I should see the requested quantity is not editable by the guest user

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify anonymous user should navigate to event wishlist page when click on Add to to item from my cart page
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "1" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see My cart quantity as "1" on "GiftRegistry EventWishlistPage" page
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    And I should be on "GiftRegistry MyCartPage" page
    Then I should see same products added with quantity "1" in my cart page
    And I click on "addMoreItemsBtn" element on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify registerd guest user should navigate to event wishlist page when click on Add to to item from my cart page
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter signin details on "GiftRegistry SignModel" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I update the quantity of the product to "1" on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    And I should see My cart quantity as "1" on "GiftRegistry EventWishlistPage" page
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    And I should be on "GiftRegistry MyCartPage" page
    Then I should see same products added with quantity "1" in my cart page
    When I click on "addMoreItemsBtn" element on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify the in-active event in the Invited Events of My Events tab for registered guest user.
    Given I visit the website as guest user for "<event>" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I change event status as In-Active in DB
    And I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    When I click on "invitedEventsTab" button on "GiftRegistry MyEventPage" page
    Then I should see event status as "In-Active" on my events page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario: Verify when guest has added the item to my cart and celebrant has deleted the same item
    Given I visit the website as guest user for "babyShower" event
    And I should be on "GiftRegistry GuestInvitationPage" page
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I enter registered details on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry GuestInvitationPage" page
    And I should see "wishlistBtn" element
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    And I click on "addToCartBtn" element on "GiftRegistry EventWishlistPage" page
    Then I should see "Success! Item Added to cart" message in snack bar
    And I should see add to cart button change to added to cart in disabled view
    And I should see my cart count updated to 1
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry GuestCart" page
    And I should see selected product is added in cart page
    And I select on "Sign Out" link from header
    Then I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials
    And I select on "My Events" link from "GiftRegistry HomePage" page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I click on "myWishlistLink" element on GiftRegistry CreateWishlistPage page
    Then I should be on "GiftRegistry MyWishlistPage" page
    When I delete the product on "GiftRegistry MyWishlistPage" page
    Then I should not see deleted product on "GiftRegistry MyWishlistPage" page
    And I select on "Sign Out" link from header
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to previous guest invitation page with guest login details
    When I click on "wishlistBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventWishlistPage" page
    When I click on "myCartLink" element on "GiftRegistry EventWishlistPage" page
    Then I should be on "GiftRegistry GuestCart" page
    And I should see added product delected from the guest cart

#  @wip
#  Scenario Outline: Verify the skip registration button disable for the guest login for the rspective companies
#    Given I visit the website as guest user for "<event>" event
#    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
#      | registerSignInBtn |
#      | skipRegistration  |
#    And I verify the skipRegistration button disable for the following <"company"> url:
#    |crate and barel|
#
#  Examples:
#  | event         |
#  | babyShower    |
#  | baptism       |
#  | birthdayAdult |
#  | birthdayKids  |
#  | wedding       |
#  | birthdayDebut |
#  | anniversary   |
#  | houseWarming  |