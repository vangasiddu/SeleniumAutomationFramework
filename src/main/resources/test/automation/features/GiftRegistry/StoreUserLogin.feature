@V4_Store
Feature: Store User Flow

  Scenario: Verify that all the fields are present on store login page
    Given I visit the website as "StoreUser"
    When I should be on "GiftRegistry StoreLoginPage" page
    Then I should see following elements on "GiftRegistry StoreLoginPage" page:
      |logo                  |
      |loginHeader           |
      | userId               |
      | password             |
      | signInButton         |

  Scenario: Verify error messages for all required fields when no input on store login page.
    Given I visit the website as "StoreUser"
    When I should be on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I verify error messages while registering with missing fields
      | userId           | Required |
      | Password         | Required |

  Scenario: Verify error messages for all required fields when invalid input on store login page.
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter invalid details field name with input values on GiftRegistry StoreLoginPage page
      | userId    | password      |
      | !@#$      | !@#$          |
    Then I should see error "Your User Id or Password is invalid!" message on "GiftRegistry StoreLoginPage" page

  Scenario: Verify store user is successfully login from store login page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page


