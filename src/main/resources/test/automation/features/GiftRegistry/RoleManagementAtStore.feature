@V4_Store
Feature: Role Management At Store

  Scenario: Verify navigation to role management page when user clicks on role management link on page header
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page

  Scenario: Verify the UI elements on role management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I should see following elements on "GiftRegistry RoleManagementPage" page:
      | roleNames     |
      | actions       |
      | createNewRole |
      | editPrivilege |
      | delete        |
      | header        |

  Scenario: Verify by creating new role in Role Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page

  Scenario: Verify by deleting role in Role Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I should be able to "Delete" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should not see deleted user role on "GiftRegistry RoleManagementPage" page

  Scenario: Verify the cancel button functionality while creating new role in Role Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "cancelBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page

  Scenario: Verify admin should be able to see all the available roles in select role drop down on edit privileges page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should be able to see all the roles list when user clicks on select role option
    And I should see no role assigned to newly created user

  Scenario: Verify all the privilege options are selected under Role Management header option when user select and save it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |
    And I select the below "header" privileges option:
      | Role Management |
    Then I should see role access granted for below "header and child" roles:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |

  Scenario: Verify all the privilege options are selected under User Management header option when user select and save it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Assign_Roles |
      | View_Users   |
    And I select the below "header" privileges option:
      | User Management |
    Then I should see role access granted for below "header and child" roles:
      | Assign_Roles |
      | View_Users   |

  Scenario: Verify all the privilege options are selected under Customer Management header option when user select and save it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |
    And I select the below "header" privileges option:
      | Customer Management |
    Then I should see role access granted for below "header and child" roles:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |

  Scenario: Verify all the privilege options are selected under Event Management header option when user select and save it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | CancelEvent                  |
      | ViewWishlist                 |
      | ConfirmEvent                 |
      | UpdateEvent                  |
      | ViewEvents                   |
      | CreateEvent                  |
      | ResendConfirmEventLink       |
      | SendInvitationViaMail        |
      | SendInvitationViaSocialMedia |
      | ManageWishlist               |
      | PrintEventCard               |
  #      |CreateWishlistusingPDTdevice      |
  #      |CreateWishlistUsingBarcodeScanner |
  #      |ReSendInvitation                  |
  #      |PurchaseUsingBarcodeScanner       |
  #      |PurchaseUsingCartBarcode          |
  #      |UpdateWishlist                    |
    And I select the below "header" privileges option:
      | Event Management |
    Then I should see role access granted for below "header and child" roles:
      | Cancel_Event                     |
      | View_Wishlist                    |
      | Confirm_Event                    |
      | Update_Event                     |
      | View_Events                      |
      | Create_Event                     |
      | Resend_Confirm_Event_Link        |
      | Send_Invitation_Via_Mail         |
      | Send_Invitation_Via_Social_Media |
      | Manage_Wishlist                  |
      | Print_Event_Card                 |

  Scenario: Verify all the privilege options are selected under Configuration Management header option when user select and save it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I collect all the role names from "GiftRegistry RoleManagementPage" page
    And I should be able to "Edit Privilege" for newly created role name on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see no role assigned to newly created user
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration_OMNI |
    And I select the below "header" privileges option:
      | Configuration Management |
    Then I should see role access granted for below "header and child" roles:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |

  Scenario: Verify admin is able to provide access to individual roles under Role management option
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |
    And I select the below "child" privileges option:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |
    Then I should see role access granted for below "child" roles:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |

  Scenario: Verify admin is able to provide access to individual roles under user management option
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Assign_Roles |
      | View_Users   |
    And I select the below "child" privileges option:
      | Assign_Roles |
      | View_Users   |
    Then I should see role access granted for below "child" roles:
      | Assign_Roles |
      | View_Users   |

  Scenario: Verify admin is able to provide access to individual roles under Customer management option
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |
    And I select the below "child" privileges option:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |
    Then I should see role access granted for below "child" roles:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |

  Scenario: Verify admin is able to provide access to individual roles under Event management option
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | CancelEvent                  |
      | ViewWishlist                 |
      | ConfirmEvent                 |
      | UpdateEvent                  |
      | ViewEvents                   |
      | CreateEvent                  |
      | ResendConfirmEventLink       |
      | SendInvitationViaMail        |
      | SendInvitationViaSocialMedia |
      | ManageWishlist               |
      | PrintEventCard               |
#      |CreateWishlistusingPDTdevice      |
#      |CreateWishlistUsingBarcodeScanner |
#      |ReSendInvitation                  |
#      |PurchaseUsingBarcodeScanner       |
#      |PurchaseUsingCartBarcode          |
#      |UpdateWishlist                    |
    And I select the below "child" privileges option:
      | Cancel_Event                     |
      | View_Wishlist                    |
      | Confirm_Event                    |
      | Update_Event                     |
      | View_Events                      |
      | Create_Event                     |
      | Resend_Confirm_Event_Link        |
      | Send_Invitation_Via_Mail         |
      | Send_Invitation_Via_Social_Media |
      | Manage_Wishlist                  |
      | Print_Event_Card                 |
    Then I should see role access granted for below "child" roles:
      | Cancel_Event                     |
      | View_Wishlist                    |
      | Confirm_Event                    |
      | Update_Event                     |
      | View_Events                      |
      | Create_Event                     |
      | Resend_Confirm_Event_Link        |
      | Send_Invitation_Via_Mail         |
      | Send_Invitation_Via_Social_Media |
      | Manage_Wishlist                  |
      | Print_Event_Card                 |

  Scenario: Verify admin is able to provide access to individual roles under Configuration management option
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration_OMNI |
    And I select the below "child" privileges option:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |
    Then I should see role access granted for below "child" roles:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |

  Scenario: Verify the UI elements present on the Edit privileges page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I should see following elements on "GiftRegistry EditPrivilegesPage" page:
      | SelectRole |
      | save       |
      | back       |

  Scenario: Verify the role access granted message displayed when privileges are selected under Role Management on edit privilege page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    And I should see role access granted for below "child" roles:
      | Update_Privilege |
      | Delete_Role      |
      | Create_Role      |
      | View_Roles       |
      | Update_Role_Name |

  Scenario: Verify the role access granted message displayed when multiple privileges selected under User Management on edit privilege page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | Assign_Roles |
      | View_Users   |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    Then I should see role access granted for below "child" roles:
      | Assign_Roles |
      | View_Users   |

  Scenario: Verify the role access granted message displayed when multiple privileges selected under Customer Management on edit privilege page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    Then I should see role access granted for below "child" roles:
      | Register_Customer                |
      | View_Customers                   |
      | Resend_Confirm_Registration_Link |


  Scenario: Verify the role access granted message displayed when multiple privileges selected under Event Management on edit privilege page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | Cancel_Event                     |
      | View_Wishlist                    |
      | Confirm_Event                    |
      | Update_Event                     |
      | View_Events                      |
      | Create_Event                     |
      | Resend_Confirm_Event_Link        |
      | Send_Invitation_Via_Mail         |
      | Send_Invitation_Via_Social_Media |
      | Manage_Wishlist                  |
      | Print_Event_Card                 |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    Then I should see role access granted for below "child" roles:
      | Cancel_Event                     |
      | View_Wishlist                    |
      | Confirm_Event                    |
      | Update_Event                     |
      | View_Events                      |
      | Create_Event                     |
      | Resend_Confirm_Event_Link        |
      | Send_Invitation_Via_Mail         |
      | Send_Invitation_Via_Social_Media |
      | Manage_Wishlist                  |
      | Print_Event_Card                 |


  Scenario: Verify the role access granted message displayed when multiple privileges selected under Configuration Management on edit privilege page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    When I click on "submitBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should see "Success! "role_name" role created successfully" message on "GiftRegistry RoleManagementPage" page
    And I should see newly created role name on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    Then I should see "Success! "role_name" Permissions updated successfully" message on "GiftRegistry EditPrivilegesPage" page
    Then I should see role access granted for below "child" roles:
      | Manage_Configurations                 |
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |

  Scenario Outline: Verify user navigation to other pages from edit privileges page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I click on "<component>" link on "GiftRegistry EditPrivilegesPage" page
    Then I should be on "GiftRegistry <page>" page
    Examples:
      | component          | page                        |
      | customerManagement | CustomerManagementPage      |
      | userManagement     | UserManagementPage          |
      | eventManagement    | EventManagementPage         |
      | roleManagement     | RoleManagementPage          |
      | Configuration      | ConfigurationManagementPage |

  Scenario: Verify latest created role is shown top on Role Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter random rol_name on "GiftRegistry RoleManagementPage" page
    And I click on "submit" button on "GiftRegistry RoleManagementPage" page
    Then I should see the role displayed at the top of the list

  Scenario: Verify latest updated role is shown top on Role Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "editPrivilege" link on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry EditPrivilegesPage" page
    And I get the "role_name"  from "select role" dropdown
    And I select the below "child" privileges option:
      | View_Configurations                   |
      | Product_Attributes_Configuration-OMNI |
    And I click on "save" button on "GiftRegistry EditPrivilegesPage" page
    And I click on "roleManagement" link on "GiftRegistry EditPrivilegesPage" page
    Then I should see the role displayed at the top of the list

  Scenario Outline: Verify the message shown when user tries to create a role with same name which already exist
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter "<role_name>" on "GiftRegistry RoleManagementPage" page
    And I click on "submit" button on "GiftRegistry RoleManagementPage" page
    Then I should see duplicate role error "Role already exists." message on "GiftRegistry RoleManagementPage" page
    Examples:
      | role_name  |
      | admin1234  |
      | AAdmin2345 |
      | Manager12  |

  Scenario Outline: Verify the message shown when user tries to create a role with name containing special characters in it
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    And I click on "createNewRole" button on "GiftRegistry RoleManagementPage" page
    And I enter "<role_name>" on "GiftRegistry RoleManagementPage" page
    And I click on "submit" button on "GiftRegistry RoleManagementPage" page
    Then I should see error "Role Name should be alphanumeric" message on "GiftRegistry RoleManagementPage" page
    Examples:
      | role_name    |
      | admin@1234   |
      | !@AAdmin2345 |
      | *#Manager12  |

  Scenario: Verify user is able to sign out from application successfully from Role management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "RoleManagement" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry RoleManagementPage" page
    When I click on "signoutBtn" button on "GiftRegistry RoleManagementPage" page
    Then I should be on "GiftRegistry StoreLoginPage" page
