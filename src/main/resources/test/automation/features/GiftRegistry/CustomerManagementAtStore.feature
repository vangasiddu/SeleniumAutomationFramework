@V4_Store
Feature: Customer Management at Store

  Scenario: Verify User should be navigated to Customer Management page after logging into the application
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page

  Scenario: Verify UI elements in Customer Management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should see following elements on "GiftRegistry CustomerManagementPage" page:
      | customerManagementheader |
      | firstName                |
      | lastName                 |
      | email                    |
      | status                   |
      | action                   |
      | searchButton             |
      | pagination               |
      | createNewCustomerIcon    |
      | refreshDataIcon          |

  Scenario Outline: Verify the details of customer shown by searching with first name on the search bar
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "<keyword>" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    Examples:
      | keyword               |
      | crate                 |
      | Smtest                |
      | CHRISTOPHER NAGARJUNA |

  Scenario Outline: Verify the details of customer shown by searching with last name on the search bar
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "<keyword>" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    Examples:
      | keyword |
      | test    |
      | one     |
      | charles |

  Scenario Outline: Verify the details of customer shown by searching with emailid on the search bar
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "<keyword>" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    Examples:
      | keyword                         |
      | hepts0z9tr885908@void.osius.com |
      | smf62efjieu27426@void.osius.com |
      | yzqfol7j58524553@void.osius.com |

  Scenario: Verify the details of customer shown by searching with space after the search string
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "test " on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed

  Scenario Outline: Verify the details of customer shown by searching with alphanumeric characters
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "<keyword>" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    Examples:
      | keyword          |
      | test@12          |
      | test..*          |
      | yzqfol7j58524553 |

  Scenario: Verify user is able clear the search content from search field on customer management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should be able to clear the contents in the search text field on "GiftRegistry CustomerManagementPage" page

  Scenario Outline: Verify if correct user details are shown on results page on searching the customer
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "<Search_String>" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see correct user details are displayed on results page "GiftRegistry CustomerManagementPage" page with "<Search_String>" search string
    Examples:
      | Search_String                   |
      | test                            |
      | hepts0z9tr885908@void.osius.com |

  Scenario: Verify the user is able to create new customer from customer management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "createNewCustomerIcon" link on "GiftRegistry CustomerManagementPage" page
    And I visit the website opened in new window
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    When I confirm the store registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |

  Scenario: Verify the user action options shown on customer management page when customer account status is "confirmation pending and registered"
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "createNewCustomerIcon" link on "GiftRegistry CustomerManagementPage" page
    And I switch "forward" from current to other tab
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid user details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    And I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "registered" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    And I should see status as "CONFIRMATION_PENDING" on customer management page
    And I should see "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "viewEvents" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "createEvent" link on "GiftRegistry CustomerManagementPage" page
    When I confirm the store registration
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |
    And I switch "back" from current to other tab
    And I refresh "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    And I should see status as "REGISTERED" on customer management page
    And I should not see "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    And I should see following elements on "GiftRegistry CustomerManagementPage" page:
      | viewEvents  |
      | createEvent |
    And I click on "viewEvents" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry EventManagementPage" page
    And I should see appropriate message on "GiftRegistry EventManagementPage" page when no events found for new user
    When I navigate back to previous page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I click on "createEvent" link on "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry CreateEventPage" page

  Scenario: Verify the pagination drop down on customer management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry CustomerManagementPage" page
    And I verify the pagination value with value "10" on "GiftRegistry CustomerManagementPage" page
    And I verify the pagination value with value "15" on "GiftRegistry CustomerManagementPage" page

  Scenario: Verify the page navigation using pagination options on customer management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I should see pagination element default value is "5" on "GiftRegistry CustomerManagementPage" page
    And I verify navigation to "next" page from pagination on "GiftRegistry CustomerManagementPage" page
    And I verify navigation to "previous" page from pagination on "GiftRegistry CustomerManagementPage" page


  Scenario: Verify the sorting order display of user records based on first name
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "first_name" in "asc" order


  Scenario: Verify the sorting order display of user records based on last name
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "last_name" in "asc" order

  Scenario: Verify the sorting order display of user records based on emailId
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "email_id" in "asc" order

  Scenario: Verify the sorting order display of user records based on first name in descending order
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "first_name" in "desc" order

  Scenario: Verify the sorting order display of user records based on last name in descending order
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "last_name" in "desc" order

  Scenario: Verify the sorting order display of user records based on emailId in descending order
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I sort the user rows by "email_id" in "desc" order

  Scenario: Verify customer account registration process from store
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "createNewCustomerIcon" link on "GiftRegistry CustomerManagementPage" page
    And I switch "forward" from current to other tab
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid user details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    And I get the "new" hexacode from database
    And I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "registered" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    And I should see status as "CONFIRMATION_PENDING" on customer management page
    And I should see "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "viewEvents" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "createEvent" link on "GiftRegistry CustomerManagementPage" page
    And I click on "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    Then I should see "Success! Registration Confirmation Link has been sent to customer email" message in snack bar on "GiftRegistry CustomerManagementPage"page
    And I verify store registration confirmation with old hexacode
    And I verify new hexcode is generated for new confirmation link
    When I confirm the store registration
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |
    And I switch "back" from current to other tab
    And I refresh "GiftRegistry CustomerManagementPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    And I should see status as "REGISTERED" on customer management page

  Scenario: Verify email link expiry functionality after clicking on resend confirmation link option on customer management page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "createNewCustomerIcon" link on "GiftRegistry CustomerManagementPage" page
    And I switch "forward" from current to other tab
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid user details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    And I get the "new" hexacode from database
    And I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I search with "registered" on the search bar on "GiftRegistry CustomerManagementPage" page
    And I click on "searchButton" button on "GiftRegistry CustomerManagementPage" page
    Then I should see the details displayed
    And I should see status as "CONFIRMATION_PENDING" on customer management page
    And I should see "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "viewEvents" link on "GiftRegistry CustomerManagementPage" page
    And I should not see "createEvent" link on "GiftRegistry CustomerManagementPage" page
    And I click on "resendConfirmationLink" link on "GiftRegistry CustomerManagementPage" page
    Then I should see "Success! Registration Confirmation Link has been sent to customer email" message in snack bar on "GiftRegistry CustomerManagementPage"page
    And I verify store registration confirmation with old hexacode
    And I verify new hexcode is generated for new confirmation link
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your verification link is invalid! |
    When I confirm the store registration
    And I should see the below success Account verification message
      | Your account has been successfully verified! |

