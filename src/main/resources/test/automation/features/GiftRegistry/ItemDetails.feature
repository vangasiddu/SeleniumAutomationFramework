@V3
Feature: Item Details Page Verifications

  @use_regression
  Scenario Outline: Verify by clicking an product in Wishlist page navigating to PDP (Item details page)for guest anonymous user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    Then I should see following elements on "GiftRegistry ItemDetailsPage" page:
      |headerText|
      |addtoWishlistBtn|
      |backBtn         |
      |quantity        |
      |overviewBlock   |
      |detailsBlock    |
      |imageBlock      |
      |productName     |

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking an product in Wishlist page navigating to PDP (Item details page)for guest registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter already registered details with case sensitive email on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    Then I should see following elements on "GiftRegistry ItemDetailsPage" page:
      |headerText|
      |addtoWishlistBtn|
      |backBtn         |
      |quantity        |
      |overviewBlock   |
      |detailsBlock    |
      |imageBlock      |
      |productName     |

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking an product in Wishlist page navigating to PDP (Item details page)for new registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    Then I should see following elements on "GiftRegistry ItemDetailsPage" page:
      |headerText|
      |addtoWishlistBtn|
      |backBtn         |
      |quantity        |
      |overviewBlock   |
      |detailsBlock    |
      |imageBlock      |
      |productName     |

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by adding the qty in PDP page should reflect in the wishlist page for guest anonymous user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "3" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    Then I should see "Success! Item Added to Cart" message in snack bar on "GiftRegistry ItemDetailsPage"page
    #And I should see "Added to Cart" in "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage"page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify the failure message there is already a cart associated for guest registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter already registered details with case sensitive email on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "3" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    Then I should see "Failure! There is already a cart associated for user" message in snack bar on "GiftRegistry ItemDetailsPage"page
    #And I should see "Added to Cart" in "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage"page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by adding the qty in PDP page should reflect in the wishlist page for new registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "1" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    Then I should see "Success! Item Added to Cart" message in snack bar on "GiftRegistry ItemDetailsPage" page
    And I should see "Added to Cart" in "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage"page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking on a product navigating to PDP from My Cart page for guest anonymous user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "skipRegistration" element on "GiftRegistry GuestInvitationPage" page
    And I enter personal details on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "3" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "backBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    And I click on "productImage" element on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry ItemDetailsPage" page

    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking on a product navigating to PDP from  My Cart page for registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    When I enter already registered details with case sensitive email on "GiftRegistry SignModel" page
    And I click on "loginButton" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "4" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "backBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    And I click on "productImage" element on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry ItemDetailsPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by clicking on a product navigating to PDP from  My Cart page for new registered user
    Given I visit the website as guest user for "<event>" event
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |
    When I click on "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I visit the website as "First time user" user
    And I should be on "GiftRegistry AdditionalDetailsPage" page
    When I enter valid details on "GiftRegistry AdditionalDetailsPage" page
    And I click on "submit" button on "GiftRegistry AdditionalDetailsPage" page
    Then I should be on "GiftRegistry EventInvitationPage" page
    And I click on "wishlistBtn" element on "GiftRegistry EventInvitationPage" page
    And I click on "productImage" element on "GiftRegistry EventWishlistPage" page
    And I add the product to wishlist with Quantity "3" on "GiftRegistry ItemDetailsPage"page
    And I click on "addtoWishlistBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "backBtn" element on "GiftRegistry ItemDetailsPage" page
    And I click on "myCart" element on "GiftRegistry EventWishlistPage" page
    And I click on "productImage" element on "GiftRegistry MyCartPage" page
    Then I should be on "GiftRegistry ItemDetailsPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |