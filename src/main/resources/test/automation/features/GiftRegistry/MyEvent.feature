@V2
Feature: My Event page functionality

  Scenario: Verify all fields are present on My Events page without creating event
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    When I select on "My Events" link from "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry MyEventPage" page
    And I should see "There are no events here!" message on "GiftRegistry MyEventPage" page
    And I should see "createFirstEventLink" element on "GiftRegistry MyEventPage" page

  @use_regression
  Scenario Outline: Verify all fields are present on My Events page after successfully creating and verifying event
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I should see "viewInvitation" link on "GiftRegistry MyEventPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify all fields of My Events page after successfully created and verification pending
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Confirmation Pending" on my events page
    And I should see "dropdownMenuLink" element
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    Then I should see "dropdownMenuList" element
    And I should see "editEventLink" element
    And I should see "cancelEventLink" element
    And I should see "resendConfirmationLink" element
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify update event details after successfully creating and verification pending
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Confirmation Pending" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "editEventLink" button on "GiftRegistry MyEventPage" page
    Then I should be on "GiftRegistry UpdateEventPage" page
    And I enter valid event details for "<event>" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" link on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Your Event has been updated! |
    And I should see event code on event confirmation page
    And I should see "resendTheConfirmationLink" element on "GiftRegistry EventConfirmationPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify update event details after successfully creating and verification is completed
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "editEventLink" button on "GiftRegistry MyEventPage" page
    Then I should be on "GiftRegistry UpdateEventPage" page
    And I update valid event details for "<event>" event on create event page
    Then I should be on GiftRegistry Event update Invitation Page
    And I click on "saveAndContinue" link on "GiftRegistry UpdateInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Your Event has been updated! |
    And I should see event code on event confirmation page
    And I should see "goToWishListButton" link on "GiftRegistry EventConfirmationPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify cancel event after successfully creating and verification is completed
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "cancelEventLink" link on "GiftRegistry MyEventPage" page
    Then I should see event cancellation dialog
    And I should see "Are you sure you want to cancel this event?" on cancellation dialog
    And I should see "Yes" element
    And I should see "No" element
    And I enter reason for event cancellation
    When I click on "Yes" button on "GiftRegistry MyEventPage" page
    Then I should see "Success! Event has been cancelled" message in snack bar
    Then I should see event status as "Cancelled" on my events page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify cancel event after successfully creating and verification is pending
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "cancelEventLink" link on "GiftRegistry MyEventPage" page
    Then I should see event cancellation dialog
    And I should see "Are you sure you want to cancel this event?" on cancellation dialog
    And I should see "Yes" element
    And I should see "No" element
    And I enter reason for event cancellation
    When I click on "Yes" button on "GiftRegistry MyEventPage" page
    Then I should see "Success! Event has been cancelled" message in snack bar
    Then I should see event status as "Cancelled" on my events page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify resend confirmation link after successfully creating and verification is pending
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "resendConfirmationLink" link on "GiftRegistry MyEventPage" page
    Then I should see "Success! Confirmation Link has been re-sent" message in snack bar
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify resend invitation link for active events
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "resendInvitationLink" link on "GiftRegistry MyEventPage" page
    Then I should see "Success! Invitation has been sent" message in snack bar
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify resend confirmation link after successfully creating and verification is done
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I should not see "resendConfirmationLink" link on "GiftRegistry MyEventPage" page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify all fields are present on My Events page after event created in draft mode
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "<event>" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    When I select on "My Events" link from "GiftRegistry EventInvitationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Draft" on my events page
    And I should see "addInvitation" link on "GiftRegistry MyEventPage" page
    And I should not see "wishlist" link on "GiftRegistry MyEventPage" page
    And I should see "dropdownMenuLink" element
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    Then I should see "dropdownMenuList" element
    And I should see "editEventLink" element
    And I should see "cancelEventLink" element
    And I should not see "resendConfirmationLink" element
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify draft event can be updated and confirmed successfully by selecting add invitation
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "<event>" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    When I select on "My Events" link from "GiftRegistry EventInvitationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Draft" on my events page
    And I click on "addInvitation" link on "GiftRegistry MyEventPage" page
    And I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see following elements on "GiftRegistry EventConfirmationPage" page:
      | eventCode                 |
      | resendTheConfirmationLink |
    When I confirm the event confirmation online
    Then I should be on "GiftRegistry EventVerificationPage" page
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | addWishlistButton                                            |
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify by selecting Cancel Event option for Draft status event in My Events page
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" in draft mode
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "cancelEventLink" link on "GiftRegistry MyEventPage" page
    Then I should see event cancellation dialog
    And I should see "Are you sure you want to cancel this event?" on cancellation dialog
    And I should see "Yes" element
    And I should see "No" element
    And I enter reason for event cancellation
    When I click on "Yes" button on "GiftRegistry MyEventPage" page
    Then I should see "Success! Event has been cancelled" message in snack bar
    Then I should see event status as "Cancelled" on my events page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify draft event can be updated and confirmed successfully by selecting edit event
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" in draft mode
    Then I should see "<event1>" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "editEventLink" button on "GiftRegistry MyEventPage" page
    Then I should be on "GiftRegistry UpdateEventPage" page
    And I enter valid event details for "<event>" event on create event page
    And I click on "saveAndContinue" link on "GiftRegistry UpdateEventPage" page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see following elements on "GiftRegistry EventConfirmationPage" page:
      | eventCode                 |
      | resendTheConfirmationLink |
    When I confirm the event confirmation online
    Then I should be on "GiftRegistry EventVerificationPage" page
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | addWishlistButton                                            |
    Examples:
      | event      | event1      |
      | babyShower | Baby Shower |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify by creating a new event of same type when existing event is in draft status(same type and same date).
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" in draft mode
    Then I should see "<event1>" on my events page
    When I select on "Home" link from "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I enter previously created event details for "<event>" event on create event page
    Then I should see "Unbale to create event. Please try again later!" error message on create event page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify by creating a new event of same type when existing event is in confirmation pending status(same type and same date).
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventConfirmationPage" page
    Then I should see "<event1>" on my events page
    When I select on "Home" link from "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I enter previously created event details for "<event>" event on create event page
    Then I should see "Unbale to create event. Please try again later!" error message on create event page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify by creating a new event of same type when existing event is in active(same type and same date).
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry MyEventPage" page
    Then I should see "<event1>" on my events page
    When I select on "Home" link from "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I enter previously created event details for "<event>" event on create event page
    Then I should see "Unbale to create event. Please try again later!" error message on create event page
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify by selecting View Invitation option for Active event in My Events page
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    And I should see "wishlist" link on "GiftRegistry MyEventPage" page
    And I click on "viewInvitation" link on "GiftRegistry MyEventPage" page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Your Event has been updated! |
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  @use_regression
  Scenario Outline: Verify the In Active status in My Events page.
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create the event "<event>" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "<event1>" on my events page
    And I should see event status as "Active" on my events page
    When I change event status as In-Active in DB
    And I refresh "My event" page
    Then I should see event status as "In-active" on my events page
    And I should not see "wishlist" link on "GiftRegistry MyEventPage" page
    And I should not see "viewInvitation" link on "GiftRegistry MyEventPage" page
    And I should not see "dropdownMenuLink" element
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |

  Scenario Outline: Verify when the event expired for the draft status event
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I create the event "<event>" in draft mode
    Then I should see "<event1>" on my events page
    And I should see event status as "Draft" on my events page
    When I change event status as In-Active in DB
    And I refresh "My event" page
    Then I should see event status as "In-active" on my events page
    And I should not see "wishlist" link on "GiftRegistry MyEventPage" page
    And I should not see "viewInvitation" link on "GiftRegistry MyEventPage" page
    And I should not see "dropdownMenuLink" element
    Examples:
      | event         | event1         |
      | babyShower    | Baby Shower    |
      | baptism       | Baptism        |
      | birthdayAdult | Birthday-Adult |
      | birthdayKids  | Birthday-Kids  |
      | wedding       | Wedding        |
      | birthdayDebut | Birthday-Debut |
      | anniversary   | Anniversary    |
      | houseWarming  | House Warming  |