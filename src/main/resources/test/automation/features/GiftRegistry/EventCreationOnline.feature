@V2
Feature: Event Creation - Online

  Scenario: Verify that all the fields are present on Enterprise gift registry home page
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    Then I should see following elements on "GiftRegistry HomePage" page:
      | homePageBanner   |
      | createYourEvent  |
      | navigationBar    |
      | homeLink         |
      | myEventsLink     |
      | aboutUsLink      |
      | contactUsLink    |
      | headerSection    |
      | bellIcon         |
      | userProfile      |
      | footerSection    |
      | babyShower       |
      | baptism          |
      | birthdayAdult    |
      | birthdayKids     |
      | birthdayDebut    |
      | wedding          |
      | viewMore         |
    When I click on "viewMore" button on "GiftRegistry HomePage" page
    Then I should see following elements on "GiftRegistry HomePage" page:
      | anniversary      |
      | houseWarming     |

    Scenario: Verify event images should display on Enterprise gift registry home page
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry HomePage" page
    When I click on "viewMore" button on "GiftRegistry HomePage" page
    Then I should see 200 response for event images

  Scenario: Verify that all the fields are present on event creation page for 'babyShower' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |

  Scenario: Verify that all the fields are present on event creation page for 'baptism' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "baptism" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |

  Scenario: Verify that all the fields are present on event creation page for 'birthdayAdult' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayAdult" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | numberOfYears           |
      | saveAndContinue         |
      | reset                   |


  Scenario: Verify that all the fields are present on event creation page for 'wedding' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
      And I select on "wedding" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | email                   |
      | isCelebrantGroom        |
      | isCelebrantBride        |
      | coRegistrantCelebrant   |
      | brideGuestCardNameFlag  |
      | groomGuestCardNameFlag  |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |


  Scenario: Verify that all the fields are present on event creation page for 'anniversary' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
      And I click on "viewMore" button on "GiftRegistry HomePage" page
      And I select on "anniversary" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | firstPreferredName      |
      | secondPreferredName     |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |


  Scenario: Verify that all the fields are present on event creation page for 'house worming' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
      And I click on "viewMore" button on "GiftRegistry HomePage" page
      And I select on "houseWarming" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |


  Scenario: Verify that all the fields are present on event creation page for 'birthdayKids' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayKids" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | numberOfYears           |
      | saveAndContinue         |
      | reset                   |


  Scenario: Verify that all the fields are present on event creation page for 'birthdayDebut' event
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayDebut" event on GiftRegistry Home page
    Then I should see following elements on "GiftRegistry CreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | isCelebrantYes          |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | numberOfYears           |
      | saveAndContinue         |
      | reset                   |

  @use_regression
  Scenario Outline: Verify celebrant is successfully created event online for all events
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "<event>" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I should see "resendConfirmationLink" link on "GiftRegistry EventConfirmationPage" page
    When I confirm the event confirmation online
    Then I should be on "GiftRegistry EventVerificationPage" page
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I should see "addWishlist" link on "GiftRegistry EventVerificationPage" page

    Examples:
      | event         |
      | babyShower    |
#      | baptism       |
#      | birthdayAdult |
#      | birthdayKids  |
#      | wedding       |
#      | birthdayDebut |
#      | anniversary   |
#      | houseWarming  |

  @use_regression
  Scenario Outline: Verify event creation with is registrant celebrant no option for all events
    Given I visit the website as new user
 #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I enter valid event details with is registrant celebrant no option for "<event>" event
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below messages on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I should see "resendTheConfirmationLink" link on "GiftRegistry EventConfirmationPage" page
    When I confirm the event confirmation online
    Then I should be on "GiftRegistry EventVerificationPage" page
    And I should see below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I should see "addWishlistButton" link on "GiftRegistry EventVerificationPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  Scenario: Verify all required fields when no input on create event page for baby shower event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "babyShower" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | eventGuestsCount        |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | giftDeliveryDate        |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredRegistryBranch |

  Scenario: Verify all required fields when no input on create event page for baptism event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "baptism" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "baptism" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  Scenario: Verify all required fields when no input on create event page for birthday adult event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayAdult" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "birthdayAdult" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | numberOfYears           |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  Scenario: Verify error messages for all required fields when no input on create event page for birthday kids event.
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayKids" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "birthdayKids" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | numberOfYears           |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  Scenario: Verify error messages for all required fields when no input on create event page.
    Given I visit the website as "Registered" user
#    When I click on "RegistryButton" button on "GiftRegistry HomePage" page
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayDebut" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "birthdayDebut" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | numberOfYears           |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  @use_regression
  Scenario: Verify all required fields when no input on create event page for wedding event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "wedding" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "wedding" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | coRegistrantCelebrant   |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  Scenario: Verify all required fields when no input on create event page for anniversary event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "anniversary" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "anniversary" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | numberOfYears           |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  Scenario: Verify all required fields when no input on create event page for house warming event.
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "houseWarming" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I verify all missing fields should be in red color while creating "houseWarming" event on create event page
      | eventDate               |
      | eventVenue              |
      | state                   |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliveryZip         |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredRegistryBranch |
      | giftDeliveryDate        |
      | eventGuestsCount        |

  @use_regression
  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for wedding event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "wedding" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                  |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters        |
      | giftDeliveryZip      | !@34       | Please Enter a valid Zip-code                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric |
      | celebrantName        | @          | Celebrant Name should be alphanumeric |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format: |
      | email                | ederf!.com | This entry must be in this format: |
      | coRegistrantCelebrant| @          | Co-Registrant/Celebrant Name should be alphanumeric |
      | coRegistrantCelebrant| 1          | Co-Registrant/Celebrant Name should be alphanumeric |
      | coRegistrantCelebrant| as!@       | Co-Registrant/Celebrant Name should be alphanumeric |
      | brideGuestCardName   | @          | Preferred Name should be alphanumeric |
      | brideGuestCardName   | 1          | Preferred Name should be alphanumeric |
      | brideGuestCardName   | as!@       | Preferred Name should be alphanumeric |
      | groomGuestCardName   | @          | Preferred Name should be alphanumeric |
      | groomGuestCardName   | 1          | Preferred Name should be alphanumeric |
      | groomGuestCardName   | as!@       | Preferred Name should be alphanumeric |
      | invalidGuestCount    | sdsdsd     | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for anniversary event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "anniversary" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                  |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters        |
      | giftDeliveryZip      | !@34       | Please Enter a valid Zip-code                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric |
      | celebrantName        | @          | Celebrant Name should be alphanumeric |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | email                | @          | This entry must be in this format: |
      | email                | ederf!.com | This entry must be in this format: |
      | firstPreferredName   | @          | Preferred Name should be alphanumeric |
      | firstPreferredName   | 1          | Preferred Name should be alphanumeric |
      | firstPreferredName   | as!@       | Preferred Name should be alphanumeric |
      | secondPreferredName  | @          | Preferred Name should be alphanumeric |
      | secondPreferredName  | 1          | Preferred Name should be alphanumeric |
      | secondPreferredName  | as!@       | Preferred Name should be alphanumeric |
      | numberOfYears        | @          | Please enter valid Number of Years.   |
      | numberOfYears        | 321        | Please enter valid Number of Years.   |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for house warming event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    When I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "houseWarming" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please enter a valid Zip-code                  |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters        |
      | giftDeliveryZip      | !@34       | Please Enter a valid Zip-code                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric |
      | celebrantName        | @          | Celebrant Name should be alphanumeric |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric |
      | preferredName        | 1          | Preferred Name should be alphanumeric |
      | preferredName        | as!@       | Preferred Name should be alphanumeric |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |


  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for baby shower event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for baptism event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "baptism" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for birthday adult event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayAdult" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                   |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric           |
      | celebrantName        | @          | Celebrant Name should be alphanumeric           |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric           |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric           |
      | preferredName        | 1          | Preferred Name should be alphanumeric           |
      | preferredName        | as!@       | Preferred Name should be alphanumeric           |
      | numberOfYears        | @          | Please enter valid Number of Years.             |
      | numberOfYears        | 321        | Please enter valid Number of Years.             |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for birthday kids event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayKids" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                  |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters        |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric |
      | celebrantName        | @          | Celebrant Name should be alphanumeric |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric |
      | preferredName        | 1          | Preferred Name should be alphanumeric |
      | preferredName        | as!@       | Preferred Name should be alphanumeric |
      | numberOfYears        | @          | Please enter valid Number of Years.   |
      | numberOfYears        | 321        | Please enter valid Number of Years.   |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  Scenario Outline: Verify error messages for all required fields when invalid input on create event page for birthday debut event.
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "birthdayDebut" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I enter invalid details "<field name>" with "<input>" value on create event page
    Then I should see the error "<error message>" message for "<field name>" on create event page
    Examples:
      | field name           | input      | error message                                   |
      | zipCode              | !          | Zip-code should be atleast 4 Characters         |
      | zipCode              | !@34       | Please Enter a valid Zip-code                  |
      | zipCode              | 1          | Zip-code should be atleast 4 Characters         |
      | giftDeliveryZip      | !          | Zip-code should be atleast 4 Characters        |
      | giftDeliveryZip      | !@34       | Please enter a valid Zip-code.                  |
      | giftDeliveryZip      | 1          | Zip-code should be atleast 4 Characters         |
      | celebrantName        | 1          | Celebrant Name should be alphanumeric |
      | celebrantName        | @          | Celebrant Name should be alphanumeric |
      | celebrantName        | as!@       | Celebrant Name should be alphanumeric |
      | celebrantPhoneNumber | 1          | This entry must be in this format: XXX-XXX-XXXX |
      | celebrantPhoneNumber | @          | This entry must be in this format: XXX-XXX-XXXX |
      | preferredName        | @          | Preferred Name should be alphanumeric |
      | preferredName        | 1          | Preferred Name should be alphanumeric |
      | preferredName        | as!@       | Preferred Name should be alphanumeric |
      | numberOfYears        | @          | Please enter valid Number of Years.   |
      | numberOfYears        | 321        | Please enter valid Number of Years.   |
      | invalidGuestCount    |  sdsdsd      | Please Enter a valid Guests Count     |

  @use_regression
  Scenario Outline: Verify by entering all the required details and click on 'Reset' button for all events event
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I enter valid event details for "<event>" event on create event page
    And I click on "reset" button on "GiftRegistry CreateEventPage" page
    Then I should see all the fields resetting to default values for "<event>" event
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify by changing the default configuration date for all events
    Given I visit the website as "Registered" user
    When I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I enter valid event details with in configured date for "<event>" event on create event page
    Then I should see error message "Event should be created atleast 10 days prior to the Event Date" on create event page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | wedding       |
      | birthdayDebut |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario: Verify user is able to create multiple events with same event type
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    Then I create the event "babyShower" successfully
    When I select on "Home" link from "GiftRegistry EventVerificationPage" page
    And I select on "babyShower" event on GiftRegistry Home page
    Then I create the event "babyShower" successfully
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    Then I should see "Baby Shower" on my events page

  @use_regression
  Scenario: verify user created event for one site and same event should not be displayed for other sites on the same browser and same session
    Given I visit the website as new user
   #   And I click on "RegistryButton" button on "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    When I select on "babyShower" event on GiftRegistry Home page
    Then I create the event "babyShower" successfully
    When I select on "Home" link from "GiftRegistry EventVerificationPage" page
    When I navigate to another company site in same session
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    When I select on "My Events" link from "GiftRegistry HomePage" page
    Then I should be on "GiftRegistry MyEventPage" page
    And I should see "There are no events here!" message on "GiftRegistry MyEventPage" page
    And I should see "createFirstEventLink" element on "GiftRegistry MyEventPage" page

  Scenario Outline: Verify by clicking on "Change Password" from celebrant icon on create your event page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I select on "Change Password" link from header
    Then I should be on "GiftRegistry ChangePasswordPage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify by clicking on "My Profile" from celebrant icon on create your event page
    Given I visit the website as "Registered" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    When I select on "My Profile" link from header
    Then I should be on "GiftRegistry ProfilePage" page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify event details persisting in Event Invitation page
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I enter valid event details for "<event>" event on create event page
    When I click on "saveAndContinue" button on "GiftRegistry CreateEventPage" page
    And I should be on "GiftRegistry ManageInvitationPage" page
    And I should see event details persisting in Event Invitation page
    And I click on "template2" link on "GiftRegistry ManageInvitationPage" page
    And I should see event details persisting in Event Invitation page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |

  @use_regression
  Scenario Outline: Verify event should not be created for the user who cancelled three or more events
    Given I visit the website with "three events cancellation" user
    And I should be on "GiftRegistry HomePage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I should be on "GiftRegistry CreateEventPage" page
    And I enter valid event details for "<event>" event on create event page
    Then I should see error message "Event can not be created due to exceeded Event cancellations. Please visit the store for creating the event" on create event page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify celebrant information should auto populate on event creation page for all the events
    Given I visit the website as new user
    Then I should be on "GiftRegistry LoginPage" page
    And I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    And I should see celebrant information auto populate on create event page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |

  Scenario Outline: Verify event details persisting in Event Invitation page for update event
    Given I visit the website as new user
    And I should be on "GiftRegistry LoginPage" page
    When I navigate to Gift Registry home
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "<event>" event on GiftRegistry Home page
    Then I create "<event>" event with verification pending state
    When I select on "My Events" link from "GiftRegistry EventVerificationPage" page
    And I should see event status as "Confirmation Pending" on my events page
    And I click on "dropdownMenuLink" button on "GiftRegistry MyEventPage" page
    And I click on "editEventLink" button on "GiftRegistry MyEventPage" page
    Then I should be on "GiftRegistry UpdateEventPage" page
    And I should see event details persisting in Event Invitation page
    Examples:
      | event         |
      | babyShower    |
      | baptism       |
      | birthdayAdult |
      | birthdayKids  |
      | birthdayDebut |
      | wedding       |
      | anniversary   |
      | houseWarming  |