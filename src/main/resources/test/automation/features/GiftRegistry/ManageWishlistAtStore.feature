@V4_Store
Feature: Manage Wishlist at Store

  Scenario: Verify that all the fields on manage wishlist page
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "Baby Shower" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "manageWishlist" element on "GiftRegistry StoreEventManagementPage" page
    And I should be on "GiftRegistry ManageWishlistPageAtStore" page
    Then I should see following elements on "GiftRegistry ManageWishlistPageAtStore" page:
      | pageName        |
      | pageBackButton  |
      | skuIdInputField |
      | addSkuButton    |
      | refreshPage     |
      | uploadFile      |
      | skuCode         |
      | productName     |
      | quantity        |
      | action          |


  Scenario Outline: Verify user is able to add items to wishlist for an event
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "manageWishlist" element on "GiftRegistry StoreEventManagementPage" page
    And I should be on "GiftRegistry ManageWishlistPageAtStore" page
    And I add product with sku id "178441" to wishlist on "GiftRegistry ManageWishlistPageAtStore" page
    And I should see "Success! Item added to wishlist" message in snack bar on "GiftRegistry StoreEventManagementPage"page
    And I should see product "178441" added to the wishlist on "GiftRegistry StoreEventManagementPage" page
    Then I verify "Most Loved" option should not be "Active" by default on "GiftRegistry ManageWishlistPageAtStore" page
    And I try to "Delete" the added product from "GiftRegistry ManageWishlistPageAtStore" page
    Then I should see the product is deleted successfully from "GiftRegistry ManageWishlistPageAtStore" page
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

