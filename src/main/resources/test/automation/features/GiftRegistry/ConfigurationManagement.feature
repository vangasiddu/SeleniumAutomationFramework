@V4_Store
Feature: Configuration Management

  @Scenario1 @v4_use_regression
  Scenario: Verify when the user select either button in Event_Confirmation_Channel should able to create the event in online and confirm on store
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "eventConfirmationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "either" and "eitherRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    And I navigate to "GiftRegistry StoreEventManagementPage"  page and search with event code
    And I search with eventcode and click on search button
    Then I click on "Confirmevent" when user status is "CONFIRMATION PENDING"on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar

  @Scenario2 @v4_use_regression
  Scenario: Verify when the user select either button in Event_Confirmation_Channel should able to create the event in store and confirm on online
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "eventConfirmationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "either" and "eitherRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "babyShower" event on create event page
    And I click on "saveAndContinue" button on "GiftRegistry StoreCreateEventPage" page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    When I confirm the storeevent confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |

  @Scenario3 @v4_use_regression
  Scenario: Verify when the user select either button in Event_Confirmation_Channel should able to create the event in online and confirm on online
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "eventConfirmationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "either" and "eitherRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    Then I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I should see "resendConfirmationLink" link on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the storeevent confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |

  @Scenario4 @v4_use_regression
  Scenario: Verify when the user select either button in Event_Confirmation_Channel should able to create the in store and confirm on store
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "eventConfirmationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "either" and "eitherRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "Baby Shower" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                      |
      | Please confirm the event registration through the link we sent to - 'email' |
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar

  @v4_use_regression
  Scenario: Verify the event confirmation email expiry in online
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "eventConfirmationExpiry" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I set the expiry "1" minute time to "eventConfirmationExpiry"channel
    And I navigate to "GiftRegistry LoginPage"  page
    Then I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on GiftRegistry Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    And I should see email expiry link message
      | Your event verification link got expired |
    And I reset the "eventConfirmationExpiry" channel with "10" mintues

  @v4_use_regression
  Scenario: Verify the user should able to see the error message in store when the configuration lead_time_prior_to_create_event_in days value set to 5
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    And I navigate to "GiftRegistry ConfigurationManagementPage"  page
    Then I click on "configuration" tab
    And I click on "event" element and "leadTimePrior" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I set the expiry "5" days to "leadTimePrior" channel
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    And I enter valid event details with in configured date for "babyShower" event on store create event page
    Then I should see error message "Event should be created atleast 5 days prior to the Event Date" on create event page
    And I click on "configuration" link on "GiftRegistry StoreCreateEventPage" page
    When if i see alert click on ok button
    Then I should be on "GiftRegistry ConfigurationManagementPage" page
    And I click on "event" element on "GiftRegistry ConfigurationManagementPage" page
    And I reset the "leadTimePrior" channel to "10" default days

  @Scenario7 @v4_use_regression
  Scenario: verify the user able to see facebook and email icons on myevents page and resend link on store event page when the Event_Invitation_Channel in Email or SocialMedia status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "invitationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "emailORSocialMedia" and "emailORSocialMediaRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    And I should see Email and Facebook icons on my event page
    And I navigate to "GiftRegistry StoreEventManagementPage"  page and search with event code
    And I should see "resendInvitation" link on "GiftRegistry StoreEventManagementPage" page


  @Scenario8 @v4_use_regression
  Scenario: verify the user able to see facebook icon on myevents page and should not display resend link on store event page when the Event_Invitation_Channel in SocialMedia status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "invitationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "socialMedia" and "socialMediaRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    And I should see "facebookIcon1" icon on my event page
    And I navigate to "GiftRegistry StoreEventManagementPage"  page and search with event code
    And I should not see "resendInvitation" link on "GiftRegistry StoreEventManagementPage" page
    And I reset the "invitationChannel" channel to "Email or SocialMedia" status

  @Scenario9 @v4_use_regression
  Scenario: verify the user able to see email icon on myevents page and resend link on store event page when the Event_Invitation_Channel in Email status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "invitationChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "email" and "emailRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    And I should see "emailIcon" icon on my event page
    And I navigate to "GiftRegistry StoreEventManagementPage"  page and search with event code
    And I should see "sendInvitationMail" link on "GiftRegistry StoreEventManagementPage" page
    And I reset the "invitationChannel" channel to "Email or SocialMedia" status

  @v4_use_regression
  Scenario: verify the user able to see ADD_TO_WISHLIST button on create wishlist page when the ONLINE_WISHLIST_MGMT_REQURIED channel in Yes status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "onlineWishlist" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I should see "addWishlist" link on "GiftRegistry EventVerificationPage" page
    And I click on "addWishlist" element on "GiftRegistry EventVerificationPage" page
    And I should see "addToWishListBtn" is enabled on "GiftRegistry CreateWishlistPage" page

  @v4_use_regression
  Scenario: verify the user should not see ADD_TO_WISHLIST button on create wishlist page when the ONLINE_WISHLIST_MGMT_REQURIED channel in No status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "onlineWishlist" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I should see below message on event confirmation page
      | Congrats! Your Event has been created!                                     |
      | Please confirm the event registration through the link we sent to - 'mail' |
    And I should see event code on event confirmation page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I should see the below success message on event verification
      | Congratulations! You have successfully confirmed your event! |
      | Your event invitation link has been sent - 'email'           |
    And I should see "addWishlist" link on "GiftRegistry EventVerificationPage" page
    And I click on "addWishlist" element on "GiftRegistry EventVerificationPage" page
    And I should not see "addToWishListBtn" element on "GiftRegistry CreateWishlistPage" page

  @v4_use_regression
  Scenario: Verify the user able to see printCard for active users when print card channel in no status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "printCardChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "babyShower" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                      |
      | Please confirm the event registration through the link we sent to - 'email' |
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should not see "printCard" element on "GiftRegistry StoreEventManagementPage" page

  @v4_use_regression
  Scenario: Verify the user able to see printCard for active users(Registrant) when print card channel in yes status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "printCardChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "babyShower" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                      |
      | Please confirm the event registration through the link we sent to - 'email' |
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "printCard" element on "GiftRegistry StoreEventManagementPage" page
    And I click on "printCardRegistrant" button on "GiftRegistry StoreEventManagementPage" page
    And I sholud see the eventcode on discount card

  @v4_use_regression
  Scenario: Verify the user able to see printCard for active users(Guest) when print card channel in yes status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "printCardChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "babyShower" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                      |
      | Please confirm the event registration through the link we sent to - 'email' |
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "printCard" element on "GiftRegistry StoreEventManagementPage" page
    And I click on "printCardGuest" button on "GiftRegistry StoreEventManagementPage" page
    And I sholud see the eventcode on discount card

  @v4_use_regression
  Scenario: Verify the user able to see register/signin button and skipregistration button on event invitation page when Guest_Registration_required channel in No status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "guestRegistration" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    And I sign out form current account in browser
    And I confirm the event invitation
    Then I should see following elements on "GiftRegistry GuestInvitationPage" page:
      | registerSignInBtn |
      | skipRegistration  |

  @v4_use_regression
  Scenario: Verify the user able to see register/signin link on event invitation page when Guest_Registration_required channel in yes status
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "guestRegistration" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    And I sign out form current account in browser
    And I confirm the event invitation
    Then I should see "registerSignInBtn" element on "GiftRegistry GuestInvitationPage" page

  @v4_use_regression
  Scenario: Verify the user able to see Buy_Online button in My_Cart page when Buy_Online_Facility channel in yes status on puschase tab
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "purchase" element and "byOnlineFacility" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    Then I should see "Baby Shower" on my events page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    And I confirm the event invitation and naviagte to "GiftRegistry EventInvitationPage" page
    And I click on "invitedEventsTab" element on "GiftRegistry MyEventPage" page
    And I add some items to "GiftRegistry EventWishlistPage" page
    And I click on "addToCart" button on "GiftRegistry EventWishlistPage" page
    Then if i see alert click on "alertYes" button and navigate to "GiftRegistry MyCartPage" page
    And I should see "buyOnline" button on "GiftRegistry MyCartPage" page

  @v4_use_regression
  Scenario: Verify the user should not see Buy_Online button in My_Cart page when Buy_Online_Facility channel in No status on puschase tab
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "purchase" element and "byOnlineFacility" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    Then I should see "Baby Shower" on my events page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    And I confirm the event invitation and naviagte to "GiftRegistry EventInvitationPage" page
    And I click on "invitedEventsTab" element on "GiftRegistry MyEventPage" page
    And I add some items to "GiftRegistry EventWishlistPage" page
    And I click on "addToCart" button on "GiftRegistry EventWishlistPage" page
    Then if i see alert click on "alertYes" button and navigate to "GiftRegistry MyCartPage" page
    And I should not see "buyOnline" button on "GiftRegistry MyCartPage" page

  @v4_use_regression
  Scenario:Verify the user able to see the most loved button on my_wish_list page when WISH_LIST_MOST_LOVED channel in yes status(Online)
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "wishListLovedChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    Then I should see "Baby Shower" on my events page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    And I click on "myWishlistLink" link on "GiftRegistry CreateWishlistPage" page
    Then I should see "mostLoved" element on "GiftRegistry MyWisjlistPage" page

  @v4_use_regression
  Scenario:Verify the user should not see the most loved button on my_wish_list page when WISH_LIST_MOST_LOVED channel in no status(Online)
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "wishListLovedChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I navigate to "GiftRegistry LoginPage"  page
    And I should be on "GiftRegistry LoginPage" page
    And I sign in with celebrant login credentials when user in "GiftRegistry LoginPage" page
    And I click on "viewMore" button on "GiftRegistry HomePage" page
    And I select on "babyShower" event on GiftRegistry Home page
    And I should be on "GiftRegistry CreateEventPage" page
    When I enter valid event details for "babyShower" event on create event page
    Then I should be on "GiftRegistry ManageInvitationPage" page
    And I click on "saveAndContinue" button on "GiftRegistry ManageInvitationPage" page
    Then I should be on "GiftRegistry EventConfirmationPage" page
    And I collect the eventcode from "GiftRegistry EventConfirmationPage" page
    When I confirm the eventcode confirmation on online
    And I click on "myEvents" element on "GiftRegistry EventVerificationPage" page
    Then I should see "Baby Shower" on my events page
    And I click on "wishlist" element on "GiftRegistry MyEventPage" page
    And I should be on "GiftRegistry CreateWishlistPage" page
    When I add product to wishlist with quantity "2" from "GiftRegistry CreateWishlistPage" page
    And I click on "myWishlistLink" link on "GiftRegistry CreateWishlistPage" page
    Then I should not see "mostLoved" element on "GiftRegistry MyWishlistPage" page

    @v4_use_regression
    Scenario:  Verify the user able to see the most loved button on store my_wish_list page when WISH_LIST_MOST_LOVED channel in yes status(store)
      Given I visit the website as "StoreUser"
      And I should be on "GiftRegistry StoreLoginPage" page
      When I enter Store User details on "GiftRegistry StoreLoginPage" page
      And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
      Then I should be on "GiftRegistry CustomerManagementPage" page
      Then I click on "configuration" tab
      And I click on "event" element and "wishListLovedChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
      And I should see "yes" and "yesRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
      And I click on "Customer Management" tab
      When I search with valid "Registered" user from the search bar
      When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
      Then I should be on "GiftRegistry StoreCreateEventPage" page
      When I enter valid event details for store "babyShower" event on create event page
      Then I should be on GiftRegistry Store Event Invitation Page
      And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
      And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
      And I search with eventcode and click on search button
      And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
      And I should see "Success! Event has been Confirmed" message on snack bar
      And I click on "manageWishlist" element on "GiftRegistry StoreEventManagementPage" page
      Then I should be on "GiftRegistry ManageWishlistPageAtStore" page
      And I add product to Store wishlist page from "GiftRegistry ManageWishlistPageAtStore" page
      Then I should see "mostLoved" button on "GiftRegistry ManageWishlistPageAtStore" page

  @v4_use_regression
  Scenario:Verify the user should not see the most loved button on store my_wish_list page when WISH_LIST_MOST_LOVED channel in no status(store)
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    Then I click on "configuration" tab
    And I click on "event" element and "wishListLovedChannel" channel panel on "GiftRegistry ConfigurationManagementPage" page
    And I should see "no" and "noRadioButton" button is selected on the "GiftRegistry ConfigurationManagementPage" page
    And I click on "Customer Management" tab
    When I search with valid "Registered" user from the search bar
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "Baby Shower" event on create event page
    And I click on "saveAndContinue" button on "GiftRegistry StoreCreateEventPage" page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "manageWishlist" element on "GiftRegistry StoreEventManagementPage" page
    Then I should be on "GiftRegistry ManageWishlistPageAtStore" page
    And I add product to Store wishlist page from "GiftRegistry ManageWishlistPageAtStore" page
    Then I should not see "mostLoved" button on "GiftRegistry ManageWishlistPageAtStore" page




