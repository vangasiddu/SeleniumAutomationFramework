@V4_Store
Feature: Event Creation - At store

  Scenario: Verify that all the fields are present on store event creation page for event
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    Then I should see following elements on "GiftRegistry StoreCreateEventPage" page:
      | eventType               |
      | eventDate               |
      | eventTime               |
      | eventVenue              |
      | state                   |
      | city                    |
      | eventZipcode            |
      | giftDeliveryAddress     |
      | giftDeliveryState       |
      | giftDeliverycity        |
      | giftDeliveryZip         |
      | isCelebrantYes          |
      | emailPromotionsOptIn    |
      | smsPromotionsOptIn      |
      | loyaltyCard             |
      | giftDeliveryDate        |
      | eventGuestsCount        |
      | isCelebrantNo           |
      | celebrantName           |
      | celebrantPhoneNumber    |
      | email                   |
      | preferredName           |
      | preferredRegistryBranch |
      | saveAndContinue         |
      | reset                   |

  Scenario Outline: Verify store representative should be able to create event at store for registered users
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see below messages on store event confirmation page
      | Congrats! Your Event has been created!                                    |
      | Please confirm the event registration through the link we sent to - email |
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

  Scenario Outline: Verify the user store actions in event management page when user in active status for all the events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Events Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I should see below the five actions for "ACTIVE" staus user on "GiftRegistry StoreEventManagementPage" page:
      | Edit Event        |
      | Resend Invitation |
      | Manage Wishlist   |
      | Cancel Event      |
      | Print Card        |
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

  Scenario Outline: Verify the user store actions in event management page when user in confirmation pending status for all events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I should see below the five actions for "CONFIRMATION PENDING" staus user on "GiftRegistry StoreEventManagementPage" page:
      | Edit Event               |
      | Resend Confirmation Link |
      | Confirm Event            |
      | Cancel Event             |
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |


  Scenario Outline: Verify the user store actions in event management page when user in draft status for all events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Event Invitation Page
    And I search with firstname and click on search button
    And I should see below the five actions for "Draft" staus user on "GiftRegistry StoreEventManagementPage" page:
      | Edit Event   |
      | Cancel Event |
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |


  Scenario Outline: Verify the user able to cancel the event in store for all events
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I search with valid "Registered" user from the search bar
    And I verify store user search with valid user
    Then I should see the details displayed
    When I click on create event from "GiftRegistry CustomerManagementPage" for the "Registered" user
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "<Event_Name>" event on create event page
    Then I should be on GiftRegistry Store Events Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I should see event code on store event confirmation page
    And I collect the eventcode from "GiftRegistry StoreEventConfirmationPage" page
    And I search with eventcode and click on search button
    And I click on "Confirmevent" element on "GiftRegistry StoreEventManagementPage" page
    And I should see "Success! Event has been Confirmed" message on snack bar
    And I click on "cancelEvent" link on "GiftRegistry StoreEventManagementPage" page
    And I give the reason for cancelling and click on Yes button
    And I should see "Success! Event has been cancelled" message on snack bar
    Examples:
      | Event_Name     |
      | Baby Shower    |
      | Anniversary    |
      | Baptism        |
      | House Warming  |
      | Birthday-Adult |
      | Birthday-Kids  |
      | Birthday-Debut |
      | Wedding        |

  @test
  Scenario: verify the user should see error message when user try to create a event with existing eventdate and eventtype
    Given I visit the website as "StoreUser"
    And I should be on "GiftRegistry StoreLoginPage" page
    When I enter Store User details on "GiftRegistry StoreLoginPage" page
    And I click on "signInButton" button on "GiftRegistry StoreLoginPage" page
    Then I should be on "GiftRegistry CustomerManagementPage" page
    When I click on "createNewCustomerIcon" link on "GiftRegistry CustomerManagementPage" page
    And I visit the website opened in new window
    Then I should be on "GiftRegistry LoginPage" page
    When I enter valid details on "GiftRegistry RegistrationModel" page
    And I click on "register" button on "GiftRegistry LoginPage" page
    Then I should be on "GiftRegistry RegistrationConfirmationPage" page
    And I should see the below message
      | We've sent an email to 'email'. Please click on the email link to verify your account |
    When I confirm the store registration online
    Then I should be on "GiftRegistry AccountVerificationPage" page
    And I should see the below success Account verification message
      | Your account has been successfully verified! |
    Then I switch from current to other "0"
    And I search with email and click on search button
    Then I should be on "GiftRegistry StoreCreateEventPage" page
    When I enter valid event details for store "babyShower" event on create event page
    Then I should be on GiftRegistry Store Events Invitation Page
    And I click on "saveAndContinue" button on "GiftRegistry StoreManageInvitationPage" page
    Then I should be on "GiftRegistry StoreEventConfirmationPage" page
    And I try to create a event with existing date




