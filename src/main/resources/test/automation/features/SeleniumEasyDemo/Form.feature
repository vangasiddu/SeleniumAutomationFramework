Feature: Selenium easy form demo

  @SeleniumEasyDemo
  Scenario: Fill selenium easy input form
    Given I visit "SeleniumEasyDemo Home" page
    And I "selectMenu" with "Input Forms, Input Form Submit" on "navigationBar" panel
    When I fill "input" form on "SeleniumEasyDemo InputForm" page with following:
      | first_name | My First Name |
      | last_name  | My Last Name  |
    And I select random value in "state" dropdown
    And I fill the selenium easy input form
    Then I should see "sendButton" is enabled
